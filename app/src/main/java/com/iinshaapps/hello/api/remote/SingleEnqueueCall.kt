package com.iinshaapps.hello.api.remote


import android.app.Activity
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.hideAppLoader
import com.iinshaapps.hello.helper.onTokenExpiredLogout
import com.iinshaapps.hello.helper.showAppLoader
import com.google.android.material.snackbar.Snackbar
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException


object SingleEnqueueCall {
    var retryCount = 0
    lateinit var snackbar :Snackbar


    fun <T> callRetrofit(activity: Activity, call: Call<T>, apiName: String, isLoaderShown: Boolean, apiListener: IGenericCallBack) {
        snackbar = Snackbar.make(activity.findViewById(android.R.id.content), Constants.CONST_NO_INTERNET_CONNECTION, Snackbar.LENGTH_INDEFINITE)
        if (isLoaderShown)
            activity.showAppLoader()
        snackbar.dismiss()
        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                hideAppLoader()
                if (response.isSuccessful) {
                    retryCount = 0
                    apiListener.success(apiName, response.body())
                } else {
                    when {
                        response.code() == 401 -> {
                            onTokenExpiredLogout()
                            return
                        }
                        response.errorBody() != null -> try {
                            var json = JSONObject(response.errorBody()!!.string())
                            var error = json.get("message") as String
                            apiListener.failure(apiName, error)
                            /*retryCount = 0
                            val gson = GsonBuilder().create()
                            try {
                                val errorModel: GeneralResponseModel = gson.fromJson(response.errorBody()!!.string(), GeneralResponseModel::class.java)
                                apiListener.failure(apiName, errorModel.messages)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                apiListener.failure(apiName, Constants.CONST_SERVER_NOT_RESPONDING)
                            }*/
                        } catch (e: Exception) {
                            e.printStackTrace()
                            apiListener.failure(apiName, Constants.CONST_SERVER_NOT_RESPONDING)
                        }
                        else -> {
                            apiListener.failure(apiName, Constants.CONST_SERVER_NOT_RESPONDING)
                            return
                        }
                    }
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                hideAppLoader()
                val callBack = this
                if (t.message != "Canceled") {
                    if (t is UnknownHostException || t is IOException) {
                        snackbar.setAction("Retry") {
                            snackbar.dismiss()
                            enqueueWithRetry(activity, call, callBack, isLoaderShown)
                        }
                        snackbar.show()
                        apiListener.failure(apiName, Constants.CONST_NO_INTERNET_CONNECTION)
                    } else {
                        retryCount = 0
                        apiListener.failure(apiName, t.toString())
                    }
                } else {
                    retryCount = 0
                }
            }
        })
    }

    fun <T> enqueueWithRetry(activity: Activity, call: Call<T>, callback: Callback<T>, isLoaderShown: Boolean) {
        activity.showAppLoader()
        call.clone().enqueue(callback)
    }
}
