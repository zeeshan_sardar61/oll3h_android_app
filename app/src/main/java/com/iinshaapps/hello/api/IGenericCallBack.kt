package com.iinshaapps.hello.api

/**
 * Created by zeeshan irfan on 03/10/2019.
 */


interface IGenericCallBack {
    fun success(apiName: String, response: Any?)

    fun failure(apiName: String,message: String?)

}