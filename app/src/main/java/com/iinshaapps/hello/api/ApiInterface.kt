package com.iinshaapps.hello.api

import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.model.requestmodel.*
import com.iinshaapps.hello.model.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    companion object {
//        const val BASE_URL = "https://oll3hapidev.stagingdesk.com/api/v1/"
        const val BASE_URL = "https://oll3hclient.messagemusellc.com/api/v1/"
    }

    @Multipart
    @POST(Constants.CREATE_PERSONAL_PROFILE_URL)
    fun createPersonalProfile(
        @Part("name") name: RequestBody,
        @Part("paymentId") paymentId: RequestBody,
        @Part("paymentType") paymentType: RequestBody?,
        @Part("isNamePublic") isNamePublic: RequestBody,
        @Part("countryCode") countryCode: RequestBody,
        @Part("phone") phone: RequestBody,
        @Part("isPhonePublic") isPhonePublic: RequestBody,
        @Part("email") email: RequestBody,
        @Part("isEmailPublic") isEmailPublic: RequestBody,
        @Part("socialMedia") socialMedia: RequestBody?,
        @Part("cardStartColor") cardStartColor: RequestBody,
        @Part("cardEndColor") cardEndColor: RequestBody,
        @Part("isVideo") isVideo: RequestBody,
        @Part file: MultipartBody.Part?): Call<CreateProfileResponseModel>

    @Multipart
    @POST(Constants.UPDATE_PERSONAL_PROFILE_URL)
    fun updatePersonalProfile(
        @Part("name") name: RequestBody,
        @Part("isNamePublic") isNamePublic: RequestBody,
        @Part("countryCode") countryCode: RequestBody,
        @Part("phone") phone: RequestBody,
        @Part("isPhonePublic") isPhonePublic: RequestBody,
        @Part("email") email: RequestBody,
        @Part("isEmailPublic") isEmailPublic: RequestBody,
        @Part("socialMedia") socialMedia: RequestBody?,
        @Part("cardStartColor") cardStartColor: RequestBody,
        @Part("cardEndColor") cardEndColor: RequestBody,
        @Part("isVideo") isVideo: RequestBody,
        @Part("profileUrl") profileUrl: RequestBody,
        @Part("profileThumbnail") profileThumbnail: RequestBody,
        @Part file: MultipartBody.Part?): Call<RecoveryCodeResponse>

    @Multipart
    @POST(Constants.CREATE_BUSINESS_PROFILE_URL)
    fun createBusinessProfile(
        @Part("businessName") businessName: RequestBody,
        @Part("paymentId") paymentId: RequestBody,
        @Part("paymentType") paymentType: RequestBody?,
        @Part("contactName") name: RequestBody,
        @Part("isContactNamePublic") isNamePublic: RequestBody,
        @Part("countryCode") countryCode: RequestBody,
        @Part("phone") phone: RequestBody,
        @Part("extension") ext: RequestBody,
        @Part("isPhonePublic") isPhonePublic: RequestBody,
        @Part("email") email: RequestBody,
        @Part("isEmailPublic") isEmailPublic: RequestBody,
        @Part("socialMedia") socialMedia: RequestBody?,
        @Part("cardStartColor") cardStartColor: RequestBody,
        @Part("cardEndColor") cardEndColor: RequestBody,
        @Part("isVideo") isVideo: RequestBody,
        @Part file: MultipartBody.Part?,
        @Part logo: MultipartBody.Part,
        @Part card: MultipartBody.Part?): Call<CreateProfileResponseModel>

    @Multipart
    @POST(Constants.UPDATE_BUSINESS_PROFILE_URL)
    fun updateBusinessProfile(
        @Part("businessName") businessName: RequestBody,
        @Part("contactName") name: RequestBody,
        @Part("isContactNamePublic") isNamePublic: RequestBody,
        @Part("countryCode") countryCode: RequestBody,
        @Part("phone") phone: RequestBody,
        @Part("extension") ext: RequestBody,
        @Part("isPhonePublic") isPhonePublic: RequestBody,
        @Part("email") email: RequestBody,
        @Part("isEmailPublic") isEmailPublic: RequestBody,
        @Part("socialMedia") socialMedia: RequestBody?,
        @Part("cardStartColor") cardStartColor: RequestBody,
        @Part("cardEndColor") cardEndColor: RequestBody,
        @Part("isVideo") isVideo: RequestBody,
        @Part file: MultipartBody.Part?,
        @Part logo: MultipartBody.Part?,
        @Part card: MultipartBody.Part?,
        @Part("profileUrl") profileUrl: RequestBody,
        @Part("profileThumbnail") profileThumbnail: RequestBody,
        @Part("logoUrl") logoUrl: RequestBody,
        @Part("logoThumbnail") logoThumbnail: RequestBody,
        @Part("businessCardUrl") businessCardUrl: RequestBody?,
        @Part("businessCardThubmnail") businessCardThubmnail: RequestBody?): Call<RecoveryCodeResponse>


    @POST(Constants.ADD_RECOVERY_NUMBER_URL)
    fun addRecoveryNumber(@Body addRecoveryNumberRequest: AddRecoveryNumberRequest): Call<RecoveryCodeResponse>

    @POST(Constants.VERIFY_CODE_URL)
    fun verifyCode(@Body addRecoveryNumberRequest: AddRecoveryNumberRequest): Call<VerifyPhoneResponse>

    @GET(Constants.GET_MY_CARDS_URL)
    fun getMyCards(): Call<GetMyCardsResponseModel>

    @POST(Constants.ADD_CONTACT_URL)
    fun addContact(@Body addRecoveryNumberRequest: AddContactRequestModel): Call<RecoveryCodeResponse>

    @POST(Constants.ADD_NOTES_URL)
    fun addNotes(@Body addNotesRequestModel: AddNotesRequestModel): Call<RecoveryCodeResponse>

    @GET(Constants.GET_MY_CONTACTS_URL)
    fun getMyContacts(): Call<GetMyContactsResponseModel>

    @GET(Constants.GET_CARDS_DETAILS_URL)
    fun getContactInnerDetails(@Query("cardId") cardId: String, @Query("isPersonal") isPersonal: Boolean): Call<CardInnerDetailsResponse>

    @GET(Constants.GET_CONTACTS_DETAILS_URL)
    fun getCardDetails(@Query("cardId") cardId: String, @Query("isPersonal") isPersonal: Boolean, @Query("isOtherUser") isOtherUser: Boolean): Call<GetCardDetailsResponse>

    @POST(Constants.REMOVE_CARD_URL)
    fun removeCard(@Body removeCardRequest: DeleteContactRequestModel): Call<RecoveryCodeResponse>

    @POST(Constants.UPDATE_MY_DETAIL_URL)
    fun updateMyDetail(@Body cardData: CardData): Call<RecoveryCodeResponse>

    @POST(Constants.ADD_CONTACT_REQUEST_URL)
    fun addContactRequest(@Body addContactRequestIdModel: AddContactRequestIdModel): Call<RecoveryCodeResponse>

    @GET(Constants.GET_CONTACT_REQUEST_URL)
    fun getContactRequest(): Call<GetContactRequestResponse>

    @GET(Constants.GET_PRIVACY_URL)
    fun getPrivacy(): Call<CreateProfileResponseModel>
}