package com.iinshaapps.hello.api.remote

import com.iinshaapps.hello.BuildConfig
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.api.ApiInterface
import com.iinshaapps.hello.helper.Constants
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    private const val baseUrl = ApiInterface.BASE_URL
    private var retrofit: Retrofit? = null
    val dispatcher = Dispatcher()

    fun getClient(): Retrofit? {
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG)
            logging.level = HttpLoggingInterceptor.Level.BODY
        else
            logging.level = HttpLoggingInterceptor.Level.NONE
        if (retrofit == null) {
            retrofit = Retrofit.Builder().client(OkHttpClient().newBuilder().readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS).retryOnConnectionFailure(false).dispatcher(
                            dispatcher
                    ).addInterceptor(Interceptor { chain: Interceptor.Chain? ->
                      //  val original: Request = chain!!.request()
                        val newRequest = chain?.request()!!.newBuilder()
                        if (!Hello.db.getString(Constants.AUTH_TOKEN, "").isNullOrEmpty())
                            newRequest.addHeader("authorization", "${Hello.db.getString(Constants.AUTH_TOKEN, "")}")
                        return@Interceptor chain.proceed(newRequest.build())
                    }).addInterceptor(logging).build())
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
        return retrofit
    }


}