package com.iinshaapps.hello;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.iinshaapps.hello.activities.MainActivity;
import com.iinshaapps.hello.activities.Splash;
import com.iinshaapps.hello.helper.Constants;

/**
 * Implementation of App Widget functionality.
 */
public class ButtonsWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.btns_widget);

        //0 mean visible
        //8 mean gone

        if (Hello.db.getString(Constants.PERSONAL_QR).length() > 0) {
            views.setViewVisibility(R.id.btnPersonal, 0);
        } else {
            views.setViewVisibility(R.id.btnPersonal, 8);
        }
        if (Hello.db.getString(Constants.BUSINESS_QR).length() > 0) {
            views.setViewVisibility(R.id.btnBusiness, 0);
        } else {
            views.setViewVisibility(R.id.btnBusiness, 8);
        }

        if (Hello.db.getString(Constants.PERSONAL_QR).length()>0 || Hello.db.getString(Constants.BUSINESS_QR).length()>0) {
            views.setViewVisibility(R.id.llNoQr, 8);
        } else {
            views.setViewVisibility(R.id.llNoQr, 0);
        }

        Intent configIntent = new Intent(context, MainActivity.class);

        configIntent.putExtra("personal", Hello.db.getString(Constants.PERSONAL_QR));
        PendingIntent configPendingIntent = PendingIntent.getActivity(context, 1, configIntent, 0);

        configIntent.putExtra("business", Hello.db.getString(Constants.BUSINESS_QR));
        PendingIntent configPendingIntent1 = PendingIntent.getActivity(context, 2, configIntent, 0);

        Intent configIntent2 = new Intent(context, Splash.class);
        configIntent2.putExtra("", "");
        PendingIntent configPendingIntent2 = PendingIntent.getActivity(context, 0, configIntent2, 0);

        views.setOnClickPendingIntent(R.id.btnPersonal, configPendingIntent);
        views.setOnClickPendingIntent(R.id.btnBusiness, configPendingIntent1);
        views.setOnClickPendingIntent(R.id.llNoQr, configPendingIntent2);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}