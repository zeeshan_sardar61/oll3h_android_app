package com.iinshaapps.hello

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceIdReceiver
import com.google.firebase.messaging.FirebaseMessaging
import com.iinshaapps.hello.api.ApiInterface
import com.iinshaapps.hello.api.remote.ApiClient
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.TinyDB


class Hello : Application(){

    private lateinit var appSharedPrefs: SharedPreferences
    private lateinit var prefsEditor: SharedPreferences.Editor
    private var SHARED_NAME = "com.iinshaapps.hello"
    // lateinit var firebaseAnalytics: FirebaseAnalytics
    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            Log.d("FCM", token)
            db.putString(Constants.FCM_TOKEN, token)
        })
        leastRecentlyUsedCacheEvictor = LeastRecentlyUsedCacheEvictor(exoPlayerCacheSize)
        exoDatabaseProvider = ExoDatabaseProvider(this)
        simpleCache = SimpleCache(cacheDir, leastRecentlyUsedCacheEvictor, exoDatabaseProvider)
        initPreferences()
        configRetrofit()
    }

    private fun configRetrofit() {
        apiService = ApiClient.getClient()!!.create(ApiInterface::class.java)
    }

    private fun initPreferences() {
        this.appSharedPrefs = getSharedPreferences(SHARED_NAME, Activity.MODE_PRIVATE)
        this.prefsEditor = appSharedPrefs.edit()
        db = TinyDB(applicationContext)
    }

    override fun onLowMemory() {
        super.onLowMemory()

        Log.i(TAG, "Freeing memory ...")

    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        Log.v(TAG, "public void onTrimMemory (int level)")
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
    }


    companion object {
        lateinit var db: TinyDB

        @SuppressLint("StaticFieldLeak")
        private var instance: Hello? = null
        lateinit var apiService: ApiInterface
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
        private val TAG = Hello::class.java.name

        lateinit var simpleCache: SimpleCache
        const val exoPlayerCacheSize: Long = 90 * 1024 * 1024
        lateinit var leastRecentlyUsedCacheEvictor: LeastRecentlyUsedCacheEvictor
        lateinit var exoDatabaseProvider: ExoDatabaseProvider


    }

}