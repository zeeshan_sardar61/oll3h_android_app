package com.iinshaapps.hello

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.Constants
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.helper.MessageEvent
import org.greenrobot.eventbus.EventBus
import java.text.SimpleDateFormat
import java.util.*

class MyFcmListenerService: FirebaseMessagingService() {
    internal var title: String? = null
    internal var message: String? = null
    internal var tag: String = ""
    lateinit var notificationManager: NotificationManager
    private val TAG = "MyFcmListenerService"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.e("FCM", "FROM : " + remoteMessage.from)

        //Verify if the message contains data
        if (remoteMessage.data.isNotEmpty()) {
            Log.d("FCM", "Message data : " + remoteMessage.data)

        }

        if (remoteMessage.notification != null) {
            Log.d("FCM", "Message body : " + remoteMessage.notification!!.body)
        }

        Hello.db.putBoolean(com.iinshaapps.hello.helper.Constants.HAVE_NOTIFICATION, true)
        EventBus.getDefault().post(MessageEvent("notify"))
        handleNotification(remoteMessage)
    }


    private fun handleNotification(mMessage: RemoteMessage) {
        Hello.db.putBoolean(com.iinshaapps.hello.helper.Constants.HAVE_NOTIFICATION, true)
        EventBus.getDefault().post(MessageEvent("notify"))
        Log.e("Tag", "Json == >> " + mMessage.data.toString())
        var intent: Intent? = null
        intent = Intent(this, MainActivity::class.java)
        // Check if message contains a notification payload.
        if (mMessage.notification != null) {
            Log.e("Tag", "Notification Body: " + mMessage.notification!!.body!!)
        }
        if (mMessage.data.isNotEmpty()) {
            Log.e(TAG, "Data Payload: " + mMessage.data.toString())
            try {
                handleSilentPush(mMessage)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }

        } else {
            Log.e(TAG, " No Data Payload: ")
        }
        handleSilentPush(mMessage)
        Hello.db.putBoolean(com.iinshaapps.hello.helper.Constants.HAVE_NOTIFICATION, true)
    }

    private fun handleSilentPush(mMessage: RemoteMessage) {
        val mIntent = Intent(this, MainActivity::class.java)
        mIntent.putExtra("name", mMessage.data["name"])
        val pendingIntent =
            PendingIntent.getActivity(this, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val CHANNEL_ID = "Oll3H"
        val name = CHANNEL_ID
        val importance = NotificationManager.IMPORTANCE_HIGH
        Hello.db.putBoolean(com.iinshaapps.hello.helper.Constants.HAVE_NOTIFICATION, true)
        EventBus.getDefault().post(MessageEvent("notify"))
        val notificationBuilder = NotificationCompat.Builder(this, "Notification")
            .setSmallIcon(R.drawable.ic_oll3h)
            .setContentTitle("Oll3H")
            .setContentText(mMessage.notification!!.body)
            .setAutoCancel(true)
            .setSound(null)
            .setContentIntent(pendingIntent)
        notificationBuilder.color = resources.getColor(R.color.orange)
        notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID)
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.setSound(null, null)
            mChannel.enableLights(false)
            mChannel.lightColor = Color.BLUE
            mChannel.enableVibration(false)
            notificationManager.createNotificationChannel(mChannel)
        }
        notificationManager.notify(createID(), notificationBuilder.build())

        //        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun createID(): Int {
        val now = Date()
        return SimpleDateFormat("ddHHmmss", Locale.US).format(now).toInt()
    }
}
