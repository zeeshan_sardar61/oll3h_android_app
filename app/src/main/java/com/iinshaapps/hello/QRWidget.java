package com.iinshaapps.hello;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;

import com.iinshaapps.hello.activities.MainActivity;
import com.iinshaapps.hello.activities.Splash;
import com.iinshaapps.hello.helper.Constants;

/**
 * Implementation of App Widget functionality.
 */
public class QRWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.qr_widget_updated);

        //0 mean visible
        //8 mean gone

        if (Hello.db.getString(Constants.PERSONAL_QR).length()>0) {
            views.setViewVisibility(R.id.llPersonal, 0);
            byte[] imageAsBytes = Base64.decode(Hello.db.getString(Constants.P_QR).getBytes(), Base64.DEFAULT);
            views.setImageViewBitmap(R.id.ivPersonal, BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        } else {
            views.setViewVisibility(R.id.llPersonal, 8);
        }
        if (Hello.db.getString(Constants.BUSINESS_QR).length()>0) {
            views.setViewVisibility(R.id.llBusiness, 0);
            byte[] imageAsBytes = Base64.decode(Hello.db.getString(Constants.B_QR).getBytes(), Base64.DEFAULT);
            views.setImageViewBitmap(R.id.ivBusiness, BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        } else {
            views.setViewVisibility(R.id.llBusiness, 8);
        }

        if (Hello.db.getString(Constants.PERSONAL_QR).length()>0 || Hello.db.getString(Constants.BUSINESS_QR).length()>0) {
            views.setViewVisibility(R.id.llNoQr, 8);
        } else {
            views.setViewVisibility(R.id.llNoQr, 0);
        }


        // Instruct the widget manager to update the widget
        Intent configIntent = new Intent(context, MainActivity.class);

        configIntent.putExtra("personal", Hello.db.getString(Constants.PERSONAL_QR));
        PendingIntent configPendingIntent = PendingIntent.getActivity(context, 1, configIntent, 0);

        configIntent.putExtra("business", Hello.db.getString(Constants.BUSINESS_QR));
        PendingIntent configPendingIntent1 = PendingIntent.getActivity(context, 2, configIntent, 0);

        Intent configIntent2 = new Intent(context, Splash.class);

        configIntent2.putExtra("", "");
        PendingIntent configPendingIntent2 = PendingIntent.getActivity(context, 0, configIntent2, 0);

        views.setOnClickPendingIntent(R.id.llPersonal, configPendingIntent);
        views.setOnClickPendingIntent(R.id.llBusiness, configPendingIntent1);
        views.setOnClickPendingIntent(R.id.llNoQr, configPendingIntent2);
        appWidgetManager.updateAppWidget(appWidgetId, views);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }


}