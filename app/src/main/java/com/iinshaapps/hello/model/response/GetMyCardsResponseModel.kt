package com.iinshaapps.hello.model.response

import java.io.Serializable

data class GetMyCardsResponseModel(
	val exception: String,
	val isError: Boolean,
	val data: MyCardData,
	val message: String
)

data class BusinessCard(
	val extension: String,
	val thumbnail: String,
	val contactName: String,
	val businessName: String,
	val logoThumbnail: String,
	val cardEndColor: String,
	val isVideo: Boolean,
	val isContactNamePublic: Boolean,
	val socialMedia: List<SocialMediaItem>,
	val cardStartColor: String,
	val url: String,
	val logoUrl: String,
	val isPhonePublic: Boolean,
	val qrCode: String,
	val phone: String,
	val countryCode: String,
	val cardId: String,
	val isEmailPublic: Boolean,
	val email: String,
	val businessCardUrl: String?,
	val businessCardThumbnail: String?
) : Serializable

data class PersonalCard(
	val isNamePublic: Boolean,
	val thumbnail: String,
	val cardEndColor: String,
	val isVideo: Boolean,
	val socialMedia: List<SocialMediaItem>,
	val cardStartColor: String,
	val url: String,
	val isPhonePublic: Boolean,
	val qrCode: String,
	val phone: String,
	val countryCode: String,
	val cardId: String,
	val name: String,
	val isEmailPublic: Boolean,
	val email: String
) : Serializable

data class MyCardData(
	val personalCard: PersonalCard?,
	val businessCard: BusinessCard?
)

