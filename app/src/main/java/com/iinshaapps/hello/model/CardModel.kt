package com.iinshaapps.hello.model

class CardModel {
    var position :Int = 0
    var colorStart :Int = 0
    var colorEnd: Int = 0
    var isOpened: Boolean = false
    var name = ""
    var phone = ""

    constructor(position: Int, name: String, phone: String, colorStart: Int, colorEnd: Int, isOpen : Boolean) {
        this.colorStart = colorStart
        this.colorEnd = colorEnd
        this.isOpened = isOpen
        this.name = name
        this.phone = phone
    }
}
