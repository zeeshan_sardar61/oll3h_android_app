package com.iinshaapps.hello.model

data class MyCardModel(val position: Int,
    val cardId: String = "",
    val qrCode: String = "",
    val thumbnailUrl: String = "",
    val logoThumbnailUrl: String = "",
    val companyName: String = "",
    val name: String,
    val countryCode: String,
    val phone: String,
    val email: String,
    val colorStart: String,
    val colorEnd: String,
    var isOpened: Boolean = false,
    val isContactNamePublic: Boolean = false,
    val isEmailPublic: Boolean = false,
    val isPhonePublic: Boolean = false,
    val isAppUser: Boolean = false,
    val isVideo: Boolean = false,
    val notes: String? = null
)