package com.iinshaapps.hello.model

data class SocialModel(
    var isSelected: Boolean,
    var socialName: String,
    var socialDrawable: Int,
    var socialDrawableFilled: Int,
    var socialLink: String,
    var socialUsername: String = "",
    var socialPublicToggle: Boolean = false
)
