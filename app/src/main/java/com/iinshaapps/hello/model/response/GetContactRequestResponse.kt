package com.iinshaapps.hello.model.response

import java.io.Serializable

data class GetContactRequestResponse(
	val exception: String,
	val isError: Boolean,
	val data: List<ContactRequestModel>,
	val message: String
)

data class ContactRequestModel(
	val countryCode: String? = null,
    val businessName: String? = null,
	val name: String,
	val id: String? = null,
    val cardId: String? = null,
	val phoneNo: String? = null,
	val email: String? = null,
    val isPersonal: Boolean,
    val isAppUser: Boolean = false,
    val notes: String? = null
) : Serializable

