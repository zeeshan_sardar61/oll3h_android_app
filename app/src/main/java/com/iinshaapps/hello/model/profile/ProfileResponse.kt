package com.iinshaapps.hello.model.profile

data class ProfileResponse(
	val exception: String = "",
	val isError: Boolean = false,
	val data: String = "",
	val message: String = ""
	)


