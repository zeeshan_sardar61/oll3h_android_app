package com.iinshaapps.hello.model.response

data class CardInnerDetailsResponse(
	val exception: String? = null,
	val isError: Boolean? = null,
	val data: CardData? = null,
	val message: String? = null
)

data class BusinessDetail(
	var website: String? = null,
	var isWebsitePublic: Boolean? = null,
	var isBusinessAddressPublic: Boolean? = null,
	var university: String? = null,
	var isBioPublic: Boolean? = null,
	var jobTitle: String? = null,
	var degree: String? = null,
	var bio: String? = null,
	var isDegreePublic: Boolean? = null,
	var emails: ArrayList<EmailsItem?>? = null,
	var isUniversityPublic: Boolean? = null,
	var isJobTitlePublic: Boolean? = null,
	var businessAddress: String? = null,
    var country: String? = null,
    var isCountryPublic: Boolean? = null,
    var state: String? = null,
    var isStatePublic: Boolean? = null,
    var city: String? = null,
    var isCityPublic: Boolean? = null,
    var street: String? = null,
    var isStreetPublic: Boolean? = null,
    var postCode: String? = null,
    var isPostCodePublic: Boolean? = null,
    var faxNumber: String? = null,
    var isFaxNumberPublic: Boolean? = null
)

data class PersonalDetail(
	var isSchoolPublic: Boolean? = null,
	var address: String? = null,
	var minor: String? = null,
	var isAboutPublic: Boolean? = null,
	var classification: String? = null,
	var emails: ArrayList<EmailsItem?>? = null,
	var isleveEducautionPublic: Boolean? = null,
	var isClassificationPublic: Boolean? = null,
	var isMajorPublic: Boolean? = null,
	var isMinorPublic: Boolean? = null,
	var major: String? = null,
	var dob: String? = null,
	var isDobPublic: Boolean? = null,
	var aboutYou: String? = null,
	var isAddressPublic: Boolean? = null,
	var schoolName: String? = null,
	var levelEducation: String? = null,
	var country: String? = null,
	var isCountryPublic: Boolean? = null,
	var state: String? = null,
	var isStatePublic: Boolean? = null,
	var city: String? = null,
	var isCityPublic: Boolean? = null,
	var street: String? = null,
	var isStreetPublic: Boolean? = null,
	var postCode: String? = null,
	var isPostCodePublic: Boolean? = null
)

data class EmailsItem(
	var isPublic: Boolean? = null,
	val id: String? = null,
	var email: String? = null
)

data class CardData(
	val personalDetail: PersonalDetail? = null,
	val businessDetail: BusinessDetail? = null
)

