package com.iinshaapps.hello.model.requestmodel

data class AddRecoveryNumberRequest(val deviceType: String?, val code: Int?, val os: String?, val phone: String?, val countryCode: String?, val deviceModel: String?, val userId: String?, val version: String?, val deviceToken: String?, val uniqueIdentifier: String?)

data class AddContactRequestModel(val cardId: String, val isPersonal: Boolean, val latitude: String?, val longitude: String?, val address: String?)

data class DeleteContactRequestModel(val cardId: String, val isPersonal: Boolean, val isAppUser: Boolean,val latitude: String?, val longitude: String?, val address: String?)

data class AddNotesRequestModel(val cardId: String, val notes: String, val isPersonal: Boolean)
