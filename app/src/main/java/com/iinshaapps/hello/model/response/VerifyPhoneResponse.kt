package com.iinshaapps.hello.model.response

import java.io.Serializable

data class VerifyPhoneResponse(
    val isError: Boolean,
    val message: String,
	val exception: String,
	val data: Data
)

data class Data(
    val authToken: String?,
    val userId: String?,
    val personalData: PersonalData,
    val businessData: BusinessData
)

data class PersonalData(
    val isNamePublic: Boolean,
    val isPhonePublic: Boolean,
    val phone: String,
    val countryCode: String,
    val name: String,
    val cardEndColor: String,
    val isVideo: Boolean,
    val isEmailPublic: Boolean,
    val profileImage: FileModel,
    val socialMedia: List<SocialMediaItem>,
    val cardStartColor: String,
    val email: String
)

data class FileModel(
	val thumbnailbUrl: String,
	val isVideo: Boolean,
	val url: String
)


data class BusinessData(
	val extension: String,
	val contactName: String,
	val profile: FileModel,
	val businessName: String,
	val cardEndColor: String,
	val isVideo: Boolean,
	val isContactNamePublic: Boolean,
	val socialMedia: List<SocialMediaItem>,
	val cardStartColor: String,
	val businessCard: FileModel,
	val isPhonePublic: Boolean,
	val phone: String,
	val countryCode: String,
	val logo: FileModel,
	val isEmailPublic: Boolean,
	val email: String
)

data class SocialMediaItem(
	val isPublic: Boolean,
	val tag: String,
	val url: String
): Serializable

data class RecoveryCodeResponse(
	val exception: String = "",
	val isError: Boolean = false,
	val data: Boolean = false,
	val message: String  = ""
)

