package com.iinshaapps.hello.model

class DesignModel {
    var colorStart : String = ""
    var colorEnd: String = ""
    var isSelected: Boolean = false

    constructor(colorStart: String, colorEnd: String, isSelected : Boolean) {
        this.colorStart = colorStart
        this.colorEnd = colorEnd
        this.isSelected = isSelected
    }
}
