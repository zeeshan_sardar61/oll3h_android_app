package com.iinshaapps.hello.model

data class OtherItemsModel(
    var businessName: String? = null,
    var name: String? = null,
    var countryCode: String? = null,
    var phone: String? = null,
    var email: String? = null
)
