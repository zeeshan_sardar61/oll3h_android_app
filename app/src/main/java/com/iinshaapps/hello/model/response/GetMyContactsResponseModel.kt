package com.iinshaapps.hello.model.response

data class GetMyContactsResponseModel(
    val exception: String,
    val isError: Boolean,
    val data: MyContactsData,
    val message: String
)

data class BusinessCardItem(
    val extension: String,
    val thumbnail: String,
    val contactName: String,
    val businessName: String,
    val logoThumbnail: String,
    val cardEndColor: String,
    val isVideo: Boolean,
    val isContactNamePublic: Boolean,
    val socialMedia: List<SocialMediaItem>,
    val cardStartColor: String,
    val url: String,
    val logoUrl: String,
    val isPhonePublic: Boolean,
    val qrCode: String,
    val phone: String,
    val countryCode: String,
    val cardId: String,
    val isEmailPublic: Boolean,
    val email: String,
    val businessCardUrl: String,
    val businessCardThumbnail: String,
    val isAppUser: Boolean,
    val notes: String? = null
)

data class PersonalCardItem(
    val isNamePublic: Boolean,
    val thumbnail: String,
    val cardEndColor: String,
    val isVideo: Boolean,
    val socialMedia: List<SocialMediaItem>,
    val cardStartColor: String,
    val url: String,
    val isPhonePublic: Boolean,
    val qrCode: String,
    val phone: String,
    val countryCode: String,
    val cardId: String,
    val name: String,
    val isEmailPublic: Boolean,
    val email: String,
    val isAppUser: Boolean,
    val notes: String? = null
)

data class MyContactsData(
    val personalCard: List<PersonalCardItem>,
    val businessCard: List<BusinessCardItem>
)
