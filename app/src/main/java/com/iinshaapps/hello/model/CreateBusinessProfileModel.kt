package com.iinshaapps.hello.model

import com.iinshaapps.hello.model.requestmodel.SocialMediaModelList
import java.io.Serializable

class CreateBusinessProfileModel : Serializable {

    var businessName: String = ""
    var paymentId: String = ""
    var contactName: String = ""
    var countryCode: String = ""
    var phone: String = ""
    var ext: String = ""
    var email: String = ""
    var startColor: String = ""
    var endColor: String = ""
    var isNamePublic: Boolean = false
    var isPhonePublic: Boolean = false
    var isEmailPublic: Boolean = false
    var socialMediaList: ArrayList<SocialMediaModelList>? = null
    var isVideo: Boolean = false
    var profileUri: String? = ""
    var logoUri: String? = ""
    var businessCardUri: String? = null

    var profileUrl: String = ""
    var profileThumbnail: String = ""
    var logoUrl: String = ""
    var logoThumbnail: String = ""
    var businessCardUrl: String ?=null
    var businessCardThubmnail: String ?=null


    constructor(businessName: String, contactName: String, countryCode: String, phone: String, ext: String, email: String, isNamePublic: Boolean, isPhonePublic: Boolean, isEmailPublic: Boolean, socialMediaList: ArrayList<SocialMediaModelList>?, startColor: String, endColor: String, isVideo: Boolean, profileUri: String?, logoUri: String?, businessCardUri: String?, profileUrl: String, profileThumbnail: String, logoUrl: String, logoThumbnail: String, businessCardUrl: String?, businessCardThubmnail: String?, paymentId: String) {
        this.businessName = businessName
        this.contactName = contactName
        this.countryCode = countryCode
        this.phone = phone
        this.ext = ext
        this.email = email
        this.startColor = startColor
        this.endColor = endColor
        this.isNamePublic = isNamePublic
        this.isPhonePublic = isPhonePublic
        this.isEmailPublic = isEmailPublic
        this.socialMediaList = socialMediaList
        this.isVideo = isVideo
        this.profileUri = profileUri
        this.logoUri = logoUri
        this.businessCardUri = businessCardUri
        this.profileUrl = profileUrl
        this.profileThumbnail = profileThumbnail
        this.logoUrl = logoUrl
        this.logoThumbnail = logoThumbnail
        this.businessCardUrl = businessCardUrl
        this.businessCardThubmnail = businessCardThubmnail

    }
}
    