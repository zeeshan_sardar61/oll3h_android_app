package com.iinshaapps.hello.model.requestmodel

import java.io.Serializable

class SocialMediaModelList : Serializable{

    var tag = ""
    var isPublic = false
    var url = ""

    constructor(tag: String, isPublic: Boolean, url: String) {
        this.tag = tag
        this.isPublic = isPublic
        this.url = url
    }

}