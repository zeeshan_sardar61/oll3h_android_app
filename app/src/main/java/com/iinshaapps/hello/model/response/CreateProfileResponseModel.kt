package com.iinshaapps.hello.model.response

class CreateProfileResponseModel {

    val exception: String = ""
    val isError: Boolean = false
    val data: String = ""
    val message: String  = ""
}