package com.iinshaapps.hello.model.response

data class GetCardDetailsResponse(
	val exception: String? = null,
	val isError: Boolean? = null,
	val data: Data1? = null,
	val message: String? = null
)

data class BusinessCard1(
	val extension: String? = null,
	val thumbnail: String? = null,
	val address: String? = null,
	val notes: String? = null,
	val contactName: String? = null,
	val latitude: String? = null,
	val businessName: String? = null,
	val logoThumbnail: String? = null,
	val cardEndColor: String? = null,
	val isVideo: Boolean? = null,
	val isContactNamePublic: Boolean? = null,
	val socialMedia: ArrayList<SocialMediaItem>? = null,
	val cardStartColor: String? = null,
	val url: String? = null,
	val logoUrl: String? = null,
	val isPhonePublic: Boolean? = null,
	val qrCode: String? = null,
	val phone: String? = null,
	val countryCode: String? = null,
	val cardId: String? = null,
	val meetOn: String? = null,
	val isEmailPublic: Boolean? = null,
	val email: String? = null,
	val longitude: String? = null,
    val businessCardUrl: String? = null,
    val businessCardThumbnail: String? = null
)

data class PersonalCard1(
	val isNamePublic: Boolean? = null,
	val thumbnail: String? = null,
	val address: String? = null,
	val notes: String? = null,
	val latitude: String? = null,
	val cardEndColor: String? = null,
	val isVideo: Boolean? = null,
	val socialMedia: ArrayList<SocialMediaItem>? = null,
	val cardStartColor: String? = null,
	val url: String? = null,
	val isPhonePublic: Boolean? = null,
	val qrCode: String? = null,
	val phone: String? = null,
	val countryCode: String? = null,
	val cardId: String? = null,
	val name: String? = null,
	val meetOn: String? = null,
	val isEmailPublic: Boolean? = null,
	val email: String? = null,
	val longitude: String? = null
)

data class Data1(
	val personalCard: PersonalCard1? = null,
	val businessCard: BusinessCard1? = null
)

