package com.iinshaapps.hello.model

import com.iinshaapps.hello.model.requestmodel.SocialMediaModelList
import java.io.Serializable

class CreatePersonalProfileModel : Serializable {

    var name: String = ""
    var paymentId: String = ""
    var countryCode: String = ""
    var phone: String = ""
    var email: String = ""
    var startColor: String = ""
    var endColor: String = ""
    var isNamePublic: Boolean = false
    var isPhonePublic: Boolean = false
    var isEmailPublic: Boolean = false
    var socialMediaList: ArrayList<SocialMediaModelList>? = null
    var isVideo: Boolean = false
    var uriPath: String?=null
    var profileUrl: String=""
    var profileThumbnail: String=""

    constructor(name: String, countryCode: String, phone: String, email: String, isNamePublic: Boolean, isPhonePublic: Boolean, isEmailPublic: Boolean, socialMediaList: ArrayList<SocialMediaModelList>, startColor: String, endColor: String, isVideo: Boolean, path: String?, profileUrl: String, profileThumbnail: String) {
        this.name = name
        this.countryCode = countryCode
        this.phone = phone
        this.email = email
        this.isNamePublic = isNamePublic
        this.isPhonePublic = isPhonePublic
        this.isEmailPublic = isEmailPublic
        this.socialMediaList = socialMediaList
        this.startColor = startColor
        this.endColor = endColor
        this.isVideo = isVideo
        this.uriPath = path
        this.profileUrl = profileUrl
        this.profileThumbnail = profileThumbnail
    }
}
    