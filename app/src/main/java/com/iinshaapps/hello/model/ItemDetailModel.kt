package com.iinshaapps.hello.model

data class ItemDetailModel(val tag: String, var body: String, var switch: Boolean = false)