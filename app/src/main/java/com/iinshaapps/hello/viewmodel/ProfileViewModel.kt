package com.iinshaapps.hello.viewmodel

import androidx.lifecycle.ViewModel
import com.iinshaapps.hello.data.HelloRepository
import com.iinshaapps.hello.model.profile.ProfileResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: HelloRepository
) : ViewModel()
{
    private val profileFlow = MutableStateFlow(ProfileResponse())
    val breakingNews: Flow<ProfileResponse> = profileFlow
}