package com.iinshaapps.hello.helper


import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.content.*
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Context.TELEPHONY_SERVICE
import android.content.res.Configuration
import android.database.Cursor
import android.database.CursorIndexOutOfBoundsException
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.media.ExifInterface
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.ContactsContract
import android.provider.MediaStore
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.activities.Splash
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.example.parking.utilities.utils.RealPathUtil
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.opensooq.supernova.gligar.GligarPicker
import java.io.*
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*
import kotlin.math.roundToInt


var fileUri: Uri? = null
private var loader: Dialog? = null

val Context.glide: RequestManager?
    get() = Glide.with(this)

fun ImageView.load(path: String) {
    try {
        context.glide!!.load(path).thumbnail(0.1f).into(this)
    } catch (ex: IllegalArgumentException) {

    }
}


var loadingDialoge: Dialog? = null

fun Context.showAppLoader() {
    if (loadingDialoge != null) {
        if (!loadingDialoge!!.isShowing) {
            loadingDialoge = Dialog(this, R.style.Theme_Dialog_Loader)
            loadingDialoge!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loadingDialoge!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            loadingDialoge!!.setContentView(R.layout.dialoge_app_loader)
            loadingDialoge!!.setCancelable(false)
            loadingDialoge!!.show()
        }
    } else {
        loadingDialoge = Dialog(this, R.style.Theme_Dialog_Loader)
        loadingDialoge!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loadingDialoge!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        loadingDialoge!!.setContentView(R.layout.dialoge_app_loader)
        loadingDialoge!!.setCancelable(false)
        loadingDialoge!!.show()
    }
}

fun hideAppLoader() {
    if (loadingDialoge != null && loadingDialoge!!.isShowing) {
        try {
            loadingDialoge!!.dismiss()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
}



fun Context.dpToPx(dp: Int): Int {
    val density = this.resources.displayMetrics.density
    return (dp.toFloat() * density).roundToInt()
}

fun Context.showToastMessage(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

internal fun Context.getDrawableCompat(@DrawableRes drawable: Int) = ContextCompat.getDrawable(this, drawable)

internal fun Context.getColorCompat(@ColorRes color: Int) = ContextCompat.getColor(this, color)

internal fun TextView.setTextColorRes(@ColorRes color: Int) = setTextColor(context.getColorCompat(color))

fun dpToPx(dp: Int, context: Context): Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), context.resources.displayMetrics).toInt()

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

internal fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return context.layoutInflater.inflate(layoutRes, this, attachToRoot)
}

internal val Context.layoutInflater: LayoutInflater
    get() = LayoutInflater.from(this)

internal val Context.inputMethodManager
    get() = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}


fun getCapsSentences(tagName: String): String? {
    val splits = tagName.toLowerCase(Locale.ROOT).split(" ".toRegex()).toTypedArray()
    val sb = StringBuilder()
    for (i in splits.indices) {
        val eachWord = splits[i]
        if (i > 0 && eachWord.length > 0) {
            sb.append(" ")
        }
        val cap = (eachWord.substring(0, 1).toUpperCase() + eachWord.substring(1))
        sb.append(cap)
    }
    return sb.toString()
}

fun convertCountToMonth(month: Int): String {
    return when (month) {
        1 -> "Jan"
        2 -> "Feb"
        3 -> "Mar"
        4 -> "Apr"
        5 -> "May"
        6 -> "Jun"
        7 -> "Jul"
        8 -> "Aug"
        9 -> "Sep"
        10 -> "Oct"
        11 -> "Nov"
        12 -> "Dec"
        else -> ""
    }
}

fun Activity.startCamera(requestCode: Int) {
    val values = ContentValues(1)
    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
    fileUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//    if (intent.resolveActivity(packageManager) != null) {
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, requestCode)
//    }
}

fun Activity.startFrontCamera(requestCode: Int) {
    val values = ContentValues(1)
    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
    fileUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    intent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT)
    intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1)
    intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true)
    if (intent.resolveActivity(packageManager) != null) {
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, requestCode)
    }
}

fun Activity.showGallery(requestCode: Int) {
    val galleryIntent: Intent?
    galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    galleryIntent.type = "image/*"
    if (galleryIntent.resolveActivity(packageManager) == null) {
        showToastMessage("This Application do not have Gallery Application")
        return
    }
    startActivityForResult(galleryIntent, requestCode)
}


fun getFileName(wholePath: String): String {
    var name: String? = null
    val start: Int = wholePath.lastIndexOf('/')
    val end: Int = wholePath.length //lastIndexOf('.');
    name = wholePath.substring((start + 1), end)
    return name
}

fun Context.compressFile(fileUri: Uri): Uri {

    val image = this.compressBitmap(this.handleSamplingAndRotationBitmap(fileUri)!!)
    val file = File(this.cacheDir, getFileName(fileUri.path!!))
    try {
        file.createNewFile()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    val bitmap = Bitmap.createScaledBitmap(image, image.width, image.height, true)
    bitmap.setHasAlpha(true)
    val bos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos)
    val bitmapdata = bos.toByteArray()
    try {
        val fos = FileOutputStream(file)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
    } catch (e: IOException) {

        e.printStackTrace()
    }

    return Uri.fromFile(file)
}

fun Context.compressBitmap(image: Bitmap): Bitmap {

    var finalImage: Bitmap? = null
    if (image.width > Constants.CONST_DEFAULT_IMAGE_WIDTH || image.height > Constants.CONST_DEFAULT_IMAGE_WIDTH) {
        if (image.width > image.height) {
            val ratio = (Constants.CONST_DEFAULT_IMAGE_WIDTH.toFloat() / image.width.toFloat())
            finalImage = Bitmap.createScaledBitmap(image, Constants.CONST_DEFAULT_IMAGE_WIDTH, (image.height * ratio).toInt(), true)

        }
        else if (image.height > image.width) {

            val ratio = (Constants.CONST_DEFAULT_IMAGE_WIDTH.toFloat() / image.height.toFloat())
            finalImage = Bitmap.createScaledBitmap(image, (image.width * ratio).toInt(), Constants.CONST_DEFAULT_IMAGE_WIDTH, true)

        }
        else {
            if (image.width > Constants.CONST_DEFAULT_IMAGE_WIDTH) {
                finalImage = Bitmap.createScaledBitmap(image, Constants.CONST_DEFAULT_IMAGE_WIDTH, Constants.CONST_DEFAULT_IMAGE_WIDTH, true)
            }
            else {
                finalImage = image
            }
        }
    }
    else {
        finalImage = if (image.width > image.height) {
            val ratio = ((image.width * 0.7) / image.width.toFloat())
            Bitmap.createScaledBitmap(image, (image.width * 0.7).toInt(), (image.height * ratio).toInt(), true)
        }
        else {
            val ratio = ((image.height * 0.7) / image.height.toFloat())
            Bitmap.createScaledBitmap(image, (image.width * ratio).toInt(), (image.height * 0.7).toInt(), true)
        }
    }
    return finalImage!!
}

@Throws(IOException::class)
fun Context.handleSamplingAndRotationBitmap(selectedImage: Uri): Bitmap? {
    val MAX_HEIGHT = 1024
    val MAX_WIDTH = 1024

    // First decode with inJustDecodeBounds=true to check dimensions
    val options = BitmapFactory.Options()
    options.inJustDecodeBounds = true
    var imageStream = contentResolver.openInputStream(selectedImage)
    BitmapFactory.decodeStream(imageStream, null, options)
    imageStream!!.close()

    // Calculate inSampleSize
    options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT)

    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false
    imageStream = contentResolver.openInputStream(selectedImage)
    var img = BitmapFactory.decodeStream(imageStream, null, options)
    if (img != null) img = rotateImageIfRequired(img, selectedImage)
    else {
        showToastMessage("Image not available")
        return null
    }

    return img
}

private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int { // Raw height and width of image
    val height = options.outHeight
    val width = options.outWidth
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {
        val heightRatio = (height.toFloat() / reqHeight.toFloat()).roundToInt()
        val widthRatio = (width.toFloat() / reqWidth.toFloat()).roundToInt()
        inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }
    }
    return inSampleSize
}

private fun rotateImageIfRequired(img: Bitmap, selectedImage: Uri): Bitmap {
    val ei = ExifInterface(selectedImage.path!!)
    val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
    return when (orientation) {
        ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(img, 90)
        ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(img, 180)
        ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(img, 270)
        else -> {
            img
        }
    }
}

private fun rotateImage(img: Bitmap, degree: Int): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(degree.toFloat())
    val rotatedImg = Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
    img.recycle()
    return rotatedImg
}

fun Activity.processCapturedPhoto(callback: ICallBackUri) {
    try {
        val cursor = contentResolver.query(Uri.parse(fileUri.toString()), Array(1) { MediaStore.Images.ImageColumns.DATA }, null, null, null)
        cursor!!.moveToFirst()
        val photoPath = cursor.getString(0)
        cursor.close()
        val file = File(photoPath)
        callback.imageUri(Uri.fromFile(file))
    } catch (ex: CursorIndexOutOfBoundsException) {
        ex.printStackTrace()

        val columns: Array<String> = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DATE_ADDED)
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, "${MediaStore.MediaColumns.DATE_ADDED} DESC")

        val coloumnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val photoPath = cursor.getString(coloumnIndex)
        cursor.close()
        val file = File(photoPath)
        callback.imageUri(Uri.fromFile(file))
    }

}

fun Context.isEmailValid(email: String): Boolean {
    return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
}

fun Context.convertBitmapToUri(bitmap: Bitmap): Uri? {
    val file = File(this.cacheDir, System.currentTimeMillis().toString() + ".png")
    try {
        file.createNewFile()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    val bos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos)
    val bitmapdata = bos.toByteArray()
    try {
        val fos = FileOutputStream(file)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
    } catch (e: IOException) {

        e.printStackTrace()
    }

    return Uri.fromFile(file)
}

fun getExternalStorageDir(context: Context, folderName: String): File? {
    val file = File(Environment.getExternalStorageDirectory(), folderName)
    if (!file.exists()) {
        if (!file?.mkdir()) {
        }
    }
    return file
}

fun saveBitmap(path: String, bitmap: Bitmap, context: Context) {
    var myDir = File(path)
    try {
        var outStream = FileOutputStream(myDir)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
        outStream.flush()
        outStream.close()
        val paths = arrayOf<String>(myDir.toString())
        MediaScannerConnection.scanFile(context, paths, null, null)
    } catch (ex: java.lang.Exception) {
        ex.printStackTrace()
    }
}

fun getBitmap(v: View): Bitmap? {
    try {
        v.clearFocus()
        v.isPressed = false
        val willNotCache = v.willNotCacheDrawing()
        v.setWillNotCacheDrawing(false)
        val color = Color.TRANSPARENT
        v.drawingCacheBackgroundColor = 0
        if (color != 0) {
            v.destroyDrawingCache()
        }
        v.isDrawingCacheEnabled = true
        v.buildDrawingCache(true)
        val bitmap = Bitmap.createBitmap(v.drawingCache)
        v.isDrawingCacheEnabled = false
        v.destroyDrawingCache()
        v.setWillNotCacheDrawing(willNotCache)
        v.drawingCacheBackgroundColor = color
        return bitmap
    } catch (ex: NullPointerException) {
        ex.printStackTrace()
        return null
    }
}


fun onTokenExpiredLogout() {
    val fcmtoken = Hello.db.getString(Constants.FCM_TOKEN)
    Hello.db.clear()
    Hello.db.putString(Constants.FCM_TOKEN, fcmtoken!!)
    val intent = Intent(MainActivity.mActivity, Splash::class.java)
    MainActivity.mActivity.startActivity(intent)
    MainActivity.mActivity.finish()
}


fun gotoHome() {
    val fm = MainActivity.mActivity.supportFragmentManager
    for (i in 0 until fm.backStackEntryCount) {
        fm.popBackStack()
    } //    (MainActivity.mActivity as BaseActivity).callFragmentWithReplace(
    //        R.id.mainContainer,
    //        DashboardFragment(),
    //        null
    //    )

}

fun gotoCheckEmail() {
    val fm = MainActivity.mActivity.supportFragmentManager
    for (i in 0 until fm.backStackEntryCount) {
        fm.popBackStack()
    } //    (MainActivity.mActivity as BaseActivity).callFragmentWithReplace(
    //        R.id.container,
    //        CheckEmailFragment(),
    //        null
    //    )

}


fun currentOS(): String {
    val builder = StringBuilder()
    val fields = Build.VERSION_CODES::class.java.fields
    for (field in fields) {
        val fieldName = field.name
        var fieldValue = -1

        try {
            fieldValue = field.getInt(Any())
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        if (fieldValue == Build.VERSION.SDK_INT) {
            builder.append(fieldName)
        }
    }
    return builder.toString()
}

fun Activity.getDeviceIMEI(): String {
    val tm = getSystemService(TELEPHONY_SERVICE) as TelephonyManager

    val tmDevice: String
    val tmSerial: String
    val androidId: String
    tmDevice = "" //    + tm.deviceSoftwareVersion
    tmSerial = "" //    + tm.simSerialNumber
    androidId = "" + Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

    val deviceUuid = UUID(androidId.hashCode().toLong(), tmDevice.hashCode().toLong() shl 32 or tmSerial.hashCode().toLong())
    val deviceId = deviceUuid.toString()
    return deviceId
}

fun Activity.startVideoCamera(requestCode: Int) {
    val values = ContentValues(1)
    values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
    fileUri = contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values)
    val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
//    if (intent.resolveActivity(packageManager) != null) {
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 60)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, requestCode)
//    }
}


fun Activity.showGalleryVideo(requestCode: Int) {
    val galleryIntent: Intent?
    galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
    galleryIntent.type = "video/*"
    if (galleryIntent.resolveActivity(packageManager) == null) {
        showToastMessage("This Application do not have Gallery Application")
        return
    }
    startActivityForResult(galleryIntent, requestCode)
}

fun Activity.processGalleryPhoto(data: Intent, callback: ICallBackUri) {
    val realPath = RealPathUtil.getRealPath(this, data.data!!)
    fileUri = try {
        Uri.fromFile(File(realPath))
    } catch (ex: Exception) {
        if (realPath != null) Uri.parse(realPath)
        else Uri.fromFile(File(data.data!!.path))
    }
    callback.imageUri(fileUri)
}


fun Context.getRootParentFragment(fragment: Fragment): Fragment {
    val parent = fragment.parentFragment
    return if (parent == null) fragment
    else getRootParentFragment(parent)
}

fun Context.copyText(text: String) {
    val clipboard: ClipboardManager = this.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
    val clip: ClipData = ClipData.newPlainText("label", text)
    clipboard.setPrimaryClip(clip)
}

fun adjustFontScale(configuration: Configuration, context: Context) {
    configuration.fontScale = 1.0.toFloat()
    val metrics = context.resources.displayMetrics
    val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    wm.defaultDisplay.getMetrics(metrics)
    metrics.scaledDensity = configuration.fontScale * metrics.density
    context.resources.updateConfiguration(configuration, metrics)
}


fun getBitmapFromView(view: View): Bitmap? {
    val returnedBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(returnedBitmap)
    val bgDrawable: Drawable? = view.background
    if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
    view.draw(canvas)
    return returnedBitmap
}

fun getPrivateStorageDir(context: Context, folderName: String): File? {
    val file = File(context.filesDir, folderName)
    if (!file.exists()) {
        if (!file?.mkdir()) {
        }
    }
    return file
}

fun getMediaPath(context: Context, uri: Uri): String {

    val resolver = context.contentResolver
    val projection = arrayOf(MediaStore.Video.Media.DATA)
    var cursor: Cursor? = null
    try {
        cursor = resolver.query(uri, projection, null, null, null)
        return if (cursor != null) {
            val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(columnIndex)

        }
        else ""

    } catch (e: Exception) {
        resolver.let {
            val filePath = (context.applicationInfo.dataDir + File.separator + System.currentTimeMillis())
            val file = File(filePath)

            resolver.openInputStream(uri)?.use { inputStream ->
                FileOutputStream(file).use { outputStream ->
                    val buf = ByteArray(4096)
                    var len: Int
                    while (inputStream.read(buf).also { len = it } > 0) outputStream.write(buf, 0, len)
                }
            }
            return file.absolutePath
        }
    } finally {
        cursor?.close()
    }
}

@Suppress("DEPRECATION")
fun saveVideoFile(filePath: String?): File? {
    filePath?.let {
        val videoFile = File(filePath)
        val videoFileName = "${System.currentTimeMillis()}_${videoFile.name}"
        val folderName = Environment.DIRECTORY_MOVIES
        if (Build.VERSION.SDK_INT >= 30) {

            val values = ContentValues().apply {

                put(MediaStore.Images.Media.DISPLAY_NAME, videoFileName)
                put(MediaStore.Images.Media.MIME_TYPE, "video/mp4")
                put(MediaStore.Images.Media.RELATIVE_PATH, folderName)
                put(MediaStore.Images.Media.IS_PENDING, 1)
            }

            val collection = MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)

            val fileUri = MainActivity.mActivity.contentResolver.insert(collection, values)

            fileUri?.let {
                MainActivity.mActivity.contentResolver.openFileDescriptor(fileUri, "rw").use { descriptor ->
                    descriptor?.let {
                        FileOutputStream(descriptor.fileDescriptor).use { out ->
                            FileInputStream(videoFile).use { inputStream ->
                                val buf = ByteArray(4096)
                                while (true) {
                                    val sz = inputStream.read(buf)
                                    if (sz <= 0) break
                                    out.write(buf, 0, sz)
                                }
                            }
                        }
                    }
                }

                values.clear()
                values.put(MediaStore.Video.Media.IS_PENDING, 0)
                MainActivity.mActivity.contentResolver.update(fileUri, values, null, null)

                return File(getMediaPath(MainActivity.mActivity, fileUri))
            }
        }
        else {
            val downloadsPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val desFile = File(downloadsPath, videoFileName)

            if (desFile.exists()) desFile.delete()

            try {
                desFile.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return desFile
        }
    }
    return null
}


fun shareCard(path: String, text: String) {
    val uri = Uri.parse(path)
    val shareIntent = Intent()
    shareIntent.action = Intent.ACTION_SEND
    shareIntent.putExtra(Intent.EXTRA_TEXT, text)
    shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
    shareIntent.type = "image/*"
    MainActivity.mActivity.startActivity(Intent.createChooser(shareIntent, "Share Card"))
}

fun shareEmailIntent(mailTo: String) {
    val uriText = "mailto:${mailTo}" + "?subject=" + Uri.encode("") + "&body=" + Uri.encode("")

    val uri = Uri.parse(uriText)

    val sendIntent = Intent(Intent.ACTION_SENDTO)
    sendIntent.data = uri
    MainActivity.mActivity.startActivity(Intent.createChooser(sendIntent, "Send email"))

}


fun validCellPhone(number: String): Boolean {
    return number.isNotEmpty() && Patterns.PHONE.matcher(number).matches()
}

fun openCreateContact() {
    val intent = Intent(Intent.ACTION_INSERT, ContactsContract.Contacts.CONTENT_URI)
    MainActivity.mActivity.startActivity(intent)
}

fun watchYoutubeVideo(id: String) {
    val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
    val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$id"))
    try {
        MainActivity.mActivity.startActivity(appIntent)
    } catch (ex: ActivityNotFoundException) {
        MainActivity.mActivity.startActivity(webIntent)
    }
}


fun Context.openNavigation(lat: Double, long: Double) {
    val uri = "http://maps.google.com/maps?daddr=$lat,$long"
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
    intent.setPackage("com.google.android.apps.maps");
    this.startActivity(intent)
}

fun Context.isMyServiceRunning(serviceClass: Class<*>): Boolean {
    val manager = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
        if (serviceClass.name.equals(service.service.className)) {
            return true
        }
    }
    return false
}

fun calculationByDistance(firstLat: Double, firstLong: Double, secondLat: Double, secodLong: Double): Double {
    val source = Location("")
    val dest = Location("")
    source.latitude = firstLat
    source.longitude = firstLong
    dest.latitude = secondLat
    dest.longitude = secodLong
    val meter = source.distanceTo(dest).toDouble()
    return meter/* val Radius = 6371 // radius of earth in Km
     val dLat = Math.toRadians(secondLat - firstLat)
     val dLon = Math.toRadians(secodLong - firstLong)
     val a = (Math.sin(dLat / 2) * Math.sin(dLat / 2)
             + (Math.cos(Math.toRadians(firstLat))
             * Math.cos(Math.toRadians(secondLat)) * Math.sin(dLon / 2)
             * Math.sin(dLon / 2)))
     val c = 2 * Math.asin(Math.sqrt(a))
     val valueResult = Radius * c
     val km = valueResult / 1
     val newFormat = DecimalFormat("####.##")
     val kmInDec: Double = newFormat.format(km * 1000).toDouble()
     val meter = valueResult % 1000
     val meterInDec: Double =newFormat.format(meter).toDouble()
     Log.i("Radius Value", "$valueResult   KM  $kmInDec Meter   $meterInDec")
     return meterInDec*/
}

fun getTimeTaken(source: Location, dest: Location?): String? {
    val meter = source.distanceTo(dest).toDouble()
    val kms = meter / 1000
    val kms_per_min = 0.083
    val mins_taken = kms / kms_per_min
    val totalMinutes = mins_taken.toInt()
    Log.d("ResponseT", "meter :$meter kms : $kms mins :$mins_taken")
    return when {
        totalMinutes < 1 -> {
            "Less than 1 min"
        }
        totalMinutes < 60 -> {
            "$totalMinutes min"
        }
        else -> {
            var minutes = (totalMinutes % 60).toString()
            minutes = if (minutes.length == 1) "0$minutes" else minutes
            (totalMinutes / 60).toString() + " hr " + minutes + " min"
        }
    }
}


fun SnapHelper.getSnapPosition(recyclerView: RecyclerView): Int {
    val layoutManager = recyclerView.layoutManager ?: return RecyclerView.NO_POSITION
    val snapView = findSnapView(layoutManager) ?: return RecyclerView.NO_POSITION
    return layoutManager.getPosition(snapView)
}

fun Activity.processGalleryMultipleVideoGligar(data: Intent, callback: ICallBackUriMultiple) {
    val imagesList = data.extras?.getStringArray(GligarPicker.IMAGES_RESULT)
    val fileUriList: ArrayList<Uri> = ArrayList()
    if (imagesList != null) {
        for (element in imagesList) {
            fileUriList.add(Uri.fromFile(File(element)))
        }
    }

    callback.imageUri(fileUriList)
}

interface ICallBackUriMultiple {
    fun imageUri(result: ArrayList<Uri>)
}

fun getIPAddress(useIPv4: Boolean): String? {
    try {
        val interfaces: List<NetworkInterface> = Collections.list(NetworkInterface.getNetworkInterfaces())
        for (intf in interfaces) {
            val addrs: List<InetAddress> = Collections.list(intf.getInetAddresses())
            for (addr in addrs) {
                if (!addr.isLoopbackAddress()) {
                    val sAddr: String = addr.getHostAddress()
                    val isIPv4 = sAddr.indexOf(':') < 0
                    if (useIPv4) {
                        if (isIPv4) return sAddr
                    } else {
                        if (!isIPv4) {
                            val delim = sAddr.indexOf('%')
                            return if (delim < 0) sAddr.toUpperCase() else sAddr.substring(0, delim).toUpperCase()
                        }
                    }
                }
            }
        }
    } catch (ignored: java.lang.Exception) {
    }
    return ""
}


//fun createImageFromString(context: Context?, qrString: String?): Bitmap? {
//    val bm = makeQRBitmap(qrString)
//    return if (bm != null) {
//        val stream = ByteArrayOutputStream()
//        bm.compress(Bitmap.CompressFormat.PNG, 100, stream)
//        bm
//    }
//    else {
//        Toast.makeText(context, "BM is null", Toast.LENGTH_SHORT).show()
//        null
//    }
//}
//
//fun makeQRBitmap(qrCodeUrl: String?): Bitmap? {
//    var bitmap: Bitmap? = null
//    val writer = QRCodeWriter()
//    try {
//        val bitMatrix = writer.encode(qrCodeUrl, BarcodeFormat.QR_CODE, 300, 300)
//        val width = bitMatrix.width
//        val height = bitMatrix.height
//        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
//        for (x in 0 until width) {
//            for (y in 0 until height) {
//                bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
//            }
//        }
//    } catch (e: WriterException) {
//        e.printStackTrace()
//    }
//    return bitmap
//}

@Throws(WriterException::class)
fun encodeAsBitmap(str: String): Bitmap? {
    var bitmap: Bitmap? = null
    try {
        val manager = MainActivity.mActivity.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = manager.defaultDisplay
        val point = Point()
        display.getSize(point)
        val width = point.x
        val height = point.y
        val result: BitMatrix
        try {
            result = MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, 600, 600, null)
        } catch (iae: IllegalArgumentException) {
            // Unsupported format
            return null
        }

        val w = result.width
        val h = result.height
        val pixels = IntArray(w * h)
        for (y in 0 until h) {
            val offset = y * w
            for (x in 0 until w) {
                pixels[offset + x] =
                    if (result.get(x, y)) Color.BLACK else Color.WHITE
            }
        }
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h)
        return bitmap

    } catch (ex: Exception) {
        ex.printStackTrace()
        return bitmap
    }

}

fun getLink(tag : String, userName: String) : String {
    val link: String= when(tag){
        Constants.facebook-> {
            Constants.FACEBOOK_URL+userName
        }
        Constants.whatsapp-> {
            Constants.WHATSAPP_URL+userName
        }
        Constants.twitter-> {
            Constants.TWITTER_URL+userName
        }
        Constants.instagram-> {
            Constants.INSTAGRAM_URL+userName
        }
        Constants.snapchat-> {
            Constants.SNAPCHAT_URL+userName
        }
        Constants.youtube-> {
            Constants.YOUTUBE_URL+userName
        }
        Constants.tiktok-> {
            Constants.TIKTOK_URL+userName
        }
        Constants.pinterest-> {
            Constants.PINTEREST_URL+userName
        }
        Constants.linkedin-> {
            Constants.LINKEDIN_URL+userName
        }
        Constants.website-> {
            if(userName.contains("http")){userName}else{"https://"+userName}
        }
        Constants.tinder-> {
            Constants.TINDER_URL+userName
        }
        Constants.bumble-> {
            Constants.BUMBLE_URL+userName
        }
        Constants.nintendo-> {
            Constants.NINTENDO_URL+userName
        }
        Constants.signal-> {
            Constants.SIGNAL_URL+userName
        }
        Constants.weChat-> {
            Constants.WE_CHAT_URL+userName
        }
        Constants.telegram-> {
            Constants.TELEGRAM_URL+userName
        }
        Constants.twitch-> {
            Constants.TWITCH_URL+userName
        }
        Constants.discord-> {
            Constants.DISCORD_URL+userName
        }
        Constants.calendly-> {
            Constants.CALENDLY_URL+userName
        }
        Constants.skype-> {
            "skype:"+userName+"?chat"
        }
        Constants.xbox-> {
            Constants.XBOX_URL+userName
        }
        Constants.playstation-> {
            Constants.PLAYSTATION_URL+userName
        }
        Constants.reddit-> {
            Constants.REDDIT_URL+userName
        }
        Constants.payPal-> {
            Constants.PAYPAL_URL+userName
        }
        Constants.zelle-> {
            Constants.ZELLE_URL+userName
        }
        Constants.cashApp-> {
            Constants.CASH_APP_URL+userName
        }
        Constants.venmo-> {
            Constants.VENMO_URL+userName
        }
        Constants.yelp-> {
            Constants.YELP_URL+userName
        }
        Constants.doorDash-> {
            Constants.DOORDASH_URL+userName
        }
        Constants.grubHub-> {
            Constants.GRUBHUB_URL+userName
        }
        Constants.uberEats-> {
            Constants.UBER_EATS_URL+userName
        }
        Constants.goToMeeting-> {
            Constants.GO_TO_MEETING_URL+userName
        }
        Constants.zoom-> {
            "https://"+userName+"zoom.us/my/"
        }
        else -> ""
    }
    return link
}