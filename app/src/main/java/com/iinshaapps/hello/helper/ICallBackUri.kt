package com.iinshaapps.hello.helper

import android.net.Uri

interface ICallBackUri {
    fun imageUri(result: Uri?)
}