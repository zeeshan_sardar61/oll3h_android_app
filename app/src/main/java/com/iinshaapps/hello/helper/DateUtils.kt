package com.iinshaapps.hello.helper

import android.os.Build
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


object DateUtils {

    /**
     * The maximum date possible.
     */
    var MAX_DATE = Date(java.lang.Long.MAX_VALUE)

    /**
     *
     * Checks if two dates are on the same day ignoring time.
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is `null`
     */
    fun isSameDay(date1: Date?, date2: Date?): Boolean {
        if (date1 == null || date2 == null) {
            throw IllegalArgumentException("The dates must not be null")
        }
        val cal1 = Calendar.getInstance()
        cal1.time = date1
        val cal2 = Calendar.getInstance()
        cal2.time = date2
        return isSameDay(cal1, cal2)
    }

    /**
     *
     * Checks if two calendars represent the same day ignoring time.
     *
     * @param cal1 the first calendar, not altered, not null
     * @param cal2 the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is `null`
     */
    fun isSameDay(cal1: Calendar?, cal2: Calendar?): Boolean {
        if (cal1 == null || cal2 == null) {
            throw IllegalArgumentException("The dates must not be null")
        }
        return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
    }

    /**
     *
     * Checks if a date is today.
     *
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is `null`
     */
    fun isToday(date: Date): Boolean {
        return isSameDay(date, Calendar.getInstance().time)
    }

    /**
     *
     * Checks if a calendar date is today.
     *
     * @param cal the calendar, not altered, not null
     * @return true if cal date is today
     * @throws IllegalArgumentException if the calendar is `null`
     */
    fun isToday(cal: Calendar): Boolean {
        return isSameDay(cal, Calendar.getInstance())
    }

    /**
     *
     * Checks if the first date is before the second date ignoring time.
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is before the second date day.
     * @throws IllegalArgumentException if the date is `null`
     */
    fun isBeforeDay(date1: Date?, date2: Date?): Boolean {
        if (date1 == null || date2 == null) {
            throw IllegalArgumentException("The dates must not be null")
        }
        val cal1 = Calendar.getInstance()
        cal1.time = date1
        val cal2 = Calendar.getInstance()
        cal2.time = date2
        return isBeforeDay(cal1, cal2)
    }

    /**
     *
     * Checks if the first calendar date is before the second calendar date ignoring time.
     *
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is before cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are `null`
     */
    fun isBeforeDay(cal1: Calendar?, cal2: Calendar?): Boolean {
        if (cal1 == null || cal2 == null) {
            throw IllegalArgumentException("The dates must not be null")
        }
        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return true
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return false
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return true
        return if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) false else cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR)
    }

    /**
     *
     * Checks if the first date is after the second date ignoring time.
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is after the second date day.
     * @throws IllegalArgumentException if the date is `null`
     */
    fun isAfterDay(date1: Date?, date2: Date?): Boolean {
        if (date1 == null || date2 == null) {
            throw IllegalArgumentException("The dates must not be null")
        }
        val cal1 = Calendar.getInstance()
        cal1.time = date1
        val cal2 = Calendar.getInstance()
        cal2.time = date2
        return isAfterDay(cal1, cal2)
    }

    /**
     *
     * Checks if the first calendar date is after the second calendar date ignoring time.
     *
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is after cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are `null`
     */
    fun isAfterDay(cal1: Calendar?, cal2: Calendar?): Boolean {
        if (cal1 == null || cal2 == null) {
            throw IllegalArgumentException("The dates must not be null")
        }
        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return false
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return true
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return false
        return if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) true else cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR)
    }

    /**
     *
     * Checks if a date is after today and within a number of days in the future.
     *
     * @param date the date to check, not altered, not null.
     * @param days the number of days.
     * @return true if the date day is after today and within days in the future .
     * @throws IllegalArgumentException if the date is `null`
     */
    fun isWithinDaysFuture(date: Date?, days: Int): Boolean {
        if (date == null) {
            throw IllegalArgumentException("The date must not be null")
        }
        val cal = Calendar.getInstance()
        cal.time = date
        return isWithinDaysFuture(cal, days)
    }

    /**
     *
     * Checks if a calendar date is after today and within a number of days in the future.
     *
     * @param cal  the calendar, not altered, not null
     * @param days the number of days.
     * @return true if the calendar date day is after today and within days in the future .
     * @throws IllegalArgumentException if the calendar is `null`
     */
    fun isWithinDaysFuture(cal: Calendar?, days: Int): Boolean {
        if (cal == null) {
            throw IllegalArgumentException("The date must not be null")
        }
        val today = Calendar.getInstance()
        val future = Calendar.getInstance()
        future.add(Calendar.DAY_OF_YEAR, days)
        return isAfterDay(cal, today) && !isAfterDay(cal, future)
    }

    /**
     * Returns the given date with the time set to the start of the day.
     */
    fun getStart(date: Date): Date? {
        return clearTime(date)
    }

    /** Determines whether or not a date has any time values (hour, minute,
     * seconds or millisecondsReturns the given date with the time values cleared.  */

    /**
     * Returns the given date with the time values cleared.
     */
    fun clearTime(date: Date?): Date? {
        if (date == null) {
            return null
        }
        val c = Calendar.getInstance()
        c.time = date
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)
        return c.time
    }

    /**
     * Determines whether or not a date has any time values.
     *
     * @param date The date.
     * @return true iff the date is not null and any of the date's hour, minute,
     * seconds or millisecond values are greater than zero.
     */
    fun hasTime(date: Date?): Boolean {
        if (date == null) {
            return false
        }
        val c = Calendar.getInstance()
        c.time = date
        if (c.get(Calendar.HOUR_OF_DAY) > 0) {
            return true
        }
        if (c.get(Calendar.MINUTE) > 0) {
            return true
        }
        return if (c.get(Calendar.SECOND) > 0) {
            true
        } else c.get(Calendar.MILLISECOND) > 0
    }

    /**
     * Returns the given date with time set to the end of the day
     */
    fun getEnd(date: Date?): Date? {
        if (date == null) {
            return null
        }
        val c = Calendar.getInstance()
        c.time = date
        c.set(Calendar.HOUR_OF_DAY, 23)
        c.set(Calendar.MINUTE, 59)
        c.set(Calendar.SECOND, 59)
        c.set(Calendar.MILLISECOND, 999)
        return c.time
    }

    /**
     * Returns the maximum of two dates. A null date is treated as being less
     * than any non-null date.
     */
    fun max(d1: Date?, d2: Date?): Date? {
        if (d1 == null && d2 == null) return null
        if (d1 == null) return d2
        if (d2 == null) return d1
        return if (d1.after(d2)) d1 else d2
    }

    /**
     * Returns the minimum of two dates. A null date is treated as being greater
     * than any non-null date.
     */
    fun min(d1: Date?, d2: Date?): Date? {
        if (d1 == null && d2 == null) return null
        if (d1 == null) return d2
        if (d2 == null) return d1
        return if (d1.before(d2)) d1 else d2
    }

    fun isToday(date: String): Boolean {
        val d = stringToDate(date)
        return android.text.format.DateUtils.isToday(d!!.time)
    }

    fun isYesterday(date: String): Boolean {
        val d = stringToDate(date)
        return android.text.format.DateUtils.isToday(d!!.time + android.text.format.DateUtils.DAY_IN_MILLIS)
    }

    fun isTomorrow(date: String): Boolean {
        val d = stringToDate(date)
        return android.text.format.DateUtils.isToday(d!!.time - android.text.format.DateUtils.DAY_IN_MILLIS)
    }

    fun stringToDate(dtString: String): Date? {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var date: Date? = null
        try {
            date = format.parse(dtString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return date
    }

    fun stringToPromoteDate(dtString: String): Date? {
        val format = SimpleDateFormat("yyyy-MM-dd")
        var date: Date? = null
        try {
            date = format.parse(dtString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return date
    }

    private fun getDayFromDateString(stringDate: String): String {
        val daysArray = arrayOf("Sat", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri")
        var day = ""

        var dayOfWeek: Int
        var date: Date
        try {
            date = stringToDate(stringDate)!!
            val c = Calendar.getInstance()
            c.time = date
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
            if (dayOfWeek < 0) {
                dayOfWeek += 7
            }
            if (dayOfWeek == 7)
                dayOfWeek = 0
            day = daysArray[dayOfWeek]
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return day
    }

    fun formatDate(dtString: String, useFormat: Boolean): String {
        var dtString = dtString
        var format = if (useFormat)
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        else
            SimpleDateFormat("yyyy-MM-dd hh:mm a")
        val targetFormat = SimpleDateFormat("MMM dd, yyyy • hh:mm a")

        var date: Date? = null
        try {
            date = format.parse(dtString)
            //            dtString = date.toString();
            dtString = targetFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        return dtString
    }

    fun formatDateIntUtc(dtString: String): String {
        var dtString = dtString

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val targetFormat = SimpleDateFormat("MMM dd, yyyy hh:mm a")

        var date: Date? = null
        try {
            date = targetFormat.parse(dtString)
            //            dtString = date.toString();
            dtString = simpleDateFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        return dtString
    }

    fun formatDateNew(dtString: String): String {
        var dtString = dtString

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val targetFormat = SimpleDateFormat("dd MMM, yyyy")

        var date: Date? = null
        try {
            date = simpleDateFormat.parse(dtString)
            //            dtString = date.toString();
            dtString = targetFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


        return dtString
    }

    fun dateToString(dtString: Date): String? {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var date: String? = null
        date = format.format(dtString)
        return date
    }


    fun convertDate(startDate: String, endDate: String): String {
        var finalDate: String? = null
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDate = df.format(Calendar.getInstance().time)
        val splitedStartDate = startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val subSplitedStartDate = splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val splitedEndDate = endDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val subSplitedndDate = splitedEndDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val time1 = convertIn12HourFormat(startDate)
        val time2 = convertIn12HourFormat(endDate)
        if (splitedStartDate[0].equals(splitedEndDate[0], ignoreCase = true)) {
            if (isToday(startDate)) {
                finalDate = "Today $time1 to $time2"
            } else if (isYesterday(startDate)) {
                finalDate = "Yesterday $time1 to $time2"
            } else if (isTomorrow(startDate)) {
                finalDate = "Tomorrow $time1 to $time2"
            } else if (getDaysDiffrenece(startDate, currentDate) in 0..5) {
                finalDate = getDayName(startDate) + ", " + time1 + " to " + time2
            } else {
                finalDate = Util.getMonthNameByValue(Integer.parseInt(subSplitedStartDate[1])) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                        " - " + time1 + " ~ " + time2
            }
        } else {
            if (isToday(startDate)) {
                finalDate = "Today $time1"
                if (isTomorrow(endDate))
                    finalDate = "$finalDate - Tomorrow $time2"
                else if (getDaysDiffrenece(endDate, currentDate) in 0..5)
                    finalDate = finalDate + " - " + getDayName(endDate) + " " + time2
                else
                    finalDate = finalDate + " - " + Util.getMonthNameByValue(Integer.parseInt(subSplitedndDate[1])) + " " + subSplitedndDate[2] + ", " + subSplitedndDate[0] + "  " + time1
            } else if (isTomorrow(startDate)) {
                finalDate = "Tomorrow $time1"
                if (isTomorrow(endDate))
                    finalDate = "$finalDate - Tomorrow $time2"
                else if (getDaysDiffrenece(endDate, currentDate) >= 0 && getDaysDiffrenece(endDate, currentDate) <= 5)
                    finalDate = finalDate + " - " + getDayName(endDate) + " " + time2
                else
                    finalDate = finalDate + " - " + Util.getMonthNameByValue(Integer.parseInt(subSplitedndDate[1])) + " " + subSplitedndDate[2] + ", " + subSplitedndDate[0] + "  " + time1
            } else if (isYesterday(startDate)) {
                finalDate = "Yesterday $time1"
                if (isToday(endDate))
                    finalDate = "$finalDate - Today $time2"
                else if (isTomorrow(endDate))
                    finalDate = "$finalDate - Tomorrow $time2"
                else if (getDaysDiffrenece(endDate, currentDate) >= 0 && getDaysDiffrenece(endDate, currentDate) <= 5)
                    finalDate = finalDate + " - " + getDayName(endDate) + " " + time2
                else
                    finalDate = finalDate + " - " + Util.getMonthNameByValue(Integer.parseInt(subSplitedndDate[1])) + " " + subSplitedndDate[2] + ", " + subSplitedndDate[0] + "  " + time1

            } else if (getDaysDiffrenece(startDate, endDate) >= 0 && getDaysDiffrenece(
                    startDate,
                    endDate
                ) <= 5
            ) {
                finalDate =
                    getDayName(startDate) + " " + time1 + " - " + getDayName(endDate) + " " + time2
            } else {
                finalDate =
                    Util.getMonthNameByValue(Integer.parseInt(subSplitedStartDate[1])) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                            "  " + time1 + " - " + Util.getMonthNameByValue(
                        Integer.parseInt(
                            subSplitedndDate[1]
                        )
                    ) + " " + subSplitedndDate[2] + ", " + subSplitedndDate[0] + " " + time2
            }
        }
        return finalDate
    }

    fun convertDateNewWithoutYear(startDate: String, endDate: String): String {
        var finalDate: String? = null
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDate = df.format(Calendar.getInstance().time)
        val splitedStartDate =
            startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val subSplitedStartDate =
            splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val splitedEndDate =
            endDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val subSplitedndDate =
            splitedEndDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val time1 = convertIn12HourFormat(startDate)
        val time2 = convertIn12HourFormat(endDate)
        if (subSplitedStartDate[2].startsWith("0"))
            subSplitedStartDate[2] = subSplitedStartDate[2].substring(1)
        if (subSplitedndDate[2].startsWith("0"))
            subSplitedndDate[2] = subSplitedndDate[2].substring(1)
        if (splitedStartDate[0].equals(splitedEndDate[0], ignoreCase = true)) {
            finalDate = "${getDayFromDateString(startDate)}, " + Util.getMonthNameByValue(
                Integer.parseInt(subSplitedStartDate[1])
            ) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                    " • " + time1 + " - " + time2
        } else {
            finalDate = "${getDayFromDateString(startDate)}, " + Util.getMonthNameByValue(
                Integer.parseInt(subSplitedStartDate[1])
            ) + " " + subSplitedStartDate[2] +
                    " • " + time1 + " - " + " \n" + "${getDayFromDateString(endDate)}, " + Util.getMonthNameByValue(
                Integer.parseInt(subSplitedndDate[1])
            ) + " " + subSplitedndDate[2] + " • " + time2
        }
        return finalDate


    }

    fun convertDateStartDateWithoutYear(startDate: String): String {
        var finalDate: String? = null
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val splitedStartDate =
            startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val subSplitedStartDate =
            splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val time1 = convertIn12HourFormat(startDate)
        if (subSplitedStartDate[2].startsWith("0"))
            subSplitedStartDate[2] = subSplitedStartDate[2].substring(1)

        finalDate = "${getDayFromDateString(startDate)}, " + Util.getMonthNameByValue(
            Integer.parseInt(subSplitedStartDate[1])
        ) + " " + subSplitedStartDate[2] +
                " • " + time1

        return finalDate


    }

    fun convertDateNewWithYear(startDate: String, endDate: String): String {
        var finalDate: String? = null
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDate = df.format(Calendar.getInstance().time)
        val splitedStartDate =
            startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val subSplitedStartDate =
            splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val splitedEndDate =
            endDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val subSplitedndDate =
            splitedEndDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val time1 = convertIn12HourFormat(startDate)
        val time2 = convertIn12HourFormat(endDate)
        if (subSplitedStartDate[2].startsWith("0"))
            subSplitedStartDate[2] = subSplitedStartDate[2].substring(1)
        if (subSplitedndDate[2].startsWith("0"))
            subSplitedndDate[2] = subSplitedndDate[2].substring(1)
        if (splitedStartDate[0].equals(splitedEndDate[0], ignoreCase = true)) {
            finalDate = "${getDayFromDateString(startDate)}, " + Util.getMonthNameByValue(
                Integer.parseInt(subSplitedStartDate[1])
            ) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                    " • " + time1 + " - " + time2
        } else {
            finalDate = "${getDayFromDateString(startDate)}, " + Util.getMonthNameByValue(
                Integer.parseInt(subSplitedStartDate[1])
            ) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                    " • " + time1 + " - " + " \n" + "${getDayFromDateString(endDate)}, " + Util.getMonthNameByValue(
                Integer.parseInt(subSplitedndDate[1])
            ) + " " + subSplitedndDate[2] + ", " + subSplitedndDate[0] + " • " + time2
        }
        return finalDate
    }


    fun convertIn12HourFormat(startTime: String): String {
        var startTime = startTime
        val splitedStartDate = startTime.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val date = splitedStartDate[0]
        val time = splitedStartDate[1]

        val sdf = SimpleDateFormat("HH:mm:ss")
        val sdfs = SimpleDateFormat("hh:mm a", Locale.US)
        val dt: Date
        try {
            dt = sdf.parse(time)
            startTime = sdfs.format(dt)
            println("Time Display: " + sdfs.format(dt)) // <-- I got result here
        } catch (e: ParseException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        return startTime
    }


    fun convertIn12HourFormatWithMinute(startTime: String): String {
        var startTime = startTime
        val splitedStartDate = startTime.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val date = splitedStartDate[0]
        val time = splitedStartDate[1]

        val sdf = SimpleDateFormat("hh:mm:ss")
        val sdfs = SimpleDateFormat("h:mm a")
        val dt: Date
        try {
            dt = sdf.parse(time)
            startTime = sdfs.format(dt)
            println("Time Display: " + sdfs.format(dt)) // <-- I got result here
            startTime = startTime.replace("am", "AM").replace("pm","PM")
        } catch (e: ParseException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        return startTime
    }


    fun getDaysDiffrenece(startDate: String, currentDate: String): Long {
        // String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(new Date());
        val diff = stringToDate(startDate)!!.time - stringToDate(currentDate)!!.time
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
    }

    fun getDayDiffrenece(startDate: String, currentDate: String): Long {
        // String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(new Date());
        val diff = stringToDate(currentDate)!!.time - stringToDate(startDate)!!.time
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
    }

    fun getSecondDiffrenece(startDate: String, currentDate: String): Long {
        // String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(new Date());
        val diff = stringToDate(currentDate)!!.time - stringToDate(startDate)!!.time
        return TimeUnit.SECONDS.convert(diff, TimeUnit.MILLISECONDS)
    }

    fun getHoursDiffrenece(startDate: String, currentDate: String): Long {
        // String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(new Date());
        val diff = stringToDate(currentDate)!!.time - stringToDate(startDate)!!.time
        return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS)
    }


    fun getMinutesDiffrenece(startDate: String, currentDate: String): Long {
        // String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(new Date());
        val diff = stringToDate(currentDate)!!.time - stringToDate(startDate)!!.time
        return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS)
    }

    fun getMillisDiffrenece(startDate: String, currentDate: String): Long {
        // String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).format(new Date());
        val diff = stringToDate(startDate)!!.time - stringToDate(currentDate)!!.time
        return TimeUnit.MILLISECONDS.convert(diff, TimeUnit.MILLISECONDS)
    }


    fun getDayName(date: String): String? {

        var name: String? = null
        val outFormat = SimpleDateFormat("EEEE")
        name = outFormat.format(stringToDate(date))

        return name
    }

    fun getDayNameThree(date: String): String? {

        var name: String? = null
        val outFormat = SimpleDateFormat("EEE")
        name = outFormat.format(stringToDate(date))

        return name
    }

    fun convertDateToTimeAgo(startDate: String): String {
        var finalDate: String? = null
        if (startDate.equals("Just Now", ignoreCase = true)) {
            finalDate = "Just Now"
        } else {
            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val currentDate = df.format(Calendar.getInstance().time)
            getMinutesDiffrenece(startDate, currentDate)
            val splitedStartDate = startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val subSplitedStartDate = splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val subSplitedStartTime = splitedStartDate[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val time1 = convertIn12HourFormatWithMinute(startDate)
            if (getSecondDiffrenece(startDate, currentDate) < 2)
//                finalDate = getSecondDiffrenece(startDate, currentDate).toString() + " Second Ago"
                finalDate = "Just Now"
            else if (getSecondDiffrenece(startDate, currentDate) < 60)
//                finalDate = getSecondDiffrenece(startDate, currentDate).toString() + " Seconds Ago"
                finalDate = "Just Now"
            else if (getMinutesDiffrenece(startDate, currentDate) < 2)
                finalDate = getMinutesDiffrenece(startDate, currentDate).toString() + " Min Ago"
            else if (getMinutesDiffrenece(startDate, currentDate) < 60)
                finalDate = getMinutesDiffrenece(startDate, currentDate).toString() + " Mins Ago"
            else if (getHoursDiffrenece(startDate, currentDate) < 2)
                finalDate = getHoursDiffrenece(startDate, currentDate).toString() + " Hour Ago"
            else if (getHoursDiffrenece(startDate, currentDate) < 24)
                finalDate = getHoursDiffrenece(startDate, currentDate).toString() + " Hours Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 2)
                finalDate = getDayDiffrenece(startDate, currentDate).toString() + " day Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 30) {
                if (getDayDiffrenece(startDate, currentDate) < 7)
                    finalDate = getDayDiffrenece(startDate, currentDate).toString() + " days Ago"
                else if (getDayDiffrenece(startDate, currentDate) < 14)
                    finalDate = "1 week Ago"
                else if (getDayDiffrenece(startDate, currentDate) < 21) {
                    finalDate = "2 week Ago"
                } else {
                    finalDate = "3 week Ago"
                }
            } else if (getDayDiffrenece(startDate, currentDate) < 60)
                finalDate = "1 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 90)
                finalDate = "2 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 120)
                finalDate = "3 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 150)
                finalDate = "4 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 180)
                finalDate = "5 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 210)
                finalDate = "6 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 240)
                finalDate = "7 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 270)
                finalDate = "8 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 300)
                finalDate = "9 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 330)
                finalDate = "10 month Ago"
            else if (getDayDiffrenece(startDate, currentDate) < 365)
                finalDate = "11 month Ago"
            else
                finalDate = Util.getMonthNameByValue(Integer.parseInt(subSplitedStartDate[1])) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                        " - " + time1
        }
        return finalDate
    }


    fun convertDateDisplaySingleDate(startDate: String): String {
        var finalDate: String? = null
        if (startDate.equals("Just Now", ignoreCase = true)) {
            finalDate = "Just Now"
        } else {
            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val currentDate = df.format(Calendar.getInstance().time)
            getMinutesDiffrenece(startDate, currentDate)
            val splitedStartDate = startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val subSplitedStartDate = splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val time1 = convertIn12HourFormatWithMinute(startDate)
            finalDate = Util.getMonthNameByValue(Integer.parseInt(subSplitedStartDate[1])) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                    " - " + time1
        }
        return finalDate
    }

    fun convertSingleDate(startDate: String): String {
        var finalDate: String? = null
        if (startDate.equals("Just Now", ignoreCase = true)) {
            finalDate = "Just Now"
        } else {

            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val currentDate = df.format(Calendar.getInstance().time)
            getMinutesDiffrenece(startDate, currentDate)
            val splitedStartDate = startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val subSplitedStartDate = splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val subSplitedStartTime = splitedStartDate[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val time1 = convertIn12HourFormatWithMinute(startDate)
            if (isToday(startDate)) {
                if (getMinutesDiffrenece(startDate, currentDate) < 0 && getMinutesDiffrenece(startDate, currentDate) < 60)
                    finalDate = getMinutesDiffrenece(startDate, currentDate).toString() + " Minutes Ago"
                else
                    finalDate = "Today, $time1"
            } else if (isYesterday(startDate)) {
                finalDate = "Yesterday, $time1"
            } else if (isTomorrow(startDate)) {
                finalDate = "Tomorrow, $time1"
            } else if (getDaysDiffrenece(startDate, currentDate) >= 0 && getDaysDiffrenece(startDate, currentDate) <= 5) {
                finalDate = getDayNameThree(startDate) + ", " + time1
            } else {
                finalDate = getDayNameThree(startDate) + ", " + Util.getMonthNameByValue(Integer.parseInt(subSplitedStartDate[1])) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0] +
                        " " + time1
            }
        }

        return finalDate
    }

    fun getMinutesDifference(startDate: String, currentDate: String): Long {
        val diff = stringToDate(currentDate)?.time!! - stringToDate(startDate)?.time!!
        return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS)
    }

    fun convertDateForEvent(startDate: String): String {
        var startDate = startDate
        //        String finalDate = null;
        //        String AM_PM;
        //        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //        String currentDate = df.format(Calendar.getInstance().getTime());
        ////        getMinutesDiffrenece(startDate, currentDate);
        //        String[] splitedStartDate = startDate.split("T");
        ////        splitedStartDate[0]
        //
        //        String[] subSplitedStartDate = splitedStartDate[0].split("-");
        //        String[] subSplitedStartTime = splitedStartDate[1].split(":");
        //
        //        if (Integer.parseInt(subSplitedStartTime[0]) < 12) {
        //            AM_PM = "AM";
        //        } else {
        //            AM_PM = "PM";
        //        }

        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(startDate)
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd hh:mm a") //this format changeable
            //            dateFormatter.setTimeZone(TimeZone.getDefault());
            startDate = dateFormatter.format(dateToUTC(value))
            //            OurDate = formatter.format(dateToUTC(value));
        } catch (e: Exception) {
            startDate = "00-00-0000 00:00"
        }

        return startDate


        //        return splitedStartDate[0] + " " + String.format("%02d:%02d", Integer.parseInt(subSplitedStartTime[0]), Integer.parseInt(subSplitedStartTime[1])) + " " + AM_PM;

    }

    fun getPromoteEventDate(date : String): String {
        var startDate = date

        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(startDate)
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
            //            dateFormatter.setTimeZone(TimeZone.getDefault());
            startDate = dateFormatter.format(dateToUTC(value))
            //            OurDate = formatter.format(dateToUTC(value));
        } catch (e: Exception) {
            startDate = "00-00-0000 00:00"
        }

        return startDate
    }

    fun getDate(startDate: String): String {
        var startDate = startDate
        //        String finalDate = null;
        //        String AM_PM;
        //        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //        String currentDate = df.format(Calendar.getInstance().getTime());
        ////        getMinutesDiffrenece(startDate, currentDate);
        //        String[] splitedStartDate = startDate.split("T");
        ////        splitedStartDate[0]
        //
        //        String[] subSplitedStartDate = splitedStartDate[0].split("-");
        //        String[] subSplitedStartTime = splitedStartDate[1].split(":");
        //
        //        if (Integer.parseInt(subSplitedStartTime[0]) < 12) {
        //            AM_PM = "AM";
        //        } else {
        //            AM_PM = "PM";
        //        }

        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(startDate)
            val dateFormatter = SimpleDateFormat("MMM dd, yyyy") //this format changeable
            //            dateFormatter.setTimeZone(TimeZone.getDefault());
            startDate = dateFormatter.format(dateToUTC(value))
            //            OurDate = formatter.format(dateToUTC(value));
        } catch (e: Exception) {
            startDate = "00-00-0000 00:00"
        }

        return startDate


        //        return splitedStartDate[0] + " " + String.format("%02d:%02d", Integer.parseInt(subSplitedStartTime[0]), Integer.parseInt(subSplitedStartTime[1])) + " " + AM_PM;

    }


    fun hourDifference(startDate: Date, endDate: Date): Long {
        //milliseconds
        var different = endDate.time - startDate.time

        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24

        val elapsedDays = different / daysInMilli
        different = different % daysInMilli

        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli

        return elapsedHours
    }


    fun getDateInUtc(OurDate: String): String {
        var OurDate = OurDate
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd hh:mm a")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(OurDate)
            //            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm a"); //this format changeable
            //            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = formatter.format(dateToUTC(value))
        } catch (e: Exception) {
            OurDate = "00-00-0000 00:00"
        }

        return OurDate
    }

    fun getDateInUtcUpdate(OurDate: String): String {
        var OurDate = OurDate
        try {
            val formatter = SimpleDateFormat("MMM dd, yyyy - hh:mm a")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(OurDate)
            //            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm a"); //this format changeable
            //            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = formatter.format(dateToUTC(value))
        } catch (e: Exception) {
            OurDate = "00-00-0000 00:00"
        }

        return OurDate
    }

    fun getSingleDateInUtc(OurDate: String): String {
        var OurDate = OurDate
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(OurDate)
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
            //            dateFormatter.setTimeZone(TimeZone.getDefault());
            OurDate = dateFormatter.format(dateToUTC(value))
            //            OurDate = formatter.format(dateToUTC(value));
        } catch (e: Exception) {
            OurDate = "00-00-0000 00:00"
        }

        return OurDate
    }

//    fun getLocalDate(OurDate: String): String {
//        try {
//            if (OurDate.equals("Just Now", ignoreCase = true)) {
//                return "Just Now"
//            }
//
//            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//            val date = simpleDateFormat.parse(OurDate)
//            return simpleDateFormat.format(dateFromUTC(date))
//        } catch (e: ParseException) {
//            e.printStackTrace()
//            return ""
//        }
//
//    }

    fun getLocalDate(OurDate: String): String {
        if (OurDate.equals("Just Now", ignoreCase = true)) {
            return "Just Now"
        }
        return try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val date = simpleDateFormat.parse(OurDate)
            simpleDateFormat.format(dateFromUTC(date))
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }


    fun getDateFromString(OurDate: String): String {
        val formatter: SimpleDateFormat
        var formattedDate = ""
        try {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK)
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("MMM dd, yyyy") //this format changeable
                formattedDate = dateFormatter.format(dateFromUTC(value))
            } else {
                formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("MMM dd, yyyy") //this format changeable
                formattedDate = dateFormatter.format(dateFromUTC(value))
            }
            return formattedDate
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }
    }

    fun getDateFromStringForAddContact(OurDate: String): String {
        val formatter: SimpleDateFormat
        var formattedDate = ""
        try {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK)
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd") //this format changeable
                formattedDate = dateFormatter.format(dateFromUTC(value))
            } else {
                formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd") //this format changeable
                formattedDate = dateFormatter.format(dateFromUTC(value))
            }
            return formattedDate
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }
    }

    fun localToGMT(): String {
        val date = Date()
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        return sdf.format(date)
    }

    fun getDateDaysHoursDifference(date: String): String {
        var days = getDaysDifferenceComming(getLocalDate(date), getCurrentDate()!!)
        var hours = getHoursDifferenceComming(getLocalDate(date), getCurrentDate()!!) - (days * 24)
        var min = getMinutesDifferenceComming(getLocalDate(date), getCurrentDate()!!) - (((days * 24) + hours) * 60)

        if (days > 0) {
            return "$days Days, $hours Hours"
        } else if (hours > 0) {
            return "$hours Hours, $min Minutes"
        } else {
            return "$min Minutes"
        }

    }

    fun getCurrentDate(): String? {
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDate = df.format(Calendar.getInstance().time)
        return currentDate
    }

    fun getDaysDifferenceComming(startDate: String, currentDate: String): Long {
        val diff = stringToDate(startDate)?.time!! - stringToDate(currentDate)?.time!!
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
    }


    fun getMinutesDifferenceComming(startDate: String, currentDate: String): Long {
        val diff = stringToDate(startDate)?.time!! - stringToDate(currentDate)?.time!!
        return TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS)
    }

    fun getHoursDifferenceComming(startDate: String, currentDate: String): Long {
        val diff = stringToDate(startDate)?.time!! - stringToDate(currentDate)?.time!!
        return TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS)
    }

    fun getLocalDateWithNewFormat(OurDate: String): String {
        try {
            if (OurDate.equals("Just Now", ignoreCase = true)) {
                return "Just Now"
            }

            val simpleDateFormat = SimpleDateFormat("MMM dd, yyyy hh:mm a")
            val date = simpleDateFormat.parse(OurDate)
            return simpleDateFormat.format(dateFromUTC(date))
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }

    }

    fun SendDateToDate(OurDate: String): String {
        val formatter: SimpleDateFormat
        var formattedDate = ""
        try {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                formatter = SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.UK)
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
                formattedDate = dateFormatter.format(value)
            } else {
                formatter = SimpleDateFormat("yyyy-MM-dd hh:mm a")
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
                formattedDate = dateFormatter.format(value)
            }
            return formattedDate
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }

    }

    fun SendDateToDateUpdate(OurDate: String): String {
        val formatter: SimpleDateFormat
        var formattedDate = ""
        try {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                formatter = SimpleDateFormat("MMM dd, yyyy - hh:mm a", Locale.UK)
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
                formattedDate = dateFormatter.format(value)
            } else {
                formatter = SimpleDateFormat("MMM dd, yyyy - hh:mm a")
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
                formattedDate = dateFormatter.format(value)
            }
            return formattedDate
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }

    }

    fun convertNotificationTime(OurDate: String): String {
        val formatter: SimpleDateFormat
        var formattedDate = ""
        try {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                formatter = SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.UK)
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
                formattedDate = dateFormatter.format(value)
            } else {
                formatter = SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy")
                val value = formatter.parse(OurDate)
                val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") //this format changeable
                formattedDate = dateFormatter.format(value)
            }
            return formattedDate
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }

    }

    fun dateFromUTC(date: Date): Date {
        return Date(date.time + Calendar.getInstance().timeZone.getOffset(date.time))
    }

    fun dateToUTC(date: Date): Date {
        return Date(date.time - Calendar.getInstance().timeZone.getOffset(date.time))
    }


    fun getBirthdayDate(OurDate: String): String {
        var OurDate = OurDate
        val birthday = OurDate.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        OurDate = birthday[1] + "/" + birthday[2] + "/" + birthday[0]
        return OurDate
    }


    fun GetTimeForLiveEvent(startDate: String): Long {
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val currentDate = df.format(Calendar.getInstance().time)
        return getMillisDiffrenece(startDate, currentDate)
    }

    fun getTimeInLong(startDate: String): Long {
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val d = df.parse(startDate)
        return d.time
    }


    fun displayDateOnlyForBlockUser(startDate: String): String {
        var finalDate: String? = null
//        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//        val currentDate = df.format(Calendar.getInstance().time)
//        getMinutesDiffrenece(startDate, currentDate)
//        val splitedStartDate = startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//        val subSplitedStartDate = splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//        finalDate = Util.getMonthNameByValue(Integer.parseInt(subSplitedStartDate[1])) + " " + subSplitedStartDate[2] + ", " + subSplitedStartDate[0]
        finalDate = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            var formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK)
            val value = formatter.parse(startDate)
            val dateFormatter = SimpleDateFormat("MMM d, yyyy") //this format changeable
            dateFormatter.format(value)
        } else {
            var formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val value = formatter.parse(startDate)
            val dateFormatter = SimpleDateFormat("MMM d, yyyy ") //this format changeable
            dateFormatter.format(value)
        }
        return finalDate
    }
    


    fun displayDateOnly(startDate: String): String {
        var finalDate: String? = null
        if (startDate.equals("Just Now", ignoreCase = true)) {
            finalDate = "Just Now"
        } else {
//            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//            val currentDate = df.format(Calendar.getInstance().time)
//            getMinutesDiffrenece(startDate, currentDate)
//            val splitedStartDate = startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//            val subSplitedStartDate = splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//            val time1 = convertIn12HourFormatWithMinute(startDate)

            finalDate = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                var formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK)
                val value = formatter.parse(startDate)
                val dateFormatter =
                    SimpleDateFormat("EEE, MMM d, yyyy • hh:mm aaa") //this format changeable
                dateFormatter.format(value)
            } else {
                var formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val value = formatter.parse(startDate)
                val dateFormatter =
                    SimpleDateFormat("EEE, MMM d • hh:mm aaa") //this format changeable
                dateFormatter.format(value)
            }
           finalDate = finalDate.replace("am", "AM").replace("pm","PM")
        }
        return finalDate
    }

    fun displayDateOnlyForAnnocument(startDate: String): String {
        var finalDate: String? = null
        if (startDate.equals("Just Now", ignoreCase = true)) {
            finalDate = "Just Now"
        } else {
//            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//            val currentDate = df.format(Calendar.getInstance().time)
//            getMinutesDiffrenece(startDate, currentDate)
//            val splitedStartDate = startDate.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//            val subSplitedStartDate = splitedStartDate[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//            val time1 = convertIn12HourFormatWithMinute(startDate)
//            finalDate = "${Util.getMonthNameByValue(Integer.parseInt(subSplitedStartDate[1]))} ${subSplitedStartDate[2]}, ${subSplitedStartDate[0]} - $time1"
            finalDate = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                var formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK)
                val value = formatter.parse(startDate)
                val dateFormatter =
                    SimpleDateFormat("MMM d, yyyy • hh:mm aaa") //this format changeable
                dateFormatter.format(value)
            } else {
                var formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val value = formatter.parse(startDate)
                val dateFormatter =
                    SimpleDateFormat("MMM d, yyyy • hh:mm aaa") //this format changeable
                dateFormatter.format(value)
            }
        }
        return finalDate!!
    }

    fun chatStreamMainDate(startDate:String):String{
        var finalDate: String? = null
        val splitedDate = startDate.split("T")
        finalDate = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.UK)
            val value = formatter.parse(splitedDate[0])
            val dateFormatter = SimpleDateFormat("EEE, MMM d, yyyy") //this format changeable
            dateFormatter.format(value)
        } else {
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            val value = formatter.parse(splitedDate[0])
            val dateFormatter = SimpleDateFormat("EEE, MMM d, yyyy") //this format changeable
            dateFormatter.format(value)
        }

        return finalDate
    }



}
