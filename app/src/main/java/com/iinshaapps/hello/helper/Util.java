package com.iinshaapps.hello.helper;

/**
 * Created by sidhu on 5/9/2016.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.WINDOW_SERVICE;
import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;


/**
 * General utilities class.
 */
public final class Util {
    /**
     * disable it at release mode
     */
    public static final boolean LOG_ENABLED = true;
    private static final String TAG = Util.class.getName();

    public static void showSnackBar(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
    }

    /**
     * @param content
     * @return
     */
    public static boolean isContentNull(String content) {
        //TODO: Replace this with your own logic
        return content.length() == 0;
    }

    /**
     * @param email
     * @return
     */
    public static boolean isEmailValid(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }


    public static boolean compareDate(String birthDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date convertedBirthDate;
        Date currentDate = Calendar.getInstance().getTime();
        try {
            convertedBirthDate = dateFormat.parse(birthDate);
            int currentYear = currentDate.getYear();
            int birthYear = convertedBirthDate.getYear();
            if (convertedBirthDate.after(currentDate)) {
                return false;
            } else return birthYear != currentYear;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public static int compareBirthDate(String birthDate) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date convertedBirthDate;
        Date currentDate = Calendar.getInstance().getTime();
        convertedBirthDate = dateFormat.parse(birthDate);
        Calendar a_currentYear = getCalendar(convertedBirthDate);
        Calendar birthYear = getCalendar(currentDate);
        int diff = a_currentYear.get(Calendar.YEAR) - birthYear.get(Calendar.YEAR);
        if (a_currentYear.get(MONTH) > birthYear.get(MONTH) ||
                (a_currentYear.get(MONTH) == birthYear.get(MONTH) && a_currentYear.get(DATE) > birthYear.get(DATE))) {
            diff--;
        }
        return diff;
    }

    

    public static void exportDatabse(String name) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (!name.endsWith(".db"))
                name = name + ".db";
            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "com.Hello.hooley" + "//databases//" + name + "";
                String backupDBPath = name;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }

                Log.d("TAG","========exporting database=========");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Bitmap handleSamplingAndRotationBitmap(Uri selectedImage, Activity activity) {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;
        // First decode with inJustDecodeBounds=true to check dimensions

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
//        InputStream imageStream = null;
//        try {
//            imageStream = activity.getContentResolver().openInputStream(selectedImage);
//            BitmapFactory.decodeStream(imageStream, null, options);
//            imageStream.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);
//        // Decode bitmap with inSampleSize set
//        options.inJustDecodeBounds = false;
//        try {
//            imageStream = activity.getContentResolver().openInputStream(selectedImage);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

//        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);
//
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), selectedImage);
        } catch (Exception e) {
            //handle exception
        }
        Bitmap result = Bitmap.createScaledBitmap(bitmap,
                MAX_WIDTH, MAX_HEIGHT, false);

        bitmap = rotateImageIfRequired(result, selectedImage);
        return bitmap;
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) {

        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedImage.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotateImage(img, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotateImage(img, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotateImage(img, 270);
                break;
        }
        return img;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        float height = options.outHeight;
        float width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            float heightRatio = Math.round(height / reqHeight);
            float widthRatio = Math.round(width / reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            if (heightRatio < widthRatio) {
                inSampleSize = (int) heightRatio;
            } else {
                inSampleSize = (int) widthRatio;
            }


            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            float totalPixels = (width * height);

            // Anything more than 2x the requested pixels we'll sample down further
            float totalReqPixelsCap = (reqWidth * reqHeight * 2);

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }


    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate((float) degree);

        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
//        if (img.isRecycled())
//            img.recycle();
        return rotatedImg;
    }

    /**
     * Deletes the fileOrDirectory passed and all its children.
     *
     * @param fileOrDirectory
     * @return
     */
    public static boolean deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        boolean success = deleteFile(fileOrDirectory);

        if (Util.LOG_ENABLED) {
            Log.v(TAG, fileOrDirectory.getPath() + ".delete=" + success);
        }
        return success;
    }

    /**
     * @param file
     * @return success
     */
    public static boolean deleteFile(File file) {
        // because the android 'open failed: EBUSY'
        // http://stackoverflow.com/questions/11539657/open-failed-ebusy-device-or-resource-busy
        final File to = new File(file.getAbsolutePath() + System.currentTimeMillis());
        file.renameTo(to);
        boolean success = to.delete();
        return success;
    }


    /**
     * @param dir
     * @return the complete size of the directory or the file passed in Bytes.
     */
    public static long dirSize(File dir) {
        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory()) {
                    result += dirSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return result; // return the file size
        }
        return 0;
    }


    public static boolean equals(Object objOne, Object objTwo) {
        if (objOne == objTwo) {
            return true;
        }

        return objOne != null && objOne.equals(objTwo);
    }

    /**
     * @param activity
     * @param title
     * @param message
     */
    public static void showToastTitleMessage(Activity activity, String title, String message) {
        int numberSpacesToAdd = (message.length() - title.length()) / 2 + 6;
        String blankSpaces = "";
        blankSpaces = blankSpaces.substring(0, numberSpacesToAdd);

        String stringToShow = blankSpaces + title + "\n" + message;

        Toast.makeText(activity.getApplicationContext(), stringToShow, Toast.LENGTH_LONG).show();
    }


    /**
     * @param activity
     * @param stringToShow
     */
    public static void showToastMessage(Activity activity, String stringToShow) {

        Toast.makeText(activity.getApplicationContext(), stringToShow, Toast.LENGTH_LONG).show();
    }


    /**
     * Prints the string to the log verbose, avoiding the logcat maxlength
     * restriction. The string is not truncated.
     *
     * @param tag
     * @param string
     */
    public static void logv(String tag, String string) {
        if (!Util.LOG_ENABLED) {
            return;
        }
        if (TextUtils.isEmpty(string) || string.length() <= 4000) {
            Log.v(tag, string);
        } else {
            int chunkCount = string.length() / 4000; // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= string.length()) {
                    Log.v(tag, "chunk " + i + " of " + chunkCount + ":" + string.substring(4000 * i));
                } else {
                    Log.v(tag, "chunk " + i + " of " + chunkCount + ":" + string.substring(4000 * i, max));
                }
            }
        }
    }

    public static void moveFile(String path_source, String path_destination) throws IOException {
        File file_Source = new File(path_source);
        File file_Destination = new File(path_destination);

        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(file_Source).getChannel();
            destination = new FileOutputStream(file_Destination).getChannel();

            long count = 0;
            long size = source.size();
            while ((count += destination.transferFrom(source, count, size - count)) < size) ;
            file_Source.delete();
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }


    /**
     * Goes to the activity indicated with 'clazz', from the activity
     * 'fromActiviy' and erases navigation history.
     *
     * @param fromActivity
     * @param clazz
     */
    public static void gotoActivityClearTop(Activity fromActivity, Class clazz, Bundle extras) {
        Intent intent = new Intent();
        if (extras != null)
            intent.putExtras(extras);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setClass(fromActivity, clazz);
        fromActivity.startActivity(intent);
        fromActivity.finish();
    }

    public static void removeFragmentFromStack(Context context, Fragment fragment) {
        if (fragment != null) {
            FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(fragment);
            trans.commit();
            manager.popBackStack();
        }
    }

    public static void removeAllFragmentFromStack(Context context) {
        {
            FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
            int count = manager.getBackStackEntryCount();
            for (int index = 0; index < count; index++)
                manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    /**
     * Goes to the activity indicated with 'clazz', from the activity
     * 'fromActiviy' . and puts bundle in intent if not null
     *
     * @param fromActivity
     * @param clazz
     * @param extras
     */
    public static void gotoActivity(Activity fromActivity, Class clazz, Bundle extras) {
        Intent intent = new Intent();
        if (extras != null)
            intent.putExtras(extras);
        intent.setClass(fromActivity, clazz);
        fromActivity.startActivity(intent);

    }

    /**
     * Restore the saved application state if needed.
     *
     * @param context
     * @param savedInstanceState
     */
    public static void restoreSavedApplicationState(Context context, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Restore value of members from saved state
        }
    }

    public static long calculateTimeDiff(String date) {
        long milliSec = 0;
        Date checkInTime = new Date();
        // SimpleDateFormat newformat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy",
                Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date msgDate = sdf.parse(date);
            milliSec = msgDate.getTime();
            checkInTime = sdf.parse(sdf.format(checkInTime));
        } catch (ParseException e) {
            Log.e("DATE Parse Exception", e.getMessage());
        }
        long currentMilliSec = checkInTime.getTime();
        long diff = (currentMilliSec - milliSec);

        return TimeUnit.MILLISECONDS.toDays(diff);
    }

    /**
     * Show loading with shimmer effect like facebook
     * @param shimmerView
     */
  /*  public static void applyShimmerEffectOnView(ShimmerFrameLayout shimmerView){
        shimmerView.setAngle(ShimmerFrameLayout.MaskAngle.CW_90);
        shimmerView.setDuration(1000);
        shimmerView.startShimmerAnimation();
    }*/


    /**
     * Encapsulated behavior for dismissing Dialogs, because of several android problems
     * related.
     */
    public static void dismissDialog(Dialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (IllegalArgumentException e) // even sometimes happens?: http://stackoverflow.com/questions/12533677/illegalargumentexception-when-dismissing-dialog-in-async-task
            {
                Log.i(TAG, "Error when attempting to dismiss dialog, it is an android problem.", e);
            }
        }
    }

    public static void showkeyPad(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    /**
     * Pass a view to method and it will hide the keyboard if opened.
     *
     * @param view
     */
    public static void hidekeyPad(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(),
                0);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Fragment getCurrentFragment(Context context) {
        FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        Fragment currentFragment = null;
        if (fragmentManager.getBackStackEntryCount() > 0) {
            String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
            currentFragment = fragmentManager
                    .findFragmentByTag(fragmentTag);
        }
        return currentFragment;
    }

    public static int getNavigationBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static int getDeviceWidth(Context context) {
        int _widthPixels = context.getResources().getDisplayMetrics().widthPixels;
        return _widthPixels;
    }

    public static int getDeviceHeight(Context context) {
        int _heigthPixels = 0;
        try {
            _heigthPixels = context.getResources().getDisplayMetrics().heightPixels;
        } catch (Exception e) {

        }
        return _heigthPixels;
    }

    public static void getKeyHash(Context context) {
        /* HASH KEY CALCULATOR */

        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.Hello.hooley", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64
                        .encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);

            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("MY KEY HASH:", "NAME NOT FOUND");
        } catch (NoSuchAlgorithmException e) {
            Log.e("MY KEY HASH:", "NO SUCH ALGORITHM");
        }

    }

    public static void hideAView(final View layout, int afterMillisecs) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                layout.animate()
                        .alpha(0.0f)
                        .setDuration(500)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                layout.setVisibility(View.GONE);
                            }
                        });
            }
        }, 4000);
    }

    /**
     * In this method we are removing character(s) [(,),-,*,/,#,.,space,N] from
     * number
     *
     * @param number String
     * @return String number after excluding all the above character(s) if
     * exists
     */

    public static String excludeDashesFromNumber(String number) {
        if (number != null && !TextUtils.isEmpty(number)) {
            StringBuffer result = new StringBuffer();
            for (int i = 0; i < number.length(); i++) {
                if (number.charAt(i) == '(' || number.charAt(i) == ')' || number.charAt(i) == '-' || number.charAt(i) == '*'
                        || number.charAt(i) == '/' || number.charAt(i) == '#' || number.charAt(i) == '.' || number.charAt(i) == ' '
                        || number.charAt(i) == 'N') {
                } else {
                    result.append(number.charAt(i));
                }
            }
            return result.toString();
        } else {
            return "";
        }

    }

    public static String getMonthNameByValue(int position) {
        switch (position - 1) {
            case Calendar.JANUARY:
                return "Jan";
            case Calendar.FEBRUARY:
                return "Feb";
            case Calendar.MARCH:
                return "Mar";
            case Calendar.APRIL:
                return "Apr";
            case Calendar.MAY:
                return "May";
            case Calendar.JUNE:
                return "Jun";
            case Calendar.JULY:
                return "Jul";
            case Calendar.AUGUST:
                return "Aug";
            case Calendar.SEPTEMBER:
                return "Sep";
            case Calendar.OCTOBER:
                return "Oct";
            case Calendar.NOVEMBER:
                return "Nov";
            case Calendar.DECEMBER:
                return "Dec";
            default:
                return "";
        }
    }

    public static boolean checkIfName(String eventName) {
        Pattern pattern = Pattern.compile(".*[a-zA-Z]+.*");
        Matcher matcher = pattern.matcher(eventName);
        return matcher.matches();
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
        return isInBackground;
    }

    public static String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("citystatecountry.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                json = new String(buffer, StandardCharsets.UTF_8);
            }


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public static void adjustFontScale(Configuration configuration, Context context) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        context.getResources().updateConfiguration(configuration, metrics);
    }

    public static Bitmap createImageFromString(Context context, String qrString) {
        Bitmap bm = makeQRBitmap(qrString);
        if (bm != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return bm;
        } else {
            Toast.makeText(context, "BM is null", Toast.LENGTH_SHORT).show();
            return null;
        }

    }

    public static Bitmap makeQRBitmap(String qrCodeUrl) {
        Bitmap bitmap = null;
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(qrCodeUrl, BarcodeFormat.QR_CODE, 200, 200);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static boolean hasSoftKeys(Activity activity) {
        Display d = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        int realHeight = realDisplayMetrics.heightPixels;
        int realWidth = realDisplayMetrics.widthPixels;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        d.getMetrics(displayMetrics);
        int displayHeight = displayMetrics.heightPixels;
        int displayWidth = displayMetrics.widthPixels;
        return (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
    }

}

