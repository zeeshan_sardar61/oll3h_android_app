package com.iinshaapps.hello.helper

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.FragmentCropImageBinding

class CropActivity : AppCompatActivity(), View.OnClickListener {

    private var finalBitmap: Bitmap? = null
    lateinit var binding: FragmentCropImageBinding
    private var cropImageUri: Uri? = null
    var filePath: String? = null
    var isLogo = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_crop_image)
        isLogo = intent.getBooleanExtra("isSquare", false)
        filePath = intent.getStringExtra("path")
        setListener()
        setData()
    }

    private fun setListener() {
        binding.tvNext.setOnClickListener(this)
        binding.tvCancel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvNext -> {
                if(isLogo){
                    cropImageUri = this.convertBitmapToUri(getBitmap(binding.rlCropPhoto)!!)
                }else{
                    cropImageUri = this.convertBitmapToUri(getBitmap(binding.rlCropPhoto1)!!)
                }

                val returnIntent = Intent()
                returnIntent.putExtra("result", cropImageUri.toString())
                setResult(RESULT_OK, returnIntent)
                finish()
            }

            R.id.tvCancel -> {
                onBackPressed()
            }
        }
    }

    private fun setData() {
        val options: BitmapFactory.Options
        var bitmap: Bitmap? = null

        try {
            bitmap = BitmapFactory.decodeFile(filePath)
        } catch (e: OutOfMemoryError) {
            try {
                options = BitmapFactory.Options()
                options.inSampleSize = 2
                bitmap = BitmapFactory.decodeFile(filePath, options)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }


        try {
            val exif = ExifInterface(filePath!!)
            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90f)
            } else if (orientation == 3) {
                matrix.postRotate(180f)
            } else if (orientation == 8) {
                matrix.postRotate(270f)
            }
            bitmap = Bitmap.createBitmap(bitmap!!, 0, 0, bitmap.width, bitmap.height, matrix, true) // rotating bitmap
        } catch (e: Exception) {
        }

        try {
            bitmap = bitmap!!.copy(Bitmap.Config.ARGB_8888, true)
            finalBitmap = bitmap
            if(!isLogo){
                binding.rlSquarePost.visibility = View.VISIBLE
                binding.rlLogo.visibility = View.GONE
                binding.ivPost1.setImageBitmap(bitmap)
                binding.ivPostBackground.setImageBitmap(bitmap)
                binding.ivPostBackgroundMid1.setImageBitmap(bitmap)
            }else {
                binding.rlSquarePost.visibility = View.GONE
                binding.rlLogo.visibility = View.VISIBLE
                binding.ivPost.setImageBitmap(bitmap)
//                binding.ivPostBackground.setImageBitmap(bitmap)
//                binding.ivPostBackgroundMid.setImageBitmap(bitmap)
            }

        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

}