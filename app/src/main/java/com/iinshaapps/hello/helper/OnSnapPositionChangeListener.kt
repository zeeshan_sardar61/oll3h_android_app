package com.iinshaapps.hello.helper

interface OnSnapPositionChangeListener {

    fun onSnapPositionChange(position: Int)

}