package com.iinshaapps.hello.fragment

import android.Manifest
import android.app.Dialog
import android.content.ContentProviderOperation
import android.content.ContentValues
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Environment
import android.provider.ContactsContract
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.ItemDetailsAdapter
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.DialogRestoreConfirmationBinding
import com.iinshaapps.hello.databinding.ItemsDetailFragmentBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.DateUtils
import com.iinshaapps.hello.helper.MessageEvent
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.ItemDetailModel
import com.iinshaapps.hello.model.OtherItemsModel
import com.iinshaapps.hello.model.requestmodel.AddContactRequestModel
import com.iinshaapps.hello.model.requestmodel.DeleteContactRequestModel
import com.iinshaapps.hello.model.response.*
import org.greenrobot.eventbus.EventBus
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ItemsDetailFragment(var cardId: String, var isPersonal: Boolean, var isOtherUser: Boolean, var otherItemsModel: OtherItemsModel?, var bitmap: Bitmap? = null, var bitmapAvatar: Bitmap? = null, var notes: String = "") : BaseFragment(), IGenericCallBack, ItemsEditDetailFragment.UpdateItemDetails {

    private val CONTACTS_PERMISSION = 1
    private val STORAGE_PERMISSION = 2

    lateinit var binding: ItemsDetailFragmentBinding
    var mList = ArrayList<ItemDetailModel>()
    var otherItemsList = ArrayList<ItemDetailModel>()
    lateinit var adapter: ItemDetailsAdapter
    lateinit var otherItemsAdapter: ItemDetailsAdapter
    lateinit var model: CardInnerDetailsResponse
    lateinit var dialog: Dialog
    var cardData: CardData? = null
    var isFirstTime = true
    val opList = ArrayList<ContentProviderOperation>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.items_detail_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initOtherItems()
        getCardInnerDetails()
    }

    private fun getCardInnerDetails() {
        val call = Hello.apiService.getContactInnerDetails(cardId, isPersonal)
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.GET_CARDS_DETAILS_URL, true, this)
    }

    private fun initViews() {

        if (isOtherUser) {
            binding.tvEdit.visibility = View.GONE
            binding.llDirectory.visibility = View.VISIBLE
            binding.llPhotoAlbum.visibility = View.VISIBLE
            binding.llRemoveCard.visibility = View.VISIBLE

            binding.llDirectory.setOnClickListener {
                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    executeAddToContacts()
                }
                else {
                    requestPermissions(arrayOf(Manifest.permission.WRITE_CONTACTS), CONTACTS_PERMISSION)
                }
            }

            binding.llPhotoAlbum.setOnClickListener {
                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    if (bitmap != null) {
                        saveToGallery("photo_" + System.currentTimeMillis() + ".png", "", "Contact Photo Saved", Bitmap.CompressFormat.PNG, 50)
                    }
                }
                else {
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION)
                }
            }

            binding.llRemoveCard.setOnClickListener {
                showDialog()
            }
        }
        else {
            binding.tvEdit.setOnClickListener {
                if(cardData != null) {
                    addFragment(MainActivity.mActivity, R.id.container, ItemsEditDetailFragment(cardData!!, isPersonal, this), "ItemsEditDetailFragment")
                }
            }
            binding.btnAddData.setOnClickListener {
                if(cardData != null) {
                    addFragment(MainActivity.mActivity, R.id.container, ItemsEditDetailFragment(cardData!!, isPersonal, this), "ItemsEditDetailFragment")
                }
            }
        }

        if (isPersonal) {
            binding.tvTitle.text = "Personal Details"
        }
        else {
            binding.tvTitle.text = "Business Details"
            binding.btnAddData.text = "Add Business Details"
        }

        binding.ivBack.setOnClickListener {
            EventBus.getDefault().post(MessageEvent("Fire"))
            MainActivity.mActivity.onBackPressed()
        }

        if(isOtherUser) initAddToContacts()

        addPhoto()
        addNotes()
    }

    private fun showDialog() {

        dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogRestoreConfirmationBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.mActivity), R.layout.dialog_restore_confirmation, null, false)
        dialog.setContentView(binding.root)

        binding.tvTitle.text = "Remove Card"
        binding.tvDescription.text = "After continue, this card will be deleted. Are you sure, you want to continue."

        binding.tvContinue.setOnClickListener {
            removeCard()
        }

        binding.tvNo.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun removeCard() {

        val call = Hello.apiService.removeCard(DeleteContactRequestModel(cardId, isPersonal, true, null, null, null))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.REMOVE_CARD_URL, true, this)
    }

    private fun initOtherItems() {

        if (otherItemsModel != null) {
            binding.rvOtherItems.visibility = View.VISIBLE
            if (otherItemsModel!!.businessName != null) {
                otherItemsList.add(ItemDetailModel("Business Name", otherItemsModel!!.businessName!!))
                addBusinessName(otherItemsModel!!.businessName!!)
            }
            if (otherItemsModel!!.name != null) {
                otherItemsList.add(ItemDetailModel("Name", otherItemsModel!!.name!!))
                addFullName(otherItemsModel!!.name!!)
            }
            if (otherItemsModel!!.phone != null) {
                otherItemsList.add(ItemDetailModel("Phone", otherItemsModel!!.countryCode + otherItemsModel!!.phone!!))
                addPhone(otherItemsModel!!.countryCode + otherItemsModel!!.phone!!)
            }
            if (otherItemsModel!!.email != null) {
                otherItemsList.add(ItemDetailModel("Email", otherItemsModel!!.email!!))
                addEmail(otherItemsModel!!.email!!)
            }
        }
        else {
            binding.rvOtherItems.visibility = View.GONE
        }

        binding.rvOtherItems.layoutManager = LinearLayoutManager(MainActivity.mActivity)
        otherItemsAdapter = ItemDetailsAdapter(otherItemsList)
        binding.rvOtherItems.adapter = otherItemsAdapter
    }

    private fun setRecyclerView() {
        binding.rvItems.layoutManager = LinearLayoutManager(MainActivity.mActivity)
        adapter = ItemDetailsAdapter(mList)
        binding.rvItems.adapter = adapter
    }

    override fun success(apiName: String, response: Any?) {
        when (apiName) {
            Constants.GET_CARDS_DETAILS_URL -> {
                model = response as CardInnerDetailsResponse
                if (model.isError!!) {
                    MainActivity.mActivity.showToastMessage(model.message!!)
                }
                else {
                    if (isPersonal) {
                        if (model.data!!.personalDetail != null) {
                            cardData = model.data!!
                            if (checkDataExistForPersonal(model.data!!.personalDetail!!)) {
                                binding.rvItems.visibility = View.VISIBLE
                                if (!isOtherUser) binding.btnAddData.visibility = View.GONE
                                setPersonalData(model.data!!.personalDetail!!)
                            }
                            else {
                                binding.rvItems.visibility = View.GONE
                                if (!isOtherUser) {
                                    binding.tvEdit.text = "Add"
                                    binding.btnAddData.visibility = View.VISIBLE
                                    if(isFirstTime){
                                        if(cardData != null) {
                                            addFragment(MainActivity.mActivity, R.id.container, ItemsEditDetailFragment(cardData!!, isPersonal, this), "ItemsEditDetailFragment")
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if (model.data!!.businessDetail != null) {
                            cardData = model.data!!
                            if (checkDataExistForBusiness(model.data!!.businessDetail!!)) {
                                binding.rvItems.visibility = View.VISIBLE
                                if (!isOtherUser) binding.btnAddData.visibility = View.GONE
                                setBusinessData(model.data!!.businessDetail!!)
                            }
                            else {
                                binding.rvItems.visibility = View.GONE
                                if (!isOtherUser) {
                                    binding.tvEdit.text = "Add"
                                    binding.btnAddData.visibility = View.VISIBLE
                                    if(isFirstTime){
                                        if(cardData != null) {
                                            addFragment(MainActivity.mActivity, R.id.container, ItemsEditDetailFragment(cardData!!, isPersonal, this), "ItemsEditDetailFragment")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Constants.REMOVE_CARD_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    EventBus.getDefault().post(MessageEvent("api"))
                    dialog.dismiss()
                    MainActivity.mActivity.onBackPressed()
                    MainActivity.mActivity.onBackPressed()
                }
            }
        }
    }


    private fun setPersonalData(personalDetail: PersonalDetail) {
        personalDetail.apply {
            mList.clear()
            if (isOtherUser) {
                if (emails!!.isNotEmpty()) {
                    for (i in personalDetail.emails!!.indices) {
                        if (emails!![i]!!.isPublic!!){
                            mList.add(ItemDetailModel("Email", emails!![i]!!.email!!))
                            addEmail(emails!![i]!!.email!!)
                        }
                    }
                }
                if (isDobPublic!!) {
                    if (dob != "0001-01-01T00:00:00") {
                        mList.add(ItemDetailModel("Date of Birth", DateUtils.getDateFromString(dob!!)))
                        addDOB(DateUtils.getDateFromStringForAddContact(dob!!))
                    }
                }
//                if (isAddressPublic!!) {
//                    if (address!!.isNotEmpty()) {
//                        mList.add(ItemDetailModel("Address", address!!))
//                        addAddress(address!!)
//                    }
//                }
                var country1 = ""; var state1 = ""; var city1 = ""; var street1 = ""; var postcode1 = ""
                if (isCountryPublic!!) {
                    if (!country.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Country", country!!))
                        country1 = country.toString()
                    }
                }
                if (isStatePublic!!) {
                    if (!state.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("State", state!!))
                        state1 = state.toString()
                    }
                }
                if (isCityPublic!!) {
                    if (!city.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("City", city!!))
                        city1 = city.toString()
                    }
                }
                if (isStreetPublic!!) {
                    if (!street.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Street", street!!))
                        street1 = street.toString()
                    }
                }
                if (isPostCodePublic!!) {
                    if (!postCode.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Postcode", postCode!!))
                        postcode1 = postCode.toString()
                    }
                }

                if(country1.isNotEmpty() || state1.isNotEmpty() || city1.isNotEmpty() || street1.isNotEmpty() || postcode1.isNotEmpty()) {
                    addAddress(street1, city1, state1, postcode1, country1)
                }

                if (isSchoolPublic!!) {
                    if (!schoolName.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("School Name", schoolName!!))
                    }
                }
                if (isleveEducautionPublic!!) {
                    if (!levelEducation.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Level of Education", levelEducation!!))
                    }
                }
                if (isMajorPublic!!) {
                    if (!major.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Major", major!!))
                    }
                }
                if (isMinorPublic!!) {
                    if (!minor.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Minor", minor!!))
                    }
                }
                if (isClassificationPublic!!) {
                    if (!classification.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Classification", classification!!))
                    }
                }
                if (isAboutPublic!!) {
                    if (!aboutYou.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("About You", aboutYou!!))
                    }
                }
            }
            else {
                if (emails!!.isNotEmpty()) {
                    for (i in personalDetail.emails!!.indices) {
                        mList.add(ItemDetailModel("Email", emails!![i]!!.email!!))
                    }
                }
                if (dob != "0001-01-01T00:00:00") {
                    mList.add(ItemDetailModel("Date of Birth", DateUtils.getDateFromString(dob!!)))
                }
//                if (address!!.isNotEmpty()) {
//                    mList.add(ItemDetailModel("Address", address!!))
//                }
                if (!country.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Country", country!!))
                }
                if (!state.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("State", state!!))
                }
                if (!city.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("City", city!!))
                }
                if (!street.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Street", street!!))
                }
                if (!postCode.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Postcode", postCode!!))
                }
                if (!schoolName.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("School Name", schoolName!!))
                }
                if (!levelEducation.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Level of Education", levelEducation!!))
                }
                if (!major.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Major", major!!))
                }
                if (!minor.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Minor", minor!!))
                }
                if (!classification.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Classification", classification!!))
                }
                if (!aboutYou.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("About You", aboutYou!!))
                }
            }
            if (!isOtherUser) {
                if (mList.isNotEmpty()) {
                    binding.btnAddData.visibility = View.GONE
                }
                else {
                    binding.btnAddData.visibility = View.VISIBLE

                    if(isFirstTime && !isOtherUser){
                        if(cardData != null) {
                            addFragment(MainActivity.mActivity, R.id.container, ItemsEditDetailFragment(cardData!!, isPersonal, this@ItemsDetailFragment), "ItemsEditDetailFragment")
                        }
                    }
                }
            }
            setRecyclerView()
        }
    }

    private fun setBusinessData(businessDetail: BusinessDetail) {
        businessDetail.apply {
            mList.clear()
            if (isOtherUser) {
                if (emails!!.isNotEmpty()) {
                    for (i in businessDetail.emails!!.indices) {
                        if (emails!![i]!!.isPublic!!) {
                            mList.add(ItemDetailModel("Email", emails!![i]!!.email!!))
                            addEmail(emails!![i]!!.email!!)
                        }
                    }
                }
//                if (isBusinessAddressPublic!!) {
//                    if (businessAddress!!.isNotEmpty()) {
//                        mList.add(ItemDetailModel("Business Address", businessAddress!!))
//                        addAddress(businessAddress!!)
//                    }
//                }
                var country1 = ""; var state1 = ""; var city1 = ""; var street1 = ""; var postcode1 = ""
                if (isCountryPublic!!) {
                    if (!country.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Country", country!!))
                        country1 = country.toString()
                    }
                }
                if (isStatePublic!!) {
                    if (!state.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("State", state!!))
                        state1 = state.toString()
                    }
                }
                if (isCityPublic!!) {
                    if (!city.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("City", city!!))
                        city1 = city.toString()
                    }
                }
                if (isStreetPublic!!) {
                    if (!street.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Street", street!!))
                        street1 = street.toString()
                    }
                }
                if (isPostCodePublic!!) {
                    if (!postCode.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Postcode", postCode!!))
                        postcode1 = postCode.toString()
                    }
                }

                if(country1.isNotEmpty() || state1.isNotEmpty() || city1.isNotEmpty() || street1.isNotEmpty() || postcode1.isNotEmpty()) {
                    addAddress(street1, city1, state1, postcode1, country1)
                }

                if (isFaxNumberPublic!!) {
                    if (!faxNumber.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Fax Number", faxNumber!!))
                    }
                }
                if (isWebsitePublic!!) {
                    if (!website.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Company Website URL", website!!))
                        addWebsite(website!!)
                    }
                }
                if (isJobTitlePublic!!) {
                    if (!jobTitle.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Occupation/Job Title", jobTitle!!))
                        addJobOccupation(jobTitle!!)
                    }
                }
                if (isUniversityPublic!!) {
                    if (!university.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("College/University", university!!))
                    }
                }
                if (isDegreePublic!!) {
                    if (!degree.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Degree(s)", degree!!))
                    }
                }
                if (isBioPublic!!) {
                    if (!bio.isNullOrEmpty()) {
                        mList.add(ItemDetailModel("Biography", bio!!))
                    }
                }
            }
            else {
                if (emails!!.isNotEmpty()) {
                    for (i in businessDetail.emails!!.indices) {
                        mList.add(ItemDetailModel("Email", emails!![i]!!.email!!))
                    }
                }
//                if (businessAddress!!.isNotEmpty()) {
//                    mList.add(ItemDetailModel("Business Address", businessAddress!!))
//                }
                if (!country.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Country", country!!))
                }
                if (!state.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("State", state!!))
                }
                if (!city.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("City", city!!))
                }
                if (!street.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Street", street!!))
                }
                if (!postCode.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Postcode", postCode!!))
                }
                if (!faxNumber.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Fax Number", faxNumber!!))
                }
                if (!website.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Company Website URL", website!!))
                }
                if (!jobTitle.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Occupation/Job Title", jobTitle!!))
                }
                if (!university.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("College/University", university!!))
                }
                if (!degree.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Degree(s)", degree!!))
                }
                if (!bio.isNullOrEmpty()) {
                    mList.add(ItemDetailModel("Biography", bio!!))
                }
            }
            if (!isOtherUser) {
                if (mList.isNotEmpty()) {
                    binding.btnAddData.visibility = View.GONE
                }
                else {
                    binding.btnAddData.visibility = View.VISIBLE

                    if(isFirstTime && !isOtherUser){
                        if(cardData != null) {
                            addFragment(MainActivity.mActivity, R.id.container, ItemsEditDetailFragment(cardData!!, isPersonal, this@ItemsDetailFragment), "ItemsEditDetailFragment")
                        }
                    }
                }
            }
            setRecyclerView()
        }
    }

    private fun checkDataExistForPersonal(models: PersonalDetail): Boolean {
        models.apply {
            if (emails!!.isEmpty() && dob == "0001-01-01T00:00:00"
//                && address == null
                && country == null
                && state == null
                && city == null
                && street == null
                && postCode == null
                && schoolName == null
                && levelEducation == null
                && major == null
                && minor == null
                && classification == null
                && aboutYou == null) {
                return false
            }
        }
        return true
    }

    private fun checkDataExistForBusiness(models: BusinessDetail): Boolean {
        models.apply {
            if (emails!!.isEmpty()
//                && businessAddress == null
                && country == null
                && state == null
                && city == null
                && street == null
                && postCode == null
                && faxNumber == null
                && website == null
                && jobTitle == null
                && university == null
                && degree == null
                && bio == null) {
                return false
            }
        }
        return true
    }

    override fun failure(apiName: String, message: String?) {
        MainActivity.mActivity.showToastMessage(message!!)
    }

    fun saveToGallery(fileName: String, subFolderPath: String, fileDescription: String?, format: Bitmap.CompressFormat?, quality: Int): Boolean {

        var fileName = fileName
        var quality = quality
        if (quality < 0 || quality > 100) quality = 50
        val currentTime = System.currentTimeMillis()
        val extBaseDir = Environment.getExternalStorageDirectory()
        val file = File(extBaseDir.absolutePath + "/DCIM/" + subFolderPath)
        if (!file.exists()) {
            if (!file.mkdirs()) {
                return false
            }
        }
        var mimeType = ""
        when (format) {
            Bitmap.CompressFormat.PNG -> {
                mimeType = "image/png"
                if (!fileName.endsWith(".png")) fileName += ".png"
            }
            Bitmap.CompressFormat.WEBP -> {
                mimeType = "image/webp"
                if (!fileName.endsWith(".webp")) fileName += ".webp"
            }
            Bitmap.CompressFormat.JPEG -> {
                mimeType = "image/jpeg"
                if (!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))) fileName += ".jpg"
            }
            else -> {
                mimeType = "image/jpeg"
                if (!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))) fileName += ".jpg"
            }
        }
        val filePath = file.absolutePath + "/" + fileName
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(filePath)
            bitmap?.compress(format, quality, out)
            out.flush()
            out.close()
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        val size = File(filePath).length()
        val values = ContentValues(8)

        // store the details
        values.put(MediaStore.Images.Media.TITLE, fileName)
        values.put(MediaStore.Images.Media.DISPLAY_NAME, fileName)
        values.put(MediaStore.Images.Media.DATE_ADDED, currentTime)
        values.put(MediaStore.Images.Media.MIME_TYPE, mimeType)
        values.put(MediaStore.Images.Media.DESCRIPTION, fileDescription)
        values.put(MediaStore.Images.Media.ORIENTATION, 0)
        values.put(MediaStore.Images.Media.DATA, filePath)
        values.put(MediaStore.Images.Media.SIZE, size)

        MainActivity.mActivity.showToastMessage("Photo saved")

        return MainActivity.mActivity.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values) != null
    }

    private fun initAddToContacts() {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
            .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
            .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
            .build())
    }

    private fun addPhoto() {

        val stream = ByteArrayOutputStream()
        if (bitmapAvatar != null) {
            bitmapAvatar!!.compress(Bitmap.CompressFormat.PNG, 75, stream)
            opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray()).build())
            try {
                stream.flush()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun addBusinessName(businessName: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, businessName)
            .build())
    }

    private fun addFullName(name: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, name)
            .build())
    }

    private fun addPhone(phone: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone)
            .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
            .build())
    }

    private fun addEmail(email: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Email.DATA, email)
            .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
            .build())
    }

    private fun addAddress(street: String, city: String, state: String, postcode: String, country: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.StructuredPostal.STREET, street)
            .withValue(ContactsContract.CommonDataKinds.StructuredPostal.CITY, city)
            .withValue(ContactsContract.CommonDataKinds.StructuredPostal.REGION, state)
            .withValue(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE, postcode)
            .withValue(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, country)
            .withValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE, ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK)
            .build())
    }

    private fun addDOB(dob: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Event.START_DATE, dob)
            .withValue(ContactsContract.CommonDataKinds.Event.TYPE, ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY)
            .build())
    }

    private fun addWebsite(website: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Website.DATA, website)
            .build())
    }

    private fun addJobOccupation(job: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, job)
            .build())
    }

    private fun addNotes() {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Note.NOTE, notes)
            .build())
    }

    private fun executeAddToContacts() {

        try {
            val results = MainActivity.mActivity.contentResolver.applyBatch(ContactsContract.AUTHORITY, opList)
            Log.d("TAGO", "try: ")
            MainActivity.mActivity.showToastMessage("Contact added")
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("TAGO", "catch: ${e.message}")
        }
    }

    override fun onUpdateCall() {
        getCardInnerDetails()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CONTACTS_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    executeAddToContacts()
                }
                else {
                    MainActivity.mActivity.showToastMessage("Please allow permission to write contacts.")
                }
            }
            STORAGE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (bitmap != null) {
                        saveToGallery("photo_" + System.currentTimeMillis() + ".png", "", "Contact Photo Saved", Bitmap.CompressFormat.PNG, 50)
                    }
                }
                else {
                    MainActivity.mActivity.showToastMessage("Please allow permission to save to photo album.")
                }
            }
        }
    }
}