package com.iinshaapps.hello.fragment

import android.annotation.SuppressLint
import android.appwidget.AppWidgetManager
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.ButtonsWidget
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.QRWidget
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.*
import com.iinshaapps.hello.databinding.FragmentSettingsBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.MessageEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class SettingsFragment : BaseFragment() {

    lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)
        manageNotification()
        return binding.root
    }

    private fun manageNotification() {
        if(Hello.db.getBoolean(Constants.HAVE_NOTIFICATION, false)){
            binding.ivNotification.visibility = View.VISIBLE
        }else {
            binding.ivNotification.visibility = View.GONE
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rlChangePhone.setOnClickListener {
            val i = Intent(MainActivity.mActivity, ChangeNumberActivity::class.java)
            MainActivity.mActivity.startActivity(i)
        }

        binding.rlStore.setOnClickListener {
            addFragment(MainActivity.mActivity, R.id.container, QRCodeFragment(), "QRCodeFragment")
        }

        binding.rlPrivacyPolicy.setOnClickListener {
            addFragment(MainActivity.mActivity, R.id.container, PrivacyPolicyFragment(), "PrivacyPolicyFragment")
        }

        binding.rlFeedBack.setOnClickListener {
            composeEmail(arrayOf("shussein@lakesidemri.com"), "Feedback")
        }

        binding.rlRateOutApp.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context?.packageName)))
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + context?.packageName)))
            }
        }

        binding.rlContactRequest.setOnClickListener {

            addFragment(MainActivity.mActivity, R.id.container, ContactRequestFragment(), "ContactRequestFragment")
        }

        binding.rlLogout.setOnClickListener {
            val fcmtoken = Hello.db.getString(Constants.FCM_TOKEN)
            Hello.db.clear()
            Hello.db.putString(Constants.FCM_TOKEN, fcmtoken!!)
            val intent = Intent(MainActivity.mActivity, QRWidget::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            val ids: IntArray = AppWidgetManager.getInstance(MainActivity.mActivity).getAppWidgetIds(ComponentName(MainActivity.mActivity, QRWidget::class.java))
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            MainActivity.mActivity.sendBroadcast(intent)

            val intent1 = Intent(MainActivity.mActivity, ButtonsWidget::class.java)
            intent1.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            val ids1: IntArray = AppWidgetManager.getInstance(MainActivity.mActivity).getAppWidgetIds(ComponentName(MainActivity.mActivity, ButtonsWidget::class.java))
            intent1.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids1)
            MainActivity.mActivity.sendBroadcast(intent1)

            val i = Intent(MainActivity.mActivity, Splash::class.java)
            startActivity(i)
            MainActivity.mActivity.finish()
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    fun composeEmail(addresses: Array<String?>?, subject: String?) {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:") // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        MainActivity.mActivity.startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "notify") {
            manageNotification()
        }
    }
}