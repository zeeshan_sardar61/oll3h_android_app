package com.iinshaapps.hello.fragment

import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.ContactRequestAdapter
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.DialogRestoreConfirmationBinding
import com.iinshaapps.hello.databinding.FragmentContactRequestBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.MessageEvent
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.requestmodel.AddContactRequestModel
import com.iinshaapps.hello.model.requestmodel.DeleteContactRequestModel
import com.iinshaapps.hello.model.response.ContactRequestModel
import com.iinshaapps.hello.model.response.GetContactRequestResponse
import com.iinshaapps.hello.model.response.RecoveryCodeResponse
import org.greenrobot.eventbus.EventBus

class ContactRequestFragment : BaseFragment(), ContactRequestAdapter.ItemClicker, IGenericCallBack, NonAppUserRequestFragment.ApiCall, PersonalCardDetailFragment.ApiCall, BusinessCardDetailFragment.ApiCall {

    private lateinit var binding: FragmentContactRequestBinding
    var mList = ArrayList<ContactRequestModel>()
    lateinit var adapter: ContactRequestAdapter
    lateinit var dialog: Dialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_request, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        callGetContactRequestApi()
    }

    private fun initViews() {

        binding.ivBack.setOnClickListener {
            MainActivity.mActivity.onBackPressed()
        }
    }

    private fun callGetContactRequestApi() {
        val call = Hello.apiService.getContactRequest()
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.GET_CONTACT_REQUEST_URL, true, this)
    }

    private fun setRecyclerView() {

        binding.rvContactRequest.layoutManager = LinearLayoutManager(MainActivity.mActivity)
        adapter = ContactRequestAdapter(mList, this)
        binding.rvContactRequest.adapter = adapter
    }

    override fun onItemClick(position: Int) {

        if (mList[position].isAppUser) {
            if (mList[position].isPersonal) {
                addFragment(MainActivity.mActivity, R.id.container, PersonalCardDetailFragment(mList[position].cardId!!, true, mList[position].id!!, this), "PersonalCardDetailFragment")
            }
            else {
                addFragment(MainActivity.mActivity, R.id.container, BusinessCardDetailFragment(mList[position].cardId!!, true, mList[position].id!!, this), "BusinessCardDetailFragment")
            }
        }
        else {
            addFragment(MainActivity.mActivity, R.id.container, NonAppUserRequestFragment(mList[position], this, true), "ContactRequestFragment")
        }
    }

    override fun onDeleteClick(position: Int) {
        showDialog(mList[position].id!!, mList[position].isPersonal, mList[position].isAppUser)
    }

    private fun showDialog(cardId: String, isPersonal: Boolean, isAppUser: Boolean) {

        dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogRestoreConfirmationBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.mActivity), R.layout.dialog_restore_confirmation, null, false)
        dialog.setContentView(binding.root)

        binding.tvTitle.text = "Remove Request"
        binding.tvDescription.text = "After continue, this request will be deleted. Are you sure, you want to continue."

        binding.tvContinue.setOnClickListener {
            dialog.dismiss()
            removeCard(cardId, isPersonal, isAppUser)
        }

        binding.tvNo.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun removeCard(cardId: String, isPersonal: Boolean, isAppUser: Boolean) {
        val call = Hello.apiService.removeCard(DeleteContactRequestModel(cardId, isPersonal, isAppUser, null, null, null))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.REMOVE_CARD_URL, true, this)
    }

    override fun success(apiName: String, response: Any?) {
        when(apiName){
            Constants.REMOVE_CARD_URL ->{
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    callGetContactRequestApi()
                }
            }
            else -> {
                val model = response as GetContactRequestResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    Hello.db.putBoolean(Constants.HAVE_NOTIFICATION, false)
                    EventBus.getDefault().post(MessageEvent("notify"))
                    val notificationManager = MainActivity.mActivity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.cancelAll()
                    if (model.data.isNotEmpty()) {
                        mList.clear()
                        mList = model.data as ArrayList<ContactRequestModel>
                        binding.tvIsEmpty.visibility = View.GONE
                        binding.rvContactRequest.visibility = View.VISIBLE
                        setRecyclerView()
                    }
                    else {
                        binding.tvIsEmpty.visibility = View.VISIBLE
                        binding.rvContactRequest.visibility = View.GONE
                    }
                }
            }
        }

    }

    override fun failure(apiName: String, message: String?) {
    }



    override fun reCallGetContactRequestApi() {

        callGetContactRequestApi()
    }
}