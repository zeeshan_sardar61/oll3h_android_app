package com.iinshaapps.hello.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.AdapterProfileEdit
import com.iinshaapps.hello.adapter.ItemDetailsEditAdapter
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.ItemsEditFragmentBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.hideKeyboard
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.ItemDetailModel
import com.iinshaapps.hello.model.response.*
import java.util.*
import kotlin.collections.ArrayList

class ItemsEditDetailFragment(val cardData: CardData, val isPersonal: Boolean, val mListener: UpdateItemDetails) : BaseFragment(), AdapterProfileEdit.ItemClicker, IGenericCallBack, ItemDetailsEditAdapter.ItemClicker {

    lateinit var binding: ItemsEditFragmentBinding
    var mmList = ArrayList<EmailsItem?>()
    var mmListCopy = ArrayList<EmailsItem?>()
    lateinit var adapter: AdapterProfileEdit
    var mList = ArrayList<ItemDetailModel>()
    var mListCopy = ArrayList<ItemDetailModel>()
    lateinit var aadapter: ItemDetailsEditAdapter
    var addCheck = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.items_edit_fragment, container, false)

        initViews()
        setListener()

        if (isPersonal) {
            setPersonalItems()
            setPersonalEmailsRecyclerView()
        }
        else {
            setBusinessItems()
            setBusinessEmailsRecyclerView()
        }

        return binding.root
    }

    private fun initViews() {

        if (isPersonal) {
            binding.tvTitle.text = "Personal Details"
        }
        else {
            binding.tvTitle.text = "Business Details"
        }
    }

    private fun setListener() {

        binding.llAddEmail.setOnClickListener {

            if (addCheck) {
                mmList.add(EmailsItem(email = "", isPublic = false))
                adapter.notifyDataSetChanged()
            }
        }

        binding.ivBack.setOnClickListener {
            binding.root.hideKeyboard()
            MainActivity.mActivity.onBackPressed()
        }

        binding.tvSave.setOnClickListener {
            if (isPersonal) {
                setUpdatedPersonalItemList()
            }
            else {
                setUpdatedBusinessItemList()
            }
        }
    }

    private fun setUpdatedPersonalItemList() {

        val updatedPersonalDetail = PersonalDetail()
        val updatedList = aadapter.getList()

        if (updatedList[0].tag == "Date of Birth") {

            if (updatedList[0].body.isEmpty()) {
                updatedPersonalDetail.dob = "0001-01-01T00:00:00"
            }
            else {
                updatedPersonalDetail.dob = updatedList[0].body
            }
            if(updatedList[0].body.isNotEmpty())
                updatedPersonalDetail.isDobPublic = updatedList[0].switch
            else
                updatedPersonalDetail.isDobPublic = false
        }

//        if (updatedList[1].tag == "Address") {
//            updatedPersonalDetail.address = updatedList[1].body
//            if(updatedList[1].body.isNotEmpty())
//                updatedPersonalDetail.isAddressPublic = updatedList[1].switch
//            else
//                updatedPersonalDetail.isAddressPublic = false
//        }

        if (updatedList[1].tag == "Country") {
            updatedPersonalDetail.country = updatedList[1].body
            if(updatedList[1].body.isNotEmpty())
                updatedPersonalDetail.isCountryPublic = updatedList[1].switch
            else
                updatedPersonalDetail.isCountryPublic = false
        }

        if (updatedList[2].tag == "State") {
            updatedPersonalDetail.state = updatedList[2].body
            if(updatedList[2].body.isNotEmpty())
                updatedPersonalDetail.isStatePublic = updatedList[2].switch
            else
                updatedPersonalDetail.isStatePublic = false
        }

        if (updatedList[3].tag == "City") {
            updatedPersonalDetail.city = updatedList[3].body
            if(updatedList[3].body.isNotEmpty())
                updatedPersonalDetail.isCityPublic = updatedList[3].switch
            else
                updatedPersonalDetail.isCityPublic = false
        }

        if (updatedList[4].tag == "Street") {
            updatedPersonalDetail.street = updatedList[4].body
            if(updatedList[4].body.isNotEmpty())
                updatedPersonalDetail.isStreetPublic = updatedList[4].switch
            else
                updatedPersonalDetail.isStreetPublic = false
        }

        if (updatedList[5].tag == "Postcode") {
            updatedPersonalDetail.postCode = updatedList[5].body
            if(updatedList[5].body.isNotEmpty())
                updatedPersonalDetail.isPostCodePublic = updatedList[5].switch
            else
                updatedPersonalDetail.isPostCodePublic = false
        }

        if (updatedList[6].tag == "School Name") {
            updatedPersonalDetail.schoolName = updatedList[6].body
            if(updatedList[6].body.isNotEmpty())
                updatedPersonalDetail.isSchoolPublic = updatedList[6].switch
            else
                updatedPersonalDetail.isSchoolPublic = false
        }

        if (updatedList[7].tag == "Level of Education") {
            updatedPersonalDetail.levelEducation = updatedList[7].body
            if(updatedList[7].body.isNotEmpty())
                updatedPersonalDetail.isleveEducautionPublic = updatedList[7].switch
            else
                updatedPersonalDetail.isleveEducautionPublic = false
        }

        if (updatedList[8].tag == "Major") {
            updatedPersonalDetail.major = updatedList[8].body
            if(updatedList[8].body.isNotEmpty())
                updatedPersonalDetail.isMajorPublic = updatedList[8].switch
            else
                updatedPersonalDetail.isMajorPublic = false
        }

        if (updatedList[9].tag == "Minor") {
            updatedPersonalDetail.minor = updatedList[9].body
            if(updatedList[9].body.isNotEmpty())
                updatedPersonalDetail.isMinorPublic = updatedList[9].switch
            else
                updatedPersonalDetail.isMinorPublic = false
        }

        if (updatedList[10].tag == "Classification") {
            updatedPersonalDetail.classification = updatedList[10].body
            if(updatedList[10].body.isNotEmpty())
                updatedPersonalDetail.isClassificationPublic = updatedList[10].switch
            else
                updatedPersonalDetail.isClassificationPublic = false
        }

        if (updatedList[11].tag == "About You") {
            updatedPersonalDetail.aboutYou = updatedList[11].body
            if(updatedList[11].body.isNotEmpty())
                updatedPersonalDetail.isAboutPublic = updatedList[11].switch
            else
                updatedPersonalDetail.isAboutPublic = false
        }

        mmList.forEachIndexed { index, emailsItem ->
            if(emailsItem!!.email!!.isEmpty()) {
                mmList.removeAt(index)
            }
        }

        updatedPersonalDetail.emails = mmList

        val call = Hello.apiService.updateMyDetail(CardData(updatedPersonalDetail, null))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.UPDATE_MY_DETAIL_URL, true, this)
    }

    private fun setUpdatedBusinessItemList() {

        val updatedBusinessDetail = BusinessDetail()
        val updatedList = aadapter.getList()

//        if (updatedList[0].tag == "Business Address") {
//            updatedBusinessDetail.businessAddress = updatedList[0].body
//            if(updatedList[0].body.isNotEmpty())
//                updatedBusinessDetail.isBusinessAddressPublic = updatedList[0].switch
//            else
//                updatedBusinessDetail.isBusinessAddressPublic = false
//        }

        if (updatedList[0].tag == "Country") {
            updatedBusinessDetail.country = updatedList[0].body
            if(updatedList[0].body.isNotEmpty())
                updatedBusinessDetail.isCountryPublic = updatedList[0].switch
            else
                updatedBusinessDetail.isCountryPublic = false
        }

        if (updatedList[1].tag == "State") {
            updatedBusinessDetail.state = updatedList[1].body
            if(updatedList[1].body.isNotEmpty())
                updatedBusinessDetail.isStatePublic = updatedList[1].switch
            else
                updatedBusinessDetail.isStatePublic = false
        }

        if (updatedList[2].tag == "City") {
            updatedBusinessDetail.city = updatedList[2].body
            if(updatedList[2].body.isNotEmpty())
                updatedBusinessDetail.isCityPublic = updatedList[2].switch
            else
                updatedBusinessDetail.isCityPublic = false
        }

        if (updatedList[3].tag == "Street") {
            updatedBusinessDetail.street = updatedList[3].body
            if(updatedList[3].body.isNotEmpty())
                updatedBusinessDetail.isStreetPublic = updatedList[3].switch
            else
                updatedBusinessDetail.isStreetPublic = false
        }

        if (updatedList[4].tag == "Postcode") {
            updatedBusinessDetail.postCode = updatedList[4].body
            if(updatedList[4].body.isNotEmpty())
                updatedBusinessDetail.isPostCodePublic = updatedList[4].switch
            else
                updatedBusinessDetail.isPostCodePublic = false
        }

        if (updatedList[5].tag == "Fax Number") {
            updatedBusinessDetail.faxNumber = updatedList[5].body
            if(updatedList[5].body.isNotEmpty())
                updatedBusinessDetail.isFaxNumberPublic = updatedList[5].switch
            else
                updatedBusinessDetail.isFaxNumberPublic = false
        }

        if (updatedList[6].tag == "Company Website URL") {
            updatedBusinessDetail.website = updatedList[6].body
            if(updatedList[6].body.isNotEmpty())
                updatedBusinessDetail.isWebsitePublic = updatedList[6].switch
            else
                updatedBusinessDetail.isWebsitePublic = false
        }

        if (updatedList[7].tag == "Occupation/Job Title") {
            updatedBusinessDetail.jobTitle = updatedList[7].body
            if(updatedList[7].body.isNotEmpty())
                updatedBusinessDetail.isJobTitlePublic = updatedList[7].switch
            else
                updatedBusinessDetail.isJobTitlePublic = false
        }

        if (updatedList[8].tag == "College/University") {
            updatedBusinessDetail.university = updatedList[8].body
            if(updatedList[8].body.isNotEmpty())
                updatedBusinessDetail.isUniversityPublic = updatedList[8].switch
            else
                updatedBusinessDetail.isUniversityPublic = false
        }

        if (updatedList[9].tag == "Degree(s)") {
            updatedBusinessDetail.degree = updatedList[9].body
            if(updatedList[9].body.isNotEmpty())
                updatedBusinessDetail.isDegreePublic = updatedList[9].switch
            else
                updatedBusinessDetail.isDegreePublic = false
        }

        if (updatedList[10].tag == "Biography") {
            updatedBusinessDetail.bio = updatedList[10].body
            if(updatedList[10].body.isNotEmpty())
                updatedBusinessDetail.isBioPublic = updatedList[10].switch
            else
                updatedBusinessDetail.isBioPublic = false
        }

        mmList.forEachIndexed { index, emailsItem ->
            if(emailsItem!!.email!!.isEmpty()) {
                mmList.removeAt(index)
            }
        }

        updatedBusinessDetail.emails = mmList

        val call = Hello.apiService.updateMyDetail(CardData(null, updatedBusinessDetail))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.UPDATE_MY_DETAIL_URL, true, this)
    }

    private fun setPersonalItems() {

        cardData.personalDetail?.apply {

            if (dob == "0001-01-01T00:00:00") {
                mList.add(ItemDetailModel("Date of Birth", "", isDobPublic ?: false))
                mListCopy.add(ItemDetailModel("Date of Birth", "", isDobPublic ?: false))
            }
            else {
                mList.add(ItemDetailModel("Date of Birth", dob!!, isDobPublic ?: false))
                mListCopy.add(ItemDetailModel("Date of Birth", dob!!, isDobPublic ?: false))
            }

//            mList.add(ItemDetailModel("Address", address ?: "", isAddressPublic ?: false))
            mList.add(ItemDetailModel("Country", country ?: "", isCountryPublic ?: false))
            mList.add(ItemDetailModel("State", state ?: "", isStatePublic ?: false))
            mList.add(ItemDetailModel("City", city ?: "", isCityPublic ?: false))
            mList.add(ItemDetailModel("Street", street ?: "", isStreetPublic ?: false))
            mList.add(ItemDetailModel("Postcode", postCode ?: "", isPostCodePublic ?: false))
            mList.add(ItemDetailModel("School Name", schoolName ?: "", isSchoolPublic ?: false))
            mList.add(ItemDetailModel("Level of Education", levelEducation ?: "", isleveEducautionPublic ?: false))
            mList.add(ItemDetailModel("Major", major ?: "", isMajorPublic ?: false))
            mList.add(ItemDetailModel("Minor", minor ?: "", isMinorPublic ?: false))
            mList.add(ItemDetailModel("Classification", classification ?: "", isClassificationPublic ?: false))
            mList.add(ItemDetailModel("About You", aboutYou ?: "", isAboutPublic ?: false))

//            mListCopy.add(ItemDetailModel("Address", address ?: "", isAddressPublic ?: false))
            mListCopy.add(ItemDetailModel("Country", country ?: "", isCountryPublic ?: false))
            mListCopy.add(ItemDetailModel("State", state ?: "", isStatePublic ?: false))
            mListCopy.add(ItemDetailModel("City", city ?: "", isCityPublic ?: false))
            mListCopy.add(ItemDetailModel("Street", street ?: "", isStreetPublic ?: false))
            mListCopy.add(ItemDetailModel("Postcode", postCode ?: "", isPostCodePublic ?: false))
            mListCopy.add(ItemDetailModel("School Name", schoolName ?: "", isSchoolPublic ?: false))
            mListCopy.add(ItemDetailModel("Level of Education", levelEducation ?: "", isleveEducautionPublic ?: false))
            mListCopy.add(ItemDetailModel("Major", major ?: "", isMajorPublic ?: false))
            mListCopy.add(ItemDetailModel("Minor", minor ?: "", isMinorPublic ?: false))
            mListCopy.add(ItemDetailModel("Classification", classification ?: "", isClassificationPublic ?: false))
            mListCopy.add(ItemDetailModel("About You", aboutYou ?: "", isAboutPublic ?: false))
        }

        setItemsRecyclerView()
    }

    private fun setBusinessItems() {

        cardData.businessDetail?.apply {

//            mList.add(ItemDetailModel("Business Address", businessAddress ?: "", isBusinessAddressPublic ?: false))
            mList.add(ItemDetailModel("Country", country ?: "", isCountryPublic ?: false))
            mList.add(ItemDetailModel("State", state ?: "", isStatePublic ?: false))
            mList.add(ItemDetailModel("City", city ?: "", isCityPublic ?: false))
            mList.add(ItemDetailModel("Street", street ?: "", isStreetPublic ?: false))
            mList.add(ItemDetailModel("Postcode", postCode ?: "", isPostCodePublic ?: false))
            mList.add(ItemDetailModel("Fax Number", faxNumber ?: "", isFaxNumberPublic ?: false))
            mList.add(ItemDetailModel("Company Website URL", website ?: "", isWebsitePublic ?: false))
            mList.add(ItemDetailModel("Occupation/Job Title", jobTitle ?: "", isJobTitlePublic ?: false))
            mList.add(ItemDetailModel("College/University", university ?: "", isUniversityPublic ?: false))
            mList.add(ItemDetailModel("Degree(s)", degree ?: "", isDegreePublic ?: false))
            mList.add(ItemDetailModel("Biography", bio ?: "", isBioPublic ?: false))

//            mListCopy.add(ItemDetailModel("Business Address", businessAddress ?: "", isBusinessAddressPublic ?: false))
            mListCopy.add(ItemDetailModel("Country", country ?: "", isCountryPublic ?: false))
            mListCopy.add(ItemDetailModel("State", state ?: "", isStatePublic ?: false))
            mListCopy.add(ItemDetailModel("City", city ?: "", isCityPublic ?: false))
            mListCopy.add(ItemDetailModel("Street", street ?: "", isStreetPublic ?: false))
            mListCopy.add(ItemDetailModel("Postcode", postCode ?: "", isPostCodePublic ?: false))
            mListCopy.add(ItemDetailModel("Fax Number", faxNumber ?: "", isFaxNumberPublic ?: false))
            mListCopy.add(ItemDetailModel("Company Website URL", website ?: "", isWebsitePublic ?: false))
            mListCopy.add(ItemDetailModel("Occupation/Job Title", jobTitle ?: "", isJobTitlePublic ?: false))
            mListCopy.add(ItemDetailModel("College/University", university ?: "", isUniversityPublic ?: false))
            mListCopy.add(ItemDetailModel("Degree(s)", degree ?: "", isDegreePublic ?: false))
            mListCopy.add(ItemDetailModel("Biography", bio ?: "", isBioPublic ?: false))
        }

        setItemsRecyclerView()
    }

    private fun setItemsRecyclerView() {

        binding.rvItems.layoutManager = LinearLayoutManager(MainActivity.mActivity)
        aadapter = ItemDetailsEditAdapter(mList)
        aadapter.mListener = this
        binding.rvItems.adapter = aadapter
    }

    private fun setPersonalEmailsRecyclerView() {

        mmList = cardData.personalDetail!!.emails!!
        mmListCopy = mmList.clone() as ArrayList<EmailsItem?>
        binding.rvEtDetails.layoutManager = LinearLayoutManager(MainActivity.mActivity)
        adapter = AdapterProfileEdit(MainActivity.mActivity, mmList)
        adapter.mListener = this
        binding.rvEtDetails.adapter = adapter
    }

    private fun setBusinessEmailsRecyclerView() {

        mmList = cardData.businessDetail!!.emails!!
        mmListCopy = mmList.clone() as ArrayList<EmailsItem?>
        binding.rvEtDetails.layoutManager = LinearLayoutManager(MainActivity.mActivity)
        adapter = AdapterProfileEdit(MainActivity.mActivity, mmList)
        adapter.mListener = this
        binding.rvEtDetails.adapter = adapter
    }

    override fun onItemClick(position: Int) {
    }

    override fun onDeleteClick(position: Int) {

        mmList.removeAt(position)
        adapter.notifyDataSetChanged()

        if (mmList.isEmpty()) {
            addCheck = true
        }

        Log.d("TAGO", "addEmail: $mmList")

        if(mListCopy == mList && mmListCopy == mmList) {
            binding.tvSave.visibility = View.GONE
        }else {
            binding.tvSave.visibility = View.VISIBLE
        }
    }

    override fun onAddCheck(check: Boolean) {

        addCheck = check
    }

    override fun addEmail(position: Int, email: String, switch: Boolean) {

        mmList[position]!!.email = email
        mmList[position]!!.isPublic = switch
        adapter.notifyDataSetChanged()

        Log.d("TAGO", "addEmail: $mmList")

        if(mListCopy == mList && mmListCopy == mmList) {
            binding.tvSave.visibility = View.GONE
        }else {
            binding.tvSave.visibility = View.VISIBLE
        }
    }

    override fun success(apiName: String, response: Any?) {

        when (apiName) {
            Constants.UPDATE_MY_DETAIL_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    MainActivity.mActivity.onBackPressed()
                    mListener.onUpdateCall()
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
    }

    interface UpdateItemDetails{
        fun onUpdateCall()
    }

    override fun onItemChange() {

        if(mListCopy == mList && mmListCopy == mmList) {
            binding.tvSave.visibility = View.GONE
        }else {
            binding.tvSave.visibility = View.VISIBLE
        }
    }
}