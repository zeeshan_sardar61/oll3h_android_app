package com.iinshaapps.hello.fragment

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.iinshaapps.hello.Hello.Companion.simpleCache
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.CreatePersonalProfileActivity
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.SocialDetailsAdapter
import com.iinshaapps.hello.databinding.DialogEnhanceQrBinding
import com.iinshaapps.hello.databinding.DialogSocialBinding
import com.iinshaapps.hello.databinding.FragmentMyPersonalCardDetailBinding
import com.iinshaapps.hello.helper.*
import com.iinshaapps.hello.model.SocialModel
import com.iinshaapps.hello.model.response.PersonalCard
import com.iinshaapps.hello.model.response.SocialMediaItem
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MyPersonalCardDetailFragment(private val personalCardData: PersonalCard) : BaseFragment(), SocialDetailsAdapter.ItemClicker {

    private val STORAGE_PERMISSION = 2

    lateinit var binding: FragmentMyPersonalCardDetailBinding
    var mList = ArrayList<SocialModel>()
    var adapterSocialDetails: SocialDetailsAdapter? = null

    private lateinit var player: SimpleExoPlayer
    private var currentWindow = 0
    private var playbackPosition: Long = 0

    private lateinit var httpDataSourceFactory: HttpDataSource.Factory
    private lateinit var defaultDataSourceFactory: DefaultDataSourceFactory
    private lateinit var cacheDataSourceFactory: DataSource.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_personal_card_detail, container, false)

        initViews()
        setUpPersonalData()
        setUpPersonalDataForScreenshot()

        return binding.root
    }

    private fun initViews() {

        binding.tvEdit.setOnClickListener {
            val i = Intent(MainActivity.mActivity, CreatePersonalProfileActivity::class.java)
            i.putExtra("signup", false)
            i.putExtra("data", personalCardData)
            MainActivity.mActivity.startActivity(i)
        }

        binding.llPersonalDetails.setOnClickListener {
            if (personalCardData.isVideo) {
                releasePlayer()
            }
            addFragment(MainActivity.mActivity, R.id.container, ItemsDetailFragment(personalCardData.cardId, true, false, null), "ItemsDetailFragment")
        }

        binding.buttonBack.setOnClickListener {
            MainActivity.mActivity.onBackPressed()
        }

        binding.ivVolume.setOnClickListener {
            if (player.volume == 0F) {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_up)
                player.volume = 1.0F
            }
            else {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_down)
                player.volume = 0F
            }
        }

        binding.ivShare.setOnClickListener {

            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                launchShareIntent()
            }
            else {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION)
            }
        }

        binding.ivQrCode.setOnClickListener {
            dialogQr()
        }
    }

    private fun launchShareIntent() {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imagePath = MediaStore.Images.Media.insertImage(MainActivity.mActivity.contentResolver, getBitmapFromView(binding.rlScreenshot), "img_$timeStamp", null) //            val shareIntent = Intent(Intent.ACTION_SEND).apply { //                type = "image/*" //                putExtra(Intent.EXTRA_STREAM, Uri.parse(imagePath))
        //                putExtra(Intent.EXTRA_TEXT, "hello123")
        //            }
        //            MainActivity.mActivity.startActivity(Intent.createChooser(shareIntent, null))

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Name: ${personalCardData.name}\nPhone: ${personalCardData.countryCode} ${personalCardData.phone}\nEmail: ${personalCardData.email}\n\n${personalCardData.qrCode}")
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(imagePath))
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        MainActivity.mActivity.startActivity(Intent.createChooser(shareIntent, "send"))
    }

    private fun setUpPersonalData() {

        if (!personalCardData.isVideo) {
            binding.ivPersonalUrl.visibility = View.VISIBLE
            binding.rlVideoView.visibility = View.GONE
            Glide.with(MainActivity.mActivity).load(personalCardData.url).into(binding.ivPersonalUrl)
            Glide.with(MainActivity.mActivity).load(personalCardData.thumbnail).placeholder(R.drawable.ic_dp_placeholder).into(binding.ivPersonalThumbnail)
        }

        binding.tvNamePersonal.text = personalCardData.name
        binding.tvPhonePersonal.text = personalCardData.countryCode + " " + personalCardData.phone
        binding.tvEmailPersonal.text = personalCardData.email

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(personalCardData.cardStartColor), Color.parseColor(personalCardData.cardEndColor)))
        gd.cornerRadius = 0f
        binding.rlColor.setBackgroundDrawable(gd)

        binding.ivQrCode.setImageBitmap(encodeAsBitmap(personalCardData.qrCode))

        if (personalCardData.socialMedia.isEmpty()) {
            binding.llRecyclerView.visibility = View.GONE
        }
        else {
            initializeRecyclerView(personalCardData.socialMedia as ArrayList<SocialMediaItem>)
        }
    }

    private fun setUpPersonalDataForScreenshot() {

        Glide.with(MainActivity.mActivity).load(personalCardData.url).into(binding.ivPersonalUrl1)

        if (personalCardData.isVideo) {
            Glide.with(MainActivity.mActivity).load(R.drawable.ic_dp_placeholder).into(binding.ivPersonalThumbnail1)
        }
        else {
            Glide.with(MainActivity.mActivity).load(personalCardData.thumbnail).placeholder(R.drawable.ic_dp_placeholder).into(binding.ivPersonalThumbnail1)
        }

        binding.tvNamePersonal1.text = personalCardData.name
        binding.tvPhonePersonal1.text = personalCardData.countryCode + " " + personalCardData.phone
        binding.tvEmailPersonal1.text = personalCardData.email

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(personalCardData.cardStartColor), Color.parseColor(personalCardData.cardEndColor)))
        gd.cornerRadius = 0f
        binding.rlColor1.setBackgroundDrawable(gd)

        binding.ivQrCode1.setImageBitmap(encodeAsBitmap(personalCardData.qrCode))
    }

    private fun initializeRecyclerView(socialMedia: ArrayList<SocialMediaItem>) {
        socialMedia.forEachIndexed { index, socialMediaItem ->
            when (socialMediaItem.tag) {
                Constants.facebook -> mList.add(SocialModel(true, Constants.facebook, R.drawable.ic_facebook, R.drawable.ic_facebook_filled, socialMediaItem.url))
                Constants.whatsapp -> mList.add(SocialModel(true, Constants.whatsapp, R.drawable.ic_whatsapp, R.drawable.ic_whatsapp_filled, socialMediaItem.url))
                Constants.twitter -> mList.add(SocialModel(true, Constants.twitter, R.drawable.ic_twitter, R.drawable.ic_twitter_filled, socialMediaItem.url))
                Constants.instagram -> mList.add(SocialModel(true, Constants.instagram, R.drawable.ic_instagram, R.drawable.ic_instagram_filled, socialMediaItem.url))
                Constants.snapchat -> mList.add(SocialModel(true, Constants.snapchat, R.drawable.ic_snapchat, R.drawable.ic_snapchat_filled, socialMediaItem.url))
                Constants.youtube -> mList.add(SocialModel(true, Constants.youtube, R.drawable.ic_youtube, R.drawable.ic_youtube_filled, socialMediaItem.url))
                Constants.tiktok -> mList.add(SocialModel(true, Constants.tiktok, R.drawable.ic_tiktok, R.drawable.ic_tiktok_filled, socialMediaItem.url))
                Constants.pinterest -> mList.add(SocialModel(true, Constants.pinterest, R.drawable.ic_pinterest, R.drawable.ic_pinterst_filled, socialMediaItem.url))
                Constants.linkedin -> mList.add(SocialModel(true, Constants.linkedin, R.drawable.ic_linkedin, R.drawable.ic_linkedin_filled, socialMediaItem.url))
                Constants.website -> mList.add(SocialModel(true, Constants.website, R.drawable.ic_web, R.drawable.ic_web_filled, socialMediaItem.url))
                Constants.tinder -> mList.add(SocialModel(true, Constants.tinder, R.drawable.ic_tinder, R.drawable.ic_tinder_filled, socialMediaItem.url))
                Constants.bumble -> mList.add(SocialModel(true, Constants.bumble, R.drawable.ic_bumble, R.drawable.ic_bumble_filled, socialMediaItem.url))
                Constants.nintendo -> mList.add(SocialModel(true, Constants.nintendo, R.drawable.ic_nitendo, R.drawable.ic_nitendo_filled, socialMediaItem.url))
                Constants.signal -> mList.add(SocialModel(true, Constants.signal, R.drawable.ic_signal, R.drawable.ic_signal_filled, socialMediaItem.url))
                Constants.weChat -> mList.add(SocialModel(true, Constants.weChat, R.drawable.ic_wechat, R.drawable.ic_wechat_filled, socialMediaItem.url))
                Constants.telegram -> mList.add(SocialModel(true, Constants.telegram, R.drawable.ic_telegram, R.drawable.ic_telegram_filled, socialMediaItem.url))
                Constants.twitch -> mList.add(SocialModel(true, Constants.twitch, R.drawable.ic_twitch, R.drawable.ic_twitch_filled, socialMediaItem.url))
                Constants.discord -> mList.add(SocialModel(true, Constants.discord, R.drawable.ic_discord, R.drawable.ic_discord_filled, socialMediaItem.url))
                Constants.calendly -> mList.add(SocialModel(true, Constants.calendly, R.drawable.ic_calendly, R.drawable.ic_calendly_filled, socialMediaItem.url))
                Constants.skype -> mList.add(SocialModel(true, Constants.skype, R.drawable.ic_skype, R.drawable.ic_skype_filled, socialMediaItem.url))
                Constants.xbox -> mList.add(SocialModel(true, Constants.xbox, R.drawable.ic_xbox, R.drawable.ic_xbox_filled, socialMediaItem.url))
                Constants.playstation -> mList.add(SocialModel(true, Constants.playstation, R.drawable.ic_ps, R.drawable.ic_ps_filled, socialMediaItem.url))
                Constants.reddit -> mList.add(SocialModel(true, Constants.reddit, R.drawable.ic_reddit, R.drawable.ic_reddit_filled, socialMediaItem.url))
                Constants.payPal -> mList.add(SocialModel(true, Constants.payPal, R.drawable.ic_paypal, R.drawable.ic_paypal_filled, socialMediaItem.url))
                Constants.zelle -> mList.add(SocialModel(true, Constants.zelle, R.drawable.ic_zelle, R.drawable.ic_zelle_filled, socialMediaItem.url))
                Constants.cashApp -> mList.add(SocialModel(true, Constants.cashApp, R.drawable.ic_cashapp, R.drawable.ic_cashapp_filled, socialMediaItem.url))
                Constants.venmo -> mList.add(SocialModel(true, Constants.venmo, R.drawable.ic_venmo, R.drawable.ic_venmo_filled, socialMediaItem.url))
            }
        }

        mList.sortBy { it.socialName }
        adapterSocialDetails = SocialDetailsAdapter(mList)
        adapterSocialDetails?.mListener = this
        binding.apply {
            recyclerView.apply {
                layoutManager = GridLayoutManager(context, 5)
                adapter = adapterSocialDetails
            }
        }
    }

    override fun onItemClick(position: Int) {
        when (mList[position].socialName) {
            Constants.zelle -> {
                openSocialDialog(mList[position])
            }
            Constants.bumble -> {
                openSocialDialog(mList[position])
            }
            Constants.nintendo -> {
                openSocialDialog(mList[position])
            }
            Constants.signal -> {
                openSocialDialog(mList[position])
            }
            Constants.skype -> {
                openSocialDialog(mList[position])
            }
            else -> {
                val i = Intent(Intent.ACTION_VIEW)
                val link = getLink(mList[position].socialName, mList[position].socialLink)
                i.setData(Uri.parse(link))
                startActivity(i)
            }
        }
    }

    private fun openSocialDialog(model: SocialModel) {
        val dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogSocialBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.mActivity), R.layout.dialog_social, null, false)
        dialog.setContentView(binding.root)

        binding.imageViewSocial.setImageResource(model.socialDrawableFilled)
        binding.textViewTitle.text = (model.socialName)
        binding.textViewSubTitle.text = (model.socialName)

        binding.editTextUsername.setText(model.socialLink)
        binding.editTextUsername.isEnabled = false
        binding.switchPublic.visibility = View.GONE
        binding.buttonSave.visibility = View.GONE
        binding.textViewPublic.visibility = View.INVISIBLE

        dialog.show()
    }

    fun getBitmapFromView(view: View): Bitmap {
        val returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable: Drawable = view.getBackground()
        if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return returnedBitmap
    }

    private fun dialogQr() {

        val dialog = Dialog(requireContext(), R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogEnhanceQrBinding = DataBindingUtil.inflate(LayoutInflater.from(requireContext()), R.layout.dialog_enhance_qr, null, false)
        dialog.setContentView(binding.root)

        binding.ivQrCode.setImageBitmap(encodeAsBitmap(personalCardData.qrCode))

        dialog.show()
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(MainActivity.mActivity, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
    }

    private fun initializePlayer(videoView: PlayerView, url: String) {
        binding.ivVolume.setImageResource(R.drawable.ic_volume_up)

        httpDataSourceFactory = DefaultHttpDataSource.Factory().setAllowCrossProtocolRedirects(true)

        defaultDataSourceFactory = DefaultDataSourceFactory(requireContext(), httpDataSourceFactory)

        cacheDataSourceFactory = CacheDataSource.Factory().setCache(simpleCache).setUpstreamDataSourceFactory(httpDataSourceFactory).setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

        player = SimpleExoPlayer.Builder(requireContext()).setMediaSourceFactory(DefaultMediaSourceFactory(cacheDataSourceFactory)).build()
        val videoUri = Uri.parse(url)
        val mediaItem = MediaItem.fromUri(videoUri)
        val mediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem)

        videoView.player = player
        videoView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL;
        videoView.hideController()
        player.playWhenReady = true
        player.seekTo(0, 0)
        player.repeatMode = Player.REPEAT_MODE_ALL
        player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
        player.setMediaSource(mediaSource, true)
        val audioAttributes: AudioAttributes = AudioAttributes.Builder().setUsage(C.USAGE_MEDIA).setContentType(C.CONTENT_TYPE_MOVIE).build()
        player.setAudioAttributes(audioAttributes, /* handleAudioFocus= */ true);
        player.prepare()

        player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    SimpleExoPlayer.STATE_ENDED -> {

                    }
                    SimpleExoPlayer.STATE_BUFFERING -> {
                        binding.pbBuffering.visibility = View.VISIBLE
                    }
                }
            }

            override fun onIsPlayingChanged(isPlaying: Boolean) {
                if (isPlaying) {
                    binding.rlVideoView.visibility = View.VISIBLE
                    binding.pbBuffering.visibility = View.GONE
                    binding.ivVolume.visibility = View.VISIBLE
                }
                else {
                    binding.pbBuffering.visibility = View.VISIBLE
                }
            }

            override fun onPlayerError(error: ExoPlaybackException) {
                Log.e("exoPlayer", error.message!!)
            }
        })
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    fun releasePlayer() {
        if (personalCardData.isVideo) {
            player.run {
                playbackPosition = this.currentPosition
                currentWindow = this.currentWindowIndex
                playWhenReady = this.playWhenReady
                release()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this);
        releasePlayer()
    }

    override fun onResume() {
        super.onResume()
        loadVideo()
    }

    fun loadVideo() {
        if (personalCardData.isVideo) {
            binding.ivPersonalUrl.visibility = View.GONE
            binding.rlVideoView.visibility = View.VISIBLE
//            Glide.with(MainActivity.mActivity).load(personalCardData.thumbnail).placeholder(R.drawable.ic_dp_placeholder).into(binding.ivPersonalThumbnail)
            Glide.with(MainActivity.mActivity).load(R.drawable.ic_dp_placeholder).into(binding.ivPersonalThumbnail)
            initializePlayer(binding.videoView, personalCardData.url)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "Fire") {
            loadVideo()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            STORAGE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchShareIntent()
                }
                else {
                    MainActivity.mActivity.showToastMessage("Please allow permission to share your profile")
                }
            }
        }
    }
}