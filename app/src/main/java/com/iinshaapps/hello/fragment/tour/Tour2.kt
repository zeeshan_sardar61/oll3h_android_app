package com.iinshaapps.hello.fragment.tour

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.TourTwoBinding
import com.iinshaapps.hello.fragment.BaseFragment

class Tour2 : BaseFragment() {

    lateinit var binding: TourTwoBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.tour_two, container, false)
        return binding.root
    }

}