package com.iinshaapps.hello.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.SocialDetailsAdapter
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.BusinessCardDetailFragmentBinding
import com.iinshaapps.hello.databinding.DialogEnlargeCardBinding
import com.iinshaapps.hello.databinding.DialogSocialBinding
import com.iinshaapps.hello.helper.*
import com.iinshaapps.hello.model.OtherItemsModel
import com.iinshaapps.hello.model.SocialModel
import com.iinshaapps.hello.model.requestmodel.AddContactRequestIdModel
import com.iinshaapps.hello.model.requestmodel.AddContactRequestModel
import com.iinshaapps.hello.model.response.BusinessCard1
import com.iinshaapps.hello.model.response.GetCardDetailsResponse
import com.iinshaapps.hello.model.response.RecoveryCodeResponse
import com.iinshaapps.hello.model.response.SocialMediaItem
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class BusinessCardDetailFragment(var cardId: String, val fromContactRequest: Boolean = false, val id: String = "", val apiCall: ApiCall? = null) : BaseFragment(), SocialDetailsAdapter.ItemClicker, IGenericCallBack {

    lateinit var binding: BusinessCardDetailFragmentBinding
    var mList = ArrayList<SocialModel>()
    val REQUEST_ADD_NOTES = 100
    val REQUEST_ADD_NOTES_CONTACT_REQUEST = 200
    var adapterSocialDetails: SocialDetailsAdapter? = null
    var otherItemsModel: OtherItemsModel? = null
    private lateinit var player: SimpleExoPlayer
    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 0
    var businessCardData: BusinessCard1? = null

    private lateinit var httpDataSourceFactory: HttpDataSource.Factory
    private lateinit var defaultDataSourceFactory: DefaultDataSourceFactory
    private lateinit var cacheDataSourceFactory: DataSource.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.business_card_detail_fragment, container, false)

        initViews()
        getCardDetails()

        return binding.root
    }

    private fun getCardDetails() {
        val call = Hello.apiService.getCardDetails(cardId, false, true)
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.GET_CONTACTS_DETAILS_URL, true, this)
    }

    private fun initViews() {

        binding.buttonDetail.setOnClickListener {
            releasePlayer()
            addFragment(MainActivity.mActivity, R.id.container, ItemsDetailFragment(cardId, false, true, otherItemsModel, getBitmapFromView(binding.rlScreenshot), getBitmapFromView(binding.ivUser), binding.tvNotes.text.toString()), "ItemsDetailFragment")
        }

        binding.buttonBack.setOnClickListener {
            MainActivity.mActivity.onBackPressed()
        }

        binding.tvPhone.setOnClickListener {
            releasePlayer()
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", binding.tvPhone.text.toString(), null))
            startActivity(intent)
        }

        binding.tvEmail.setOnClickListener {
            releasePlayer()
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(binding.tvEmail.text.toString()))
            intent.putExtra(Intent.EXTRA_SUBJECT, "From your dear friend: ")
            MainActivity.mActivity.startActivity(intent)
        }

        binding.rlNotes.setOnClickListener {
            releasePlayer()
            val addNotesFragment = AddNotesFragment(cardId, binding.tvNotes.text.toString(), false, false)
            addNotesFragment.setTargetFragment(this@BusinessCardDetailFragment, REQUEST_ADD_NOTES)
            addFragment(MainActivity.mActivity, R.id.container, addNotesFragment, "AddNotesFragment")
        }

        binding.tvNotes.setOnClickListener {
            if (binding.tvNotes.hint.toString() == "Add Notes Here") {
                releasePlayer()
                val addNotesFragment = AddNotesFragment(cardId, binding.tvNotes.text.toString(), false, false)
                addNotesFragment.setTargetFragment(this@BusinessCardDetailFragment, REQUEST_ADD_NOTES)
                addFragment(MainActivity.mActivity, R.id.container, addNotesFragment, "AddNotesFragment")
            }
        }

        binding.ivVolume.setOnClickListener {
            if (player.volume == 0F) {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_up)
                player.volume = 1.0F
            }
            else {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_down)
                player.volume = 0F
            }
        }

        binding.btnAddContact.setOnClickListener {
            if (fromContactRequest) {
                addContactRequest()
            }
            else {
                addContact()
            }
        }
    }

    private fun addContact() {
        val call = Hello.apiService.addContact(AddContactRequestModel(cardId, false, null, null, null))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.ADD_CONTACT_URL, true, this)
    }

    private fun addContactRequest() {
        val call = Hello.apiService.addContactRequest(AddContactRequestIdModel(id))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.ADD_CONTACT_REQUEST_URL, true, this)
    }

    private fun dialogEnlargeCard(url: String) {

        val dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogEnlargeCardBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_enlarge_card, null, false)
        dialog.setContentView(binding.root)

        Glide.with(MainActivity.mActivity).load(url).into(binding.ivCustomBusinessCard)

        dialog.show()
    }

    private fun setUpCard(startColor: String, endColor: String) {

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(startColor), Color.parseColor(endColor)))
        gd.cornerRadius = 0f
        binding.rlColor.setBackgroundDrawable(gd)
    }

    private fun setUpCardForPhotoAlbum(startColor: String, endColor: String) {

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(startColor), Color.parseColor(endColor)))
        gd.cornerRadius = 0f
        binding.rlColor1.setBackgroundDrawable(gd)
    }

    private fun initializeRecyclerView(socialMedia: ArrayList<SocialMediaItem>) {
        mList.clear()
        val list = socialMedia.filter { it.isPublic }
        list.forEachIndexed { index, socialMediaItem ->
            when (socialMediaItem.tag) {
                Constants.facebook -> mList.add(SocialModel(true, Constants.facebook, R.drawable.ic_facebook, R.drawable.ic_facebook_filled, socialMediaItem.url))
                Constants.whatsapp -> mList.add(SocialModel(true, Constants.whatsapp, R.drawable.ic_whatsapp, R.drawable.ic_whatsapp_filled, socialMediaItem.url))
                Constants.twitter -> mList.add(SocialModel(true, Constants.twitter, R.drawable.ic_twitter, R.drawable.ic_twitter_filled, socialMediaItem.url))
                Constants.instagram -> mList.add(SocialModel(true, Constants.instagram, R.drawable.ic_instagram, R.drawable.ic_instagram_filled, socialMediaItem.url))
                Constants.skype -> mList.add(SocialModel(true, Constants.skype, R.drawable.ic_skype, R.drawable.ic_skype_filled, socialMediaItem.url))
                Constants.linkedin -> mList.add(SocialModel(true, Constants.linkedin, R.drawable.ic_linkedin, R.drawable.ic_linkedin_filled, socialMediaItem.url))
                Constants.youtube -> mList.add(SocialModel(true, Constants.youtube, R.drawable.ic_youtube, R.drawable.ic_youtube_filled, socialMediaItem.url))
                Constants.tiktok -> mList.add(SocialModel(true, Constants.tiktok, R.drawable.ic_tiktok, R.drawable.ic_tiktok_filled, socialMediaItem.url))
                Constants.venmo -> mList.add(SocialModel(true, Constants.venmo, R.drawable.ic_venmo, R.drawable.ic_venmo_filled, socialMediaItem.url))
                Constants.payPal -> mList.add(SocialModel(true, Constants.payPal, R.drawable.ic_paypal, R.drawable.ic_paypal_filled, socialMediaItem.url))
                Constants.cashApp -> mList.add(SocialModel(true, Constants.cashApp, R.drawable.ic_cashapp, R.drawable.ic_cashapp_filled, socialMediaItem.url))
                Constants.zelle -> mList.add(SocialModel(true, Constants.zelle, R.drawable.ic_zelle, R.drawable.ic_zelle_filled, socialMediaItem.url))
                Constants.yelp -> mList.add(SocialModel(true, Constants.yelp, R.drawable.ic_yelp, R.drawable.ic_yelp_filled, socialMediaItem.url))
                Constants.doorDash -> mList.add(SocialModel(true, Constants.doorDash, R.drawable.ic_dd, R.drawable.ic_dd_filled, socialMediaItem.url))
                Constants.grubHub -> mList.add(SocialModel(true, Constants.grubHub, R.drawable.ic_grubhub, R.drawable.ic_grubhub_filled, socialMediaItem.url))
                Constants.uberEats -> mList.add(SocialModel(true, Constants.uberEats, R.drawable.ic_uber, R.drawable.ic_uber_filled, socialMediaItem.url))
                Constants.reddit -> mList.add(SocialModel(true, Constants.reddit, R.drawable.ic_reddit, R.drawable.ic_reddit_filled, socialMediaItem.url))
                Constants.pinterest -> mList.add(SocialModel(true, Constants.pinterest, R.drawable.ic_pinterest, R.drawable.ic_pinterst_filled, socialMediaItem.url))
                Constants.goToMeeting -> mList.add(SocialModel(true, Constants.goToMeeting, R.drawable.ic_goto, R.drawable.ic_goto_filled, socialMediaItem.url))
                Constants.zoom -> mList.add(SocialModel(true, Constants.zoom, R.drawable.ic_zoom, R.drawable.ic_zoom_filled, socialMediaItem.url))
            }
        }

        if (mList.isNotEmpty()) {
        mList.sortBy { it.socialName }
        adapterSocialDetails = SocialDetailsAdapter(mList)
        adapterSocialDetails?.mListener = this
        binding.apply {
            recyclerView.apply {
                layoutManager = GridLayoutManager(context, 5)
                adapter = adapterSocialDetails
            }
        }
        }
        else {
            binding.llRecyclerView.visibility = View.GONE
        }
    }

    override fun onItemClick(position: Int) {
        when (mList[position].socialName) {
            Constants.zelle -> {
                openSocialDialog(mList[position])
            }
            Constants.bumble -> {
                openSocialDialog(mList[position])
            }
            Constants.nintendo -> {
                openSocialDialog(mList[position])
            }
            Constants.signal -> {
                openSocialDialog(mList[position])
            }
            Constants.skype -> {
                openSocialDialog(mList[position])
            }
            else -> {
                val i = Intent(Intent.ACTION_VIEW)
                val link = getLink(mList[position].socialName, mList[position].socialLink)
                i.setData(Uri.parse(link))
                MainActivity.mActivity.startActivity(i)
            }
        }
    }

    private fun openSocialDialog(model: SocialModel) {
        val dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogSocialBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.mActivity), R.layout.dialog_social, null, false)
        dialog.setContentView(binding.root)

        binding.imageViewSocial.setImageResource(model.socialDrawableFilled)
        binding.textViewTitle.text = (model.socialName)
        binding.textViewSubTitle.text = (model.socialName)

        binding.editTextUsername.setText(model.socialLink)
        binding.editTextUsername.isEnabled = false
        binding.switchPublic.visibility = View.GONE
        binding.buttonSave.visibility = View.GONE
        binding.textViewPublic.visibility = View.INVISIBLE

        dialog.show()
    }

    override fun success(apiName: String, response: Any?) {
        when (apiName) {
            Constants.GET_CONTACTS_DETAILS_URL -> {
                val model = response as GetCardDetailsResponse
                if (model.isError!!) {
                    MainActivity.mActivity.showToastMessage(model.message!!)
                }
                else {
                    binding.rlParent.visibility = View.VISIBLE

                    setUpCard(model.data!!.businessCard!!.cardStartColor!!, model.data.businessCard!!.cardEndColor!!)
                    setCardData(model.data.businessCard)

                    setUpCardForPhotoAlbum(model.data.businessCard.cardStartColor!!, model.data.businessCard.cardEndColor!!)
                    setCardDataForPhotoAlbum(model.data.businessCard)

                    if (model.data.businessCard.socialMedia != null && model.data.businessCard.socialMedia.isNotEmpty()) {
                        initializeRecyclerView(model.data.businessCard.socialMedia)
                    }
                    else {
                        binding.llRecyclerView.visibility = View.GONE
                    }

                    if (model.data.businessCard.businessCardUrl != null && model.data.businessCard.businessCardUrl.isNotEmpty()) {
                        binding.ivEnlargeCard.setOnClickListener {
                            dialogEnlargeCard(model.data.businessCard.businessCardUrl)
                        }
                    }
                    else {
                        binding.ivEnlargeCard.visibility = View.GONE
                    }

                    otherItemsModel = OtherItemsModel()
                    otherItemsModel!!.businessName = model.data.businessCard.businessName
                    otherItemsModel!!.name = model.data.businessCard.contactName

                    if (model.data.businessCard.isPhonePublic!!) {
                        otherItemsModel!!.countryCode = model.data.businessCard.countryCode
                        otherItemsModel!!.phone = model.data.businessCard.phone
                    }

                    if (model.data.businessCard.isEmailPublic!!) {
                        otherItemsModel!!.email = model.data.businessCard.email
                    }
                }
            }
            Constants.ADD_CONTACT_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    releasePlayer()
                    val addNotesFragment = AddNotesFragment(cardId, binding.tvNotes.text.toString(), false, false)
                    addNotesFragment.setTargetFragment(this@BusinessCardDetailFragment, REQUEST_ADD_NOTES)
                    addFragment(MainActivity.mActivity, R.id.container, addNotesFragment, "AddNotesFragment")
                }
            }
            Constants.ADD_CONTACT_REQUEST_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    releasePlayer()
                    val addNotesFragment = AddNotesFragment(cardId, binding.tvNotes.text.toString(), false, false)
                    addNotesFragment.setTargetFragment(this@BusinessCardDetailFragment, REQUEST_ADD_NOTES_CONTACT_REQUEST)
                    addFragment(MainActivity.mActivity, R.id.container, addNotesFragment, "AddNotesFragment")
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
        MainActivity.mActivity.showToastMessage(message!!)
    }

    fun loadVideo() {
        if (businessCardData!!.isVideo!!) {
            binding.rlVideoView.visibility = View.VISIBLE
            initializePlayer(binding.videoView, businessCardData?.url!!)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "Fire") {
            loadVideo()
        }
    }

    fun setCardData(model: BusinessCard1) {
        businessCardData = model
        Glide.with(MainActivity.mActivity).load(model.logoThumbnail).placeholder(R.drawable.ic_logo_placeholder).into(binding.ivLogo)
        if (model.isVideo!!) {
            loadVideo()
        }
        else {
            binding.ivVolume.visibility = View.GONE
            Glide.with(MainActivity.mActivity).load(model.url).into(binding.ivAvatar)

            if(model.thumbnail!!.isNotEmpty()) {
                Glide.with(MainActivity.mActivity).load(model.thumbnail).into(binding.ivUser)
            }
        }
        binding.apply {
            tvCompanyName.text = model.businessName
            tvName.text = model.contactName
            if (model.isPhonePublic!!) {
                tvPhone.text = model.countryCode + model.phone + ""
            }
            else {
                tvPhone.visibility = View.GONE
            }
            if (model.isEmailPublic!!) {
                tvEmail.text = model.email
            }
            else {
                tvEmail.visibility = View.GONE
            }
            if (model.meetOn != null) {
                if (model.latitude != null) {
                    tvAddress.text = DateUtils.getDateFromString(model.meetOn!!) + " - " + model.address
                }
                else {
                    tvAddress.text = DateUtils.getDateFromString(model.meetOn!!)
                }

                if (model.notes != null) {
                    if(model.notes.isNotEmpty()){
                        tvNotes.text = model.notes
                        tvNotes.isEnabled = false
                        tvNotes.isClickable = false
                        rlNotes.visibility = View.VISIBLE
                    }else {
                        tvNotes.text = ""
                        tvNotes.isEnabled = true
                        tvNotes.isClickable = true
                        rlNotes.visibility = View.GONE
                    }
                }
                else {
                    rlNotes.visibility = View.GONE
                }
                llNotes.visibility = View.VISIBLE
                llMetOn.visibility = View.VISIBLE
                btnAddContact.visibility = View.GONE
                buttonDetail.visibility = View.VISIBLE
            }
            else {
                llNotes.visibility = View.GONE
                llMetOn.visibility = View.GONE
                btnAddContact.visibility = View.VISIBLE
                buttonDetail.visibility = View.GONE
            }
        }
    }

    private fun setCardDataForPhotoAlbum(model: BusinessCard1) {

        Glide.with(MainActivity.mActivity).load(model.logoUrl).placeholder(R.drawable.ic_logo_placeholder).into(binding.ivLogo1)
        if (model.isVideo!!) {
            Glide.with(MainActivity.mActivity).load(model.thumbnail).into(binding.ivAvatar1)
        }
        else {
            Glide.with(MainActivity.mActivity).load(model.url).into(binding.ivAvatar1)
        }
        binding.apply {
            tvCompanyName1.text = model.businessName
            tvName1.text = model.contactName
            if (model.isPhonePublic!!) {
                tvPhone1.text = model.countryCode + model.phone + ""
            }
            else {
                tvPhone1.visibility = View.GONE
            }
            if (model.isEmailPublic!!) {
                tvEmail1.text = model.email
            }
            else {
                tvEmail1.visibility = View.GONE
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_ADD_NOTES -> { //                    val notes = data!!.getStringExtra("notes")!!
                    //                    if (notes.isNotEmpty()) {
                    //                        binding.tvNotes.text = notes
                    //                        binding.tvNotes.isEnabled = false
                    //                        binding.tvNotes.isClickable = false
                    //                        binding.rlNotes.visibility = View.VISIBLE
                    //                    }
                    getCardDetails()
                }
                REQUEST_ADD_NOTES_CONTACT_REQUEST -> {
                    MainActivity.mActivity.showToastMessage("Contact Added")
                    MainActivity.mActivity.onBackPressed()
                    apiCall?.reCallGetContactRequestApi()
                }
            }
        }
        else {
            getCardDetails()
        }
    }

    fun getBitmapFromView(view: View): Bitmap {
        val returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable: Drawable? = view.getBackground()
        if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return returnedBitmap
    }

    private fun initializePlayer(videoView: PlayerView, url: String) {
        binding.ivVolume.setImageResource(R.drawable.ic_volume_up)

        httpDataSourceFactory = DefaultHttpDataSource.Factory().setAllowCrossProtocolRedirects(true)

        defaultDataSourceFactory = DefaultDataSourceFactory(requireContext(), httpDataSourceFactory)

        cacheDataSourceFactory = CacheDataSource.Factory().setCache(Hello.simpleCache).setUpstreamDataSourceFactory(httpDataSourceFactory).setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

        player = SimpleExoPlayer.Builder(requireContext()).setMediaSourceFactory(DefaultMediaSourceFactory(cacheDataSourceFactory)).build()
        val videoUri = Uri.parse(url)
        val mediaItem = MediaItem.fromUri(videoUri)
        val mediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem)

        videoView.player = player
        videoView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL;
        videoView.hideController()
        player.playWhenReady = true
        player.seekTo(0, 0)
        player.repeatMode = Player.REPEAT_MODE_ALL
        player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
        player.setMediaSource(mediaSource, true)
        val audioAttributes: AudioAttributes = AudioAttributes.Builder().setUsage(C.USAGE_MEDIA).setContentType(C.CONTENT_TYPE_MOVIE).build()
        player.setAudioAttributes(audioAttributes, /* handleAudioFocus= */ true);
        player.prepare()

        player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    SimpleExoPlayer.STATE_ENDED -> {

                    }
                    SimpleExoPlayer.STATE_BUFFERING -> {
                        binding.pbBuffering.visibility = View.VISIBLE
                    }
                }
            }

            override fun onIsPlayingChanged(isPlaying: Boolean) {
                if (isPlaying) {
                    binding.rlVideoView.visibility = View.VISIBLE
                    binding.pbBuffering.visibility = View.GONE
                    binding.ivVolume.visibility = View.VISIBLE
                }
                else {
                    binding.pbBuffering.visibility = View.VISIBLE
                }
            }

            override fun onPlayerError(error: ExoPlaybackException) {
                Log.e("exoPlayer", error.message!!)
            }
        })
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(MainActivity.mActivity, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    fun releasePlayer() {
        if (businessCardData!!.isVideo!!) {
            player.run {
                playbackPosition = this.currentPosition
                currentWindow = this.currentWindowIndex
                playWhenReady = this.playWhenReady
                release()
            }
        }

    }


    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this);
        releasePlayer()
    }

    interface ApiCall{
        fun reCallGetContactRequestApi()
    }
}