package com.iinshaapps.hello.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.databinding.DashboardFragmentBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.MessageEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class DashBoardFragment : BaseFragment() {

    lateinit var binding: DashboardFragmentBinding

    private lateinit var homeFragment: HomeFragment
    lateinit var contactsFragment: ContactsFragment
    private lateinit var settingsFragment: SettingsFragment

    private val fragments: Array<Fragment>
        get() = arrayOf(homeFragment, contactsFragment, settingsFragment)

    val selectedFragment get() = fragments[MainActivity.selectedIndex]

    fun selectFragment(selectedFragment: Fragment) {
        var transaction = activity?.supportFragmentManager?.beginTransaction()
        fragments.forEachIndexed { index, fragment ->
            if (selectedFragment == fragment) {
                transaction = transaction?.attach(fragment)
                MainActivity.selectedIndex = index
            }
            else {
                transaction = transaction?.detach(fragment)
            }
        }
        transaction?.commit()

        binding.apply {
            when (selectedFragment) {
                is HomeFragment -> {
                    navHome.setImageResource(R.drawable.ic_home_filled)
                    navContacts.setImageResource(R.drawable.ic_contacts)
                    navSettings.setImageResource(R.drawable.ic_settings)
                }
                is ContactsFragment -> {
                    navHome.setImageResource(R.drawable.ic_home)
                    navContacts.setImageResource(R.drawable.ic_contacts_filled)
                    navSettings.setImageResource(R.drawable.ic_settings)
                }
                is SettingsFragment -> {
                    navHome.setImageResource(R.drawable.ic_home)
                    navContacts.setImageResource(R.drawable.ic_contacts)
                    navSettings.setImageResource(R.drawable.ic_settings_filled)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment, container, false)

        if (savedInstanceState == null) {
            homeFragment = HomeFragment()
            contactsFragment = ContactsFragment()
            settingsFragment = SettingsFragment()

            activity?.supportFragmentManager?.beginTransaction()
                ?.add(R.id.fragment_container, homeFragment, TAG_HOME_FRAGMENT)
                ?.add(R.id.fragment_container, contactsFragment, TAG_CONTACTS_FRAGMENT)
                ?.add(R.id.fragment_container, settingsFragment, TAG_SETTINGS_FRAGMENT)?.commit()
        }
        else {
            homeFragment = activity?.supportFragmentManager?.findFragmentByTag(TAG_HOME_FRAGMENT) as HomeFragment
            contactsFragment = activity?.supportFragmentManager?.findFragmentByTag(TAG_CONTACTS_FRAGMENT) as ContactsFragment
            settingsFragment = activity?.supportFragmentManager?.findFragmentByTag(TAG_SETTINGS_FRAGMENT) as SettingsFragment

            MainActivity.selectedIndex = savedInstanceState.getInt(KEY_SELECTED_INDEX, 0)
        }

        selectFragment(selectedFragment)

        binding.apply {
            navHome.setOnClickListener {
                selectFragment(homeFragment)
            }
            navContacts.setOnClickListener {
                selectFragment(contactsFragment)
            }
            navSettings.setOnClickListener {
                selectFragment(settingsFragment)
            }
        }

        manageNotification()

        return binding.root
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "notify") {
            manageNotification()
        }
    }

    private fun manageNotification() {
        if(Hello.db.getBoolean(Constants.HAVE_NOTIFICATION, false)){
            binding.ivNotification.visibility = View.VISIBLE
        }else {
            binding.ivNotification.visibility = View.GONE
        }
    }
}

private const val TAG_HOME_FRAGMENT = "TAG_HOME_FRAGMENT"
private const val TAG_CONTACTS_FRAGMENT = "TAG_CONTACTS_FRAGMENT"
private const val TAG_SETTINGS_FRAGMENT = "TAG_SETTINGS_FRAGMENT"
private const val KEY_SELECTED_INDEX = "KEY_SELECTED_INDEX"