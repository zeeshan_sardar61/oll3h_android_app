package com.iinshaapps.hello.fragment

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ContentProviderOperation
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.ContactsContract
import android.provider.MediaStore
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.github.ramiz.nameinitialscircleimageview.NameInitialsCircleImageView
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.DialogRestoreConfirmationBinding
import com.iinshaapps.hello.databinding.FragmentNonAppUserRequestBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.MediaBottomSheetFragment
import com.iinshaapps.hello.helper.MessageEvent
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.requestmodel.AddContactRequestIdModel
import com.iinshaapps.hello.model.requestmodel.DeleteContactRequestModel
import com.iinshaapps.hello.model.response.ContactRequestModel
import com.iinshaapps.hello.model.response.RecoveryCodeResponse
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class NonAppUserRequestFragment(val contactRequestModel: ContactRequestModel, val apiCall: ApiCall? = null, val isFromRequest: Boolean = false) : BaseFragment(), IGenericCallBack, MediaBottomSheetFragment.ISelectListener {

    private val CONTACTS_PERMISSION = 1
    private val STORAGE_PERMISSION = 2
    private val REQUEST_ADD_NOTES = 100
    private val REQUEST_EDIT_NOTES = 200
    private lateinit var binding: FragmentNonAppUserRequestBinding

    private lateinit var bottomSheetListFragment: MediaBottomSheetFragment
    private var itemList: ArrayList<String> = ArrayList()
    val opList = ArrayList<ContentProviderOperation>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_non_app_user_request, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (isFromRequest) {
            binding.tvTitle.text = "Save Contact Request"
            binding.btnAddContact.visibility = View.VISIBLE

            binding.cvImage.visibility = View.GONE

            val cardViewMarginParams = binding.rlCards.layoutParams as ViewGroup.MarginLayoutParams
            cardViewMarginParams.topMargin = MainActivity.mActivity.resources.getDimension(R.dimen._5sdp).toInt()
            binding.rlCards.requestLayout()
        }
        else {
            binding.tvTitle.text = ""
            binding.btnAddContact.visibility = View.GONE
            binding.buttonDetail.visibility = View.VISIBLE
            binding.llNotes.visibility = View.VISIBLE

            binding.apply {
                if (contactRequestModel.notes != null) {
                    if(contactRequestModel.notes.isNotEmpty()){
                        tvNotes.text = contactRequestModel.notes
                        tvNotes.isEnabled = false
                        tvNotes.isClickable = false
                        rlNotes.visibility = View.VISIBLE
                    }else {
                        tvNotes.text = ""
                        tvNotes.isEnabled = true
                        tvNotes.isClickable = true
                        rlNotes.visibility = View.GONE
                    }
                }
                else {
                    rlNotes.visibility = View.GONE
                }
            }
        }

        if (contactRequestModel.isPersonal) {
            setPersonalCard()
        }
        else {
            setBusinessCard()
        }

        setListeners()
    }

    private fun setListeners() {

        binding.buttonBack.setOnClickListener {
            MainActivity.mActivity.onBackPressed()
        }

        binding.btnAddContact.setOnClickListener {
            addContactRequest()
        }

        binding.tvPhonePersonal.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", binding.tvPhonePersonal.text.toString(), null))
            startActivity(intent)
        }

        binding.tvEmailPersonal.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(binding.tvEmailPersonal.text.toString()))
            intent.putExtra(Intent.EXTRA_SUBJECT, "From your dear friend: ")
            MainActivity.mActivity.startActivity(intent)
        }

        binding.tvPhoneBusiness.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", binding.tvPhoneBusiness.text.toString(), null))
            startActivity(intent)
        }

        binding.tvEmailBusiness.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(binding.tvEmailBusiness.text.toString()))
            intent.putExtra(Intent.EXTRA_SUBJECT, "From your dear friend: ")
            MainActivity.mActivity.startActivity(intent)
        }

        binding.buttonDetail.setOnClickListener {
            showDetailsDialog()
        }

        binding.rlNotes.setOnClickListener {
            val addNotesFragment = AddNotesFragment(contactRequestModel.cardId!!, binding.tvNotes.text.toString(), false, contactRequestModel.isPersonal)
            addNotesFragment.setTargetFragment(this@NonAppUserRequestFragment, REQUEST_EDIT_NOTES)
            addFragment(MainActivity.mActivity, R.id.container, addNotesFragment, "AddNotesFragment")
        }

        binding.tvNotes.setOnClickListener {
            if (binding.tvNotes.hint.toString() == "Add Notes Here") {
                val addNotesFragment = AddNotesFragment(contactRequestModel.cardId!!, binding.tvNotes.text.toString(), false, contactRequestModel.isPersonal)
                addNotesFragment.setTargetFragment(this@NonAppUserRequestFragment, REQUEST_EDIT_NOTES)
                addFragment(MainActivity.mActivity, R.id.container, addNotesFragment, "AddNotesFragment")
            }
        }
    }

    private fun setPersonalCard() {

        val initials = contactRequestModel.name.split(' ').mapNotNull { it.firstOrNull()?.toString() }.reduce { acc, s -> acc + s }

        //        if(!initials.contains(" ")) {
        //            initials += " "
        //        }

        if (initials.length > 2) {
            binding.iivPersonal.setImageInfo(NameInitialsCircleImageView.ImageInfo.Builder(initials.substring(0, 2).toUpperCase()).setTextColor(R.color.white).setCircleBackgroundColorRes(R.color.grey).build())
        }
        else {
            binding.iivPersonal.setImageInfo(NameInitialsCircleImageView.ImageInfo.Builder(initials.toUpperCase()).setTextColor(R.color.white).setCircleBackgroundColorRes(R.color.grey).build())
        }

        binding.tvNamePersonal.text = contactRequestModel.name

        if (PhoneNumberUtils.isGlobalPhoneNumber("${contactRequestModel.countryCode}${contactRequestModel.phoneNo}")) {
            binding.tvPhonePersonal.text = "${contactRequestModel.countryCode} ${contactRequestModel.phoneNo}"
        }
        else {
            binding.tvPhonePersonal.text = ""
        }

        binding.tvEmailPersonal.text = contactRequestModel.email

        binding.llCardPersonal.visibility = View.VISIBLE
    }

    private fun setBusinessCard() {

        binding.tvBusinessName.text = contactRequestModel.businessName
        binding.tvNameBusiness.text = contactRequestModel.name

        if (PhoneNumberUtils.isGlobalPhoneNumber("${contactRequestModel.countryCode}${contactRequestModel.phoneNo}")) {
            binding.tvPhoneBusiness.text = "${contactRequestModel.countryCode} ${contactRequestModel.phoneNo}"
        }
        else {
            binding.tvPhoneBusiness.text = ""
        }

        binding.tvEmailBusiness.text = contactRequestModel.email

        binding.llCardBusiness.visibility = View.VISIBLE
    }

    private fun addContactRequest() {
        val call = Hello.apiService.addContactRequest(AddContactRequestIdModel(contactRequestModel.id!!))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.ADD_CONTACT_REQUEST_URL, true, this)
    }

    override fun success(apiName: String, response: Any?) {

        when (apiName) {
            Constants.ADD_CONTACT_REQUEST_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    showDialog()
                }
            }
            Constants.REMOVE_CARD_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    EventBus.getDefault().post(MessageEvent("api"))
                    MainActivity.mActivity.onBackPressed()
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_ADD_NOTES -> {
                    MainActivity.mActivity.showToastMessage("Contact Added")
                    MainActivity.mActivity.onBackPressed()
                    apiCall?.reCallGetContactRequestApi()
                }
                REQUEST_EDIT_NOTES -> {
                    if(data != null) {
                        val notes = data.getStringExtra("notes")
                        binding.tvNotes.text = notes
                        EventBus.getDefault().post(MessageEvent("api"))
                    }
                }
            }
        }
    }

    interface ApiCall {
        fun reCallGetContactRequestApi()
    }

    private fun showDialog() {

        val dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogRestoreConfirmationBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.mActivity), R.layout.dialog_restore_confirmation, null, false)
        dialog.setContentView(binding.root)

        binding.tvTitle.text = "Want to add notes?"
        binding.tvDescription.text = "After continue, you will be redirected to notes screen."

        binding.tvContinue.setOnClickListener {
            dialog.dismiss()
            val addNotesFragment = AddNotesFragment(contactRequestModel.id!!, "", false, contactRequestModel.isPersonal)
            addNotesFragment.setTargetFragment(this@NonAppUserRequestFragment, REQUEST_ADD_NOTES)
            addFragment(MainActivity.mActivity, R.id.container, addNotesFragment, "AddNotesFragment")
        }

        binding.tvNo.setOnClickListener {
            dialog.dismiss()
            MainActivity.mActivity.onBackPressed()
            apiCall?.reCallGetContactRequestApi()
        }

        dialog.show()
    }

    private fun showDeleteCardDialog() {

        val dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogRestoreConfirmationBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.mActivity), R.layout.dialog_restore_confirmation, null, false)
        dialog.setContentView(binding.root)

        binding.tvTitle.text = "Remove Card"
        binding.tvDescription.text = "After continue, this card will be deleted. Are you sure, you want to continue."

        binding.tvContinue.setOnClickListener {
            dialog.dismiss()
            removeCard()
        }

        binding.tvNo.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun removeCard() {
        val call = Hello.apiService.removeCard(DeleteContactRequestModel(contactRequestModel.cardId!!, contactRequestModel.isPersonal, false, null, null, null))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.REMOVE_CARD_URL, true, this)
    }

    private fun showDetailsDialog() {

        itemList.clear()
        itemList.add("Add to Directory")
        itemList.add("Add to Photo Album")
        itemList.add("Remove Card")

        bottomSheetListFragment = MediaBottomSheetFragment(MainActivity.mActivity, itemList, "")
        bottomSheetListFragment.setMyListener(this)
        if (!bottomSheetListFragment.isAdded) {
            bottomSheetListFragment.show(MainActivity.mActivity.supportFragmentManager, "")
        }
    }

    override fun onMediaSelect(pos: Int) {
        when (itemList[pos]) {
            "Add to Directory" -> {

                initAddToContacts()
                addFullName(contactRequestModel.name)
                addPhone(contactRequestModel.countryCode + contactRequestModel.phoneNo)
                addEmail(contactRequestModel.email!!)

                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                    executeAddToContacts()
                }
                else {
                    requestPermissions(arrayOf(Manifest.permission.WRITE_CONTACTS), CONTACTS_PERMISSION)
                }
            }
            "Add to Photo Album" -> {

                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    saveToGallery("photo_" + System.currentTimeMillis() + ".png", "", "Contact Photo Saved", Bitmap.CompressFormat.PNG, 50)
                }
                else {
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION)
                }
            }
            "Remove Card" -> {
                showDeleteCardDialog()
            }
        }
    }

    override fun onMediaCancel() {
    }

    private fun initAddToContacts() {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null).withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build())
    }

    private fun addFullName(name: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, name).build())
    }

    private fun addPhone(phone: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone).withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build())
    }

    private fun addEmail(email: String) {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.Email.DATA, email).withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK).build())
    }

    private fun addNotes() {

        opList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE).withValue(ContactsContract.CommonDataKinds.Note.NOTE, "").build())
    }

    private fun executeAddToContacts() {

        try {
            val results = MainActivity.mActivity.contentResolver.applyBatch(ContactsContract.AUTHORITY, opList)
            Log.d("TAGO", "try: ")
            MainActivity.mActivity.showToastMessage("Contact added")
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("TAGO", "catch: ${e.message}")
        }
    }

    fun getBitmapFromView(view: View): Bitmap {
        val returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable: Drawable? = view.getBackground()
        if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return returnedBitmap
    }

    fun saveToGallery(fileName: String, subFolderPath: String, fileDescription: String?, format: Bitmap.CompressFormat?, quality: Int): Boolean {

        var fileName = fileName
        var quality = quality
        if (quality < 0 || quality > 100) quality = 50
        val currentTime = System.currentTimeMillis()
        val extBaseDir = Environment.getExternalStorageDirectory()
        val file = File(extBaseDir.absolutePath + "/DCIM/" + subFolderPath)
        if (!file.exists()) {
            if (!file.mkdirs()) {
                return false
            }
        }
        var mimeType = ""
        when (format) {
            Bitmap.CompressFormat.PNG -> {
                mimeType = "image/png"
                if (!fileName.endsWith(".png")) fileName += ".png"
            }
            Bitmap.CompressFormat.WEBP -> {
                mimeType = "image/webp"
                if (!fileName.endsWith(".webp")) fileName += ".webp"
            }
            Bitmap.CompressFormat.JPEG -> {
                mimeType = "image/jpeg"
                if (!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))) fileName += ".jpg"
            }
            else -> {
                mimeType = "image/jpeg"
                if (!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))) fileName += ".jpg"
            }
        }
        val filePath = file.absolutePath + "/" + fileName
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(filePath)
            getBitmapFromView(binding.rlWhole).compress(format, quality, out)
            out.flush()
            out.close()
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        val size = File(filePath).length()
        val values = ContentValues(8)

        // store the details
        values.put(MediaStore.Images.Media.TITLE, fileName)
        values.put(MediaStore.Images.Media.DISPLAY_NAME, fileName)
        values.put(MediaStore.Images.Media.DATE_ADDED, currentTime)
        values.put(MediaStore.Images.Media.MIME_TYPE, mimeType)
        values.put(MediaStore.Images.Media.DESCRIPTION, fileDescription)
        values.put(MediaStore.Images.Media.ORIENTATION, 0)
        values.put(MediaStore.Images.Media.DATA, filePath)
        values.put(MediaStore.Images.Media.SIZE, size)

        MainActivity.mActivity.showToastMessage("Photo saved")

        return MainActivity.mActivity.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values) != null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CONTACTS_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    executeAddToContacts()
                }
                else {
                    MainActivity.mActivity.showToastMessage("Please allow permission to write contacts.")
                }
            }
            STORAGE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveToGallery("photo_" + System.currentTimeMillis() + ".png", "", "Contact Photo Saved", Bitmap.CompressFormat.PNG, 50)
                }
                else {
                    MainActivity.mActivity.showToastMessage("Please allow permission to save to photo album.")
                }
            }
        }
    }
}