package com.iinshaapps.hello.fragment

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.BusinessCardAdapter
import com.iinshaapps.hello.adapter.PersonalCardAdapter
import com.iinshaapps.hello.adapter.PreCachingLayoutManager
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.FragmentContactsBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.MessageEvent
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.MyCardModel
import com.iinshaapps.hello.model.response.ContactRequestModel
import com.iinshaapps.hello.model.response.GetMyContactsResponseModel
import com.iinshaapps.hello.model.response.MyContactsData
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ContactsFragment : BaseFragment(), PersonalCardAdapter.PersonalItemClicker, BusinessCardAdapter.BusinessItemClicker, IGenericCallBack {

    private val CAMERA_PERMISSION = 2
    private var REQUEST_QR_CODE = 100

    lateinit var binding: FragmentContactsBinding
    var mList = ArrayList<MyCardModel>()
    var mCopyList = ArrayList<MyCardModel>()
    lateinit var personalAdapter: PersonalCardAdapter
    lateinit var businessAdapter: BusinessCardAdapter
    private var previousPosition: Int = -1
    private var currentAdapter = CurrentAdapter.PersonalAdapter
    private var myContactsData: MyContactsData? = null

    companion object {
        var isSearchOpened = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contacts, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        callGetMyContactsApi()
    }

    private fun initViews() {

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        binding.apply {
            tvPersonal.setOnClickListener {

                if (currentAdapter == CurrentAdapter.BusinessAdapter) {
                    tvHeading.text = MainActivity.mActivity.resources.getString(R.string.personal)
                    tvPersonal.setTextColor(resources.getColor(R.color.yellow))
                    tvBusiness.setTextColor(resources.getColor(R.color.white))

                    previousPosition = -1
                    currentAdapter = CurrentAdapter.PersonalAdapter
                    onCrossClicked()
                    initializePersonalItemList()
                }
            }

            tvBusiness.setOnClickListener {

                if (currentAdapter == CurrentAdapter.PersonalAdapter) {
                    tvHeading.text = MainActivity.mActivity.resources.getString(R.string.business)
                    tvBusiness.setTextColor(resources.getColor(R.color.yellow))
                    tvPersonal.setTextColor(resources.getColor(R.color.white))

                    previousPosition = -1
                    currentAdapter = CurrentAdapter.BusinessAdapter
                    onCrossClicked()
                    initializeBusinessItemList()
                }
            }

            buttonSearch.setOnClickListener {
                onSearchClicked()
            }

            buttonCross.setOnClickListener {
                onCrossClicked()
            }
        }

        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty()) {
                    val mIterationList = ArrayList<MyCardModel>()
                    mIterationList.addAll(mList)
                    mCopyList.clear()
                    for (i in mIterationList.indices) {
                        if (mIterationList[i].name.toLowerCase().contains(s.toString())) {
                            mCopyList.add(mIterationList[i])
                        }
                    }
                    personalAdapter.notifyDataSetChanged()
                    businessAdapter.notifyDataSetChanged()
                }
                else {
                    mCopyList.clear()
                    mCopyList.addAll(mList)
                    personalAdapter.notifyDataSetChanged()
                    businessAdapter.notifyDataSetChanged()
                }
            }
        })

        binding.buttonQr.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                val scanQRFragment = ScanQRFragment()
                scanQRFragment.setTargetFragment(this@ContactsFragment, REQUEST_QR_CODE)
                addFragment(MainActivity.mActivity, R.id.container, scanQRFragment, "ScanQRFragment")
            }
            else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION), CAMERA_PERMISSION)
            }
        }
    }

    private fun callGetMyContactsApi() {

        val call = Hello.apiService.getMyContacts()
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.GET_MY_CONTACTS_URL, true, this)
    }

    private fun onSearchClicked() {

        binding.apply {
            rlSearchBar.visibility = View.VISIBLE
            etSearch.requestFocus()
            val imm: InputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT)
            mCopyList.map { it.isOpened = false }
            personalAdapter.notifyDataSetChanged()
            businessAdapter.notifyDataSetChanged()
            isSearchOpened = true
        }
    }

    fun onCrossClicked() {

        binding.apply {
            rlSearchBar.visibility = View.GONE
            val imm: InputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(etSearch.windowToken, 0)
            mCopyList.map { it.isOpened = false }
            etSearch.setText("")
            mCopyList.clear()
            mCopyList.addAll(mList)
            personalAdapter.notifyDataSetChanged()
            businessAdapter.notifyDataSetChanged()
            isSearchOpened = false
        }
    }

    private fun setPersonalRecyclerView() {

        val displayMetrics: DisplayMetrics = requireContext().resources.displayMetrics
        val layoutManager = PreCachingLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.setExtraLayoutSpace(displayMetrics.heightPixels)
        binding.rvPersonalCards.layoutManager = layoutManager //        mList.add(CardModel(0, "Alvin Boss", "+1321568789", -0x9e9d9f, -0xececed, false))
        //        mList.add(CardModel(1, "Boss Boss", "+1321556789", -0x453DB7, -0xD70651, false))
        //        mList.add(CardModel(2, "Naruto Boss", "+1321568749", -0xC09A35, -0xE30F02, false))
        //        mList.add(CardModel(3, "Mikasa Boss", "+1321568346", -0x37BDB2, -0x0268E3, false))
        //        mList.add(CardModel(4, "Eren Boss", "+1321456785", -0x8CBE3D, -0x06B3DE, false))
        //        mList.add(CardModel(5, "Erwin Boss", "+1421568789", -0x9e9d9f, -0xececed, false))
        //        mList.add(CardModel(6, "Jean Boss", "+176568789", -0x453DB7, -0xD70651, false))
        //        mList.add(CardModel(7, "Historia Boss", "+13438789", -0xC09A35, -0xE30F02, false))
        //        mList.add(CardModel(8, "Conny Boss", "+132345643", -0x37BDB2, -0x0268E3, false))
        //        mList.add(CardModel(9, "Sasha Boss", "+176532657", -0x8CBE3D, -0x06B3DE, false))
        personalAdapter = PersonalCardAdapter(mCopyList, this)
        personalAdapter.mListener = this
        binding.rvPersonalCards.adapter = personalAdapter

        val animator: RecyclerView.ItemAnimator? = binding.rvPersonalCards.itemAnimator
        if (animator is SimpleItemAnimator) {
            (animator as SimpleItemAnimator).supportsChangeAnimations = false
        }
    }

    private fun setBusinessRecyclerView() {

        val displayMetrics: DisplayMetrics = requireContext().resources.displayMetrics
        val layoutManager = PreCachingLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.setExtraLayoutSpace(displayMetrics.heightPixels)
        binding.rvBusinessCards.layoutManager = layoutManager
        businessAdapter = BusinessCardAdapter(mCopyList, this)
        businessAdapter.mListener = this
        binding.rvBusinessCards.adapter = businessAdapter

        val animator: RecyclerView.ItemAnimator? = binding.rvBusinessCards.itemAnimator
        if (animator is SimpleItemAnimator) {
            (animator as SimpleItemAnimator).supportsChangeAnimations = false
        }
    }

    override fun onPersonalItemClick(position: Int) {

        if (position < mCopyList.size - 1) {

            if (mCopyList[position + 1].isOpened) {
                if (mCopyList[position].isAppUser) {
                    addFragment(MainActivity.mActivity, R.id.container, PersonalCardDetailFragment(mCopyList[position].cardId), "PersonalCardDetailFragment")
                    return

                }
                else {

                    val contactRequestModel = ContactRequestModel(cardId = mCopyList[position].cardId, isPersonal = true, name = mCopyList[position].name, email = mCopyList[position].email, countryCode = mCopyList[position].countryCode, phoneNo = mCopyList[position].phone, notes = mCopyList[position].notes)

                    addFragment(MainActivity.mActivity, R.id.container, NonAppUserRequestFragment(contactRequestModel), "ContactRequestFragment")
                    return
                }
            }
        }
        else if (position == mCopyList.size - 1) {
            if (mCopyList[position].isAppUser) {
                addFragment(MainActivity.mActivity, R.id.container, PersonalCardDetailFragment(mCopyList[position].cardId), "PersonalCardDetailFragment")
                return
            }
            else {

                val contactRequestModel = ContactRequestModel(cardId = mCopyList[position].cardId,isPersonal = true, name = mCopyList[position].name, email = mCopyList[position].email, countryCode = mCopyList[position].countryCode, phoneNo = mCopyList[position].phone, notes = mCopyList[position].notes)

                addFragment(MainActivity.mActivity, R.id.container, NonAppUserRequestFragment(contactRequestModel), "ContactRequestFragment")
                return
            }
        }

        mCopyList.map { it.isOpened = false }
        if (position == 0) {
            mCopyList[position + 1].isOpened = true
        }
        else if (position < mList.size - 1) {
            mCopyList[position + 1].isOpened = true
        }

        if (previousPosition != -1) {
            personalAdapter.notifyItemChanged(previousPosition + 1)
        }
        personalAdapter.notifyItemChanged(position + 1)
        previousPosition = position

        binding.rvPersonalCards.scrollToPosition(position + 1)
    }

    override fun onBusinessItemClick(position: Int) {

        if (position < mCopyList.size - 1) {

            if (mCopyList[position + 1].isOpened) {
                if (mCopyList[position].isAppUser) {
                    addFragment(MainActivity.mActivity, R.id.container, BusinessCardDetailFragment(mCopyList[position].cardId), "BusinessCardDetailFragment")
                    return
                }
                else {

                    val contactRequestModel = ContactRequestModel(cardId = mCopyList[position].cardId,isPersonal = false, name = mCopyList[position].name, businessName = mCopyList[position].companyName, email = mCopyList[position].email, countryCode = mCopyList[position].countryCode, phoneNo = mCopyList[position].phone, notes = mCopyList[position].notes)

                    addFragment(MainActivity.mActivity, R.id.container, NonAppUserRequestFragment(contactRequestModel), "ContactRequestFragment")
                    return
                }
            }
        }
        else if (position == mCopyList.size - 1) {
            if (mCopyList[position].isAppUser) {
                addFragment(MainActivity.mActivity, R.id.container, BusinessCardDetailFragment(mCopyList[position].cardId), "BusinessCardDetailFragment")
                return
            }
            else {
                val contactRequestModel = ContactRequestModel(cardId = mCopyList[position].cardId, isPersonal = false, name = mCopyList[position].name, businessName = mCopyList[position].companyName, email = mCopyList[position].email, countryCode = mCopyList[position].countryCode, phoneNo = mCopyList[position].phone, notes = mCopyList[position].notes)

                addFragment(MainActivity.mActivity, R.id.container, NonAppUserRequestFragment(contactRequestModel), "ContactRequestFragment")
                return
            }
        }

        mCopyList.map { it.isOpened = false }
        if (position == 0) {
            mCopyList[position + 1].isOpened = true
        }
        else if (position < mList.size - 1) {
            mCopyList[position + 1].isOpened = true
        }

        if (previousPosition != -1) {
            businessAdapter.notifyItemChanged(previousPosition + 1)
        }
        businessAdapter.notifyItemChanged(position + 1)
        previousPosition = position

        binding.rvBusinessCards.scrollToPosition(position + 1)
    }

    private enum class CurrentAdapter {
        PersonalAdapter, BusinessAdapter
    }

    override fun onPause() {
        super.onPause()

        isSearchOpened = false
    }

    override fun success(apiName: String, response: Any?) {

        val model = response as GetMyContactsResponseModel
        if (model.isError) {
            MainActivity.mActivity.showToastMessage(model.message)
        }
        else {
            myContactsData = model.data

            setBusinessRecyclerView()
            setPersonalRecyclerView()

            if (currentAdapter == CurrentAdapter.PersonalAdapter) {
                initializePersonalItemList()
            }
            else {
                initializeBusinessItemList()
            }

            //            Log.d("TAGO", "success: ${model.data.businessCard[2].businessCardUrl}")
        }
    }

    private fun initializePersonalItemList() {

        binding.nestedBusinessScrollView.visibility = View.GONE
        if (myContactsData != null) {
            mList.clear()
            personalAdapter
            myContactsData!!.personalCard.forEachIndexed { index, personalCardItem ->

                mList.add(MyCardModel(position = index, cardId = personalCardItem.cardId, qrCode = personalCardItem.qrCode, thumbnailUrl = personalCardItem.thumbnail, name = personalCardItem.name, countryCode = personalCardItem.countryCode, phone = personalCardItem.phone, email = personalCardItem.email, colorStart = personalCardItem.cardStartColor, colorEnd = personalCardItem.cardEndColor, isEmailPublic = personalCardItem.isEmailPublic, isPhonePublic = personalCardItem.isPhonePublic, isAppUser = personalCardItem.isAppUser, isVideo = personalCardItem.isVideo, notes = personalCardItem.notes))
            }

            Handler().postDelayed(Runnable {
                mCopyList.clear()
                mCopyList.addAll(mList)
                personalAdapter.notifyDataSetChanged()
                binding.nestedPersonalScrollView.visibility = View.VISIBLE
            }, 500)
        }

        if (mList.isEmpty()) {
            binding.tvIsEmpty.visibility = View.VISIBLE
        }
        else {
            binding.tvIsEmpty.visibility = View.GONE
        }
    }

    private fun initializeBusinessItemList() {

        binding.nestedPersonalScrollView.visibility = View.GONE
        if (myContactsData != null) {
            mList.clear()
            myContactsData!!.businessCard.forEachIndexed { index, businessCardItem ->

                mList.add(MyCardModel(position = index, cardId = businessCardItem.cardId, logoThumbnailUrl = businessCardItem.logoThumbnail, qrCode = businessCardItem.qrCode, companyName = businessCardItem.businessName, name = businessCardItem.contactName, countryCode = businessCardItem.countryCode, phone = businessCardItem.phone, email = businessCardItem.email, colorStart = businessCardItem.cardStartColor, colorEnd = businessCardItem.cardEndColor, isEmailPublic = businessCardItem.isEmailPublic, isPhonePublic = businessCardItem.isPhonePublic, isAppUser = businessCardItem.isAppUser, isVideo = businessCardItem.isVideo, notes = businessCardItem.notes))
            }

            Handler().postDelayed(Runnable {
                mCopyList.clear()
                mCopyList.addAll(mList)
                businessAdapter.notifyDataSetChanged()
                binding.nestedBusinessScrollView.visibility = View.VISIBLE
            }, 500)
        }

        if (mList.isEmpty()) {
            binding.tvIsEmpty.visibility = View.VISIBLE
        }
        else {
            binding.tvIsEmpty.visibility = View.GONE
        }
    }

    override fun failure(apiName: String, message: String?) {

    }

    override fun onPersonalPhoneClick(position: Int) {

        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mCopyList[position].countryCode + mCopyList[position].phone, null))
        startActivity(intent)
    }

    override fun onPersonalEmailClick(position: Int) {

        if (android.util.Patterns.EMAIL_ADDRESS.matcher(mCopyList[position].email).matches()) {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(mCopyList[position].email))
            intent.putExtra(Intent.EXTRA_SUBJECT, "From your dear friend: ")
            MainActivity.mActivity.startActivity(intent)
        }
    }

    override fun onBusinessPhoneClick(position: Int) {

        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mCopyList[position].countryCode + mCopyList[position].phone, null))
        startActivity(intent)
    }

    override fun onBusinessEmailClick(position: Int) {

        if (android.util.Patterns.EMAIL_ADDRESS.matcher(mCopyList[position].email).matches()) {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(mCopyList[position].email))
            intent.putExtra(Intent.EXTRA_SUBJECT, "From your dear friend: ")
            MainActivity.mActivity.startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    addFragment(MainActivity.mActivity, R.id.container, ScanQRFragment(), "ScanTicketFragment")
                }
                else {
                    activity?.showToastMessage("Please grant camera permission to scan cards")
                }

                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    activity?.showToastMessage("Location permission denied")
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this);
        currentAdapter = CurrentAdapter.PersonalAdapter

        binding.tvHeading.text = MainActivity.mActivity.resources.getString(R.string.personal)
        binding.tvPersonal.setTextColor(resources.getColor(R.color.yellow))
        binding.tvBusiness.setTextColor(resources.getColor(R.color.white))

        previousPosition = -1
        currentAdapter = CurrentAdapter.PersonalAdapter
        onCrossClicked()
        initializePersonalItemList()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_QR_CODE -> {
                    callGetMyContactsApi()
                }
            }
        }
        else {
            callGetMyContactsApi()
        }
    }

    override fun onResume() {
        super.onResume()
        if (currentAdapter == CurrentAdapter.PersonalAdapter) {
            binding.tvHeading.text =MainActivity.mActivity.resources.getString(R.string.personal)
        }
        else {
            binding.tvHeading.text =MainActivity.mActivity.resources.getString(R.string.business)
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "api") {
            callGetMyContactsApi()
        }
    }


}