package com.iinshaapps.hello.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.FragmentPrivacyBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.response.CreateProfileResponseModel
import com.iinshaapps.hello.model.response.RecoveryCodeResponse

class PrivacyPolicyFragment() : BaseFragment(), IGenericCallBack {

    lateinit var binding: FragmentPrivacyBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_privacy, container, false)

        setListeners()
        getPrivacyPolicy()

        return binding.root
    }

    private fun getPrivacyPolicy() {
        val call = Hello.apiService.getPrivacy()
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.GET_PRIVACY_URL, true, this)
    }


    private fun setListeners() {


        binding.ivBack.setOnClickListener {
            MainActivity.mActivity.onBackPressed()
        }



    }

    override fun success(apiName: String, response: Any?) {
        val model = response as CreateProfileResponseModel
        if (model.isError) {
            MainActivity.mActivity.showToastMessage(model.message)
        }
        else {
            binding.tvText.text = model.data
        }
    }

    override fun failure(apiName: String, message: String?) {
        MainActivity.mActivity.showToastMessage(message!!)
    }
}