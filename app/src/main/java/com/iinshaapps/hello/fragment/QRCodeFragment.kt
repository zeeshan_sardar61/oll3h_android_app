package com.iinshaapps.hello.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.FragmentQrCodeBinding
import com.iinshaapps.hello.helper.encodeAsBitmap

class QRCodeFragment : BaseFragment() {

    lateinit var binding: FragmentQrCodeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_qr_code, container, false)

        initViews()


        return binding.root
    }

    private fun initViews() {
        binding.ivQr.setImageBitmap(encodeAsBitmap("https://play.google.com/store/apps/details?id=com.iinshaapps.hello"))
        binding.imageViewBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }
}