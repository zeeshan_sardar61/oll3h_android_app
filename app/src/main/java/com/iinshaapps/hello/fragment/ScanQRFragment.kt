package com.iinshaapps.hello.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.PointF
import android.location.Geocoder
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.FragmentScanQrCodeBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.requestmodel.AddContactRequestModel
import com.iinshaapps.hello.model.response.RecoveryCodeResponse
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.iinshaapps.hello.helper.MessageEvent
import org.greenrobot.eventbus.EventBus
import java.util.*

class ScanQRFragment : BaseFragment(), IGenericCallBack {

    lateinit var binding: FragmentScanQrCodeBinding
    lateinit var cardId: String
    private var isPersonal = false
    private var isScanned = false
    private var mediaPlayer: MediaPlayer? = null
    private var latitude = 0.0
    private var longitude = 0.0
    var address = ""
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    var isScanner = false
    lateinit var codeScanner: CodeScanner

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_scan_qr_code, container, false)

        codeScanner = CodeScanner(MainActivity.mActivity, binding.scannerView)

        binding.llBack.setOnClickListener {
            MainActivity.mActivity.onBackPressed()
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    latitude = it.latitude
                    longitude = it.longitude
                    Log.d("TAGO", "onCreateView: latitude $latitude")
                    Log.d("TAGO", "onCreateView: longitude $longitude")
                    address = getAddressFromLatLng()
                    Log.d("TAGO", "onCreateView: $address")
                }
            }
        }

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            Handler().postDelayed({
                initializeScanner()
            }, 350)
        }

        return binding.root
    }

    private fun initializeScanner() {

        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true

        codeScanner.decodeCallback = DecodeCallback {
            MainActivity.mActivity.runOnUiThread {
                codeScanner.stopPreview()

                if (!isScanned) {
                    playSound()
                    handleQrResult(it.text)
                }
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS

        }

        codeScanner.startPreview()
    }

    private fun getAddressFromLatLng(): String {
        var strAdd = ""
        val geocoder = Geocoder(activity, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(latitude, longitude, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder()

                for (i in 0..returnedAddress.maxAddressLineIndex) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i))
                }
                strAdd = strReturnedAddress.toString()
                Log.e("Tag", "My Current location address$strReturnedAddress")
            }
            else {
                Log.e("Tag", "My Current location address" + "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Tag", "My Current location address" + "Cannot get Address!")
        }

        return strAdd
    }

    private fun startAnimation() {
        val topToBottom = AnimationUtils.loadAnimation(MainActivity.mActivity, R.anim.top_to_bottom)
        val bottomToTop = AnimationUtils.loadAnimation(MainActivity.mActivity, R.anim.bottom_to_top)
        bottomToTop.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                binding.ivScannerAnim.startAnimation(topToBottom)
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        topToBottom.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                binding.ivScannerAnim.startAnimation(bottomToTop)
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        binding.ivScannerAnim.startAnimation(topToBottom)
    }

    private fun handleQrResult(text: String) {
        isScanned = true
//        if (!text.contains("http://oll3hdev.stagingdesk.com?")) {
        if (!text.contains("http://oll3hadmin.messagemusellc.com?")) {
            isScanned = false
            MainActivity.mActivity.showToastMessage("Not Oll3h QR Code")

            Handler().postDelayed({
            codeScanner.startPreview()
            }, 350)
            return
        }

        val mainString = text.split("\\?".toRegex()).dropLastWhile { it.isEmpty() }
        val separateString = mainString[1].split("&".toRegex()).dropLastWhile { it.isEmpty() }
        val cardString = separateString[0].split("=".toRegex()).dropLastWhile { it.isEmpty() }
        val personalString = separateString[1].split("=".toRegex()).dropLastWhile { it.isEmpty() }

        try {
            cardId = cardString[1]
            isPersonal = personalString[1] == "true"

        } catch (ex: IndexOutOfBoundsException) {
            isScanned = false
            MainActivity.mActivity.showToastMessage("Invalid QR code")

            Handler().postDelayed({
                codeScanner.startPreview()
            }, 350)
            return
        }

        try {
            addContact()
        } catch (ex: IndexOutOfBoundsException) {
            isScanned = false

            Handler().postDelayed({
                codeScanner.startPreview()
            }, 350)
        }
    }

    private fun addContact() {
        val call = Hello.apiService.addContact(AddContactRequestModel(cardId, isPersonal, latitude.toString(), longitude.toString(), address))
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.ADD_CONTACT_URL, true, this)
    }

    private fun playSound() {
        mediaPlayer = MediaPlayer()
        try {
            val afd = MainActivity.mActivity.assets.openFd("ic_scan_beep.mp3")
            mediaPlayer!!.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
            afd.close()
            mediaPlayer!!.prepare()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mediaPlayer!!.start()
    }

    override fun onResume() {
        super.onResume()
        isScanned = false
//        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    override fun success(apiName: String, response: Any?) {
        val model = response as RecoveryCodeResponse
        if (model.isError) {
            isScanned = false
            MainActivity.mActivity.showToastMessage(model.message)
        }
        else {
            EventBus.getDefault().post(MessageEvent("api"))
            isScanned = false
            isScanner = true
            codeScanner.releaseResources()
            MainActivity.mActivity.supportFragmentManager.popBackStack()
            addFragment(MainActivity.mActivity, R.id.container, AddNotesFragment(cardId, "", true, isPersonal), "AddNotesFragment")
        }
    }

    override fun failure(apiName: String, message: String?) {
        isScanned = false
        MainActivity.mActivity.showToastMessage(message!!)
        Handler().postDelayed({
            codeScanner.startPreview()
            }, 350)
    }
}