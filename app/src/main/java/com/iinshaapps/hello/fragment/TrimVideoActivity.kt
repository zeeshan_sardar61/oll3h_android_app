package com.iinshaapps.hello.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.FragmentTrimVideoBinding
import com.iinshaapps.hello.helper.showToastMessage
import com.deep.videotrimmer.interfaces.OnTrimVideoListener
import java.io.File

class TrimVideoActivity : AppCompatActivity(), OnTrimVideoListener {

    lateinit var binding : FragmentTrimVideoBinding
    lateinit var pathUri: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_trim_video)
        pathUri = intent.getStringExtra("path")!!
        setTrimVideoView()

    }



    private fun setTrimVideoView() {
        val f = File(pathUri)
        val size = f.length()
        binding.vtVideo.setMaxDuration(60)
        binding.vtVideo.setOnTrimVideoListener(this)
        binding.vtVideo.maxFileSize = size.toInt()
        binding.vtVideo.setDestinationPath(this.filesDir?.absolutePath + "/")
        binding.vtVideo.setVideoURI(Uri.parse(pathUri))
    }

    override fun getResult(uri: Uri?) {
//        Log.e("Cropper:", uri!!.path)
        this.runOnUiThread {
            val intent = Intent()
            intent.putExtra("videoUri", uri?.path)
            setResult(RESULT_OK, intent)
            finish()
//            targetFragment!!.onActivityResult(
//                    targetRequestCode,
//                    Activity.RESULT_OK,
//                    intent
//            )
//            (ActivityBase.activity as EasyPeasyMainActivity).showToolbar()

        }
    }

    override fun cancelAction() {
        binding.vtVideo.destroy()
//        (ActivityBase.activity as EasyPeasyMainActivity).showToolbar()
        onBackPressed()
    }

    override fun error(error: String?) {
        runOnUiThread {
            try {
                showToastMessage("Video length is too short")
            } catch (ex: RuntimeException) {
            }
        }
    }
}