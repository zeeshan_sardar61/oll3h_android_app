package com.iinshaapps.hello.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.FragmentAddNotesBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.MessageEvent
import com.iinshaapps.hello.helper.hideKeyboard
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.requestmodel.AddNotesRequestModel
import com.iinshaapps.hello.model.response.RecoveryCodeResponse
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class AddNotesFragment(var cardId: String, var notes: String, var isFromScanner: Boolean, var isPersonal: Boolean) : BaseFragment(), IGenericCallBack {

    lateinit var binding: FragmentAddNotesBinding
    private var intent: Intent? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_notes, container, false)

        setListeners()
        setData()

        return binding.root
    }

    private fun setData() {
        binding.etNotes.setText(notes)
    }

    private fun setListeners() {
        if(isFromScanner){
            binding.tvSave.text = "Skip"
        }else {
            binding.tvSave.text = "Save"
        }

        binding.ivBack.setOnClickListener {
//            if (isFromScanner) {
//                ScanQRFragment.isScanner = false
//                ScanQRFragment.codeScanner.startPreview()
//            }else {
//                EventBus.getDefault().post(MessageEvent("Fire"))
//            }
//            MainActivity.mActivity.onBackPressed()

            backClicked()
        }

        binding.tvSave.setOnClickListener {
            binding.root.hideKeyboard()
            if (binding.tvSave.text.toString() == "Save") {
                val call = Hello.apiService.addNotes(AddNotesRequestModel(cardId, binding.etNotes.text.toString(), isPersonal))
                SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.ADD_NOTES_URL, true, this)
            }
            else {
                if (isFromScanner) {
//                    ScanQRFragment.isScanner = false
//                    ScanQRFragment.codeScanner.startPreview()
                    MainActivity.mActivity.onBackPressed()
                }else {
                    MainActivity.mActivity.onBackPressed()
                }
            }
        }

        binding.etNotes.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isEmpty()) {
                    if(isFromScanner){
                        binding.tvSave.text = "Skip"
                    }
                }
                else {
                    binding.tvSave.text = "Save"
                }

            }
        })

    }

    fun backClicked() {
        if (isFromScanner) {
            MainActivity.mActivity.supportFragmentManager.popBackStack()
        }
        else {
            EventBus.getDefault().post(MessageEvent("Fire"))
            intent = Intent()
            intent!!.putExtra("notes", binding.etNotes.text.toString())
            MainActivity.mActivity.supportFragmentManager.popBackStackImmediate()
            targetFragment!!.onActivityResult(
                targetRequestCode,
                Activity.RESULT_OK,
                intent
            )
        }
    }

    override fun success(apiName: String, response: Any?) {
        val model = response as RecoveryCodeResponse
        if (model.isError) {
            MainActivity.mActivity.showToastMessage(model.message)
        }
        else {
            backClicked()
        }
    }

    override fun failure(apiName: String, message: String?) {
        MainActivity.mActivity.showToastMessage(message!!)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "Back") {
            backClicked()
        }
    }
}