package com.iinshaapps.hello.fragment

import android.Manifest
import android.app.Dialog
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.iinshaapps.hello.ButtonsWidget
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.QRWidget
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.CreateBusinessProfileActivity
import com.iinshaapps.hello.activities.CreatePersonalProfileActivity
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.MyCardAdapters
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.DialogEnhanceQrBinding
import com.iinshaapps.hello.databinding.FragmentHomeBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.encodeAsBitmap
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.MyCardModel
import com.iinshaapps.hello.model.requestmodel.AddContactRequestIdModel
import com.iinshaapps.hello.model.response.BusinessCard
import com.iinshaapps.hello.model.response.GetMyCardsResponseModel
import com.iinshaapps.hello.model.response.PersonalCard
import dagger.hilt.android.internal.Contexts.getApplication
import java.io.ByteArrayOutputStream


class HomeFragment : BaseFragment(), MyCardAdapters.ItemClicker, IGenericCallBack {

    private val LOCATION_PERMISSION = 1
    private val CAMERA_PERMISSION = 2

    lateinit var binding: FragmentHomeBinding
    var list = ArrayList<Int>()
    var mList = ArrayList<MyCardModel>()
    lateinit var adapter: MyCardAdapters

    var personalCardToSend: PersonalCard? = null
    var businessCardToSend: BusinessCard? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        callGetMyCardsApi()
    }

    private fun initViews() {
        binding.buttonQr.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                addFragment(MainActivity.mActivity, R.id.container, ScanQRFragment(), "ScanTicketFragment")
            }
            else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION), CAMERA_PERMISSION)
            }
        }
    }

    private fun callGetMyCardsApi() {

        val call = Hello.apiService.getMyCards()
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.GET_MY_CARDS_URL, true, this)
    }

    private fun setRecyclerView() {
        binding.rvCards.layoutManager = LinearLayoutManager(context)
        adapter = MyCardAdapters(mList, this)
        adapter.mListener = this
        binding.rvCards.adapter = adapter

        if(MainActivity.widgetClicked==1){
            if(mList.size==1)
                adapter.mListener.onItemClick(0)
            else {
                adapter.mListener.onItemClick(0)
                adapter.mListener.onItemClick(0)
            }
        } else if(MainActivity.widgetClicked==2){
            adapter.mListener.onItemClick(1)
        }
    }

    override fun onItemClick(position: Int) {
        MainActivity.widgetClicked = 0
        if(mList.size==1){
            if (personalCardToSend != null) {
                addFragment(MainActivity.mActivity, R.id.container, MyPersonalCardDetailFragment(personalCardToSend!!), "MyPersonalCardDetailFragment")
            }else {
                addFragment(MainActivity.mActivity, R.id.container, MyBusinessCardDetailFragment(businessCardToSend!!), "MyBusinessCardDetailFragment")
            }
        }else{
            if (position < mList.size - 1) {
                if (mList[position + 1].isOpened) {
                    if (position== 0) {
                        if (personalCardToSend != null) {
                            addFragment(MainActivity.mActivity, R.id.container, MyPersonalCardDetailFragment(personalCardToSend!!), "MyPersonalCardDetailFragment")
                        }
                    }
                }
            } else  {
                if (businessCardToSend != null) {
                    addFragment(MainActivity.mActivity, R.id.container, MyBusinessCardDetailFragment(businessCardToSend!!), "MyBusinessCardDetailFragment")
                }
            }
        }

        mList.map { it.isOpened = false }
        if (position < mList.size - 1) {
            mList[position + 1].isOpened = true
        }
        adapter.notifyDataSetChanged()
    }

    override fun onPhoneClick(position: Int) {
    }

    override fun onEmailClick(position: Int) {
    }

    override fun onQrClick(bitmap: Bitmap) {

        val dialog = Dialog(requireContext(), R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogEnhanceQrBinding = DataBindingUtil.inflate(LayoutInflater.from(requireContext()), R.layout.dialog_enhance_qr, null, false)
        dialog.setContentView(binding.root)

        binding.ivQrCode.setImageBitmap(bitmap)


        dialog.show()
    }

    override fun success(apiName: String, response: Any?) {

        when (apiName) {
            Constants.GET_MY_CARDS_URL -> {
                val model = response as GetMyCardsResponseModel
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    mList.clear()

                    if (model.data.personalCard != null) {
                        mList.add(MyCardModel(position = 0, cardId = model.data.personalCard.cardId, qrCode = model.data.personalCard.qrCode, thumbnailUrl = model.data.personalCard.thumbnail, name = model.data.personalCard.name, countryCode = model.data.personalCard.countryCode, phone = model.data.personalCard.phone, email = model.data.personalCard.email, colorStart = model.data.personalCard.cardStartColor, colorEnd = model.data.personalCard.cardEndColor, isEmailPublic = model.data.personalCard.isEmailPublic, isPhonePublic = model.data.personalCard.isPhonePublic, isVideo = model.data.personalCard.isVideo))
                        Hello.db.putString(Constants.P_CARDID, model.data.personalCard.cardId)
                        binding.ivAddCard.setImageResource(R.drawable.ic_business_person)
                        binding.tvAddCard.text = getString(R.string.add_business_card)
                        binding.rlAddCard.visibility = View.VISIBLE

                        binding.rlAddCard.setOnClickListener {

                            val i = Intent(context, CreateBusinessProfileActivity::class.java)
                            i.putExtra("signup", false)
                            startActivity(i)
                        }

                        personalCardToSend = model.data.personalCard
                        Hello.db.putString(Constants.PERSONAL_QR, model.data.personalCard.qrCode)

                        val bm = encodeAsBitmap(model.data.personalCard.qrCode)
                        val baos = ByteArrayOutputStream()
                        bm!!.compress(Bitmap.CompressFormat.PNG, 100, baos) //bm is the bitmap object
                        val b: ByteArray = baos.toByteArray()
                        val encoded: String = Base64.encodeToString(b, Base64.DEFAULT)
                        Hello.db.putString(Constants.P_QR, encoded)
                    }

                    if (model.data.businessCard != null) {
                        mList.add(MyCardModel(position = 1, cardId = model.data.businessCard.cardId, logoThumbnailUrl = model.data.businessCard.logoThumbnail, qrCode = model.data.businessCard.qrCode, companyName = model.data.businessCard.businessName, name = model.data.businessCard.contactName, countryCode = model.data.businessCard.countryCode, phone = model.data.businessCard.phone, email = model.data.businessCard.email, colorStart = model.data.businessCard.cardStartColor, colorEnd = model.data.businessCard.cardEndColor, isEmailPublic = model.data.businessCard.isEmailPublic, isPhonePublic = model.data.businessCard.isPhonePublic, isContactNamePublic = model.data.businessCard.isContactNamePublic, isVideo = model.data.businessCard.isVideo))
                        Hello.db.putString(Constants.B_CARDID, model.data.businessCard.cardId)
                        binding.ivAddCard.setImageResource(R.drawable.ic_personal_person)
                        binding.tvAddCard.text = getString(R.string.add_personal_card)
                        binding.rlAddCard.visibility = View.VISIBLE

                        binding.rlAddCard.setOnClickListener {
                            val i = Intent(context, CreatePersonalProfileActivity::class.java)
                            i.putExtra("signup", false)
                            startActivity(i)
                        }

                        businessCardToSend = model.data.businessCard
                        Hello.db.putString(Constants.BUSINESS_QR, model.data.businessCard.qrCode)

                        val bm = encodeAsBitmap(model.data.businessCard.qrCode)
                        val baos = ByteArrayOutputStream()
                        bm!!.compress(Bitmap.CompressFormat.PNG, 100, baos) //bm is the bitmap object
                        val b: ByteArray = baos.toByteArray()
                        val encoded: String = Base64.encodeToString(b, Base64.DEFAULT)
                        Hello.db.putString(Constants.B_QR, encoded)
                    }

                    val intent = Intent(MainActivity.mActivity, QRWidget::class.java)
                    intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
                    val ids: IntArray = AppWidgetManager.getInstance(MainActivity.mActivity).getAppWidgetIds(ComponentName(MainActivity.mActivity, QRWidget::class.java))
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
                    MainActivity.mActivity.sendBroadcast(intent)

                    val intent1 = Intent(MainActivity.mActivity, ButtonsWidget::class.java)
                    intent1.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
                    val ids1: IntArray = AppWidgetManager.getInstance(MainActivity.mActivity).getAppWidgetIds(ComponentName(MainActivity.mActivity, ButtonsWidget::class.java))
                    intent1.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids1)
                    MainActivity.mActivity.sendBroadcast(intent1)

                    if (model.data.personalCard != null && model.data.businessCard != null) {
                        binding.rlAddCard.visibility = View.GONE
                    }
                    setRecyclerView()
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
        MainActivity.mActivity.showToastMessage(message!!)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    addFragment(MainActivity.mActivity, R.id.container, ScanQRFragment(), "ScanTicketFragment")
                }
                else {
                    MainActivity.mActivity.showToastMessage("Please grant camera permission to scan cards")
                }

                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    MainActivity.mActivity.showToastMessage("Location permission denied")
                }
            }
        }
    }
}