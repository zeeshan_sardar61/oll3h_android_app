package com.iinshaapps.hello.fragment

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.CreateBusinessProfileActivity
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.adapter.SocialDetailsAdapter
import com.iinshaapps.hello.databinding.DialogEnhanceQrBinding
import com.iinshaapps.hello.databinding.DialogSocialBinding
import com.iinshaapps.hello.databinding.FragmentMyBusinessCardDetailBinding
import com.iinshaapps.hello.helper.*
import com.iinshaapps.hello.model.SocialModel
import com.iinshaapps.hello.model.response.BusinessCard
import com.iinshaapps.hello.model.response.SocialMediaItem
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MyBusinessCardDetailFragment(private val businessCardData: BusinessCard) : BaseFragment(), SocialDetailsAdapter.ItemClicker {

    private val STORAGE_PERMISSION = 2

    lateinit var binding: FragmentMyBusinessCardDetailBinding
    var mList = ArrayList<SocialModel>()
    var adapterSocialDetails: SocialDetailsAdapter? = null

    private lateinit var player: SimpleExoPlayer
    private var currentWindow = 0
    private var playbackPosition: Long = 0

    private lateinit var httpDataSourceFactory: HttpDataSource.Factory
    private lateinit var defaultDataSourceFactory: DefaultDataSourceFactory
    private lateinit var cacheDataSourceFactory: DataSource.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_business_card_detail, container, false)

        initViews()
        setUpBusinessData()
        setUpBusinessDataForScreenshot()

        return binding.root
    }

    private fun initViews() {

        binding.tvEdit.setOnClickListener {
            val i = Intent(activity, CreateBusinessProfileActivity::class.java)
            i.putExtra("signup", false)
            i.putExtra("data", businessCardData)
            activity?.startActivity(i)
        }

        binding.llBusinessDetails.setOnClickListener {
            if (businessCardData.isVideo) {
                releasePlayer()
            }
            addFragment(MainActivity.mActivity, R.id.container, ItemsDetailFragment(businessCardData.cardId, false, false, null), "ItemsDetailFragment")
        }

        binding.buttonBack.setOnClickListener {
            MainActivity.mActivity.onBackPressed()
        }

        binding.ivShare.setOnClickListener {

            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                launchShareIntent()
            }
            else {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION)
            }
        }

        binding.ivQrCode.setOnClickListener {
            dialogQr()
        }

        binding.ivVolume.setOnClickListener {
            if (player!!.volume == 0F) {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_up)
                player!!.volume = 1.0F
            }
            else {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_down)
                player!!.volume = 0F
            }
        }
    }

    private fun launchShareIntent() {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imagePath = MediaStore.Images.Media.insertImage(MainActivity.mActivity.contentResolver, getBitmapFromView(binding.rlScreenshot), "img_$timeStamp", null) //            val shareIntent = Intent(Intent.ACTION_SEND).apply { //                type = "image/*"
        //                putExtra(Intent.EXTRA_STREAM, Uri.parse(imagePath))
        //            }
        //            MainActivity.mActivity.startActivity(Intent.createChooser(shareIntent, null))

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Business Name: ${businessCardData.businessName}\nContact Name: ${businessCardData.contactName}\nPhone: ${businessCardData.countryCode} ${businessCardData.phone}\nEmail: ${businessCardData.email}\n\n${businessCardData.qrCode}")
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(imagePath))
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        MainActivity.mActivity.startActivity(Intent.createChooser(shareIntent, "send"))
    }

    private fun setUpBusinessData() {

        Glide.with(MainActivity.mActivity).load(businessCardData.logoThumbnail).placeholder(R.drawable.ic_logo_placeholder).into(binding.ivLogo)

        if (businessCardData.isVideo) {
            loadVideo()
        }
        else {
            binding.ivVolume.visibility = View.INVISIBLE
            Glide.with(MainActivity.mActivity).load(businessCardData.url).into(binding.ivBusinessUrl)
        }

        binding.tvCompanyName.text = businessCardData.businessName
        binding.tvNameBusiness.text = businessCardData.contactName
        binding.tvPhoneBusiness.text = businessCardData.countryCode + " " + businessCardData.phone
        binding.tvEmailBusiness.text = businessCardData.email

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(businessCardData.cardStartColor), Color.parseColor(businessCardData.cardEndColor)))
        gd.cornerRadius = 0f
        binding.rlColor.setBackgroundDrawable(gd)

        binding.ivQrCode.setImageBitmap(encodeAsBitmap(businessCardData.qrCode))
        if (businessCardData.socialMedia.isEmpty()) {
            binding.llRecyclerView.visibility = View.GONE
        }
        else {
            initializeRecyclerView(businessCardData.socialMedia as ArrayList<SocialMediaItem>)
        }
    }

    private fun setUpBusinessDataForScreenshot() {

        Glide.with(MainActivity.mActivity).load(businessCardData.url).into(binding.ivBusinessUrl1)
        Glide.with(MainActivity.mActivity).load(businessCardData.logoThumbnail).placeholder(R.drawable.ic_logo_placeholder).into(binding.ivLogo1)

        binding.tvCompanyName1.text = businessCardData.businessName
        binding.tvNameBusiness1.text = businessCardData.contactName
        binding.tvPhoneBusiness1.text = businessCardData.countryCode + " " + businessCardData.phone
        binding.tvEmailBusiness1.text = businessCardData.email

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(businessCardData.cardStartColor), Color.parseColor(businessCardData.cardEndColor)))
        gd.cornerRadius = 0f
        binding.rlColor1.setBackgroundDrawable(gd)

        binding.ivQrCode1.setImageBitmap(encodeAsBitmap(businessCardData.qrCode))
    }

    //    private fun initializeRecyclerView() {
    //
    //        mList.add(SocialModel(true, getString(R.string.facebook), R.drawable.ic_facebook, R.drawable.ic_facebook_filled, "facebook.com"))
    //        mList.add(SocialModel(true, getString(R.string.whatsapp), R.drawable.ic_whatsapp, R.drawable.ic_whatsapp_filled, "web.whatsapp.com"))
    //        mList.add(SocialModel(true, getString(R.string.twitter), R.drawable.ic_twitter, R.drawable.ic_twitter_filled, "twitter.com"))
    //        mList.add(SocialModel(true, getString(R.string.instagram), R.drawable.ic_instagram, R.drawable.ic_instagram_filled, "instagram.com"))
    //        mList.add(SocialModel(true, getString(R.string.snapchat), R.drawable.ic_snapchat, R.drawable.ic_snapchat_filled, "snapchat.com"))
    //        mList.add(SocialModel(true, getString(R.string.youtube), R.drawable.ic_youtube, R.drawable.ic_youtube_filled, "youtube.com"))
    //        mList.add(SocialModel(true, getString(R.string.tiktok), R.drawable.ic_tiktok, R.drawable.ic_tiktok_filled, "tiktok.com"))
    //        mList.add(SocialModel(true, getString(R.string.pinterest), R.drawable.ic_pinterest, R.drawable.ic_pinterst_filled, "pinterest.com"))
    //        mList.add(SocialModel(true, getString(R.string.linkedin), R.drawable.ic_linkedin, R.drawable.ic_linkedin_filled, "linkedin.com"))
    //        mList.add(SocialModel(true, getString(R.string.web), R.drawable.ic_web, R.drawable.ic_web_filled, ""))
    //        mList.add(SocialModel(true, getString(R.string.tinder), R.drawable.ic_tinder, R.drawable.ic_tinder_filled, "tinder.com"))
    //        mList.add(SocialModel(true, getString(R.string.bumble), R.drawable.ic_bumble, R.drawable.ic_bumble_filled, "bumble.com"))
    //
    //        adapterSocialDetails = SocialDetailsAdapter(mList)
    //        adapterSocialDetails?.mListener = this
    //        binding.apply {
    //            recyclerView.apply {
    //                layoutManager = GridLayoutManager(context, 5)
    //                adapter = adapterSocialDetails
    //            }
    //        }
    //    }

    private fun initializeRecyclerView(socialMedia: ArrayList<SocialMediaItem>) {
        socialMedia.forEachIndexed { index, socialMediaItem ->
            when (socialMediaItem.tag) {
                Constants.facebook -> mList.add(SocialModel(true, Constants.facebook, R.drawable.ic_facebook, R.drawable.ic_facebook_filled, socialMediaItem.url))
                Constants.whatsapp -> mList.add(SocialModel(true, Constants.whatsapp, R.drawable.ic_whatsapp, R.drawable.ic_whatsapp_filled, socialMediaItem.url))
                Constants.twitter -> mList.add(SocialModel(true, Constants.twitter, R.drawable.ic_twitter, R.drawable.ic_twitter_filled, socialMediaItem.url))
                Constants.instagram -> mList.add(SocialModel(true, Constants.instagram, R.drawable.ic_instagram, R.drawable.ic_instagram_filled, socialMediaItem.url))
                Constants.skype -> mList.add(SocialModel(true, Constants.skype, R.drawable.ic_skype, R.drawable.ic_skype_filled, socialMediaItem.url))
                Constants.linkedin -> mList.add(SocialModel(true, Constants.linkedin, R.drawable.ic_linkedin, R.drawable.ic_linkedin_filled, socialMediaItem.url))
                Constants.youtube -> mList.add(SocialModel(true, Constants.youtube, R.drawable.ic_youtube, R.drawable.ic_youtube_filled, socialMediaItem.url))
                Constants.tiktok -> mList.add(SocialModel(true, Constants.tiktok, R.drawable.ic_tiktok, R.drawable.ic_tiktok_filled, socialMediaItem.url))
                Constants.venmo -> mList.add(SocialModel(true, Constants.venmo, R.drawable.ic_venmo, R.drawable.ic_venmo_filled, socialMediaItem.url))
                Constants.payPal -> mList.add(SocialModel(true, Constants.payPal, R.drawable.ic_paypal, R.drawable.ic_paypal_filled, socialMediaItem.url))
                Constants.cashApp -> mList.add(SocialModel(true, Constants.cashApp, R.drawable.ic_cashapp, R.drawable.ic_cashapp_filled, socialMediaItem.url))
                Constants.zelle -> mList.add(SocialModel(true, Constants.zelle, R.drawable.ic_zelle, R.drawable.ic_zelle_filled, socialMediaItem.url))
                Constants.yelp -> mList.add(SocialModel(true, Constants.yelp, R.drawable.ic_yelp, R.drawable.ic_yelp_filled, socialMediaItem.url))
                Constants.doorDash -> mList.add(SocialModel(true, Constants.doorDash, R.drawable.ic_dd, R.drawable.ic_dd_filled, socialMediaItem.url))
                Constants.grubHub -> mList.add(SocialModel(true, Constants.grubHub, R.drawable.ic_grubhub, R.drawable.ic_grubhub_filled, socialMediaItem.url))
                Constants.uberEats -> mList.add(SocialModel(true, Constants.uberEats, R.drawable.ic_uber, R.drawable.ic_uber_filled, socialMediaItem.url))
                Constants.reddit -> mList.add(SocialModel(true, Constants.reddit, R.drawable.ic_reddit, R.drawable.ic_reddit_filled, socialMediaItem.url))
                Constants.pinterest -> mList.add(SocialModel(true, Constants.pinterest, R.drawable.ic_pinterest, R.drawable.ic_pinterst_filled, socialMediaItem.url))
                Constants.goToMeeting -> mList.add(SocialModel(true, Constants.goToMeeting, R.drawable.ic_goto, R.drawable.ic_goto_filled, socialMediaItem.url))
                Constants.zoom -> mList.add(SocialModel(true, Constants.zoom, R.drawable.ic_zoom, R.drawable.ic_zoom_filled, socialMediaItem.url))
            }
        }

        mList.sortBy { it.socialName }
        adapterSocialDetails = SocialDetailsAdapter(mList)
        adapterSocialDetails?.mListener = this
        binding.apply {
            recyclerView.apply {
                layoutManager = GridLayoutManager(context, 5)
                adapter = adapterSocialDetails
            }
        }
    }

    override fun onItemClick(position: Int) {
        when (mList[position].socialName) {
            Constants.zelle -> {
                openSocialDialog(mList[position])
            }
            Constants.bumble -> {
                openSocialDialog(mList[position])
            }
            Constants.nintendo -> {
                openSocialDialog(mList[position])
            }
            Constants.signal -> {
                openSocialDialog(mList[position])
            }
            Constants.skype -> {
                openSocialDialog(mList[position])
            }
            else -> {
                val i = Intent(Intent.ACTION_VIEW)
                val link = getLink(mList[position].socialName, mList[position].socialLink)
                i.setData(Uri.parse(link))
                MainActivity.mActivity.startActivity(i)
            }
        }
    }

    private fun openSocialDialog(model: SocialModel) {
        val dialog = Dialog(MainActivity.mActivity, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogSocialBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.mActivity), R.layout.dialog_social, null, false)
        dialog.setContentView(binding.root)

        binding.imageViewSocial.setImageResource(model.socialDrawableFilled)
        binding.textViewTitle.text = (model.socialName)
        binding.textViewSubTitle.text = (model.socialName)

        binding.editTextUsername.setText(model.socialLink)
        binding.editTextUsername.isEnabled = false
        binding.switchPublic.visibility = View.GONE
        binding.buttonSave.visibility = View.GONE
        binding.textViewPublic.visibility = View.INVISIBLE

        dialog.show()
    }

    fun getBitmapFromView(view: View): Bitmap {
        val returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable: Drawable = view.getBackground()
        if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return returnedBitmap
    }

    private fun dialogQr() {

        val dialog = Dialog(requireContext(), R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogEnhanceQrBinding = DataBindingUtil.inflate(LayoutInflater.from(requireContext()), R.layout.dialog_enhance_qr, null, false)
        dialog.setContentView(binding.root)

        binding.ivQrCode.setImageBitmap(encodeAsBitmap(businessCardData.qrCode))

        dialog.show()
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val dataSourceFactory: com.google.android.exoplayer2.upstream.DataSource.Factory = DefaultDataSourceFactory(MainActivity.mActivity, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
    }

    private fun initializePlayer(videoView: PlayerView, url: String) {
        binding.ivVolume.setImageResource(R.drawable.ic_volume_up)

        httpDataSourceFactory = DefaultHttpDataSource.Factory().setAllowCrossProtocolRedirects(true)

        defaultDataSourceFactory = DefaultDataSourceFactory(requireContext(), httpDataSourceFactory)

        cacheDataSourceFactory = CacheDataSource.Factory().setCache(Hello.simpleCache).setUpstreamDataSourceFactory(httpDataSourceFactory).setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

        player = SimpleExoPlayer.Builder(requireContext()).setMediaSourceFactory(DefaultMediaSourceFactory(cacheDataSourceFactory)).build()
        val videoUri = Uri.parse(url)
        val mediaItem = MediaItem.fromUri(videoUri)
        val mediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem)

        videoView.player = player
        videoView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL;
        videoView.hideController()
        player.playWhenReady = true
        player.seekTo(0, 0)
        player.repeatMode = Player.REPEAT_MODE_ALL
        player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
        player.setMediaSource(mediaSource, true)
        val audioAttributes: AudioAttributes = AudioAttributes.Builder().setUsage(C.USAGE_MEDIA).setContentType(C.CONTENT_TYPE_MOVIE).build()
        player.setAudioAttributes(audioAttributes, /* handleAudioFocus= */ true);
        player.prepare()

        player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    SimpleExoPlayer.STATE_ENDED -> {

                    }
                    SimpleExoPlayer.STATE_BUFFERING -> {
                        binding.pbBuffering.visibility = View.VISIBLE
                    }
                }
            }

            override fun onIsPlayingChanged(isPlaying: Boolean) {
                if (isPlaying) {
                    binding.rlVideoView.visibility = View.VISIBLE
                    binding.pbBuffering.visibility = View.GONE
                    binding.ivVolume.visibility = View.VISIBLE
                }
                else {
                    binding.pbBuffering.visibility = View.VISIBLE
                }
            }

            override fun onPlayerError(error: ExoPlaybackException) {
                Log.e("exoPlayer", error.message!!)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    fun releasePlayer() {
        if (businessCardData.isVideo) {
            player.run {
                playbackPosition = this.currentPosition
                currentWindow = this.currentWindowIndex
                playWhenReady = this.playWhenReady
                release()
            }
        }

    }


    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this);
        releasePlayer()
    }

    override fun onResume() {
        super.onResume()
        loadVideo()
    }

    fun loadVideo() {
        if (businessCardData.isVideo) {
            binding.rlVideoView.visibility = View.VISIBLE
            initializePlayer(binding.videoView, businessCardData.url)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        if (event.message == "Fire") {
            loadVideo()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            STORAGE_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchShareIntent()
                }
                else {
                    MainActivity.mActivity.showToastMessage("Please allow permission to share your profile")
                }
            }
        }
    }
}