package com.iinshaapps.hello.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.ActivityRestorePurchasedBinding
import com.iinshaapps.hello.databinding.DialogRestoreConfirmationBinding
import com.iinshaapps.hello.helper.Constants

class RestorePurchasedActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRestorePurchasedBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_restore_purchased)

        initViews()
    }

    private fun initViews() {

        binding.buttonCreateAccount.setOnClickListener {
            showDialog()
        }

        binding.btnContinue.setOnClickListener {
            val i = Intent(this, RecoveryNumberActivity::class.java)
            i.putExtra("isFromPayment", false)
            startActivity(i)
            finish()
        }

    }

    private fun showDialog() {

        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val binding: DialogRestoreConfirmationBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_restore_confirmation, null, false)
        dialog.setContentView(binding.root)

        binding.tvContinue.setOnClickListener {
            Hello.db.putString(Constants.USER_ID, "")
            dialog.dismiss()
            val i = Intent(this, CreateProfileActivity::class.java)
            startActivity(i)
            finish()
        }

        binding.tvNo.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}