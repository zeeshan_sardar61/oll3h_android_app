package com.iinshaapps.hello.activities

import android.content.ContentProviderOperation
import android.content.ContentValues
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Environment
import android.provider.ContactsContract
import android.provider.ContactsContract.CommonDataKinds.*
import android.provider.ContactsContract.Data
import android.provider.MediaStore.Images
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.adapter.SocialDetailsAdapter
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.TestActivityBinding
import com.iinshaapps.hello.helper.*
import com.iinshaapps.hello.model.SocialModel
import com.iinshaapps.hello.model.requestmodel.AddContactRequestIdModel
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class TestActivity : AppCompatActivity(), SocialDetailsAdapter.ItemClicker {

    lateinit var binding: TestActivityBinding
    var mList = ArrayList<SocialModel>()
    var adapterSocialDetails: SocialDetailsAdapter? = null
    private lateinit var bottomSheetListFragment: MediaBottomSheetFragment
    var videoUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.test_activity)

        initViews()
        setUpCard()
        initializeRecyclerView()
    }

    private fun initViews() {

        binding.buttonDetail.setOnClickListener {
            saveToGallery("bucks", "", "MPAndroidChart-Library Save", Bitmap.CompressFormat.PNG, 50)
        }

        binding.buttonBack.setOnClickListener {
            addToContacts();
        }
    }

    private fun setUpCard() {

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(resources.getColor(R.color.personalCardStart), resources.getColor(R.color.personalCardEnd)))
        gd.cornerRadius = 0f
        binding.rlColor.setBackgroundDrawable(gd)
    }

    private fun initializeRecyclerView() {

        mList.add(SocialModel(false, getString(R.string.facebook), R.drawable.ic_facebook, R.drawable.ic_facebook_filled, "facebook.com"))
        mList.add(SocialModel(false, getString(R.string.whatsapp), R.drawable.ic_whatsapp, R.drawable.ic_whatsapp_filled, "web.whatsapp.com"))
        mList.add(SocialModel(false, getString(R.string.twitter), R.drawable.ic_twitter, R.drawable.ic_twitter_filled, "twitter.com"))
        mList.add(SocialModel(false, getString(R.string.instagram), R.drawable.ic_instagram, R.drawable.ic_instagram_filled, "instagram.com"))
        mList.add(SocialModel(false, getString(R.string.snapchat), R.drawable.ic_snapchat, R.drawable.ic_snapchat_filled, "snapchat.com"))
        mList.add(SocialModel(false, getString(R.string.youtube), R.drawable.ic_youtube, R.drawable.ic_youtube_filled, "youtube.com"))
        mList.add(SocialModel(false, getString(R.string.tiktok), R.drawable.ic_tiktok, R.drawable.ic_tiktok_filled, "tiktok.com"))
        mList.add(SocialModel(false, getString(R.string.pinterest), R.drawable.ic_pinterest, R.drawable.ic_pinterst_filled, "pinterest.com"))
        mList.add(SocialModel(false, getString(R.string.linkedin), R.drawable.ic_linkedin, R.drawable.ic_linkedin_filled, "linkedin.com"))
        mList.add(SocialModel(false, getString(R.string.web), R.drawable.ic_web, R.drawable.ic_web_filled, ""))
        mList.add(SocialModel(false, getString(R.string.tinder), R.drawable.ic_tinder, R.drawable.ic_tinder_filled, "tinder.com"))
        mList.add(SocialModel(false, getString(R.string.bumble), R.drawable.ic_bumble, R.drawable.ic_bumble_filled, "bumble.com"))

        adapterSocialDetails = SocialDetailsAdapter(mList)
        adapterSocialDetails?.mListener = this
        binding.apply {
            recyclerView.apply {
                layoutManager = GridLayoutManager(this@TestActivity, 5)
                adapter = adapterSocialDetails
            }
        }
    }

    override fun onItemClick(position: Int) {
    }

    fun saveToGallery(fileName: String, subFolderPath: String, fileDescription: String?, format: CompressFormat?, quality: Int): Boolean { // restrain quality

        //        binding.rlScreenshot.visibility = View.VISIBLE

        var fileName = fileName
        var quality = quality
        if (quality < 0 || quality > 100) quality = 50
        val currentTime = System.currentTimeMillis()
        val extBaseDir = Environment.getExternalStorageDirectory()
        val file = File(extBaseDir.absolutePath + "/DCIM/" + subFolderPath)
        if (!file.exists()) {
            if (!file.mkdirs()) {
                return false
            }
        }
        var mimeType = ""
        when (format) {
            CompressFormat.PNG -> {
                mimeType = "image/png"
                if (!fileName.endsWith(".png")) fileName += ".png"
            }
            CompressFormat.WEBP -> {
                mimeType = "image/webp"
                if (!fileName.endsWith(".webp")) fileName += ".webp"
            }
            CompressFormat.JPEG -> {
                mimeType = "image/jpeg"
                if (!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))) fileName += ".jpg"
            }
            else -> {
                mimeType = "image/jpeg"
                if (!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))) fileName += ".jpg"
            }
        }
        val filePath = file.absolutePath + "/" + fileName
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(filePath)
            val b: Bitmap = getBitmapFromView(binding.rlScreenshot)
            b.compress(format, quality, out)
            out.flush()
            out.close()
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        val size = File(filePath).length()
        val values = ContentValues(8)

        // store the details
        values.put(Images.Media.TITLE, fileName)
        values.put(Images.Media.DISPLAY_NAME, fileName)
        values.put(Images.Media.DATE_ADDED, currentTime)
        values.put(Images.Media.MIME_TYPE, mimeType)
        values.put(Images.Media.DESCRIPTION, fileDescription)
        values.put(Images.Media.ORIENTATION, 0)
        values.put(Images.Media.DATA, filePath)
        values.put(Images.Media.SIZE, size)

        //        binding.rlScreenshot.visibility = View.INVISIBLE

        return contentResolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values) != null
    }

    fun getBitmapFromView(view: View): Bitmap {
        val returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable: Drawable = view.getBackground()
        if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        return returnedBitmap
    }

    private fun addToContacts() {

        Log.d("TAGO", "addToContacts: ")

        val opList = ArrayList<ContentProviderOperation>()
        opList.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
            .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
            .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
            //.withValue(RawContacts.AGGREGATION_MODE, RawContacts.AGGREGATION_MODE_DEFAULT)
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
            .withValue(StructuredName.GIVEN_NAME, "Abubakar")
            .withValue(StructuredName.FAMILY_NAME, "Sheikh")
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
            .withValue(Phone.NUMBER, "123456789")
            .withValue(Phone.TYPE, Phone.TYPE_HOME)
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
            .withValue(Phone.NUMBER, "1122334455")
            .withValue(Phone.TYPE, Phone.TYPE_HOME)
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
            .withValue(Phone.NUMBER, "987654321")
            .withValue(Phone.TYPE, Phone.TYPE_WORK)
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE)
            .withValue(Email.DATA, "abc@xyz.com")
            .withValue(Email.TYPE, Email.TYPE_WORK)
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, StructuredPostal.CONTENT_ITEM_TYPE)
            .withValue(StructuredPostal.STREET, "Dummy Street")
            .withValue(StructuredPostal.CITY, "Dummy City")
            .withValue(StructuredPostal.REGION, "Dummy State")
            .withValue(StructuredPostal.POSTCODE, "Dummy Postcode")
            .withValue(StructuredPostal.COUNTRY, "Dummy Country")
            .withValue(StructuredPostal.TYPE, StructuredPostal.TYPE_WORK)
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, Website.CONTENT_ITEM_TYPE)
            .withValue(Website.DATA, "www.bucks.com")
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, Organization.CONTENT_ITEM_TYPE)
            .withValue(Organization.TITLE, "Android Professional")
            .build())

        opList.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID, 0)
            .withValue(Data.MIMETYPE, Event.CONTENT_ITEM_TYPE)
            .withValue(Event.START_DATE, "26-05-2015")
            .withValue(Event.TYPE, Event.TYPE_BIRTHDAY)
            .build())

        try {
            val results = contentResolver.applyBatch(ContactsContract.AUTHORITY, opList)
            Log.d("TAGO", "try: ")
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("TAGO", "catch: ${e.message}")
        }
    }
}