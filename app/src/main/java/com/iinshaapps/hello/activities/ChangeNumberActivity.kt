package com.iinshaapps.hello.activities

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.ActivityChangeNumberBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.requestmodel.AddRecoveryNumberRequest
import com.iinshaapps.hello.model.response.RecoveryCodeResponse

class ChangeNumberActivity : AppCompatActivity(), IGenericCallBack {

    lateinit var binding: ActivityChangeNumberBinding
    private var isNumberValid = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_number)

        initViews()
        setListener()
    }

    private fun initViews() {

        val cc = Hello.db.getString(Constants.USER_CC, "")
        val phone = Hello.db.getString(Constants.USER_PHONE, "")

        binding.tvPreviousPhone.text = "$cc $phone"
    }

    private fun setListener() {

        binding.ccpNumber.registerPhoneNumberTextView(binding.etPhone)
        binding.ccpNumber.setPhoneNumberInputValidityListener { _, b ->
            isNumberValid = b
        }

        binding.etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                validateButton()
            }
        })

        binding.buttonVerify.setOnClickListener {

            if(!isNumberValid) {
                MainActivity.mActivity.showToastMessage("Phone number is invalid")
                return@setOnClickListener
            }

            val userId = if(Hello.db.getString(Constants.USER_ID).isNullOrEmpty()){
                null
            }else {
                Hello.db.getString(Constants.USER_ID)
            }

            val call = Hello.apiService.addRecoveryNumber(AddRecoveryNumberRequest(
                null,
                null,
                null,
                binding.etPhone.text.toString(),
                "+" + binding.ccpNumber.selectedCountryCode.toString().trim(),
                null,
                userId,
                null,
                null,
                null
            ))

            SingleEnqueueCall.callRetrofit(this, call, Constants.ADD_RECOVERY_NUMBER_URL, true, this)

//            val i = Intent(this, VerifyPhoneActivity::class.java)
//            startActivity(i)
        }

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun validateButton() {

        binding.apply {
            if (etPhone.text.toString().isNotEmpty()) {
                buttonVerify.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.button))
                buttonVerify.setTextColor(resources.getColor(R.color.white))
                buttonVerify.isEnabled = true
            }
            else {
                buttonVerify.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
                buttonVerify.setTextColor(resources.getColor(R.color.text))
                buttonVerify.isEnabled = false
            }
        }
    }

    override fun success(apiName: String, response: Any?) {
        when (apiName) {
            Constants.ADD_RECOVERY_NUMBER_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    MainActivity.mActivity.showToastMessage(model.message)
                }
                else {
                    val i = Intent(this, VerifyPhoneActivity::class.java)
                    i.putExtra("countryCode", "+" + binding.ccpNumber.selectedCountryCode.toString().trim())
                    i.putExtra("phone", binding.etPhone.text.toString())
                    startActivity(i)
                    finish()
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
        when (apiName) {
            Constants.ADD_RECOVERY_NUMBER_URL -> {
                if (message != null) {
                    MainActivity.mActivity.showToastMessage(message)
                }
            }
        }
    }
}