package com.iinshaapps.hello.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.iinshaapps.hello.R
import com.iinshaapps.hello.adapter.SocialAdapter
import com.iinshaapps.hello.databinding.ActivityPersonalProfileBinding
import com.iinshaapps.hello.databinding.DialogSocialBinding
import com.iinshaapps.hello.fragment.TrimVideoActivity
import com.iinshaapps.hello.helper.*
import com.iinshaapps.hello.model.CreatePersonalProfileModel
import com.iinshaapps.hello.model.SocialModel
import com.iinshaapps.hello.model.requestmodel.SocialMediaModelList
import com.iinshaapps.hello.model.response.PersonalCard
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.opensooq.supernova.gligar.GligarPicker
import com.otaliastudios.transcoder.Transcoder
import com.otaliastudios.transcoder.TranscoderListener
import com.otaliastudios.transcoder.strategy.DefaultVideoStrategy
import com.otaliastudios.transcoder.strategy.size.FractionResizer
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class CreatePersonalProfileActivity : AppCompatActivity(), SocialAdapter.ItemClicker, MediaBottomSheetFragment.ISelectListener {

    lateinit var binding: ActivityPersonalProfileBinding
    var mList = ArrayList<SocialModel>()
    var adapterSocial: SocialAdapter? = null
    var setName = false
    var setPhone = false
    var setEmail = false
    private lateinit var bottomSheetListFragment: MediaBottomSheetFragment
    private var itemList: ArrayList<String> = ArrayList()
    private val REQ_CODE_CAMERA = 1
    private val REQ_CODE_GALLERY = 2
    private val REQ_CODE_VIDEO_CAMERA = 3
    private val REQ_CODE_VIDEO_GALLERY = 4
    private val TRIM_VIDEO_CODE = 5
    private var cameraImageUri: Uri? = null
    var videoUri: Uri? = null
    var player: SimpleExoPlayer? = null
    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 0
    var socialModelList = ArrayList<SocialMediaModelList>()
    private var isVideo = false
    private var isNumberValid = false
    var isFromSignup = false

    var isFromEdit = false
    var model: PersonalCard? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_personal_profile)

        checkPermissions()
        initViews()
        setListeners()
        setList()

        isFromSignup = intent.getBooleanExtra("signup", false)

        if (intent.getSerializableExtra("data") != null) {
            isFromEdit = true
            model = (intent.getSerializableExtra("data") as? PersonalCard)!!
            binding.apply {
                etName.setText(model!!.name)
                etPhone.setText(model!!.phone)
                etEmail.setText(model!!.email)
                ccpNumber.setCountryForPhoneCode(model!!.countryCode.toInt())
                swName.isChecked = model!!.isNamePublic
                swPhone.isChecked = model!!.isPhonePublic
                swEmail.isChecked = model!!.isEmailPublic

                ivProfile.visibility = View.VISIBLE
                imageViewProfile.visibility = View.INVISIBLE
                isVideo =  model!!.isVideo
                Glide.with(this@CreatePersonalProfileActivity).load(model!!.url).into(ivProfile)

                for (i in mList.indices) {
                    for (j in model!!.socialMedia.indices) {
                        if (mList[i].socialName == model!!.socialMedia[j].tag) {
                            mList[i].isSelected = true
                            mList[i].socialUsername = model!!.socialMedia[j].url
                            mList[i].socialPublicToggle = model!!.socialMedia[j].isPublic
                            socialModelList.add(SocialMediaModelList(mList[i].socialName, mList[i].socialPublicToggle, mList[i].socialUsername))
                        }
                    }
                }
                setSocialMediaAdapter()
            }
        }
        else {
            setSocialMediaAdapter()
        }
    }

    private fun setList() {
        mList.clear()
        mList.add(SocialModel(false, Constants.facebook, R.drawable.ic_facebook, R.drawable.ic_facebook_filled, ""))
        mList.add(SocialModel(false, Constants.whatsapp, R.drawable.ic_whatsapp, R.drawable.ic_whatsapp_filled, ""))
        mList.add(SocialModel(false, Constants.twitter, R.drawable.ic_twitter, R.drawable.ic_twitter_filled, ""))
        mList.add(SocialModel(false, Constants.instagram, R.drawable.ic_instagram, R.drawable.ic_instagram_filled, ""))
        mList.add(SocialModel(false, Constants.snapchat, R.drawable.ic_snapchat, R.drawable.ic_snapchat_filled, ""))
        mList.add(SocialModel(false, Constants.youtube, R.drawable.ic_youtube, R.drawable.ic_youtube_filled, ""))
        mList.add(SocialModel(false, Constants.tiktok, R.drawable.ic_tiktok, R.drawable.ic_tiktok_filled, ""))
        mList.add(SocialModel(false, Constants.pinterest, R.drawable.ic_pinterest, R.drawable.ic_pinterst_filled, ""))
        mList.add(SocialModel(false, Constants.linkedin, R.drawable.ic_linkedin, R.drawable.ic_linkedin_filled, ""))
        mList.add(SocialModel(false, Constants.website, R.drawable.ic_web, R.drawable.ic_web_filled, ""))
        mList.add(SocialModel(false, Constants.tinder, R.drawable.ic_tinder, R.drawable.ic_tinder_filled, ""))
        mList.add(SocialModel(false, Constants.bumble, R.drawable.ic_bumble, R.drawable.ic_bumble_filled, ""))
        mList.add(SocialModel(false, Constants.nintendo, R.drawable.ic_nitendo, R.drawable.ic_nitendo_filled, ""))
        mList.add(SocialModel(false, Constants.signal, R.drawable.ic_signal, R.drawable.ic_signal_filled, ""))
        mList.add(SocialModel(false, Constants.weChat, R.drawable.ic_wechat, R.drawable.ic_wechat_filled, ""))
        mList.add(SocialModel(false, Constants.telegram, R.drawable.ic_telegram, R.drawable.ic_telegram_filled, ""))
        mList.add(SocialModel(false, Constants.twitch, R.drawable.ic_twitch, R.drawable.ic_twitch_filled, ""))
        mList.add(SocialModel(false, Constants.discord, R.drawable.ic_discord, R.drawable.ic_discord_filled, ""))
        mList.add(SocialModel(false, Constants.calendly, R.drawable.ic_calendly, R.drawable.ic_calendly_filled, ""))
        mList.add(SocialModel(false, Constants.skype, R.drawable.ic_skype, R.drawable.ic_skype_filled, ""))
        mList.add(SocialModel(false, Constants.xbox, R.drawable.ic_xbox, R.drawable.ic_xbox_filled, ""))
        mList.add(SocialModel(false, Constants.playstation, R.drawable.ic_ps, R.drawable.ic_ps_filled, ""))
        mList.add(SocialModel(false, Constants.reddit, R.drawable.ic_reddit, R.drawable.ic_reddit_filled, ""))
        mList.add(SocialModel(false, Constants.payPal, R.drawable.ic_paypal, R.drawable.ic_paypal_filled, ""))
        mList.add(SocialModel(false, Constants.zelle, R.drawable.ic_zelle, R.drawable.ic_zelle_filled, ""))
        mList.add(SocialModel(false, Constants.cashApp, R.drawable.ic_cashapp, R.drawable.ic_cashapp_filled, ""))
        mList.add(SocialModel(false, Constants.venmo, R.drawable.ic_venmo, R.drawable.ic_venmo_filled, ""))
    }

    private fun setListeners() {
        binding.ccpNumber.registerPhoneNumberTextView(binding.etPhone)

        binding.ccpNumber.setPhoneNumberInputValidityListener { _, b ->
            isNumberValid = b
        }

        binding.ivCamera.setOnClickListener {
            showImageDialog()
        }

        binding.buttonNext.setOnClickListener {
            callPersonalProfileApi()
        }

        binding.textViewNext.setOnClickListener {
            callPersonalProfileApi()
        }

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setSocialMediaAdapter() {

        mList.sortBy { it.socialName }
        adapterSocial = SocialAdapter(mList)
        adapterSocial?.mListener = this
        binding.apply {
            recyclerView.apply {
                layoutManager = GridLayoutManager(this@CreatePersonalProfileActivity, 5)
                adapter = adapterSocial
            }
        }
    }

    private fun callPersonalProfileApi() {
        if (isFromEdit) {
            if (!binding.ccpNumber.isValid) {
                showToastMessage("Phone number format is invalid.")
                return
            }

            if (!binding.swPhone.isChecked && !binding.swEmail.isChecked) {
                showToastMessage("Phone or Email public privacy is mandatory")
                return
            }

            val ccode = "+" + binding.ccpNumber.selectedCountryCode.toString().trim()
            var editModel: CreatePersonalProfileModel? = null
            if (cameraImageUri != null) {
                editModel = CreatePersonalProfileModel(binding.etName.text.toString(), ccode, binding.etPhone.text.toString(), binding.etEmail.text.toString(), binding.swName.isChecked, binding.swPhone.isChecked, binding.swEmail.isChecked, socialModelList, model!!.cardStartColor, model!!.cardEndColor, isVideo, cameraImageUri!!.path!!, model!!.url, model!!.thumbnail)
            }else if (videoUri != null) {
                editModel = CreatePersonalProfileModel(binding.etName.text.toString(), ccode, binding.etPhone.text.toString(), binding.etEmail.text.toString(), binding.swName.isChecked, binding.swPhone.isChecked, binding.swEmail.isChecked, socialModelList, model!!.cardStartColor, model!!.cardEndColor, isVideo, videoUri!!.path, model!!.url, model!!.thumbnail)
            }else {
                editModel = CreatePersonalProfileModel(binding.etName.text.toString(), ccode, binding.etPhone.text.toString(), binding.etEmail.text.toString(), binding.swName.isChecked, binding.swPhone.isChecked, binding.swEmail.isChecked, socialModelList, model!!.cardStartColor, model!!.cardEndColor, isVideo, null, model!!.url, model!!.thumbnail)
            }
            val intent = Intent(baseContext, PersonalizedCardActivity::class.java)
            intent.putExtra("signup", isFromSignup)
            intent.putExtra("isEdit", true)
            intent.putExtra("previousActivity", "PersonalProfileActivity")
            intent.putExtra("model", editModel)
            startActivity(intent)
        }
        else {
//            if (cameraImageUri == null && videoUri == null) {
//                showToastMessage("Please select profile image/video.")
//                return
//            }

            if (!binding.ccpNumber.isValid) {
                showToastMessage("Phone number format is invalid.")
                return
            }


            if (!binding.swPhone.isChecked && !binding.swEmail.isChecked) {
                showToastMessage("Phone or Email public privacy is mandatory")
                return
            }

            val ccode = "+" + binding.ccpNumber.selectedCountryCode.toString().trim()

            val model = if (cameraImageUri != null) {
                CreatePersonalProfileModel(binding.etName.text.toString(), ccode, binding.etPhone.text.toString(), binding.etEmail.text.toString(), binding.swName.isChecked, binding.swPhone.isChecked, binding.swEmail.isChecked, socialModelList, "", "", isVideo, cameraImageUri!!.path!!, "", "")
            } else if (videoUri != null) {
                CreatePersonalProfileModel(binding.etName.text.toString(), ccode, binding.etPhone.text.toString(), binding.etEmail.text.toString(), binding.swName.isChecked, binding.swPhone.isChecked, binding.swEmail.isChecked, socialModelList, "", "", isVideo, videoUri!!.path!!, "", "")
            } else {
                CreatePersonalProfileModel(binding.etName.text.toString(), ccode, binding.etPhone.text.toString(), binding.etEmail.text.toString(), binding.swName.isChecked, binding.swPhone.isChecked, binding.swEmail.isChecked, socialModelList, "", "", isVideo, null, "", "")
            }

            val intent = Intent(baseContext, PersonalizedCardActivity::class.java)
            intent.putExtra("signup", isFromSignup)
            Log.d("bucks", "personal: $isFromSignup")
            intent.putExtra("isEdit", false)
            intent.putExtra("previousActivity", "PersonalProfileActivity")
            intent.putExtra("model", model)
            startActivity(intent)
        }
    }

    private fun checkPermissions() {
        Permissions.checkPermission(this)
    }

    private fun showImageDialog() {
        itemList.clear()
        itemList.add("Photo Camera")
        itemList.add("Photo Album")
        itemList.add("Video Camera")
        itemList.add("Video Album")
        if (cameraImageUri != null || videoUri != null) {
            itemList.add("Remove")
        }
        else {
            if (model != null) {
                if (model!!.url.isNotEmpty()) {
                    itemList.add("Remove")
                }
            }

        }
        bottomSheetListFragment = MediaBottomSheetFragment(this, itemList, "")
        bottomSheetListFragment.setMyListener(this)
        if (!bottomSheetListFragment.isAdded) {
            bottomSheetListFragment.show(supportFragmentManager, "")
        }
    }

    override fun onMediaSelect(pos: Int) {
        when (itemList[pos]) {
            "Photo Camera" -> {
                if (Permissions.checkSinglePermission(this, Manifest.permission.CAMERA)) {
                    startCamera(REQ_CODE_CAMERA)
                }
            }
            "Photo Album" -> {
                if (Permissions.checkSinglePermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (Permissions.checkSinglePermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        GligarPicker().limit(1).disableCamera(true).isVideo(false).cameraDirect(false).requestCode(REQ_CODE_GALLERY).withActivity(this).show()
                    }
                }
            }
            "Video Camera" -> {
                startVideoCamera(REQ_CODE_VIDEO_CAMERA)
            }
            "Video Album" -> {
                GligarPicker().limit(1).disableCamera(true).isVideo(true).cameraDirect(false).requestCode(REQ_CODE_VIDEO_GALLERY).withActivity(this).show()
            }
            "Remove" -> {
                cameraImageUri = null
                videoUri = null

                binding.ivProfile.visibility = View.GONE
                binding.imageViewProfile.visibility = View.VISIBLE

                if (player != null) {
                    player!!.release()
                    binding.rlVideoView.visibility = View.GONE
                    binding.ivVolume.visibility = View.GONE
                    binding.pbBuffering.visibility = View.GONE
                }
            }
        }
    }

    override fun onMediaCancel() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                100 -> {
                    val result: Uri = Uri.parse(data!!.getStringExtra("result"))
                    binding.rlVideoView.visibility = View.GONE
                    binding.ivVolume.visibility = View.GONE
                    binding.pbBuffering.visibility = View.GONE
                    cameraImageUri = compressFile(result)
                    binding.ivProfile.setImageBitmap(handleSamplingAndRotationBitmap(cameraImageUri!!))
                    Glide.with(this@CreatePersonalProfileActivity).load(handleSamplingAndRotationBitmap(cameraImageUri!!)).into(binding.ivProfile)
                    binding.ivProfile.visibility = View.VISIBLE
                    binding.imageViewProfile.visibility = View.INVISIBLE
                    isVideo = false
                    if (player != null) {
                        player!!.release()
                        binding.rlVideoView.visibility = View.GONE
                        binding.ivVolume.visibility = View.GONE
                        binding.pbBuffering.visibility = View.GONE
                    }
                }
                REQ_CODE_GALLERY -> if (data != null) {
                    processGalleryMultipleVideoGligar(data, object : ICallBackUriMultiple {
                        override fun imageUri(result: ArrayList<Uri>) {
                            val i = Intent(this@CreatePersonalProfileActivity, CropActivity::class.java)
                            i.putExtra("isSquare", false)
                            i.putExtra("path", result[0].path!!)
                            startActivityForResult(i, 100)
                        }
                    })
                }
                REQ_CODE_CAMERA -> processCapturedPhoto(object : ICallBackUri {
                    override fun imageUri(result: Uri?) {
                        val i = Intent(this@CreatePersonalProfileActivity, CropActivity::class.java)
                        i.putExtra("isSquare", false)
                        i.putExtra("path", result!!.path!!)
                        startActivityForResult(i, 100)

                    }
                })
                REQ_CODE_VIDEO_CAMERA -> {
                    processCapturedPhoto(object : ICallBackUri {
                        override fun imageUri(result: Uri?) {
                            videoUri = result!!
                            executeMyCompression()
                            Looper.myLooper()?.let {
                                Handler(it).postDelayed({ //                                    binding.isCompressing = false
                                    //                                    binding.percent.visibility = View.GONE
                                }, 50)
                            }
                            var thumb = result.path?.let { ThumbnailUtils.createVideoThumbnail(it, MediaStore.Images.Thumbnails.MINI_KIND) }
                            val file = File(cacheDir, "test.png")
                            try {
                                file.createNewFile()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                            thumb = compressBitmap(thumb!!) //                            binding.ivProfile.setImageBitmap(thumb)
                            val bitmap = Bitmap.createScaledBitmap(thumb!!, thumb!!.width, thumb!!.height, true)
                            val bos = ByteArrayOutputStream()
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
                            val bitmapdata = bos.toByteArray()
                            try {
                                val fos = FileOutputStream(file)
                                fos.write(bitmapdata)
                                fos.flush()
                                fos.close() //                              callUploadMediaVideoThumbnail(Uri.fromFile(file))
                                isVideo = true
                                initializePlayer(binding.videoView, videoUri!!)

                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                    })
                }
                REQ_CODE_VIDEO_GALLERY -> {
                    if (data != null) {
                        processGalleryMultipleVideoGligar(data, object : ICallBackUriMultiple {
                            override fun imageUri(result: ArrayList<Uri>) {
                                val i = Intent(this@CreatePersonalProfileActivity, TrimVideoActivity::class.java)
                                i.putExtra("path", result[0].path!!)
                                startActivityForResult(i, TRIM_VIDEO_CODE)
                            }
                        })
                    }
                }
                TRIM_VIDEO_CODE -> {
                    videoUri = Uri.parse(data!!.getStringExtra("videoUri"))
                    executeMyCompression()
                    var thumb = videoUri!!.path?.let { ThumbnailUtils.createVideoThumbnail(it, MediaStore.Images.Thumbnails.MINI_KIND) }
                    val file = File(cacheDir, "test.png")
                    try {
                        file.createNewFile()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    thumb = compressBitmap(thumb!!) //                    binding.ivProfile.setImageBitmap(thumb)
                    val bitmap = Bitmap.createScaledBitmap(thumb!!, thumb!!.width, thumb!!.height, true)
                    val bos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
                    val bitmapdata = bos.toByteArray()
                    try {
                        val fos = FileOutputStream(file)
                        fos.write(bitmapdata)
                        fos.flush()
                        fos.close() //                      callUploadMediaVideoThumbnail(Uri.fromFile(file))
                        isVideo = true
                        initializePlayer(binding.videoView, videoUri!!)

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    private fun executeMyCompression() {
        val mTranscodeVideoStrategy = DefaultVideoStrategy.Builder().addResizer(FractionResizer(0.5f)).frameRate(DefaultVideoStrategy.DEFAULT_FRAME_RATE).build()
        val filePath = filesDir.absolutePath + "/${getFileName(videoUri!!.path!!)}.mp4" //        binding.isCompressing = true //        binding.percent.visibility = View.VISIBLE //        binding.percent.text = "" //        binding.progressBar.progress = 0
        Transcoder.into(filePath).addDataSource(videoUri!!.path!!).setVideoTrackStrategy(mTranscodeVideoStrategy).setListener(object : TranscoderListener {
            override fun onTranscodeProgress(progress: Double) {
                Log.e("compressorTrimmer:", " ${progress * 100}")
                runOnUiThread { //                        binding.percent.text = "${(progress * 100).toInt()}%"
                    //                        binding.progressBar.progress = (progress * 100).toInt()
                }
            }

            override fun onTranscodeCompleted(successCode: Int) {
                if (successCode == Transcoder.SUCCESS_TRANSCODED) {
                    videoUri = Uri.parse(filePath) //                        callUploadMediaVideo()
                    Looper.myLooper()?.let {
                        Handler(it).postDelayed({ //                                binding.isCompressing = false
                            //                                binding.percent.visibility = View.GONE
                        }, 50)
                    }
                }
                else if (successCode == Transcoder.SUCCESS_NOT_NEEDED) { //                        binding.isCompressing = false
                    //                        binding.percent.visibility = View.GONE
                    //                        callUploadMediaVideo()
                }
            }

            override fun onTranscodeCanceled() { //                    binding.isCompressing = false
                //                    binding.percent.visibility = View.GONE
                //                    callUploadMediaVideo()
            }

            override fun onTranscodeFailed(exception: Throwable) { //                    binding.isCompressing = false
                //                    binding.percent.visibility = View.GONE
                //                    callUploadMediaVideo()
                Log.wtf("failureMessage", exception.message)
            }
        }).transcode()
    }

    private fun buildMediaSource(uri: Uri): MediaSource? {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(this, "exoplayer-codelab")
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
    }

    private fun initializePlayer(videoView: PlayerView, uri: Uri) {
        binding.ivVolume.setImageResource(R.drawable.ic_volume_up)
        if (player != null) player!!.release()
        player = null
        player = SimpleExoPlayer.Builder(this).build() //        videoView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT;
        videoView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL;
        videoView.hideController()
        videoView.player = player //        val uri = Uri.parse(url)
        val mediaSource = buildMediaSource(uri)
        player!!.playWhenReady = playWhenReady
        player!!.seekTo(currentWindow, playbackPosition)
        player!!.prepare(mediaSource!!, false, false)
        player!!.repeatMode = Player.REPEAT_MODE_ALL
        player!!.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;

        player!!.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    SimpleExoPlayer.STATE_ENDED -> {
                        if (player != null) player!!.release()
                        player = null
                        onBackPressed()

                    }
                    SimpleExoPlayer.STATE_BUFFERING -> {
                        binding.pbBuffering.visibility = View.VISIBLE
                    }
                }
            }

            override fun onIsPlayingChanged(isPlaying: Boolean) {
                if (isPlaying) {
                    binding.rlVideoView.visibility = View.VISIBLE
                    binding.pbBuffering.visibility = View.GONE
                    binding.imageViewProfile.visibility = View.INVISIBLE
                    binding.ivVolume.visibility = View.VISIBLE
                    binding.ivProfile.visibility = View.GONE
                }
                else {
                    binding.pbBuffering.visibility = View.VISIBLE
                }
            }

            override fun onPlayerError(error: ExoPlaybackException) {
                Log.e("exoPlayer", error.message!!)
            }
        })

        binding.ivVolume.setOnClickListener {

            if (player!!.volume == 0F) {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_up)
                player!!.volume = 1.0F
            }
            else {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_down)
                player!!.volume = 0F
            }
        }
    }

    private fun initViews() {

        var isNameClicked = false
        var isPhoneClicked = false
        var isEmailClicked = false

        //        binding.etName.setOnFocusChangeListener { v, hasFocus ->
        //            run {
        //                if (hasFocus && !isNameClicked) {
        //                    binding.swName.isChecked = true
        //                    isNameClicked = true
        //                }
        //            }
        //        }

        binding.etPhone.setOnFocusChangeListener { v, hasFocus ->
            run {
                if (hasFocus && !isPhoneClicked) {
                    binding.swPhone.isChecked = true
                    isPhoneClicked = true
                }
            }
        }

        binding.etEmail.setOnFocusChangeListener { v, hasFocus ->
            run {
                if (hasFocus && !isEmailClicked) {
                    binding.swEmail.isChecked = true
                    isEmailClicked = true
                }
            }
        }

        binding.etName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                validateButton()
            }
        })

        binding.etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                validateButton()
            }
        })

        binding.etEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                validateButton()
            }
        })
    }

    private fun validateButton() {
        binding.apply {
            if (etName.text.toString().isNotEmpty() && etPhone.text.toString().isNotEmpty() && etEmail.text.toString().isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.text.toString()).matches()) {
                buttonNext.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.button))
                buttonNext.setTextColor(resources.getColor(R.color.white))
                buttonNext.isEnabled = true
                textViewNext.visibility = View.VISIBLE
            }
            else {
                buttonNext.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
                buttonNext.setTextColor(resources.getColor(R.color.text))
                buttonNext.isEnabled = false
                textViewNext.visibility = View.GONE
            }
        }
    }

    override fun onItemClick(position: Int) {

        val dialog = Dialog(this, R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val dialogSocialBinding: DialogSocialBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_social, null, false)
        dialog.setContentView(dialogSocialBinding.root)

        dialogSocialBinding.imageViewSocial.setImageResource(mList[position].socialDrawableFilled)
        dialogSocialBinding.textViewTitle.text = (mList[position].socialName)
        dialogSocialBinding.textViewSubTitle.text = (mList[position].socialName)

        dialogSocialBinding.editTextUsername.setText(mList[position].socialUsername)

        dialogSocialBinding.apply {
            switchPublic.isChecked = mList[position].socialPublicToggle

            if (editTextUsername.text.toString() != mList[position].socialUsername) {
                buttonSave.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.button))
                buttonSave.setTextColor(resources.getColor(R.color.white))
                buttonSave.isEnabled = true
            }
            else {
                buttonSave.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
                buttonSave.setTextColor(resources.getColor(R.color.text))
                buttonSave.isEnabled = false
            }
        }

        dialogSocialBinding.buttonSave.setOnClickListener {

            dialogSocialBinding.root.hideKeyboard()

            if (dialogSocialBinding.editTextUsername.text.toString().isEmpty()) {
                mList[position].isSelected = false
                mList[position].socialPublicToggle = false

                mList[position].socialUsername = dialogSocialBinding.editTextUsername.text.toString()
                val item = socialModelList.find {
                    it.tag == mList[position].socialName
                }

                if(item!=null){
                    socialModelList.remove(item)
                }

            }
            else {

                mList[position].isSelected = true
                mList[position].socialUsername = dialogSocialBinding.editTextUsername.text.toString()
                mList[position].socialPublicToggle = dialogSocialBinding.switchPublic.isChecked

                val item = socialModelList.find {
                    it.tag == mList[position].socialName
                }
                if(item==null){
                    socialModelList.add(SocialMediaModelList(mList[position].socialName, mList[position].socialPublicToggle, mList[position].socialUsername))
                }else {
                    for (i in socialModelList.indices){
                        if(socialModelList[i].tag == mList[position].socialName){
                            socialModelList[i].isPublic = mList[position].socialPublicToggle
                            socialModelList[i].url = mList[position].socialUsername
                        }
                    }
                }

            }

            adapterSocial!!.notifyDataSetChanged()
            dialog.dismiss()

//            this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }

        //        var isUsernameClicked = false

        dialogSocialBinding.editTextUsername.setOnFocusChangeListener { v, hasFocus ->
            run {
                if (hasFocus && dialogSocialBinding.editTextUsername.text.toString().isEmpty()) {
                    dialogSocialBinding.switchPublic.isChecked = true
                    mList[position].socialPublicToggle = true
                    //                    isUsernameClicked = true
                }
            }
        }

        dialogSocialBinding.editTextUsername.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {

                dialogSocialBinding.apply {
                    if (editTextUsername.text.toString() != mList[position].socialUsername) {
                        buttonSave.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.button))
                        buttonSave.setTextColor(resources.getColor(R.color.white))
                        buttonSave.isEnabled = true
                    }
                    else {
                        buttonSave.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
                        buttonSave.setTextColor(resources.getColor(R.color.text))
                        buttonSave.isEnabled = false
                    }
                }
            }
        })

        dialogSocialBinding.switchPublic.setOnCheckedChangeListener { _, isChecked ->

            dialogSocialBinding.apply {
                if (switchPublic.isChecked != mList[position].socialPublicToggle || editTextUsername.text.toString() != mList[position].socialUsername) {
                    if(editTextUsername.text.toString().isNotEmpty()) {
                        buttonSave.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.button))
                        buttonSave.setTextColor(resources.getColor(R.color.white))
                        buttonSave.isEnabled = true
                    }
                }
                else {
                    buttonSave.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
                    buttonSave.setTextColor(resources.getColor(R.color.text))
                    buttonSave.isEnabled = false
                }
            }
        }

        dialog.setOnCancelListener {
            if(mList[position].socialUsername.isEmpty()) {
                mList[position].isSelected = false
                mList[position].socialPublicToggle = false
            }
        }

        dialog.show()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    fun releasePlayer() {
        player?.run {
            playbackPosition = this.currentPosition
            currentWindow = this.currentWindowIndex
            playWhenReady = this.playWhenReady
            release()
        }
        player = null

    }


    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    override fun onResume() {
        super.onResume()
        if(!isFromEdit)
            if (isVideo) initializePlayer(binding.videoView, videoUri!!)
    }
}