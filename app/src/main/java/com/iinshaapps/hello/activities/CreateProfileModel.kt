package com.iinshaapps.hello.activities

class CreateProfileModel {

    var title = ""
    var startColor = 0
    var endColor = 0
    var icon = 0
    var position = 0

    constructor(title: String, startColor: Int, endColor: Int, icon: Int, position: Int) {
        this.title = title
        this.startColor = startColor
        this.endColor = endColor
        this.icon = icon
        this.position = position
    }

    constructor()


}
