package com.iinshaapps.hello.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

open class AuthActivity : BaseActivity() {

    companion object {
        lateinit var mActivity: AppCompatActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity
    }


}