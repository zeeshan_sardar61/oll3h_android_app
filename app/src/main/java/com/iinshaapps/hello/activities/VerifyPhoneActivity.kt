package com.iinshaapps.hello.activities

import `in`.aabhasjindal.otptextview.OTPListener
import android.app.Activity
import android.content.*
import android.content.res.ColorStateList
import android.os.Build
import android.view.View
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.ActivityVerifyPhoneBinding
import com.iinshaapps.hello.helper.*
import com.iinshaapps.hello.model.requestmodel.AddRecoveryNumberRequest
import com.iinshaapps.hello.model.response.RecoveryCodeResponse
import com.iinshaapps.hello.model.response.VerifyPhoneResponse

class VerifyPhoneActivity : BaseActivity(), IGenericCallBack {

    lateinit var binding: ActivityVerifyPhoneBinding
    var cc = ""
    var phone = ""
    val userId = if (Hello.db.getString(Constants.USER_ID).isNullOrEmpty()) {
        null
    }
    else {
        Hello.db.getString(Constants.USER_ID)
    }

    private val SMS_CONSENT_REQUEST = 2  // Set to an unused request code

    private val smsVerificationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
                val extras = intent.extras
                val smsRetrieverStatus = extras?.get(SmsRetriever.EXTRA_STATUS) as Status

                when (smsRetrieverStatus.statusCode) {
                    CommonStatusCodes.SUCCESS -> {
                        binding.root.hideKeyboard()
                        Log.e("sms", "2_sms")
                        val consentIntent = extras.getParcelable<Intent>(SmsRetriever.EXTRA_CONSENT_INTENT)
                        try {
                            startActivityForResult(consentIntent, SMS_CONSENT_REQUEST)
                        } catch (e: ActivityNotFoundException) {

                        }
                    }
                    CommonStatusCodes.TIMEOUT -> {

                    }
                }
            }
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verify_phone)

        startSMSListener()
        cc = intent.getStringExtra("countryCode")!!
        phone = intent.getStringExtra("phone")!!

        binding.tvPhone.text = cc + phone
        initViews()
    }

    private fun startSMSListener() {
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        registerReceiver(smsVerificationReceiver, intentFilter)

        val mClient = SmsRetriever.getClient(this)
        val mTask = mClient.startSmsUserConsent(null)
        mTask.addOnSuccessListener {

        }
        mTask.addOnFailureListener {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }

    }

    private fun initViews() {
        binding.countdownView.start(60 * 1000)
        binding.otpView.requestFocusOTP()

        binding.otpView.otpListener = object : OTPListener {
            override fun onInteractionListener() {
                val otp = binding.otpView.otp
                if (otp?.length!! < 4) {
                    binding.buttonVerify.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
                    binding.buttonVerify.setTextColor(resources.getColor(R.color.text))
                    binding.buttonVerify.isEnabled = false
                }
            }

            override fun onOTPComplete(otp: String) {
                binding.buttonVerify.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.button))
                binding.buttonVerify.setTextColor(resources.getColor(R.color.white))
                binding.buttonVerify.isEnabled = true
            }
        }


        binding.tvTryAgain.setOnClickListener {
            binding.tvText.visibility = View.GONE
            binding.tvTryAgain.visibility = View.GONE

            val call = Hello.apiService.addRecoveryNumber(AddRecoveryNumberRequest(null, null, null, phone, cc, null, userId, null, null, null))

            SingleEnqueueCall.callRetrofit(this, call, Constants.ADD_RECOVERY_NUMBER_URL, true, this)
        }


        binding.countdownView.setOnCountdownEndListener {
            binding.tvText.visibility = View.VISIBLE
            binding.tvTryAgain.visibility = View.VISIBLE
        }

        binding.buttonVerify.setOnClickListener {

            if (intent.extras?.getString("settings") != null) {
                val intent = Intent(baseContext, MainActivity::class.java)
                intent.putExtra("settings", "settings")
                startActivity(intent)
            }
            else {

                val deviceType = 1
                val deviceModel: String = Build.MANUFACTURER + " " + Build.MODEL
                val os: String = currentOS()
                val version: String = Build.VERSION.RELEASE
                val uniqueIdentifier: String = this.getDeviceIMEI()

                val call = Hello.apiService.verifyCode(AddRecoveryNumberRequest(deviceType.toString(), binding.otpView.otp.toString().toInt(), os, phone, cc, deviceModel, userId, version, Hello.db.getString(Constants.FCM_TOKEN), uniqueIdentifier))
                SingleEnqueueCall.callRetrofit(this, call, Constants.VERIFY_CODE_URL, true, this)

            }
        }

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }
    }

    override fun success(apiName: String, response: Any?) {
        when (apiName) {
            Constants.ADD_RECOVERY_NUMBER_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    activity.showToastMessage(model.message)
                }
                else {
                    binding.countdownView.start(60 * 1000)
                    binding.tvText.visibility = View.GONE
                    binding.tvTryAgain.visibility = View.GONE
                }
            }
            Constants.VERIFY_CODE_URL -> {
                val model = response as VerifyPhoneResponse
                if (model.isError) {
                    binding.otpView.showError()
                    activity.showToastMessage(model.message)
                }
                else {
                    binding.otpView.showSuccess()
                    model.data.authToken?.let { Hello.db.putString(Constants.AUTH_TOKEN, it) }
                    model.data.userId?.let { Hello.db.putString(Constants.USER_ID, it) }
                    Hello.db.putString(Constants.USER_CC, cc)
                    Hello.db.putString(Constants.USER_PHONE, phone)
                    Hello.db.putBoolean(Constants.IS_LOGIN, true)
                    val i = Intent(this, MainActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                    finish()
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
        activity.showToastMessage(message!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SMS_CONSENT_REQUEST ->
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE)!!
                    val oneTimeCode = message.substring(0, 4)

                    binding.otpView.setOTP(oneTimeCode)
                }
        }

    }
}