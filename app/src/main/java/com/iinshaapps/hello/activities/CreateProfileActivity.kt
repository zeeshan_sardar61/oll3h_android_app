package com.iinshaapps.hello.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.PagerSnapHelper
import com.iinshaapps.hello.R
import com.iinshaapps.hello.adapter.ProfileAdapter
import com.iinshaapps.hello.databinding.CreateProfileBinding
import com.iinshaapps.hello.helper.OnSnapPositionChangeListener
import com.iinshaapps.hello.helper.SnapOnScrollListener

class CreateProfileActivity : AppCompatActivity(), ProfileAdapter.ItemClicker, OnSnapPositionChangeListener {

    var mList = ArrayList<CreateProfileModel>()
    lateinit var binding: CreateProfileBinding
    var adapterProfiles: ProfileAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.create_profile)

        mList.add(CreateProfileModel("Personal", Color.parseColor("#3841BF"), Color.parseColor("#E60046"), R.drawable.ic_personal, 1))
        mList.add(CreateProfileModel("Business", Color.parseColor("#245199"), Color.parseColor("#07275C"), R.drawable.ic_business, 2))

        val snap = PagerSnapHelper()
        snap.attachToRecyclerView(binding.rvProfileCards)

        adapterProfiles = ProfileAdapter(mList)
        adapterProfiles?.mListener = this
        binding.rvProfileCards.adapter = adapterProfiles

        binding.tvAlreadyAccount.setOnClickListener {
            val i = Intent(this, RecoveryNumberActivity::class.java)
            i.putExtra("isFromPayment", false)
            startActivity(i)
        }

        val snapOnScrollListener = SnapOnScrollListener(snap, SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL, this)
        binding.rvProfileCards.addOnScrollListener(snapOnScrollListener)

        binding.rvProfileCards.setIntervalRatio(0.8f)
    }


    override fun onItemClick(position: Int) {
        if(position==0){
            val i = Intent(this, CreatePersonalProfileActivity::class.java)
            i.putExtra("signup", true)
            startActivity(i)

        }else if(position==1){
            val i = Intent(this, CreateBusinessProfileActivity::class.java)
            i.putExtra("signup", true)
            startActivity(i)
        }
    }

    override fun itemPositionChanged(position: Int) {
    }

    override fun onSnapPositionChange(position: Int) {
        when (position) {
            0 -> {
                binding.ivPage1.setImageResource(R.drawable.ic_dot_selected)
                binding.ivPage2.setImageResource(R.drawable.ic_dot_unselected)
            }
            1 -> {
                binding.ivPage1.setImageResource(R.drawable.ic_dot_unselected)
                binding.ivPage2.setImageResource(R.drawable.ic_dot_selected)
            }
        }
    }
}
