package com.iinshaapps.hello.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.ActivitySplashBinding
import com.iinshaapps.hello.helper.Constants

class Splash : AppCompatActivity() {
    lateinit var binding: ActivitySplashBinding

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) //        val umm: UiModeManager = getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
        //        umm.nightMode = AppCompatDelegate.MODE_NIGHT_YES
        //        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        //        delegate.setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash) //        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        //            setStatusBarColor(
        //                ContextCompat.getColor(this, android.R.color.white),
        //                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        //            )
        //        }
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        Handler(Looper.myLooper()!!).postDelayed({
            toTheAuth()
        }, 4000)
    }

    private fun toTheAuth() {

        val i = if (Hello.db.getBoolean(Constants.IS_LOGIN)) {
            Intent(baseContext, MainActivity::class.java)
        }
        else {
            if (Hello.db.getString(Constants.USER_ID).isNullOrEmpty()) {
                Intent(baseContext, TourScreen::class.java)
            }
            else {
                Intent(baseContext, RestorePurchasedActivity::class.java)
            }
        }

        if (Hello.db.getBoolean(Constants.IS_LOGIN)) {
            if (intent != null) {
                if (intent!!.extras != null && intent.hasExtra("name")) {
                    val name = intent!!.extras!!.get("name") as String
                    i.putExtra("name", name)
                }
            }
        }

        startActivity(i)
        finish()


    }
}