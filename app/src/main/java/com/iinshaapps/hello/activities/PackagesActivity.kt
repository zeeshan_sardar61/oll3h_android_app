package com.iinshaapps.hello.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.android.billingclient.api.*
import com.google.gson.Gson
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.adapter.PackageAdapter
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.PackagesActivityBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.OnSnapPositionChangeListener
import com.iinshaapps.hello.helper.SnapOnScrollListener
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.CreateBusinessProfileModel
import com.iinshaapps.hello.model.CreatePersonalProfileModel
import com.iinshaapps.hello.model.response.CreateProfileResponseModel
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File


class PackagesActivity : AppCompatActivity(), OnSnapPositionChangeListener, PackageAdapter.ItemClicker, IGenericCallBack, PurchasesUpdatedListener, AcknowledgePurchaseResponseListener {

    lateinit var binding: PackagesActivityBinding
    var mList = ArrayList<PackageModel>()
    var list = ArrayList<String>()
    var adapter: PackageAdapter? = null
    private var position_ = 0
    var paymentId = ""
    var paymentType = 1

    private var skuList = ArrayList<String>()
    lateinit var billingClient : BillingClient
    lateinit var personalProfileModel: CreatePersonalProfileModel
    lateinit var businessProfileModel: CreateBusinessProfileModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.packages_activity)

        billingClient = BillingClient
            .newBuilder(this)
            .setListener(this)
            .enablePendingPurchases()
            .build()

        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    Log.e("Billing", "BILLING | startConnection | RESULT OK")
                } else {
                    Log.e("Billing", "BILLING | startConnection | RESULT: "+billingResult.responseCode)
                }
            }

            override fun onBillingServiceDisconnected() {
                Log.e("Billing", "BILLING | onBillingServiceDisconnected | DISCONNECTED")
            }
        })


        if (intent.getBooleanExtra("isPersonal", false)) {
            personalProfileModel = (intent.getSerializableExtra("model") as? CreatePersonalProfileModel)!!
        }
        else {
            businessProfileModel = (intent.getSerializableExtra("model") as? CreateBusinessProfileModel)!!
        }
        list.add("Save Content")
        list.add("Add to Wallet")
        list.add("My Card Directory")
        list.add("Auto Renew Subscription")

        mList.add(PackageModel(0.99, "PER MONTH", list))
        mList.add(PackageModel(4.99, "PER YEAR", list))

        val snap = PagerSnapHelper()
        snap.attachToRecyclerView(binding.rvPackages)
        binding.rvPackages.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        adapter = PackageAdapter(this, mList)
        adapter?.mListener = this
        binding.rvPackages.adapter = adapter

        val snapOnScrollListener = SnapOnScrollListener(snap, SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL, this)
        binding.rvPackages.addOnScrollListener(snapOnScrollListener)
    }

    override fun onSnapPositionChange(position: Int) {
        when (position) {
            0 -> {
                binding.ivPage1.setImageResource(R.drawable.ic_dot_selected)
                binding.ivPage2.setImageResource(R.drawable.ic_dot_unselected)
            }
            else -> {
                binding.ivPage1.setImageResource(R.drawable.ic_dot_unselected)
                binding.ivPage2.setImageResource(R.drawable.ic_dot_selected)
            }
        }
    }

    override fun onItemClick(position: Int) {
        position_ = position
        var paymentType_ = ""
        if (position_ == 0) {
            paymentType = 1
            skuList.clear()
//            skuList.add(resources.getString(R.string.monthly_product_id))
            skuList.add("android.test.purchased")
//            paymentType_ = BillingClient.SkuType.SUBS
            paymentType_ = BillingClient.SkuType.INAPP
        }
        else {
            paymentType =2
            skuList.clear()
/*
            OLD
            skuList.add(resources.getString(R.string.product_id))
*/

            //NEW
//            skuList.add(resources.getString(R.string.annual_product_id))
            skuList.add("android.test.purchased")
//            paymentType_ = BillingClient.SkuType.SUBS
            paymentType_ = BillingClient.SkuType.INAPP
        }
        if (billingClient.isReady) {
            val params = SkuDetailsParams
                .newBuilder()
                .setSkusList(skuList)
                .setType(paymentType_)
                .build()
            billingClient.querySkuDetailsAsync(params) { billingResult, skuDetailsList ->
                if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                    Log.e("Billing", "querySkuDetailsAsync, responseCode: "+ billingResult.responseCode)
                    launchBillingFlow(skuDetailsList!!)
                } else {
                    Log.e("Billing", "Can't querySkuDetailsAsync, responseCode: "+billingResult.responseCode)
                }
            }
        } else {
            Log.e("Billing", "Billing Client not ready")
        }

    }

    private fun launchBillingFlow(skuDetailsList: List<SkuDetails>) {
        val billingFlowParams = BillingFlowParams
            .newBuilder()
            .setSkuDetails(skuDetailsList[0])
            .build()
        billingClient.launchBillingFlow(this, billingFlowParams)
    }

    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: MutableList<Purchase>?) {
        Log.e("Billing", "onPurchasesUpdated: "+billingResult.responseCode)
        if(billingResult.responseCode == BillingClient.BillingResponseCode.OK){
            handlePurchase(purchases?.first()!!)
            paymentId = purchases.first().orderId
            callApi()
        }
    }

    private fun handlePurchase(purchase: Purchase) {
        if (!purchase.isAcknowledged) {
            val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder().setPurchaseToken(purchase.purchaseToken).build()
            billingClient.acknowledgePurchase(acknowledgePurchaseParams, this)
        }

    }


    private fun callApi() {
        if (intent.getBooleanExtra("isPersonal", false)) {
            var socialData: String? = null
            if (personalProfileModel.socialMediaList!!.isNotEmpty()) {
                socialData = Gson().toJson(personalProfileModel.socialMediaList)
                Log.d("socialModel", socialData)
            }

            personalProfileModel.paymentId = mList[position_].amount.toString()+" " +paymentId

            val name = personalProfileModel.name.toRequestBody("text/plain".toMediaTypeOrNull())
            val paymentId = personalProfileModel.paymentId.toRequestBody("text/plain".toMediaTypeOrNull())
            val paymentType_ = paymentType.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val isNamePublic = personalProfileModel.isNamePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val email = personalProfileModel.email.toRequestBody("text/plain".toMediaTypeOrNull())
            val isEmailPublic = personalProfileModel.isEmailPublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val c_code = personalProfileModel.countryCode.toRequestBody("text/plain".toMediaTypeOrNull())
            val contact = personalProfileModel.phone.toRequestBody("text/plain".toMediaTypeOrNull())
            val isPhonePublic = personalProfileModel.isPhonePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val socialMedia = socialData?.toRequestBody("text/plain".toMediaTypeOrNull())
            val cardStartColor = personalProfileModel.startColor.toRequestBody("text/plain".toMediaTypeOrNull())
            val cardEndColor = personalProfileModel.endColor.toRequestBody("text/plain".toMediaTypeOrNull())
            val isVideo = personalProfileModel.isVideo.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            var profilePic: MultipartBody.Part? = null

            if (personalProfileModel.uriPath != null) {
                if (personalProfileModel.isVideo) {
                    val mediaType = "video/*"
                    val fileName = "video_" + System.currentTimeMillis() + ".mp4"
                    val file: File = File(personalProfileModel.uriPath!!)
                    val reqFile = file.asRequestBody(mediaType.toMediaTypeOrNull())
                    profilePic = MultipartBody.Part.createFormData("file", fileName, reqFile)
                }
                else {
                    val mediaType: String = "image/png"
                    val fileName: String = "photo_" + System.currentTimeMillis() + ".png"
                    val file: File = File(personalProfileModel.uriPath!!)

                    val reqFile = file.asRequestBody(mediaType.toMediaTypeOrNull())
                    profilePic = MultipartBody.Part.createFormData("file", fileName, reqFile)

                }
            }

            val call = Hello.apiService.createPersonalProfile(name, paymentId, paymentType_, isNamePublic, c_code, contact, isPhonePublic, email, isEmailPublic, socialMedia, cardStartColor, cardEndColor, isVideo, profilePic)
            SingleEnqueueCall.callRetrofit(this, call, Constants.CREATE_PERSONAL_PROFILE_URL, true, this@PackagesActivity)

        } else {

            var socialData: String? = null
            if (businessProfileModel.socialMediaList!!.isNotEmpty()) {
                socialData = Gson().toJson(businessProfileModel.socialMediaList)
                Log.d("socialModel", socialData)
            }

            businessProfileModel.paymentId = mList[position_].amount.toString()+" " +paymentId

            val businessName = businessProfileModel.businessName.toRequestBody("text/plain".toMediaTypeOrNull())
            val paymentId = businessProfileModel.paymentId.toRequestBody("text/plain".toMediaTypeOrNull())
            val paymentType_ = paymentType.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val name = businessProfileModel.contactName.toRequestBody("text/plain".toMediaTypeOrNull())
            val isNamePublic = businessProfileModel.isNamePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val email = businessProfileModel.email.toRequestBody("text/plain".toMediaTypeOrNull())
            val isEmailPublic = businessProfileModel.isEmailPublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val c_code = businessProfileModel.countryCode.toRequestBody("text/plain".toMediaTypeOrNull())
            val contact = businessProfileModel.phone.toRequestBody("text/plain".toMediaTypeOrNull())
            val extension = businessProfileModel.ext.toRequestBody("text/plain".toMediaTypeOrNull())
            val isPhonePublic = businessProfileModel.isPhonePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val socialMedia = socialData?.toRequestBody("text/plain".toMediaTypeOrNull())

            val cardStartColor = businessProfileModel.startColor.toRequestBody("text/plain".toMediaTypeOrNull())
            val cardEndColor = businessProfileModel.endColor.toRequestBody("text/plain".toMediaTypeOrNull())

            val isVideo = businessProfileModel.isVideo.toString().toRequestBody("text/plain".toMediaTypeOrNull())

            var mediaType = "image/png"
            var fileName = "photo_" + System.currentTimeMillis() + ".png"

            if (businessProfileModel.isVideo) {
                mediaType = "video/*"
                fileName = "video_" + System.currentTimeMillis() + ".mp4"
            }

            var profilePic: MultipartBody.Part? = null
            var logo: MultipartBody.Part? = null
            var card: MultipartBody.Part? = null

            if (businessProfileModel.profileUri != null) {
                val file: File = File(businessProfileModel.profileUri!!)
                val reqFile1 = file.asRequestBody(mediaType.toMediaTypeOrNull())
                profilePic = MultipartBody.Part.createFormData("profile", fileName, reqFile1)
            }
            if (businessProfileModel.logoUri != null) {
                val logoF: File = File(businessProfileModel.logoUri!!)
                val reqFile2 = logoF.asRequestBody(mediaType.toMediaTypeOrNull())
                logo = MultipartBody.Part.createFormData("logo", fileName, reqFile2)
            }
            if (businessProfileModel.businessCardUri != null) {
                val cardF: File = File(businessProfileModel.businessCardUri!!)
                val reqFile3 = cardF.asRequestBody(mediaType.toMediaTypeOrNull())
                card = MultipartBody.Part.createFormData("businessCard", fileName, reqFile3)
            }

            val call = Hello.apiService.createBusinessProfile(businessName, paymentId, paymentType_, name, isNamePublic, c_code, contact, extension, isPhonePublic, email, isEmailPublic, socialMedia, cardStartColor, cardEndColor, isVideo, profilePic!!, logo!!, card)
            SingleEnqueueCall.callRetrofit(this, call, Constants.CREATE_BUSINESS_PROFILE_URL, true, this@PackagesActivity)

        }
    }


    override fun onDestroy() {
        billingClient.endConnection()
        super.onDestroy()
    }

    override fun success(apiName: String, response: Any?) {
        when (apiName) {
            Constants.CREATE_PERSONAL_PROFILE_URL -> {
                val model = response as CreateProfileResponseModel
                if (model.isError) {
                    showToastMessage(model.message)
                }
                else {
                    Hello.db.putString(Constants.USER_ID, model.data)
                    gotoSuccessScreen()
                }
            }
            Constants.CREATE_BUSINESS_PROFILE_URL -> {
                val model = response as CreateProfileResponseModel
                if (model.isError) {
                    showToastMessage(model.message)
                }
                else {
                    Hello.db.putString(Constants.USER_ID, model.data)
                    gotoSuccessScreen()
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
        showToastMessage(message!!)
    }

    private fun gotoSuccessScreen() {
        val i = Intent(this, PaymentSuccessfulActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
        finish()
    }

    override fun onAcknowledgePurchaseResponse(billingResult: BillingResult) {

    }

}