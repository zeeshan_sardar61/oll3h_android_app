package com.iinshaapps.hello.activities

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.ActivityRecoveryNumberBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.requestmodel.AddRecoveryNumberRequest
import com.iinshaapps.hello.model.response.RecoveryCodeResponse

class RecoveryNumberActivity : BaseActivity(), IGenericCallBack {

    lateinit var binding: ActivityRecoveryNumberBinding
    private var isNumberValid = false
    var isFromPayment = false
    var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recovery_number)
        isFromPayment = intent.getBooleanExtra("isFromPayment", false)
        setListener()
    }

    private fun setListener() {
//        binding.ccpNumber.backgroundColor = resources.getColor(R.color.white)
//        binding.ccpNumber.textColor = resources.getColor(R.color.black)
        binding.ccpNumber.registerPhoneNumberTextView(binding.etPhone)
        binding.ccpNumber.setPhoneNumberInputValidityListener { _, b ->
            isNumberValid = b
        }

        binding.etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                validateButton()
            }
        })

        binding.buttonVerify.setOnClickListener {

            val userId = if (Hello.db.getString(Constants.USER_ID).isNullOrEmpty()) {
                null
            }
            else {
                Hello.db.getString(Constants.USER_ID)
            }

            val call = Hello.apiService.addRecoveryNumber(AddRecoveryNumberRequest(null, null, null, binding.etPhone.text.toString(), "+" + binding.ccpNumber.selectedCountryCode.toString().trim(), null, userId, null, null, null))

            SingleEnqueueCall.callRetrofit(this, call, Constants.ADD_RECOVERY_NUMBER_URL, true, this)

        }

        if (isFromPayment) {
            binding.imageViewBack.visibility = View.GONE
        }

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun validateButton() {

        binding.apply {
            if (etPhone.text.toString().isNotEmpty()) {
                buttonVerify.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.button))
                buttonVerify.setTextColor(resources.getColor(R.color.white))
                buttonVerify.isEnabled = true
            }
            else {
                buttonVerify.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.grey))
                buttonVerify.setTextColor(resources.getColor(R.color.text))
                buttonVerify.isEnabled = false
            }
        }
    }

    override fun success(apiName: String, response: Any?) {
        when (apiName) {
            Constants.ADD_RECOVERY_NUMBER_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    showToastMessage(model.message)
                }
                else {
                    val i = Intent(this, VerifyPhoneActivity::class.java)
                    i.putExtra("countryCode", "+" + binding.ccpNumber.selectedCountryCode.toString().trim())
                    i.putExtra("phone", binding.etPhone.text.toString())
                    startActivity(i)
                    finish()
                }
            }
        }
    }

    override fun failure(apiName: String, message: String?) {
        when (apiName) {
            Constants.ADD_RECOVERY_NUMBER_URL -> {
                if (message != null) {
                    showToastMessage(message)
                }
            }
        }
    }

    override fun onBackPressed() {
        if (isFromPayment) {
            if (doubleBackToExitPressedOnce) {
                finishAffinity()
                return
            }
            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
            Handler(Looper.getMainLooper()).postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
        }
        else {
            super.onBackPressed()
        }
    }
}