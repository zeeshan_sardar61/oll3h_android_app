package com.iinshaapps.hello.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.media.ThumbnailUtils
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.R
import com.iinshaapps.hello.adapter.ChooseColorAdapter
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.PersonalizedCardActivityBinding
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.compressBitmap
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.CreateBusinessProfileModel
import com.iinshaapps.hello.model.CreatePersonalProfileModel
import com.iinshaapps.hello.model.DesignModel
import com.iinshaapps.hello.model.response.CreateProfileResponseModel
import com.iinshaapps.hello.model.response.RecoveryCodeResponse
import com.bumptech.glide.Glide
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.io.IOException

class PersonalizedCardActivity : AuthActivity(), ChooseColorAdapter.ItemClicker, IGenericCallBack {

    lateinit var binding: PersonalizedCardActivityBinding
    var mList = ArrayList<DesignModel>()
    lateinit var adapter: ChooseColorAdapter
    lateinit var personalProfileModel: CreatePersonalProfileModel
    lateinit var businessProfileModel: CreateBusinessProfileModel
    var title = ""
    var startColor = ""
    var endColor = ""
    var isFromSignup = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.personalized_card_activity)

        isFromSignup = intent.getBooleanExtra("signup", false)
        Log.d("bucks", "personalized: $isFromSignup")

        title = intent.getStringExtra("previousActivity")!!
        if (title != "RegisterBusinessProfile") {
            binding.rlCardPersonal.visibility = View.VISIBLE
            binding.rlCardBusiness.visibility = View.GONE
            personalProfileModel = (intent.getSerializableExtra("model") as? CreatePersonalProfileModel)!!

            setPersonalCardData()
        }
        else {
            binding.rlCardPersonal.visibility = View.GONE
            binding.rlCardBusiness.visibility = View.VISIBLE
            businessProfileModel = (intent.getSerializableExtra("model") as? CreateBusinessProfileModel)!!

            setBusinessCardData()
        }

        when (title) {
            "RegisterBusinessProfile" -> {
                binding.tvTitle.text = getString(R.string.business)
            }
        }

        setDesignAdapter()

        if (intent.getBooleanExtra("isEdit", false)) {
            binding.buttonActivate.text = "Update Card"
        }

        binding.buttonActivate.setOnClickListener {

            if (isFromSignup) {
                val intent = Intent(baseContext, PackagesActivity::class.java)
                if (title != "RegisterBusinessProfile") {
                    intent.putExtra("isPersonal", true)
                    intent.putExtra("model", personalProfileModel)
                }
                else {
                    intent.putExtra("isPersonal", false)
                    intent.putExtra("model", businessProfileModel)
                }
                startActivity(intent)
            }
            else {
                callApi()
            }
        }
    }

    private fun setPersonalCardData() {
        if (personalProfileModel.uriPath != null) {
            if (personalProfileModel.isVideo) {
                var thumb = personalProfileModel.uriPath!!.let { ThumbnailUtils.createVideoThumbnail(it, MediaStore.Images.Thumbnails.MINI_KIND) }
                val file = File(cacheDir, "test.png")
                try {
                    file.createNewFile()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                thumb = compressBitmap(thumb!!)
                binding.ivPersonal.setImageBitmap(thumb)
            }
            else {
                val imgFile = File(personalProfileModel.uriPath!!)

                if (imgFile.exists()) {
                    val myBitmap: Bitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
                    binding.ivPersonal.setImageBitmap(myBitmap)
                }
            }
        } else if(personalProfileModel.profileUrl.isNotEmpty()){
            Glide.with(this).load(personalProfileModel.profileUrl).into(binding.ivPersonal)
        }
        binding.tvNamePersonal.text = personalProfileModel.name
        binding.tvPhonePersonal.text = personalProfileModel.countryCode + " " + personalProfileModel.phone
        binding.tvEmailPersonal.text = personalProfileModel.email
    }

    private fun setBusinessCardData() {

        if (businessProfileModel.logoUri != null) {
            val imgFile = File(businessProfileModel.logoUri!!)

            if (imgFile.exists()) {
                val myBitmap: Bitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
                binding.ivLogo.setImageBitmap(myBitmap)
            }
        }
        else {
            Glide.with(this).load(businessProfileModel.logoThumbnail).into(binding.ivLogo)
        }

        binding.tvCompanyName.text = businessProfileModel.businessName
        binding.tvNameBusiness.text = businessProfileModel.contactName
        binding.tvPhoneBusiness.text = businessProfileModel.countryCode + " " + businessProfileModel.phone
        binding.tvEmailBusiness.text = businessProfileModel.email


    }

    private fun callApi() {

        if (title != "RegisterBusinessProfile") {
            var socialData: String? = null
            if (personalProfileModel.socialMediaList!!.isNotEmpty()) {
                socialData = Gson().toJson(personalProfileModel.socialMediaList)
                Log.d("socialModel", socialData)
            }

            val name = personalProfileModel.name.toRequestBody("text/plain".toMediaTypeOrNull())
            val paymentId = "".toRequestBody("text/plain".toMediaTypeOrNull())
            val isNamePublic = personalProfileModel.isNamePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val email = personalProfileModel.email.toRequestBody("text/plain".toMediaTypeOrNull())
            val isEmailPublic = personalProfileModel.isEmailPublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val c_code = personalProfileModel.countryCode.toRequestBody("text/plain".toMediaTypeOrNull())
            val contact = personalProfileModel.phone.toRequestBody("text/plain".toMediaTypeOrNull())
            val isPhonePublic = personalProfileModel.isPhonePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val socialMedia = socialData?.toRequestBody("text/plain".toMediaTypeOrNull())

            val cardStartColor = personalProfileModel.startColor.toRequestBody("text/plain".toMediaTypeOrNull())
            val cardEndColor = personalProfileModel.endColor.toRequestBody("text/plain".toMediaTypeOrNull())

            val isVideo = personalProfileModel.isVideo.toString().toRequestBody("text/plain".toMediaTypeOrNull())

            var url_: RequestBody? = null
            var thumb_: RequestBody? = null
            var profilePic: MultipartBody.Part? = null

            if (personalProfileModel.uriPath != null) {
                if (personalProfileModel.isVideo) {
                    val mediaType = "video/*"
                    val fileName = "video_" + System.currentTimeMillis() + ".mp4"
                    val file: File = File(personalProfileModel.uriPath!!)
                    val reqFile = file.asRequestBody(mediaType.toMediaTypeOrNull())
                    profilePic = MultipartBody.Part.createFormData("file", fileName, reqFile)
                }
                else {
                    val mediaType: String = "image/png"
                    val fileName: String = "photo_" + System.currentTimeMillis() + ".png"
                    val file: File = File(personalProfileModel.uriPath!!)

                    val reqFile = file.asRequestBody(mediaType.toMediaTypeOrNull())
                    profilePic = MultipartBody.Part.createFormData("file", fileName, reqFile)

                }
            }

            if (intent.getBooleanExtra("isEdit", false)) {
                url_ = personalProfileModel.profileUrl.toRequestBody("text/plain".toMediaTypeOrNull())
                thumb_ = personalProfileModel.profileThumbnail.toRequestBody("text/plain".toMediaTypeOrNull())
            }

            if (intent.getBooleanExtra("isEdit", false)) {
                val call = Hello.apiService.updatePersonalProfile(name, isNamePublic, c_code, contact, isPhonePublic, email, isEmailPublic, socialMedia, cardStartColor, cardEndColor, isVideo, url_!!, thumb_!!, profilePic)
                SingleEnqueueCall.callRetrofit(this, call, Constants.UPDATE_PERSONAL_PROFILE_URL, true, this@PersonalizedCardActivity)
            }
            else {
                val call = Hello.apiService.createPersonalProfile(name, paymentId, null,isNamePublic, c_code, contact, isPhonePublic, email, isEmailPublic, socialMedia, cardStartColor, cardEndColor, isVideo, profilePic)
                SingleEnqueueCall.callRetrofit(this, call, Constants.CREATE_PERSONAL_PROFILE_URL, true, this@PersonalizedCardActivity)
            }


        }
        else {
            var socialData: String? = null
            if (businessProfileModel.socialMediaList!!.isNotEmpty()) {
                socialData = Gson().toJson(businessProfileModel.socialMediaList)
                Log.d("socialModel", socialData)
            }

            val businessName = businessProfileModel.businessName.toRequestBody("text/plain".toMediaTypeOrNull())
            val paymentId = "".toRequestBody("text/plain".toMediaTypeOrNull())
            val name = businessProfileModel.contactName.toRequestBody("text/plain".toMediaTypeOrNull())
            val isNamePublic = businessProfileModel.isNamePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val email = businessProfileModel.email.toRequestBody("text/plain".toMediaTypeOrNull())
            val isEmailPublic = businessProfileModel.isEmailPublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val c_code = businessProfileModel.countryCode.toRequestBody("text/plain".toMediaTypeOrNull())
            val contact = businessProfileModel.phone.toRequestBody("text/plain".toMediaTypeOrNull())
            val extension = businessProfileModel.ext.toRequestBody("text/plain".toMediaTypeOrNull())
            val isPhonePublic = businessProfileModel.isPhonePublic.toString().toRequestBody("text/plain".toMediaTypeOrNull())
            val socialMedia = socialData?.toRequestBody("text/plain".toMediaTypeOrNull())

            val cardStartColor = businessProfileModel.startColor.toRequestBody("text/plain".toMediaTypeOrNull())
            val cardEndColor = businessProfileModel.endColor.toRequestBody("text/plain".toMediaTypeOrNull())

            val isVideo = businessProfileModel.isVideo.toString().toRequestBody("text/plain".toMediaTypeOrNull())

            var mediaType = "image/png"
            var fileName = "photo_" + System.currentTimeMillis() + ".png"

            if (businessProfileModel.isVideo) {
                mediaType = "video/*"
                fileName = "video_" + System.currentTimeMillis() + ".mp4"
            }


            var profilePic: MultipartBody.Part? = null
            var logo: MultipartBody.Part? = null
            var card: MultipartBody.Part? = null
            var url_: RequestBody? = null
            var thumb_: RequestBody? = null
            var logourl_: RequestBody? = null
            var logothumb_: RequestBody? = null
            var cardurl_: RequestBody? = null
            var cardthumb_: RequestBody? = null


            if (businessProfileModel.profileUri != null) {
                val file: File = File(businessProfileModel.profileUri!!)
                val reqFile1 = file.asRequestBody(mediaType.toMediaTypeOrNull())
                profilePic = MultipartBody.Part.createFormData("profile", fileName, reqFile1)
            }

            if (businessProfileModel.logoUri != null) {
                val logoF: File = File(businessProfileModel.logoUri!!)
                val reqFile2 = logoF.asRequestBody(mediaType.toMediaTypeOrNull())
                logo = MultipartBody.Part.createFormData("logo", fileName, reqFile2)
            }

            if (businessProfileModel.businessCardUri != null) {
                val cardF: File = File(businessProfileModel.businessCardUri!!)
                val reqFile3 = cardF.asRequestBody(mediaType.toMediaTypeOrNull())
                card = MultipartBody.Part.createFormData("businessCard", fileName, reqFile3)
            }

            if (intent.getBooleanExtra("isEdit", false)) {
                url_ = businessProfileModel.profileUrl.toRequestBody("text/plain".toMediaTypeOrNull())
                thumb_ = businessProfileModel.profileThumbnail.toRequestBody("text/plain".toMediaTypeOrNull())
                logourl_ = businessProfileModel.logoUrl.toRequestBody("text/plain".toMediaTypeOrNull())
                logothumb_ = businessProfileModel.logoThumbnail.toRequestBody("text/plain".toMediaTypeOrNull())
                if (businessProfileModel.businessCardUrl != null) {
                    cardurl_ = businessProfileModel.businessCardUrl!!.toRequestBody("text/plain".toMediaTypeOrNull())
                    cardthumb_ = businessProfileModel.businessCardThubmnail!!.toRequestBody("text/plain".toMediaTypeOrNull())
                }

            }

            if (intent.getBooleanExtra("isEdit", false)) {
                val call = Hello.apiService.updateBusinessProfile(businessName, name, isNamePublic, c_code, contact, extension, isPhonePublic, email, isEmailPublic, socialMedia, cardStartColor, cardEndColor, isVideo, profilePic, logo, card, url_!!, thumb_!!, logourl_!!, logothumb_!!, cardurl_, cardthumb_)
                SingleEnqueueCall.callRetrofit(this, call, Constants.UPDATE_PERSONAL_PROFILE_URL, true, this@PersonalizedCardActivity)
            }
            else {
                val call = Hello.apiService.createBusinessProfile(businessName, paymentId, null,name, isNamePublic, c_code, contact, extension, isPhonePublic, email, isEmailPublic, socialMedia, cardStartColor, cardEndColor, isVideo, profilePic, logo!!, card)
                SingleEnqueueCall.callRetrofit(this, call, Constants.CREATE_BUSINESS_PROFILE_URL, true, this@PersonalizedCardActivity)
            }
        }
    }

    private fun setDesignAdapter() {
        binding.rvCardDesigns.layoutManager = GridLayoutManager(this@PersonalizedCardActivity, 4)
        if (title != "RegisterBusinessProfile") {
            mList.add(DesignModel("#F0810F", "#E6DF44", false))
            mList.add(DesignModel("#EB8D8D", "#A890FE", false))
            mList.add(DesignModel("#373491", "#0E074D", false))
            mList.add(DesignModel("#38BFB1", "#0064E6", false))
            mList.add(DesignModel("#BFA038", "#E60800", false))
            mList.add(DesignModel("#C33764", "#1D2671", false))
            mList.add(DesignModel("#FF61D2", "#FE908F", false))
            mList.add(DesignModel("#151B3B", "#24D4C4", false))
            mList.add(DesignModel("#91BF38", "#00B3E6", false))
            mList.add(DesignModel("#DA72F8", "#0551B4", false))
            mList.add(DesignModel("#4E65FF", "#92EFFE", false))
            mList.add(DesignModel("#3841BF", "#3841BF", false))

            if (intent.getBooleanExtra("isEdit", false)) {
                for (i in mList.indices) {
                    if (personalProfileModel.startColor == mList[i].colorStart) {
                        mList[i].isSelected = true
                        break
                    }
                }
            }
            else {
                mList[0].isSelected = true
            }

            startColor = mList.find { it.isSelected }!!.colorStart
            endColor = mList.find { it.isSelected }!!.colorEnd

            personalProfileModel.startColor = startColor
            personalProfileModel.endColor = endColor

        }
        else {
            mList.add(DesignModel("#6C4629", "#57371E", false))
            mList.add(DesignModel("#06575B", "#05788B", false))
            mList.add(DesignModel("#1C568F", "#0C2D4D", false))
            mList.add(DesignModel("#1E1F26", "#4D648D", false))
            mList.add(DesignModel("#25273C", "#6D6E7C", false))
            mList.add(DesignModel("#373491", "#0E074D", false))
            mList.add(DesignModel("#5C821A", "#0F1B07", false))
            mList.add(DesignModel("#01262C", "#556E72", false))
            mList.add(DesignModel("#E5494B", "#910402", false))
            mList.add(DesignModel("#26549D", "#06265A", false))
            mList.add(DesignModel("#67686E", "#47484B", false))
            mList.add(DesignModel("#323232", "#060606", false))

            if (intent.getBooleanExtra("isEdit", false)) {
                for (i in mList.indices) {
                    if (businessProfileModel.startColor == mList[i].colorStart) {
                        mList[i].isSelected = true
                        break
                    }
                }
            }
            else {
                mList[0].isSelected = true
            }

            startColor = mList.find { it.isSelected }!!.colorStart
            endColor = mList.find { it.isSelected }!!.colorEnd

            businessProfileModel.startColor = startColor
            businessProfileModel.endColor = endColor

        }

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(startColor), Color.parseColor(endColor)))
        gd.cornerRadius = 0f
        binding.rlColor.setBackgroundDrawable(gd)

        adapter = ChooseColorAdapter(mList)
        adapter.mListener = this
        binding.rvCardDesigns.adapter = adapter
    }

    override fun onItemClick(position: Int) {
        mList.map { it.isSelected = false }
        mList[position].isSelected = true
        startColor = mList[position].colorStart
        endColor = mList[position].colorEnd
        if (title != "RegisterBusinessProfile") {
            personalProfileModel.startColor = startColor
            personalProfileModel.endColor = endColor
        }
        else {
            businessProfileModel.startColor = startColor
            businessProfileModel.endColor = endColor
        }
        adapter.notifyDataSetChanged()
        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(startColor), Color.parseColor(endColor)))
        gd.cornerRadius = 0f
        binding.rlColor.setBackgroundDrawable(gd)
    }

    override fun success(apiName: String, response: Any?) {
        when (apiName) {
            Constants.CREATE_PERSONAL_PROFILE_URL -> {
                val model = response as CreateProfileResponseModel
                if (model.isError) {
                    showToastMessage(model.message)
                }
                else {
                    Hello.db.putString(Constants.USER_ID, model.data)
                    gotoMain()
                }
            }
            Constants.CREATE_BUSINESS_PROFILE_URL -> {
                val model = response as CreateProfileResponseModel
                if (model.isError) {
                    showToastMessage(model.message)
                }
                else {
                    Hello.db.putString(Constants.USER_ID, model.data)
                    gotoMain()
                }
            }
            Constants.UPDATE_PERSONAL_PROFILE_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    showToastMessage(model.message)
                }
                else {
                    val i = Intent(this, MainActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                }
            }
            Constants.UPDATE_BUSINESS_PROFILE_URL -> {
                val model = response as RecoveryCodeResponse
                if (model.isError) {
                    showToastMessage(model.message)
                }
                else {
                    val i = Intent(this, MainActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                }
            }
        }
    }

    private fun gotoMain() {
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
        finish()
    }

    override fun failure(apiName: String, message: String?) {
        showToastMessage(message!!)
    }
}