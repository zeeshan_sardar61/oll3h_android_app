package com.iinshaapps.hello.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.ActivityPaymentSuccessfulBinding

class PaymentSuccessfulActivity : AppCompatActivity() {

    lateinit var binding: ActivityPaymentSuccessfulBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_successful)

        binding.buttonVerify.setOnClickListener {
            val i = Intent(this, RecoveryNumberActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            i.putExtra("isFromPayment", true)
            startActivity(i)
            finish()
        }
    }
}