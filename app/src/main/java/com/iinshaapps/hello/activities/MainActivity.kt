package com.iinshaapps.hello.activities

import android.annotation.TargetApi
import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Point
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.iinshaapps.hello.ButtonsWidget
import com.iinshaapps.hello.Hello
import com.iinshaapps.hello.QRWidget
import com.iinshaapps.hello.R
import com.iinshaapps.hello.api.IGenericCallBack
import com.iinshaapps.hello.api.remote.SingleEnqueueCall
import com.iinshaapps.hello.databinding.ActivityMainBinding
import com.iinshaapps.hello.fragment.*
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.MessageEvent
import com.iinshaapps.hello.helper.encodeAsBitmap
import com.iinshaapps.hello.helper.showToastMessage
import com.iinshaapps.hello.model.MyCardModel
import com.iinshaapps.hello.model.response.GetMyCardsResponseModel
import org.greenrobot.eventbus.EventBus
import java.io.ByteArrayOutputStream

class MainActivity : AppCompatActivity(), IGenericCallBack {

    lateinit var binding: ActivityMainBinding
    lateinit var dashBoardFragment: DashBoardFragment
    var profile = 0

    companion object {
        var selectedIndex = 0
        lateinit var mActivity: AppCompatActivity
        var widgetClicked = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initViews()
        val wm = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        wm.defaultDisplay.getRealSize(size)
        val resolution: String = size.x.toString() + "x" + size.y

        Constants.DEVICE_HEIGHT = size.y

        Log.d("TAG", "onCreate: " + resolution)
        if (intent.getStringExtra("personal") != null) {
            widgetClicked = 1
        }

        if (intent.getStringExtra("business") != null) {
            widgetClicked = 2
        }

        if (intent.extras?.getString("settings") != null) {
            selectedIndex = 2
            Toast.makeText(this, "Updated.", Toast.LENGTH_LONG).show()
        }

        dashBoardFragment = DashBoardFragment()
        addFragment(R.id.container, dashBoardFragment, "DashBoardFragment")

        if (intent.extras != null) {
            if (intent.hasExtra("name")) {
                Hello.db.putBoolean(com.iinshaapps.hello.helper.Constants.HAVE_NOTIFICATION, true)
                EventBus.getDefault().post(MessageEvent("notify"))
            }
        }
        else if (intent.data != null) {
            val data = intent.data
            if (data != null && data.isHierarchical) {
                val uri = intent.dataString
                Log.d("Link", "MyApp Deep link clicked " + uri!!)
                gotoCard(uri)
                return
            }
        }
    }

    private fun initViews() { //        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        //            setStatusBarColor(ContextCompat.getColor(this, R.color.black), View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        //        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun setStatusBarColor(color: Int, flag: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = color
            window.decorView.systemUiVisibility = flag
        }
    }

    override fun onResume() {
        super.onResume()
        if (binding.container.childCount > 1) {
            when (supportFragmentManager.findFragmentById(R.id.container)) {
                is MyPersonalCardDetailFragment -> run {
                    EventBus.getDefault().post(MessageEvent("Fire"))
                }
                is MyBusinessCardDetailFragment -> run {
                    EventBus.getDefault().post(MessageEvent("Fire"))
                }
                is PersonalCardDetailFragment -> run {
                    EventBus.getDefault().post(MessageEvent("Fire"))
                }
                is BusinessCardDetailFragment -> run {
                    EventBus.getDefault().post(MessageEvent("Fire"))
                }
            }
        }
    }

    override fun onBackPressed() {
        if (binding.container.childCount > 1) {
            when (supportFragmentManager.findFragmentById(R.id.container)) {
                is AddNotesFragment -> run { //                    if (ScanQRFragment.isScanner) {
                    //                        ScanQRFragment.isScanner = false
                    ////                        ScanQRFragment.binding.qrCodeReader.startCamera()
                    ////                        ScanQRFragment.binding.qrCodeReader.setQRDecodingEnabled(true)
                    //                        ScanQRFragment.codeScanner.startPreview()
                    //                    }
//                    supportFragmentManager.popBackStackImmediate()
                    EventBus.getDefault().post(MessageEvent("Back"))
                }
                is ItemsDetailFragment -> run {
                    EventBus.getDefault().post(MessageEvent("Fire"))
                    super.onBackPressed()
                }
                else -> {
                    super.onBackPressed()
                }
            }
        }
        else {
            when (supportFragmentManager.findFragmentById(R.id.fragment_container)) {
                is ContactsFragment -> run {
                    if (ContactsFragment.isSearchOpened) {
                        dashBoardFragment.contactsFragment.onCrossClicked()
                    }
                    else {
                        selectedIndex = 0
                        dashBoardFragment.selectFragment(dashBoardFragment.selectedFragment)
                    }
                }
                is HomeFragment -> run {
                    finishAffinity()
                }
                is SettingsFragment -> run {
                    selectedIndex = 0
                    dashBoardFragment.selectFragment(dashBoardFragment.selectedFragment)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val fragment = supportFragmentManager.findFragmentById(R.id.container)
            when (fragment) {
                is PersonalCardDetailFragment -> fragment.onActivityResult(requestCode, resultCode, data)
                is BusinessCardDetailFragment -> fragment.onActivityResult(requestCode, resultCode, data)
                is ContactsFragment -> fragment.onActivityResult(requestCode, resultCode, data)
                else -> super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    /**
     * if you will pass tag as title of fragment it will add that
     * fragment to stack otherwise will not add on stack.
     *
     * @param containerId
     * @param fragment
     * @param tag
     */
    fun addFragment(containerId: Int, fragment: Fragment, tag: String?) {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val transaction = supportFragmentManager.beginTransaction().add(containerId, fragment, tag).setCustomAnimations(R.anim.slide_in_enter, R.anim.slide_in_exit, R.anim.slide_pop_enter, R.anim.slide_pop_exit)
        if (tag != null) transaction.addToBackStack(tag).commit()
        else transaction.commit()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val data = intent.data
        if (data != null && data.isHierarchical) {
            val uri = intent.dataString
            Log.d("Link", "MyApp Deep link clicked " + uri!!)
            gotoCard(uri)
            return
        }
    }

    private fun gotoCard(text: String) {
        val mainString = text.split("\\?".toRegex()).dropLastWhile { it.isEmpty() }
        val separateString = mainString[1].split("&".toRegex()).dropLastWhile { it.isEmpty() }
        val cardString = separateString[0].split("=".toRegex()).dropLastWhile { it.isEmpty() }
        val personalString = separateString[1].split("=".toRegex()).dropLastWhile { it.isEmpty() }

        if (cardString[1] == Hello.db.getString(Constants.P_CARDID)) {
            profile = 1
            getMyCards()
        }
        else if (cardString[1] == Hello.db.getString(Constants.B_CARDID)) {
            profile = 2
            getMyCards()
        }
        else {
            if (personalString[1] == "true") {
                addFragment(R.id.container, PersonalCardDetailFragment(cardString[1]), "PersonalCardDetailFragment")
            }
            else {
                addFragment(R.id.container, BusinessCardDetailFragment(cardString[1]), "BusinessCardDetailFragment")
            }
        }
    }

    private fun getMyCards() {
        val call = Hello.apiService.getMyCards()
        SingleEnqueueCall.callRetrofit(MainActivity.mActivity, call, Constants.GET_MY_CARDS_URL, true, this)

    }

    override fun success(apiName: String, response: Any?) {
        val model = response as GetMyCardsResponseModel
        if (model.isError) {
            showToastMessage(model.message)
        }
        else {
            if(profile==1){
                addFragment(R.id.container, MyPersonalCardDetailFragment(model.data.personalCard!!), "PersonalCardDetailFragment")
            }else if(profile==2){
                addFragment(R.id.container, MyBusinessCardDetailFragment(model.data.businessCard!!), "PersonalCardDetailFragment")
            }
        }




    }

    override fun failure(apiName: String, message: String?) {
        showToastMessage(message!!)
    }
}