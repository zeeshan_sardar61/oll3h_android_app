package com.iinshaapps.hello.activities

class PackageModel {

    var amount = 0.0
    var time = ""
    var list = ArrayList<String>()

    constructor(amount: Double, time:String, list: ArrayList<String>) {
        this.amount = amount
        this.time = time
        this.list = list
    }
}
