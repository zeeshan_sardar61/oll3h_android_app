package com.iinshaapps.hello.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.TourActivityBinding
import com.iinshaapps.hello.fragment.tour.Tour1
import com.iinshaapps.hello.fragment.tour.Tour2
import com.iinshaapps.hello.fragment.tour.Tour3
import com.iinshaapps.hello.adapter.VPDashBoardAdapter

class TourScreen : AppCompatActivity() {

    lateinit var binding: TourActivityBinding
    lateinit var adapter: VPDashBoardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.tour_activity)

        setUpPager()

        binding.tvSkip.setOnClickListener {
            val i = Intent(this, CreateProfileActivity::class.java)
            startActivity(i)
            finish()
        }
    }

    private fun setUpPager() {
        adapter = VPDashBoardAdapter(supportFragmentManager)
        adapter.addFragment(Tour1(), "Tour1")
        adapter.addFragment(Tour2(), "Tour2")
        adapter.addFragment(Tour3(), "Tour3")
        binding.vpTour.adapter = adapter
        binding.dotsIndicator.setViewPager(binding.vpTour)

        binding.vpTour.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if(position==2){
                    binding.tvSkip.text = "Next"
                }else {
                    binding.tvSkip.text = "Skip"
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }
}