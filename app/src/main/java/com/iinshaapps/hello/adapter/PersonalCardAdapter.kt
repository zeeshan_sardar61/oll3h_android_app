package com.iinshaapps.hello.adapter

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.telephony.PhoneNumberUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.AdapterPersonalCardBinding
import com.iinshaapps.hello.fragment.ContactsFragment
import com.iinshaapps.hello.model.MyCardModel
import com.bumptech.glide.Glide
import com.github.ramiz.nameinitialscircleimageview.NameInitialsCircleImageView
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.helper.Constants
import java.util.*

class PersonalCardAdapter(private var mList: ArrayList<MyCardModel>, private val context: ContactsFragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterPersonalCardBinding
    lateinit var mListener: PersonalItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_personal_card, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        mHolder.binding.tvName.text = mList[position].name
        if(mList[position].isPhonePublic && PhoneNumberUtils.isGlobalPhoneNumber("${mList[position].countryCode}${mList[position].phone}")){
            mHolder.binding.tvPhone.visibility = View.VISIBLE
            mHolder.binding.tvPhone.text = mList[position].countryCode + " " + mList[position].phone
        }else{
            mHolder.binding.tvPhone.visibility = View.GONE
        }
        if(mList[position].isEmailPublic){
            mHolder.binding.tvEmail.visibility = View.VISIBLE
            mHolder.binding.tvEmail.text = mList[position].email
        }else{
            mHolder.binding.tvEmail.visibility = View.GONE
        }

        if (mList[position].isAppUser) {
            val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(mList[position].colorStart), Color.parseColor(mList[position].colorEnd)))
            gd.cornerRadius = 0f
            mHolder.binding.rlColor.setBackgroundDrawable(gd)

            mHolder.binding.ivUser.visibility = View.VISIBLE
            mHolder.binding.iivUser.visibility = View.INVISIBLE

            if (mList[position].isVideo) {
                Glide.with(context).load(R.drawable.ic_dp_placeholder).into(mHolder.binding.ivUser)
            }
            else {
                Glide.with(context).load(mList[position].thumbnailUrl).placeholder(R.drawable.ic_dp_placeholder).into(mHolder.binding.ivUser)
            }
        }
        else {
            val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor("#727272"), Color.parseColor("#727272")))
            gd.cornerRadius = 0f
            mHolder.binding.rlColor.setBackgroundDrawable(gd)

            mHolder.binding.ivUser.visibility = View.INVISIBLE
            mHolder.binding.iivUser.visibility = View.VISIBLE

            var initials = mList[position].name
                .split(' ')
                .mapNotNull { it.firstOrNull()?.toString() }
                .reduce { acc, s -> acc + s }

//            if(!initials.contains(" ")) {
//                initials += " "
//            }

            if(initials.length>2){
                mHolder.binding.iivUser.setImageInfo(NameInitialsCircleImageView.ImageInfo.Builder(initials.substring(0, 2).toUpperCase()).setTextColor(R.color.white).setCircleBackgroundColorRes(R.color.grey).build())
            }else {
                mHolder.binding.iivUser.setImageInfo(NameInitialsCircleImageView.ImageInfo.Builder(initials.toUpperCase()).setTextColor(R.color.white).setCircleBackgroundColorRes(R.color.grey).build())
            }
        }

        mHolder.binding.tvPhone.setOnClickListener {
            mListener.onPersonalPhoneClick(position)
        }

        mHolder.binding.tvEmail.setOnClickListener {
            mListener.onPersonalEmailClick(position)
        }

        mHolder.binding.llCard.setOnClickListener {
            mListener.onPersonalItemClick(position)
        }

        if (mList[position].isOpened) {
            val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
            cardViewMarginParams.topMargin = MainActivity.mActivity.resources.getDimension(R.dimen._10sdp).toInt()
            mHolder.binding.llCard.requestLayout()
        }
        else {
            if (position > 0) {
                val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                var topMargin = 0
                if(Constants.DEVICE_HEIGHT>=2560){
                    topMargin = -500
                }else if(Constants.DEVICE_HEIGHT>1520){
                    topMargin = -410
                }else{
                    topMargin = -290
                }
                cardViewMarginParams.topMargin = topMargin
                mHolder.binding.llCard.requestLayout()
            }
            else {
                val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                cardViewMarginParams.topMargin = MainActivity.mActivity.resources.getDimension(R.dimen._10sdp).toInt()
                mHolder.binding.llCard.requestLayout()
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: AdapterPersonalCardBinding = DataBindingUtil.bind(view)!!
    }

    interface PersonalItemClicker {
        fun onPersonalItemClick(position: Int)
        fun onPersonalPhoneClick(position: Int)
        fun onPersonalEmailClick(position: Int)
    }
}
