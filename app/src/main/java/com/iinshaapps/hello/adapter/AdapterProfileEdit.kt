package com.iinshaapps.hello.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.AdapterProfileEditBinding
import com.iinshaapps.hello.model.response.EmailsItem
import java.util.*


class AdapterProfileEdit(private val context: Context, private var mList: ArrayList<EmailsItem?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterProfileEditBinding
    lateinit var mListener: ItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_profile_edit, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        mHolder.binding.etEmail.setText(mList[position]!!.email)
        mHolder.binding.switchEmail.isChecked = mList[position]!!.isPublic!!

        mHolder.binding.ivDelete.setOnClickListener {
            mListener.onDeleteClick(position)
        }

        mHolder.binding.etEmail.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {

                if(android.util.Patterns.EMAIL_ADDRESS.matcher(mHolder.binding.etEmail.text.toString().trim()).matches()) {

                    mListener.addEmail(position, mHolder.binding.etEmail.text.toString().trim(), mHolder.binding.switchEmail.isChecked)

                    if (position == mList.size - 1) {
                        mListener.onAddCheck(mHolder.binding.etEmail.text.toString().trim().isNotEmpty())
                    }

                    val imm: InputMethodManager? = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
                    imm?.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)

                    return@setOnEditorActionListener true
                } else {

                    mHolder.binding.etEmail.error = "Invalid email detected."
                }
            }
            return@setOnEditorActionListener false
        }

        mHolder.binding.switchEmail.setOnClickListener {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(mHolder.binding.etEmail.text.toString().trim()).matches()) {
                mListener.addEmail(position, mHolder.binding.etEmail.text.toString().trim(), mHolder.binding.switchEmail.isChecked)

                if (position == mList.size - 1) {
                    mListener.onAddCheck(mHolder.binding.etEmail.text.toString().trim().isNotEmpty())
                }
            }
        }

        if (position == mList.size - 1) {
            mListener.onAddCheck(mHolder.binding.etEmail.text.toString().trim().isNotEmpty())
            mHolder.binding.etEmail.requestFocus()
        }

        mHolder.binding.etEmail.setOnFocusChangeListener { v, hasFocus ->
            run {
                if (hasFocus && mHolder.binding.etEmail.text.toString().isEmpty()) {
                    mHolder.binding.switchEmail.isChecked = true
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: AdapterProfileEditBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
        fun onDeleteClick(position: Int)
        fun onAddCheck(check: Boolean)
        fun addEmail(position: Int, email: String, switch: Boolean)
    }
}