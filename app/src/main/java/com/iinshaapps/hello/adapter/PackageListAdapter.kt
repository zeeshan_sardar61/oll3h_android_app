package com.iinshaapps.hello.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.AdapterPackageListBinding
import java.util.*


class PackageListAdapter(var context: Context, private var mList: ArrayList<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterPackageListBinding
    lateinit var mListener: ItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_package_list, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        if(position%2==0){
            binding.rlItem.setBackgroundColor(context.resources.getColor(R.color.c1))
        }else {
            binding.rlItem.setBackgroundColor(context.resources.getColor(R.color.c2))
        }
        mHolder.binding.tvText.text = mList[position]





    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: AdapterPackageListBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
    }

}
