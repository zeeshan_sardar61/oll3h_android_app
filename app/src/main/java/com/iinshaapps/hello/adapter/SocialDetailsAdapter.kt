package com.iinshaapps.hello.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.ItemDetailSocialBinding
import com.iinshaapps.hello.model.SocialModel
import java.util.ArrayList

class SocialDetailsAdapter(private var mList: ArrayList<SocialModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: ItemDetailSocialBinding
    lateinit var mListener: ItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_detail_social, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val mHolder = holder as ItemViewHolder
        mHolder.setIsRecyclable(false)

        mHolder.binding.imageViewSocial.setImageResource(mList[position].socialDrawableFilled)

        mHolder.binding.imageViewSocial.setOnClickListener{
            mListener.onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: ItemDetailSocialBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
    }
}