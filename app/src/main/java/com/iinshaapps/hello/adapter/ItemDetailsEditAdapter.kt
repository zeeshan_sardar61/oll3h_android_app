package com.iinshaapps.hello.adapter

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.databinding.LayoutItemDetailsEditBinding
import com.iinshaapps.hello.helper.DateUtils
import com.iinshaapps.hello.model.ItemDetailModel
import java.text.SimpleDateFormat
import java.util.*

class ItemDetailsEditAdapter(private var mList: ArrayList<ItemDetailModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: LayoutItemDetailsEditBinding
    lateinit var mListener: ItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_item_details_edit, parent, false)
        return ItemViewHolder(binding.root)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        mHolder.binding.apply {
            tvTag.text = mList[position].tag
            switchTag.isChecked = mList[position].switch

            switchTag.setOnCheckedChangeListener { _, isChecked ->
                mList[position].switch = isChecked
                mListener.onItemChange()
            }

            if(mList[position].tag == "Date of Birth") {
                etBody.setText(DateUtils.getDateFromString(mList[position].body))
                etBody.isClickable = true
                etBody.isFocusable = false

                etBody.setOnClickListener {
                    val c = Calendar.getInstance()
                    val year = c.get(Calendar.YEAR)
                    val month = c.get(Calendar.MONTH)
                    val day = c.get(Calendar.DAY_OF_MONTH)

                    val dpd = DatePickerDialog(MainActivity.mActivity, { view, year, monthOfYear, dayOfMonth ->

                        val cal = Calendar.getInstance()
                        cal.timeInMillis = 0
                        cal.set(year, monthOfYear, dayOfMonth, 0, 0, 0)
                        val chosenDate = cal.time

                        val dateFormatter = SimpleDateFormat("MMM dd, yyyy") //this format changeable
                        val formattedDate = dateFormatter.format(chosenDate)
                        if(etBody.text.toString().isEmpty()){
                            switchTag.isChecked = true
                        }
                        etBody.setText(formattedDate)

                        val dateFormatter1 = SimpleDateFormat("yyyy-MM-dd") //this format changeable
                        val formattedDate1 = dateFormatter1.format(chosenDate)

                        mList[position].body = formattedDate1 + "T00:00:00"
                        mListener.onItemChange()

                    }, year, month, day)

                    dpd.show()
                }

            }else{
                etBody.setText(mList[position].body)
                etBody.addTextChangedListener(object : TextWatcher{

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        mList[position].body = etBody.text.toString().trim()
                        mListener.onItemChange()
                    }
                })
            }

            etBody.setOnFocusChangeListener { v, hasFocus ->
                run {
                    if (hasFocus && etBody.text.toString().isEmpty()) {
                        switchTag.isChecked = true
                        mList[position].switch = true
                        mListener.onItemChange()
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    fun getList(): ArrayList<ItemDetailModel> {
        return mList
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: LayoutItemDetailsEditBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemChange()
    }
}