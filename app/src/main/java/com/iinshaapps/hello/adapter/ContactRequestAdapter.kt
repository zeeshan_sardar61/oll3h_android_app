package com.iinshaapps.hello.adapter

import android.annotation.SuppressLint
import android.telephony.PhoneNumberUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.ramiz.nameinitialscircleimageview.NameInitialsCircleImageView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.LayoutContactRequestBinding
import com.iinshaapps.hello.model.response.ContactRequestModel
import java.util.*

class ContactRequestAdapter(private var mList: ArrayList<ContactRequestModel>, private val listener: ItemClicker) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: LayoutContactRequestBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_contact_request, parent, false)
        return ItemViewHolder(binding.root)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        mHolder.binding.apply {

            if (mList[position].name != null) {

                if (mList[position].name.isNotEmpty()) {
                    val initials = mList[position].name.split(' ').mapNotNull { it.firstOrNull()?.toString() }.reduce { acc, s -> acc + s }

//                    if (!initials.contains(" ")) {
//                        initials += " "
//                    }

                    if(initials.length>2){
                        initialsImageView.setImageInfo(NameInitialsCircleImageView.ImageInfo.Builder(initials.substring(0, 2).toUpperCase()).setTextColor(R.color.white).setCircleBackgroundColorRes(R.color.grey).build())
                    }else {
                        initialsImageView.setImageInfo(NameInitialsCircleImageView.ImageInfo.Builder(initials.toUpperCase()).setTextColor(R.color.white).setCircleBackgroundColorRes(R.color.grey).build())
                    }

                    tvName.text = mList[position].name
                }
            }

            if (PhoneNumberUtils.isGlobalPhoneNumber("${mList[position].countryCode}${mList[position].phoneNo}")) {
                tvPhone.text = mList[position].countryCode + " " + mList[position].phoneNo
            }
            else {
                tvPhone.text = ""
            }
        }

        mHolder.binding.rlMain.setOnClickListener {
            listener.onItemClick(position)
        }

        mHolder.binding.rlDelete.setOnClickListener {
            listener.onDeleteClick(position)
        }


    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: LayoutContactRequestBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
        fun onDeleteClick(position: Int)
    }
}