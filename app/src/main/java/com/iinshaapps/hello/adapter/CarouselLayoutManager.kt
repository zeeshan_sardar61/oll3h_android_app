package com.iinshaapps.hello.adapter

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class CarouselLayoutManager(context: Context?, orientation: Int, reverseLayout: Boolean) :
    LinearLayoutManager(context, orientation, reverseLayout) {
    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State
    ): Int {
        val orientation = orientation
        return if (orientation == HORIZONTAL) {
            val scrolled = super.scrollHorizontallyBy(dx, recycler, state)
            val minifyAmount = 0.15f
            val minifyDistance = 0.65f
            val parentMidpoint = width / 2f
            val d0 = 0f
            val d1 = parentMidpoint * minifyDistance
            val s0 = 1f
            val s1 = 1f - minifyAmount
            for (i in 0 until childCount) {
                val child = getChildAt(i)
                val childMidpoint = (getDecoratedLeft(child!!) + getDecoratedRight(
                    child
                )) / 2f
                val d = Math.min(d1, Math.abs(parentMidpoint - childMidpoint))
                val scaleFactor = s0 + (s1 - s0) * (d - d0) / (d1 - d0)
                child.scaleX = scaleFactor
                child.scaleY = scaleFactor
            }
            scrolled
        } else {
            0
        }
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        super.onLayoutChildren(recycler, state)
        scrollHorizontallyBy(0, recycler, state)
    }
}