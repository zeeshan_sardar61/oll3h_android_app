package com.iinshaapps.hello.adapter

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.AdapterDesignsBinding
import com.iinshaapps.hello.model.DesignModel
import java.util.*


class ChooseColorAdapter(private var mList: ArrayList<DesignModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterDesignsBinding
    lateinit var mListener: ItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_designs, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(mList[position].colorStart), Color.parseColor(mList[position].colorEnd)))
        gd.cornerRadius = 0f
        mHolder.binding.rlColor.setBackgroundDrawable(gd)

        if(mList[position].isSelected){
            mHolder.binding.ivSelected.visibility = View.VISIBLE
        }else {
            mHolder.binding.ivSelected.visibility = View.GONE
        }

        mHolder.binding.rlColor.setOnClickListener {
            mListener.onItemClick(position)
        }


    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: AdapterDesignsBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
    }

}
