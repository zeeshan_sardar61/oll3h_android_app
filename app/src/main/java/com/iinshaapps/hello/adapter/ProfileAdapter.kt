package com.iinshaapps.hello.adapter

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.CreateProfileModel
import com.iinshaapps.hello.databinding.AdapterSelectProfileBinding
import java.util.*

class ProfileAdapter(private var mList: ArrayList<CreateProfileModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterSelectProfileBinding
    lateinit var mListener: ItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_select_profile, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder
        mHolder.setIsRecyclable(false)
//        if (position == 0 || position == mList.size - 1) {
//            mHolder.binding.cvMain.visibility = View.INVISIBLE
//        }
        val gd = GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, intArrayOf(mList[position].startColor, mList[position].endColor))
        gd.cornerRadius = 0f
        mHolder.binding.rlColor.setBackgroundDrawable(gd)

        mHolder.binding.tvTitle.text = mList[position].title
        mHolder.binding.ivProfile.setImageResource(mList[position].icon)

        mListener.itemPositionChanged(position)

        mHolder.binding.cvMain.setOnClickListener {
            mListener.onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: AdapterSelectProfileBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
        fun itemPositionChanged(position: Int)
    }

}