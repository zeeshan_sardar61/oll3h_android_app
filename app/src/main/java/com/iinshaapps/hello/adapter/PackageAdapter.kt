package com.iinshaapps.hello.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.PackageModel
import com.iinshaapps.hello.databinding.AdapterPackageBinding
import java.util.ArrayList

class PackageAdapter(var context: Context, private var mList: ArrayList<PackageModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterPackageBinding
    lateinit var mListener: ItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_package, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        mHolder.binding.tvAmount.text = "$"+mList[position].amount
        mHolder.binding.tvYear.text = mList[position].time

        mHolder.binding.rvList.layoutManager = LinearLayoutManager(context)
        val adapter = PackageListAdapter(context, mList[position].list)
        mHolder.binding.rvList.adapter = adapter

        mHolder.binding.buttonNext.setOnClickListener {
            mListener.onItemClick(position)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: AdapterPackageBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
    }

}