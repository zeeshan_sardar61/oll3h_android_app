package com.iinshaapps.hello.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.LayoutItemDetailsBinding
import com.iinshaapps.hello.model.ItemDetailModel
import java.util.ArrayList

class ItemDetailsAdapter(private var mList: ArrayList<ItemDetailModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: LayoutItemDetailsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_item_details, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        mHolder.binding.apply {
            tvTag.text = mList[position].tag
            tvBody.text = mList[position].body

            if(position == mList.size - 1) viewSeparator.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: LayoutItemDetailsBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
    }
}