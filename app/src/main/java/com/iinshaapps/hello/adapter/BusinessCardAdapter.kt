package com.iinshaapps.hello.adapter

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.telephony.PhoneNumberUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.iinshaapps.hello.R
import com.iinshaapps.hello.databinding.AdapterBusinessCardBinding
import com.iinshaapps.hello.fragment.ContactsFragment
import com.iinshaapps.hello.model.MyCardModel
import com.bumptech.glide.Glide
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.helper.Constants
import java.util.*

class BusinessCardAdapter(private var mList: ArrayList<MyCardModel>, private val context: ContactsFragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var binding: AdapterBusinessCardBinding
    lateinit var mListener: BusinessItemClicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.adapter_business_card, parent, false)
        return ItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mHolder = holder as ItemViewHolder

        mHolder.binding.tvCompanyName.text = mList[position].companyName
        mHolder.binding.tvName.text = mList[position].name
        if(mList[position].isPhonePublic && PhoneNumberUtils.isGlobalPhoneNumber("${mList[position].countryCode}${mList[position].phone}")){
            mHolder.binding.tvPhone.visibility = View.VISIBLE
            mHolder.binding.tvPhone.text = mList[position].countryCode + " " + mList[position].phone
        }else{
            mHolder.binding.tvPhone.visibility = View.GONE
        }
        if(mList[position].isEmailPublic){
            mHolder.binding.tvEmail.visibility = View.VISIBLE
            mHolder.binding.tvEmail.text = mList[position].email
        }else{
            mHolder.binding.tvEmail.visibility = View.GONE
        }

        if (mList[position].isAppUser) {
            val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(mList[position].colorStart), Color.parseColor(mList[position].colorEnd)))
            gd.cornerRadius = 0f
            mHolder.binding.rlColor.setBackgroundDrawable(gd)

            mHolder.binding.ivLogo.visibility = View.VISIBLE
            Glide.with(context).load(mList[position].logoThumbnailUrl).placeholder(R.drawable.ic_logo_placeholder).into(mHolder.binding.ivLogo)
        }
        else {
            val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor("#727272"), Color.parseColor("#727272")))
            gd.cornerRadius = 0f
            mHolder.binding.rlColor.setBackgroundDrawable(gd)
        }

        mHolder.binding.tvPhone.setOnClickListener {
            mListener.onBusinessPhoneClick(position)
        }

        mHolder.binding.tvEmail.setOnClickListener {
            mListener.onBusinessEmailClick(position)
        }

        mHolder.binding.llCard.setOnClickListener {
            mListener.onBusinessItemClick(position)
        }

        if (mList[position].isOpened) {
            val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
            cardViewMarginParams.topMargin = MainActivity.mActivity.resources.getDimension(R.dimen._10sdp).toInt()
            mHolder.binding.llCard.requestLayout()
        }
        else {
            if (position > 0) {
                val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                var topMargin = 0
                if(Constants.DEVICE_HEIGHT>=2560){
                    topMargin = -500
                }else if(Constants.DEVICE_HEIGHT>1520){
                    topMargin = -410
                }else{
                    topMargin = -290
                }
                cardViewMarginParams.topMargin = topMargin

                mHolder.binding.llCard.requestLayout()
            }
            else {
                val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                cardViewMarginParams.topMargin = MainActivity.mActivity.resources.getDimension(R.dimen._10sdp).toInt()
                mHolder.binding.llCard.requestLayout()
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: AdapterBusinessCardBinding = DataBindingUtil.bind(view)!!
    }

    interface BusinessItemClicker {
        fun onBusinessItemClick(position: Int)
        fun onBusinessPhoneClick(position: Int)
        fun onBusinessEmailClick(position: Int)
    }
}
