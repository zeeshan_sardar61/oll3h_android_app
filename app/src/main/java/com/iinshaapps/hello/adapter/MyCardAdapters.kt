package com.iinshaapps.hello.adapter

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.iinshaapps.hello.R
import com.iinshaapps.hello.activities.MainActivity
import com.iinshaapps.hello.databinding.MyBusinessCardsBinding
import com.iinshaapps.hello.databinding.MyPersonalCardsBinding
import com.iinshaapps.hello.fragment.HomeFragment
import com.iinshaapps.hello.helper.Constants
import com.iinshaapps.hello.helper.encodeAsBitmap
import com.iinshaapps.hello.model.CardModel
import com.iinshaapps.hello.model.MyCardModel
import java.util.*


class MyCardAdapters(private var mList: ArrayList<MyCardModel>, private val context: HomeFragment) : RecyclerView.Adapter<MyCardAdapters.BaseViewHolder<*>>() {

    lateinit var personalbinding: MyPersonalCardsBinding
    lateinit var businessbinding: MyBusinessCardsBinding
    lateinit var mListener: ItemClicker

    companion object {
        private const val PERSONAL_CARD = 0
        private const val BUSINESS_CARD = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            PERSONAL_CARD -> {
                personalbinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.my_personal_cards, parent, false)
                PersonalViewHolder(personalbinding.root)
            }
            BUSINESS_CARD -> {
                businessbinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.my_business_cards, parent, false)
                BusinessViewHolder(businessbinding.root)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {

        when (holder) {
            is PersonalViewHolder -> {

                val mHolder = holder as PersonalViewHolder

                if (mList[position].isVideo) {
                    Glide.with(context).load(R.drawable.ic_dp_placeholder).into(mHolder.binding.ivProfile)
                }
                else {
                    Glide.with(context).load(mList[position].thumbnailUrl).placeholder(R.drawable.ic_dp_placeholder).into(mHolder.binding.ivProfile)
                }

                mHolder.binding.tvName.text = mList[position].name
                mHolder.binding.tvPhone.text = mList[position].countryCode + " " + mList[position].phone
                mHolder.binding.tvEmail.text = mList[position].email

                mHolder.binding.ivQrCode.setImageBitmap(encodeAsBitmap(mList[position].qrCode))

                val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(mList[position].colorStart), Color.parseColor(mList[position].colorEnd)))
                gd.cornerRadius = 0f
                mHolder.binding.rlColor.setBackgroundDrawable(gd)

                mHolder.binding.tvPhone.setOnClickListener {
                    mListener.onPhoneClick(position)
                }

                mHolder.binding.tvEmail.setOnClickListener {
                    mListener.onEmailClick(position)
                }

                mHolder.binding.ivQrCode.setOnClickListener {
                    mListener.onQrClick(encodeAsBitmap(mList[position].qrCode)!!)
                }

                mHolder.binding.llCard.setOnClickListener {
                    mListener.onItemClick(position)
                }

                if (mList[position].isOpened) {
                    val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                    val animator = ValueAnimator.ofInt(cardViewMarginParams.topMargin, MainActivity.mActivity.resources.getDimension(R.dimen._10sdp).toInt())
                    animator.addUpdateListener { valueAnimator ->
                        cardViewMarginParams.topMargin = valueAnimator.animatedValue as Int
                        mHolder.binding.llCard.requestLayout()
                    }
                    animator.duration = 300
                    animator.start()
                }
                else {
                    if (position > 0) { //                val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                        //                cardViewMarginParams.setMargins(0, -400, 0, 0);
                        //                mHolder.binding.llCard.requestLayout()
                        val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                        var topMargin = 0
                        if(Constants.DEVICE_HEIGHT>=2560){
                            topMargin = -500
                        }else if(Constants.DEVICE_HEIGHT>1520){
                            topMargin = -410
                        }else{
                            topMargin = -290
                        }
                        val animator = ValueAnimator.ofInt(cardViewMarginParams.topMargin, topMargin)
                        animator.addUpdateListener { valueAnimator ->
                            cardViewMarginParams.topMargin = valueAnimator.animatedValue as Int
                            mHolder.binding.llCard.requestLayout()
                        }
                        animator.duration = 300
                        animator.start()
                    }
                }
            }
            is BusinessViewHolder -> {

                val mHolder = holder as BusinessViewHolder

                Glide.with(context).load(mList[position].logoThumbnailUrl).placeholder(R.drawable.ic_logo_placeholder).into(mHolder.binding.ivLogo)
                mHolder.binding.ivQrCode.setImageBitmap(encodeAsBitmap(mList[position].qrCode))

                mHolder.binding.tvCompanyName.text = mList[position].companyName
                mHolder.binding.tvName.text = mList[position].name
                mHolder.binding.tvPhone.text = mList[position].countryCode + " " + mList[position].phone
                mHolder.binding.tvEmail.text = mList[position].email

                val gd = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(Color.parseColor(mList[position].colorStart), Color.parseColor(mList[position].colorEnd)))
                gd.cornerRadius = 0f
                mHolder.binding.rlColor.setBackgroundDrawable(gd)

                mHolder.binding.tvPhone.setOnClickListener {
                    mListener.onPhoneClick(position)
                }

                mHolder.binding.tvEmail.setOnClickListener {
                    mListener.onEmailClick(position)
                }

                mHolder.binding.ivQrCode.setOnClickListener {
                    mListener.onQrClick(encodeAsBitmap(mList[position].qrCode)!!)
                }

                mHolder.binding.llCard.setOnClickListener {
                    mListener.onItemClick(position)
                }

                if (mList[position].isOpened) {
                    val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                    val animator = ValueAnimator.ofInt(cardViewMarginParams.topMargin, MainActivity.mActivity.resources.getDimension(R.dimen._10sdp).toInt())
                    animator.addUpdateListener { valueAnimator ->
                        cardViewMarginParams.topMargin = valueAnimator.animatedValue as Int
                        mHolder.binding.llCard.requestLayout()
                    }
                    animator.duration = 300
                    animator.start()
                }
                else {
                    if (position > 0) { //                val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                        //                cardViewMarginParams.setMargins(0, -400, 0, 0);
                        //                mHolder.binding.llCard.requestLayout()
                        val cardViewMarginParams = mHolder.binding.llCard.layoutParams as ViewGroup.MarginLayoutParams
                        var topMargin = 0
                        if(Constants.DEVICE_HEIGHT>=2560){
                            topMargin = -500
                        }else if(Constants.DEVICE_HEIGHT>1520){
                            topMargin = -410
                        }else{
                            topMargin = -290
                        }
                        val animator = ValueAnimator.ofInt(cardViewMarginParams.topMargin, topMargin)
                        animator.addUpdateListener { valueAnimator ->
                            cardViewMarginParams.topMargin = valueAnimator.animatedValue as Int
                            mHolder.binding.llCard.requestLayout()
                        }
                        animator.duration = 300
                        animator.start()
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class BusinessViewHolder(view: View) : BaseViewHolder<CardModel>(view) { //        val binding: MyBusinessCardsBinding = DataBindingUtil.bind(view)!!
        val binding: MyBusinessCardsBinding = DataBindingUtil.bind(view)!!
    }

    class PersonalViewHolder(view: View) : BaseViewHolder<CardModel>(view) {
        val binding: MyPersonalCardsBinding = DataBindingUtil.bind(view)!!
    }

    interface ItemClicker {
        fun onItemClick(position: Int)
        fun onPhoneClick(position: Int)
        fun onEmailClick(position: Int)
        fun onQrClick(bitmap: Bitmap)
    }

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun getItemViewType(position: Int): Int {

        return when (mList[position].position % 2) {
            0 -> PERSONAL_CARD
            1 -> BUSINESS_CARD
            else -> throw IllegalArgumentException("Invalid type of data $position")
        }
    }
}
