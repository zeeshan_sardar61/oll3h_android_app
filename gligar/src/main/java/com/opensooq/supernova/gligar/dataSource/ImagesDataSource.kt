package com.opensooq.supernova.gligar.dataSource

import android.content.ContentResolver
import android.database.Cursor
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import com.opensooq.OpenSooq.ui.imagePicker.model.AlbumItem
import com.opensooq.OpenSooq.ui.imagePicker.model.ImageItem
import com.opensooq.OpenSooq.ui.imagePicker.model.ImageSource
import com.opensooq.supernova.gligar.utils.*

/**
 * Created by Hani AlMomani on 24,September,2019
 */


internal class ImagesDataSource(private val contentResolver: ContentResolver) {

//    fun loadAlbums(): ArrayList<AlbumItem> {
//        val albumCursor = contentResolver.query(
//            cursorUri,
//            arrayOf(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME),
//            null,
//            null,
//            ORDER_BY
//        )
//        val list = arrayListOf<AlbumItem>()
//        try {
//            list.add(AlbumItem("All", true))
//            if (albumCursor == null) {
//                return list
//            }
//            albumCursor.doWhile {
//                val albumItem = AlbumItem(
//                    albumCursor.getString(albumCursor.getColumnIndex(DISPLAY_NAME_COLUMN)),
//                    false
//                )
//                if (!list.contains(albumItem)) {
//                    list.add(albumItem)
//                }
//            }
//        } finally {
//            if (albumCursor != null && !albumCursor.isClosed) {
//                albumCursor.close()
//            }
//        }
//        return list
//    }

//    fun loadAlbumImages(
//        albumItem: AlbumItem?,
//        page: Int
//    ): ArrayList<ImageItem> {
//        val offset = page * PAGE_SIZE
//        val list: ArrayList<ImageItem> = arrayListOf()
//        var photoCursor: Cursor? = null
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                // Get All data in Cursor by sorting in DESC order
//
//                if (albumItem == null || albumItem.isAll) {
//                    photoCursor = contentResolver.query(
//                        cursorUri,
//                        arrayOf(
//                            ID_COLUMN,
//                            PATH_COLUMN
//                        ),
//                        Bundle().apply {
//                            // Limit & Offset
//                            putInt(ContentResolver.QUERY_ARG_LIMIT, PAGE_SIZE)
//                            putInt(ContentResolver.QUERY_ARG_OFFSET, offset)
//                            // Sort function
//                            putStringArray(ContentResolver.QUERY_ARG_SORT_COLUMNS,
//                                arrayOf(MediaStore.Files.FileColumns.DATE_MODIFIED)
//                            )
//                            putInt(
//                                ContentResolver.QUERY_ARG_SORT_DIRECTION,
//                                ContentResolver.QUERY_SORT_DIRECTION_DESCENDING
//                            )
//                        }, null
//                    )
//                }else{
//
//                    photoCursor = contentResolver.query(
//                        cursorUri,
//                        arrayOf(
//                            ID_COLUMN,
//                            PATH_COLUMN
//                        ),
//                        Bundle().apply {
//                            // Limit & Offset
//                            putInt(ContentResolver.QUERY_ARG_LIMIT, PAGE_SIZE)
//                            putInt(ContentResolver.QUERY_ARG_OFFSET, offset)
//                            // Sort function
//                            putStringArray(ContentResolver.QUERY_ARG_SORT_COLUMNS,
//                                arrayOf(MediaStore.Files.FileColumns.DATE_MODIFIED)
//                            )
//                            putInt(
//                                ContentResolver.QUERY_ARG_SORT_DIRECTION,
//                                ContentResolver.QUERY_SORT_DIRECTION_DESCENDING
//                            )
//                            // Selection
//                            putString(ContentResolver.QUERY_ARG_SQL_SELECTION,
//                                MediaStore.Files.FileColumns.BUCKET_DISPLAY_NAME + "=?")
//                            // Selection Args
//                            putStringArray(
//                                ContentResolver.QUERY_ARG_SQL_SELECTION_ARGS,
//                                arrayOf(albumItem.name)
//                            )
//                        }, null
//                    )
//                }
//            }else {
//
//                if (albumItem == null || albumItem.isAll) {
//                    photoCursor = contentResolver.query(cursorUri, arrayOf(ID_COLUMN, PATH_COLUMN),
//                        null, null, "$ORDER_BY LIMIT $PAGE_SIZE OFFSET $offset")
//                }
//                else {
//                    photoCursor = contentResolver.query(cursorUri, arrayOf(ID_COLUMN, PATH_COLUMN),
//                        "${MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME} =?",
//                        arrayOf(albumItem.name), "$ORDER_BY LIMIT $PAGE_SIZE OFFSET $offset")
//                }
//            }
//            photoCursor?.isAfterLast ?: return list
//            photoCursor.doWhile {
//                val image = photoCursor.getString((photoCursor.getColumnIndex(PATH_COLUMN)))
//                list.add(ImageItem(image, ImageSource.GALLERY, 0))
//            }
//        } finally {
//            if (photoCursor != null && !photoCursor.isClosed) {
//                photoCursor.close()
//            }
//        }
//        return list
//    }

    fun loadAlbums(): ArrayList<AlbumItem> {
        val albumCursor = contentResolver.query(
            cursorUri,
            arrayOf(DISPLAY_NAME_COLUMN,MediaStore.Images.ImageColumns.BUCKET_ID),
            null,
            null,
            ORDER_BY
        )
        val list = arrayListOf<AlbumItem>()
        try {
            list.add(AlbumItem("All", true,"0"))
            if (albumCursor == null) {
                return list
            }
            albumCursor.doWhile {
                val bucketId = albumCursor.getString(albumCursor.getColumnIndex(MediaStore.Images.ImageColumns.BUCKET_ID))
                val name = albumCursor.getString(albumCursor.getColumnIndex(DISPLAY_NAME_COLUMN)) ?: bucketId
                var albumItem = AlbumItem(name, false, bucketId)
                if (!list.contains(albumItem)) {
                    list.add(albumItem)
                }
            }
        } finally {
            if (albumCursor != null && !albumCursor.isClosed) {
                albumCursor.close()
            }
        }
        return list
    }

    fun loadAlbumImages(
        albumItem: AlbumItem?,
        page: Int
    ): ArrayList<ImageItem> {
        val offset = page * PAGE_SIZE
        val list: ArrayList<ImageItem> = arrayListOf()
        var photoCursor: Cursor? = null
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                // Get All data in Cursor by sorting in DESC order

                                if (albumItem == null || albumItem.isAll) {
                                    photoCursor = contentResolver.query(
                                        cursorUri,
                                        arrayOf(
                                            ID_COLUMN,
                                            PATH_COLUMN
                                        ),
                                        Bundle().apply {
                                            // Limit & Offset
                                            putInt(ContentResolver.QUERY_ARG_LIMIT, PAGE_SIZE)
                                            putInt(ContentResolver.QUERY_ARG_OFFSET, offset)
                                            // Sort function
                                            putStringArray(ContentResolver.QUERY_ARG_SORT_COLUMNS,
                                                arrayOf(MediaStore.Files.FileColumns.DATE_MODIFIED)
                                            )
                                            putInt(
                                                ContentResolver.QUERY_ARG_SORT_DIRECTION,
                                                ContentResolver.QUERY_SORT_DIRECTION_DESCENDING
                                            )
                                        }, null
                                    )
                                }else{

                                    photoCursor = contentResolver.query(
                                        cursorUri,
                                        arrayOf(
                                            ID_COLUMN,
                                            PATH_COLUMN
                                        ),
                                        Bundle().apply {
                                            // Limit & Offset
                                            putInt(ContentResolver.QUERY_ARG_LIMIT, PAGE_SIZE)
                                            putInt(ContentResolver.QUERY_ARG_OFFSET, offset)
                                            // Sort function
                                            putStringArray(ContentResolver.QUERY_ARG_SORT_COLUMNS,
                                                arrayOf(MediaStore.Files.FileColumns.DATE_MODIFIED)
                                            )
                                            putInt(
                                                ContentResolver.QUERY_ARG_SORT_DIRECTION,
                                                ContentResolver.QUERY_SORT_DIRECTION_DESCENDING
                                            )
                                            // Selection
                                            putString(ContentResolver.QUERY_ARG_SQL_SELECTION,
                                                MediaStore.Files.FileColumns.BUCKET_DISPLAY_NAME + "=?")
                                            // Selection Args
                                            putStringArray(
                                                ContentResolver.QUERY_ARG_SQL_SELECTION_ARGS,
                                                arrayOf(albumItem.name)
                                            )
                                        }, null
                                    )
                                }
                            }else {

                if (albumItem == null || albumItem.isAll) {
                    photoCursor = contentResolver.query(cursorUri, arrayOf(ID_COLUMN, PATH_COLUMN),
                        null, null, "$ORDER_BY LIMIT $PAGE_SIZE OFFSET $offset")
                }
                else {
                    photoCursor = contentResolver.query(cursorUri, arrayOf(ID_COLUMN, PATH_COLUMN),
                        "${MediaStore.Images.ImageColumns.BUCKET_ID} =?",
                        arrayOf(albumItem.bucketId), "$ORDER_BY LIMIT $PAGE_SIZE OFFSET $offset")
                }
            }
            photoCursor?.isAfterLast ?: return list
            photoCursor.doWhile {
                val image = photoCursor.getString((photoCursor.getColumnIndex(PATH_COLUMN)))
                list.add(ImageItem(image, ImageSource.GALLERY, 0))
            }
        } finally {
            if (photoCursor != null && !photoCursor.isClosed()) {
                photoCursor.close()
            }
        }
        return list
    }

    //Video

    fun loadVideoAlbums(): ArrayList<AlbumItem> {
        val albumCursor = contentResolver.query(
            cursorUriVideo,
            arrayOf(MediaStore.Video.VideoColumns.BUCKET_DISPLAY_NAME),
            null,
            null,
            ORDER_BY_VIDEO
        )
        val list = arrayListOf<AlbumItem>()
        try {
            list.add(AlbumItem("All", true))
            if (albumCursor == null) {
                return list
            }
            albumCursor.doWhile {
                val albumItem = AlbumItem(
                    albumCursor.getString(albumCursor.getColumnIndex(DISPLAY_NAME_COLUMN_VIDEO)),
                    false
                )
                if (!list.contains(albumItem)) {
                    list.add(albumItem)
                }
            }
        } finally {
            if (albumCursor != null && !albumCursor.isClosed) {
                albumCursor.close()
            }
        }
        return list
    }

    fun loadVideoAlbumImages(
        albumItem: AlbumItem?,
        page: Int
    ): ArrayList<ImageItem> {
        val offset = page * PAGE_SIZE
        val list: ArrayList<ImageItem> = arrayListOf()
        var photoCursor: Cursor? = null
        try {
            /**
             * Change the way to fetch Media Store
             */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                // Get All data in Cursor by sorting in DESC order

                if (albumItem == null || albumItem.isAll) {
                    photoCursor = contentResolver.query(
                        cursorUriVideo,
                        arrayOf(
                            ID_COLUMN_VIDEO,
                            PATH_COLUMN_VIDEO
                        ),
                        Bundle().apply {
                            // Limit & Offset
                            putInt(ContentResolver.QUERY_ARG_LIMIT, PAGE_SIZE)
                            putInt(ContentResolver.QUERY_ARG_OFFSET, offset)
                            // Sort function
                            putStringArray(ContentResolver.QUERY_ARG_SORT_COLUMNS,
                                arrayOf(MediaStore.Files.FileColumns.DATE_MODIFIED)
                            )
                            putInt(
                                ContentResolver.QUERY_ARG_SORT_DIRECTION,
                                ContentResolver.QUERY_SORT_DIRECTION_DESCENDING
                            )
                            // Selection
                            putString(ContentResolver.QUERY_ARG_SQL_SELECTION,
                                MediaStore.Files.FileColumns.SIZE + ">0")
                        }, null
                    )
                }else{

                    photoCursor = contentResolver.query(
                        cursorUriVideo,
                        arrayOf(
                            ID_COLUMN_VIDEO,
                            PATH_COLUMN_VIDEO
                        ),
                        Bundle().apply {
                            // Limit & Offset
                            putInt(ContentResolver.QUERY_ARG_LIMIT, PAGE_SIZE)
                            putInt(ContentResolver.QUERY_ARG_OFFSET, offset)
                            // Sort function
                            putStringArray(ContentResolver.QUERY_ARG_SORT_COLUMNS,
                                arrayOf(MediaStore.Files.FileColumns.DATE_MODIFIED)
                            )
                            putInt(
                                ContentResolver.QUERY_ARG_SORT_DIRECTION,
                                ContentResolver.QUERY_SORT_DIRECTION_DESCENDING
                            )
                            // Selection
                            putString(ContentResolver.QUERY_ARG_SQL_SELECTION,
                                MediaStore.Files.FileColumns.BUCKET_DISPLAY_NAME + "=?")
                            putStringArray(
                                ContentResolver.QUERY_ARG_SQL_SELECTION_ARGS,
                                arrayOf(albumItem.name)
                            )
                        }, null
                    )
                }
            }else{

                if (albumItem == null || albumItem.isAll) {

                    photoCursor = contentResolver.query(
                        cursorUriVideo,
                        arrayOf(
                            ID_COLUMN_VIDEO,
                            PATH_COLUMN_VIDEO
                        ),
                        MediaStore.Video.Media.SIZE + ">0",
                        null,
                        "$ORDER_BY_VIDEO LIMIT $PAGE_SIZE OFFSET $offset"
                    )
                } else {
                    photoCursor = contentResolver.query(
                        cursorUriVideo,
                        arrayOf(
                            ID_COLUMN_VIDEO,
                            PATH_COLUMN_VIDEO
                        ),
                        "${MediaStore.Video.VideoColumns.BUCKET_DISPLAY_NAME} =?",
                        arrayOf(albumItem.name),
                        "$ORDER_BY_VIDEO LIMIT $PAGE_SIZE OFFSET $offset"
                    )
                }
            }
            photoCursor?.isAfterLast ?: return list
            photoCursor.doWhile {
                val image = photoCursor.getString((photoCursor.getColumnIndex(PATH_COLUMN_VIDEO)))
                list.add(ImageItem(image, ImageSource.GALLERY, 0))
            }
        } finally {
            if (photoCursor != null && !photoCursor.isClosed) {
                photoCursor.close()
            }
        }
        return list
    }
}